﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuComm.Security.Encryption;
using Telerik.Web.UI;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class MyPlan : BasePage
{
    private MasterPage2 _master;
    private string _employeeId = "";

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["TwwIdSession"] = value; }
    }

    public static string[] SearchKeys
    {
        get
        {
            var value = HttpContext.Current.Session["SearchKeysSession"];
            return value == null ? new string[] { } : (string[])value;
        }
        set { HttpContext.Current.Session["SearchKeysSession"] = value; }
    }

    public static string CourseLinkTitleToLearner
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToLearnerSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToLearnerSession"] = value; }
    }

    public static string CourseLinkTitleToApprover
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToApproverSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToApproverSession"] = value; }
    }

    public static string CourseDescription
    {
        get
        {
            var value = HttpContext.Current.Session["CourseDescriptionSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseDescriptionSession"] = value; }
    }

    public static int SearchCriteria
    {
        get
        {
            var value = HttpContext.Current.Session["SearchCriteriaSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["SearchCriteriaSession"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Disable Page Cache 
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Page.IsPostBack)
        {
            try
            {
                Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
                lblLNAME.Text = CurrLogName.ToString();
                _employeeId = lblLNAME.Text;
            }
            catch
            {
                Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
                lblLNAME.Text = CurrLogName.ToString();
            }
            try
            {
                var lv = (MultiView)Master.FindControl("mvMenu");
                string CurrIndx = CurrVwIndx.ToString();
                lv.ActiveViewIndex = Int32.Parse(CurrIndx);
            }
            catch
            {
                var lv = (MultiView)Master.FindControl("mvMenu");
                lv.ActiveViewIndex = lv.ActiveViewIndex;
            }

            try
            {
                Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
                lblWel.Text = CurrTWWID.ToString();
                _employeeId = lblWel.Text;
            }
            catch
            {
                Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
                _employeeId = lblWel.Text;
            }
            hidCIM.Value = _employeeId;

            TwwId = _employeeId;
            SearchKeys = new[] { TwwId };
            CourseLinkTitleToLearner = "";
            CourseLinkTitleToApprover = "";
            CourseDescription = "";
            SearchCriteria = 0;

            SetPage_RolePreviledges();

            DataHelper.UpdateCourseEnrolmentViewStatus(Convert.ToInt32(TwwId));
            DataHelper.ResetNotifCount(Convert.ToInt32(TwwId));
        }

        GetLang();
    }

    private void GetLang()
    {
        //Learner
        var masterTableView = gridEmployee.MasterTableView.DetailTables[0];
        var column = masterTableView.GetColumn("TitleLink");
        column.HeaderText = Resources.LocalizedResource.rgcol_Title.ToString();
        var column2 = masterTableView.GetColumn("CreateDate");
        column2.HeaderText = Resources.LocalizedResource.rgcol_EnAssDate.ToString();
        var column3 = masterTableView.GetColumn("DueDate");
        column3.HeaderText = Resources.LocalizedResource.rgcol_DueDate.ToString();
        var column4 = masterTableView.GetColumn("Progress");
        column4.HeaderText = Resources.LocalizedResource.rgcol_Progress.ToString();
        var column5 = masterTableView.GetColumn("CourseType");
        column5.HeaderText = Resources.LocalizedResource.rgcol_Coursetype.ToString();
        var column6 = masterTableView.GetColumn("StatusID");
        column6.HeaderText = Resources.LocalizedResource.rgcol_Status.ToString();

        btnExportExcel.Text = Resources.LocalizedResource.ExportToExcel + "<img style='border:0; padding-left: 3px; height: 20px' src='Images/excel.png'/>";
        masterTableView.Rebind();
    }

    private void getSession()
    {
        try
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            _employeeId = lblWel.Text;
        }
        catch
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            _employeeId = lblWel.Text;
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Non-APAC Admin") || Roles.IsUserInRole("Trainer") || Roles.IsUserInRole("Manager"))
        {
            tdSearchCrit.Visible = true;
            tdMngr.Visible = true;
            tdAdminSearch.Visible = true;
            tdBtnSearch.Visible = true;
        }
    }

    private static void GetCheckedItems(RadComboBox comboBox, RadTextBox txtSearch)
    {
        var sb = new StringBuilder();
        var collection = comboBox.CheckedItems;

        if (collection.Count != 0)
        {
            foreach (var item in collection)
                sb.Append(item.Value + ",");

            txtSearch.Text = sb.ToString().TrimEnd(',');
            txtSearch.ToolTip = sb.ToString().TrimEnd(',');
        }
        else
        {
            txtSearch.Text = "";
        }
    }

    protected void cbDirectReports_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        GetCheckedItems(cbDirectReports, txtSearch);
    }

    private void BindcbDirectReports(int twwid)
    {
        using (SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["Intranet"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("pr_TranscomUniversity_Lkp_GetDirectReports", sqlcon))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@CIM", twwid);
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Int32;
                cmd.Parameters.Add(param);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();

                    da.Fill(dt);
                    cbDirectReports.DataSource = dt;

                    cbDirectReports.DataBind();
                }
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            SearchKeys = txtSearch.Text.Split(',').ToArray();

            if (txtSearch.Text != TwwId)
            {
                gridEmployee.MasterTableView.DetailTables[0].Columns.FindByUniqueName("DeleteColumn").Display = false;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("Title").Display = true;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("TitleLink").Display = false;
            }
            else
            {
                gridEmployee.MasterTableView.DetailTables[0].Columns.FindByUniqueName("DeleteColumn").Display = true;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("Title").Display = false;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("TitleLink").Display = true;
            }

            if (SearchKeys.Count() > 1)
            {
                gridEmployee.MasterTableView.DetailTables[0].AllowPaging = true;
                gridEmployee.MasterTableView.DetailTables[0].PageSize = 10;
                gridEmployee.MasterTableView.DetailTables[0].PagerStyle.Mode = GridPagerMode.Slider;
                gridEmployee.MasterTableView.DetailTables[0].PagerStyle.AlwaysVisible = true;
            }
            else
            {
                gridEmployee.MasterTableView.DetailTables[0].AllowPaging = false;
                gridEmployee.MasterTableView.DetailTables[0].PagerStyle.AlwaysVisible = false;
            }
        }
        else
        {
            SearchKeys = new[] { TwwId };

            gridEmployee.MasterTableView.DetailTables[0].AllowPaging = false;
            gridEmployee.MasterTableView.DetailTables[0].PagerStyle.AlwaysVisible = false;

            gridEmployee.MasterTableView.DetailTables[0].Columns.FindByUniqueName("DeleteColumn").Display = true;
            gridEmployee.MasterTableView.DetailTables[0].GetColumn("Title").Display = false;
            gridEmployee.MasterTableView.DetailTables[0].GetColumn("TitleLink").Display = true;
        }
        gridEmployee.Rebind();
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        var ds = new DataSet();
        foreach (var id in SearchKeys)
        {
            var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(id));
            ds.Tables.Add(ConvertToDataTable(data, id));
        }

        ExcelHelper.ToExcel(ds, "TU_Report_MyLearningPlan_" + DateTime.Now + ".xls", Page.Response);

        //Perry
        //var ds = new DataSet();
        //var dsNoTags = new DataSet();
        //if (SearchKeys != null)
        //{
        //    foreach (var id in SearchKeys)
        //    {
        //        var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(id));
        //        ds.Tables.Add(ConvertToDataTable(data, id));
        //        dsNoTags = RemoveHtmlTags(ds);
        //    }
        //}
        //else
        //{
        //    getSession();
        //    var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(_employeeId));
        //    ds.Tables.Add(ConvertToDataTable(data, _employeeId));
        //    dsNoTags = RemoveHtmlTags(ds);
        //}

        //gridEmployee.MasterTableView.DetailTables[0].DataSource = dsNoTags;
        //gridEmployee.DataBind();

        //GetLang();

        //gridEmployee.ExportSettings.ExportOnlyData = true;
        //gridEmployee.ExportSettings.IgnorePaging = true;
        //gridEmployee.ExportSettings.OpenInNewWindow = true;
        //gridEmployee.ExportSettings.FileName = "TU_Report_MyLearningPlan_" + DateTime.Now;
        //gridEmployee.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
        //gridEmployee.ExportSettings.HideStructureColumns = true;
        //gridEmployee.MasterTableView.HierarchyDefaultExpanded = true;
        //gridEmployee.MasterTableView.DetailTables[0].HierarchyDefaultExpanded = true;

        //foreach (GridColumn item in gridEmployee.MasterTableView.DetailTables[0].Columns)
        //{
        //    string StrX = item.UniqueName;
        //    if (StrX == "TitleLink" || StrX == "Progress" || StrX == "Status")
        //    {
        //        item.Display = false;
        //        item.Visible = false;
        //    }
        //}

        //gridEmployee.MasterTableView.DetailTables[0].Rebind();
        //gridEmployee.MasterTableView.HierarchyLoadMode = GridChildLoadMode.Client;
        //gridEmployee.MasterTableView.DetailTables[0].HierarchyLoadMode = GridChildLoadMode.Client;
        //gridEmployee.MasterTableView.ExportToExcel();
    }

    protected DataSet RemoveHtmlTags(DataSet ds)
    {
        DataSet dsHTML = ds;

        foreach (DataTable table in dsHTML.Tables)
        {
            foreach (DataRow dr in table.Rows)
            {
                string HtmlToData = dr["description"].ToString();
                string noHTML = Regex.Replace(HtmlToData, @"<[^>]+>|&nbsp;", "").Trim();
                string noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");
                dr["description"] = noHTMLNormalised;
            }
        }

        return dsHTML;
    }

    public DataTable ConvertToDataTable<T>(IList<T> data, string tableName)
    {
        var properties = TypeDescriptor.GetProperties(typeof(T));
        var table = new DataTable(tableName);

        foreach (PropertyDescriptor prop in properties)
        {
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        }

        foreach (T item in data)
        {
            var row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }

        table.Columns.Remove("EncryptedCourseId");
        table.Columns.Remove("EnrolmentRequired");
        table.Columns.Remove("RowNum");
        table.Columns.Remove("SupCim");
        table.Columns.Remove("StatusId");
        table.Columns.Remove("Active");
        table.Columns.Remove("IsViewed");

        return table;
    }

    protected void gridEmployee_PreRender(object sender, EventArgs e)
    {
        if (gridEmployee.MasterTableView.Items.Count == 1)
        {
            gridEmployee.MasterTableView.Items[0].Expanded = true;

            gridEmployee.MasterTableView.Items[0].BackColor = Color.FromArgb(0, 33, 54);
            gridEmployee.MasterTableView.Items[0].ForeColor = Color.White;

            gridEmployee.MasterTableView.ExpandCollapseColumn.Visible = false;
        }
    }

    protected void GridEmployeeNeedDataSource(object sender, GridNeedDataSourceEventArgs e) 
    {
        List<DataHelperModel.User> ds;
        switch (SearchCriteria)
        {
            case 0:
                LoadEmployeeDefault();
                break;
            case 1:
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    ds = DataHelper.GetUsers_ByFirstNames(txtSearch.Text).OrderBy(p => p.CimNo).ToList();
                    gridEmployee.DataSource = ds;
                }
                else
                {
                    LoadEmployeeDefault();
                }
                break;
            case 2:
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    ds = DataHelper.GetUsers_ByLastNames(txtSearch.Text).OrderBy(p => p.CimNo).ToList();
                    gridEmployee.DataSource = ds;
                }
                else
                {
                    LoadEmployeeDefault();
                }
                break;
        }
    }

    private void LoadEmployeeDefault()
    {
        var ds = DataHelper.GetUsers_ByTwwIds(SearchKeys).OrderBy(p => p.CimNo).ToList();
        gridEmployee.DataSource = ds;
    }

    protected void gridEmployee_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        var dataItem = e.DetailTableView.ParentItem;

        if (e.DetailTableView.Name == "gridLearningPlan")
        {
            var twwid = dataItem.GetDataKeyValue("CimNo").ToString();

            var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(twwid));
            e.DetailTableView.DataSource = data;
        }
    }

    protected void GridEmployeeItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "MasterTable")
        {
            e.Item.BackColor = Color.FromArgb(217, 217, 217);
            e.Item.ForeColor = Color.Black;
        }
        if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "gridLearningPlan")
        {
            var dataItem = (GridDataItem)e.Item;

            var courseId = dataItem.GetDataKeyValue("CourseID").ToString();
            var encCourseId = Server.UrlEncode(UTF8.EncryptText(dataItem.GetDataKeyValue("CourseID").ToString()));
            var sTestCategoryId = dataItem.GetDataKeyValue("EncryptedCourseID").ToString();
            var statusId = dataItem.GetDataKeyValue("StatusId").ToString();
            var status = dataItem.GetDataKeyValue("Status").ToString();
            var title = dataItem.GetDataKeyValue("Title").ToString();
            var courseTypeID = Convert.ToInt32(dataItem.GetDataKeyValue("CourseTypeID"));
            var courseType = dataItem.GetDataKeyValue("CourseType").ToString();
            var hasTest = Convert.ToInt32(dataItem.GetDataKeyValue("HasTest"));
            var passed = (dataItem.GetDataKeyValue("Passed") == null) ? 0 : Convert.ToInt32(dataItem.GetDataKeyValue("Passed"));
            var login = string.IsNullOrEmpty(dataItem.GetDataKeyValue("CourseLogin").ToString()) ? "" : dataItem.GetDataKeyValue("CourseLogin").ToString();

            string urlWithParams;

            if (courseTypeID == 2)
            {
                urlWithParams = string.Format("CourseLaunch.aspx?CourseID={0}", encCourseId);
            }
            else
            {
                urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId, sTestCategoryId);
            }

            var linkTitle = dataItem.FindControl("linkCourseTitle") as LinkButton;
            var linkStat = dataItem.FindControl("btnPreviousReg") as LinkButton;

            //SET HIDDENFIELD VALUE OF PROGRESS
            var progressval = 0.00;

            if (courseTypeID == 1)
            {
                if (hasTest > 0) //COURSE WITH ASSESSMENT
                {
                    if (login == "1")
                    {
                        progressval = 20;

                        if (passed == 1)
                        {
                            progressval = progressval + 80;
                        }
                    }
                    else
                    {
                        progressval = 0;
                    }
                }
                else //COURSE WO ASSESSMENT
                {
                    if (login == "1")
                    {
                        progressval = 100;
                    }
                }
                linkStat.Text = status;
            }
            else if (courseTypeID == 2)
            {
                progressval = Convert.ToDouble(ScormDataHelper.GetOverallProgressByCourseID(CurrTWWID, Convert.ToInt32(courseId)));

                if (courseType == "Enrolled")
                {
                    if (statusId == "2")
                    {
                        if (progressval > 0 && progressval < 100)
                        {
                            linkStat.Text = "In Progress";
                        }
                        else if (progressval == 100)
                        {
                            linkStat.Text = "Complete";
                        }
                    }
                }
            }

            //1	Pending Approval
            //2	Approved
            //3	Declined
            //4	Cancelled (By Learner)
            //5	Not Started
            //6	Complete
            //7	Over Due
            //8	Cancelled (By Assigner)
            //9 In Progress

            //Decorate items
            switch (statusId)
            {
                case "1":
                    linkStat.Style.Add("font-size", "10pt !important");
                    linkTitle.Style.Add("color", "gray !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return false;");
                    break;
                case "2":
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);
                    break;
                case "3":
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "4":
                    linkStat.Style.Add("font-size", "8pt !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "5":

                case "6":
                    linkStat.Style.Add("color", "black !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "7":
                    linkStat.Style.Add("color", "red !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "8":
                    linkStat.Style.Add("font-size", "8pt !important");
                    linkStat.Style.Add("color", "black !important");
                    linkTitle.Style.Add("color", "gray !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return false;");
                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "9":
                    linkStat.Style.Add("color", "black !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
            }

            var hidProgressVal = (HiddenField)dataItem["Progress"].FindControl("hidProgressVal");

            if (!double.IsNaN(progressval) && progressval.ToString("#.##") != "")
            {
                hidProgressVal.Value = progressval.ToString("#.##");
            }
            else
            {
                hidProgressVal.Value = "0";
            }

            if (dataItem["DueDate"].Text == @"&nbsp;")
            {
                dataItem["DueDate"].Text = @"N/A";
            }
            dataItem.ToolTip = title;
        }
    }

    protected void GridEmployeeItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;
        if (e.CommandName == RadGrid.DeleteCommandName && e.Item.OwnerTableView.Name == "gridLearningPlan")
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(TwwId));

                if (supQry != null)
                {
                    var enrolmentId = deletedItem.GetDataKeyValue("EnrollmentId").ToString();
                    var title = deletedItem.GetDataKeyValue("Title").ToString();
                    var desc = deletedItem.GetDataKeyValue("Description").ToString();

                    var retVal = DataHelper.UpdateCourseEnrolmentStatus(Convert.ToInt32(enrolmentId), "", 4,
                                                                        Convert.ToInt32(TwwId), false);

                    switch (retVal)
                    {
                        case true:
                            const string msg = "The course enrollment has been cancelled by learner.";

                            var owner = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                            var recipient = SendEmail.ValidateAddress(supQry.Email1)
                                                ? supQry.Email1
                                                : supQry.Email1_fallback;

                            const string url = "http://transcomuniversity.com/MyTeam.aspx";
                            var titleLink = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, title);

                            SendEmail.SendEmail_CourseEnrolment(owner, recipient, msg, "", titleLink, desc);

                            e.Item.OwnerTableView.Rebind();

                            //const string alertMsg = "Your enrollment to the course is now cancelled.";
                            //_master.AlertMessage(true, alertMsg);
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                "alert('Your enrollment to the course is now cancelled.');", true);
                            break;
                        case false:
                            //_master.AlertMessage(false, "Request failed. Please try again.");
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                "alert('Request failed. Please try again.');", true);
                            break;
                    }
                }
            }
        }
        else if (e.CommandName == RadGrid.ExpandCollapseCommandName)
        {
            if (e.Item.Expanded)
            {
                e.Item.BackColor = Color.FromArgb(217, 217, 217);
                e.Item.ForeColor = Color.Black;
            }
            else
            {
                e.Item.BackColor = Color.FromArgb(0, 33, 54);
                e.Item.ForeColor = Color.White;
            }

            foreach (GridItem item in e.Item.OwnerTableView.Items)
            {
                if (item.Expanded && item != e.Item)
                {
                    item.Expanded = false;
                    item.BackColor = Color.FromArgb(217, 217, 217);
                    item.ForeColor = Color.Black;
                }
            }
        }
    }

    protected void btnPreviousReg_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var enrollmentId = item.GetDataKeyValue("EnrollmentId").ToString();
        var enroleeCim = item.GetDataKeyValue("EnrolleeCim").ToString();
        var courseId = item.GetDataKeyValue("CourseID").ToString();
        var title = item.GetDataKeyValue("Title").ToString();

        if (enrollmentId != "0")
        {
            var script = "function f(){$find(\"" + windowEnrollmentHistory.ClientID +
                         "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(this, GetType(), "key", script, true);

            var qry =
                DataHelper.GetCourseEnrolment_History(Convert.ToInt32(enroleeCim), Convert.ToInt32(courseId))
                          .OrderByDescending(p => p.CreateDate);

            if (qry != null)
            {
                lblCourseName1.Text = title;
                gridEnrollmentHistory.DataSource = qry;
                gridEnrollmentHistory.DataBind();
            }
        }
        else
        {
            var script = "function f(){$find(\"" + windowAssignmentHistory.ClientID +
                         "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(this, GetType(), "key", script, true);

            var qry = DataHelper.GetAssignedCourse_History(Convert.ToInt32(enroleeCim), Convert.ToInt32(courseId));

            if (qry != null)
            {
                lblCourseName2.Text = title;
                gridAssignmentHistory.DataSource = qry;
                gridAssignmentHistory.DataBind();
            }
        }
    }

    protected void btnEnrolCourse_Click(object sender, EventArgs e)
    {
        GetSessions();
        var enrolmentStatus = DataHelper.ChkUserEnrolmentStatusToCourse(Convert.ToInt32(hidCourseId.Value),
                                                                        Convert.ToInt32(TwwId));
        _master = Master;

        switch (enrolmentStatus)
        {
            case 0:
            case 3:
            case 4: //0 = New, 3 = Denied, 4 = Cancelled by user
                var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(TwwId));

                if (supQry != null)
                {
                    var retVal = DataHelper.InsertCourseEnrolment(Convert.ToInt32(hidCourseId.Value),
                                                                  Convert.ToInt32(TwwId), Convert.ToInt32(supQry.Cim1));

                    if (retVal)
                    {
                        const string msgToLearner = "Your course enrollment has been received and is going through approval process.";
                        const string msgToApprover = "Course registration for your approval.";

                        var owner = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                        var recipient = SendEmail.ValidateAddress(supQry.Email1)
                                            ? supQry.Email1
                                            : supQry.Email1_fallback;

                        SendEmail.SendEmail_CourseEnrolment(owner, owner, msgToLearner, "", CourseLinkTitleToLearner, CourseDescription);
                        SendEmail.SendEmail_CourseEnrolment(owner, recipient, msgToApprover, "", CourseLinkTitleToApprover, CourseDescription);

                        gridEmployee.MasterTableView.Rebind();

                        //const string alertMsg = "Your request for enrollment has been sent to your manager for approval. Please check your email for updates on the status of your enrollment.";
                        //_master.AlertMessage(true, alertMsg);
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                    "alert('Your request for enrollment has been sent to your manager for approval. Please check your email for updates on the status of your enrollment.');", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                    }
                    else
                    {
                        //_master.AlertMessage(false, "Request failed. Please try again.");
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                    "alert('Request failed. Please try again.');", true);
                    }
                }
                else
                {
                    //_master.AlertMessage(false, "Request failed. Please try again.");
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                    "alert('Request failed. Please try again.');", true);
                }
                break;
            case 1: //For Approval
                //_master.AlertMessage(false, "Your enrollment request is still subject for approval. Please wait for the email notification that will be sent to your registered email.");
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                "alert('Your enrollment request is still subject for approval. Please wait for the email notification that will be sent to your registered email.');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                break;
            case 2: //Approved
                var url = hidUrlWithParams.Value;
                var s = "window.open('" + url +
                        "', 'popup_window', 'width=700,height=550,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=yes');";
                ClientScript.RegisterStartupScript(GetType(), "script", s, true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                break;
        }
    }

    protected void linkCourseTitle_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;
        var title = item.GetDataKeyValue("Title").ToString();
        var desc = item.GetDataKeyValue("Description").ToString();
        string EnrolReq;

        if (item.GetDataKeyValue("EnrolmentRequired").ToString() == "False")
        {
            EnrolReq = "False";
        }
        else
        {
            EnrolReq = "True";
        }

        string url_to_view;

        const string url1 = "~/CourseRedirect.aspx";
        var titleLink1 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url1, title);

        const string url2 = "http://transcomuniversity.com/MyPlan.aspx";
        var titleLink2 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url2, title);

        var sCourseID = Server.UrlEncode(NuComm.Security.Encryption.UTF8.EncryptText(item.GetDataKeyValue("CourseID").ToString()));
        var sCourseID_1 = item.GetDataKeyValue("CourseID").ToString();
        var sTestCategoryID = item.GetDataKeyValue("EncryptedCourseID").ToString();
        var CourseType = item.GetDataKeyValue("CourseType").ToString();
        HttpContext.Current.Session["EnrolCourseID"] = sCourseID_1;

        GetSessions();

        string Course_Path = GetCourseURL(sCourseID_1, sTestCategoryID, _employeeId);
        var courseTypeID = item.GetDataKeyValue("CourseTypeID") ?? 1;

        //int isEnrolReq = DataHelper.ChkUserEnrolmentReq(Convert.ToInt32(sCourseID_1));

        var _xml = "<DocumentElement>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>1073</PrereqCourseID>\r\n    <EncryptedCourseID />\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>984</PrereqCourseID>\r\n    <EncryptedCourseID>1382</EncryptedCourseID>\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>987</PrereqCourseID>\r\n    <EncryptedCourseID>1385</EncryptedCourseID>\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>979</PrereqCourseID>\r\n    <EncryptedCourseID>1377</EncryptedCourseID>\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>980</PrereqCourseID>\r\n    <EncryptedCourseID>1378</EncryptedCourseID>\r\n  </encCourseIDs>\r\n</DocumentElement>";
        var preReq = DataHelper.ArePrerequisiteMet(Convert.ToInt32(CurrTWWID), Convert.ToInt32(sCourseID_1), _xml);
        var statusChecker = DataHelper.StatusChecker(Convert.ToInt32(preReq.ReqCourseID), Convert.ToInt32(CurrTWWID));

        string _msg = preReq.MSG.ToString();
        string _reqCourseID = "";

        if (statusChecker != null)
            _reqCourseID = statusChecker.Status.ToString();

        if (_msg != "" && _reqCourseID != "Completed")
        {
            string script = string.Format(@"ShowNote('<span>Thank you for your interest in taking this course. The following requirement/s must be fulfilled:</span><br/><br/><br/><span>{0}</span>')", _msg);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

            //string script = "function f(){$find(\"" + windowPrereq.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //(windowPrereq.ContentContainer.FindControl("lblPrerequisiteMessage") as Label).Text = _msg;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
        }
        else
        {
            if (EnrolReq == "False")
            {
                if (Convert.ToInt32(courseTypeID) == 1)
                {
                    url_to_view = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", sCourseID, sTestCategoryID);
                    Int32 CID_ = Convert.ToInt32(sCourseID_1);
                    string CourseEnddate = DataHelper.GetCourseEndDate(CID_);
                    HttpContext.Current.Session["EnrolCourseID_EndDate"] = CourseEnddate;
                    HttpContext.Current.Session["EnrolCourse_Path"] = Course_Path;
                    HttpContext.Current.Session["EnrolUrl_to_view"] = url_to_view;
                    HttpContext.Current.Session["EnrolCourseType"] = Convert.ToInt32(courseTypeID);
                    rmPopup.RadConfirm("Are you sure you want to take this course?", "confirmCallBackFn", 350, 180, null, "Launch Course");
                }
                else
                {
                    url_to_view = string.Format("CourseLaunch.aspx?CourseID={0}", sCourseID);
                    rmPopup.RadConfirm("Are you sure you want to take this course?", "confirmCallBackFn", 350, 180, null, "Launch Course");
                    Int32 CID_ = Convert.ToInt32(sCourseID_1);
                    string CourseEnddate = DataHelper.GetCourseEndDate(CID_);
                    HttpContext.Current.Session["EnrolCourseID_EndDate"] = CourseEnddate;
                    HttpContext.Current.Session["EnrolCourse_Path"] = Course_Path;
                    HttpContext.Current.Session["EnrolUrl_to_view"] = url_to_view;
                    HttpContext.Current.Session["EnrolCourseType"] = Convert.ToInt32(courseTypeID);
                }
            }
            else
            {
                string script = "function f(){$find(\"" + windowEnrollCourse.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            }
        }
    }

    protected void RadAjaxManager2_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument.ToString() == "ok")
        {
            lblRadConfirm_Res.Text = "OK";

            var AJ_CourseEndDate = HttpContext.Current.Session["EnrolCourseID_EndDate"];
            var AJ_EnrollCourseID = HttpContext.Current.Session["EnrolCourseID"];
            var AJ_CoursePath = HttpContext.Current.Session["EnrolCourse_Path"];
            var AJ_url_to_view = HttpContext.Current.Session["EnrolUrl_to_view"];
            var AJ_Course_Type = HttpContext.Current.Session["EnrolCourseType"];

            if (Convert.ToString(AJ_Course_Type) == "1")
            {
                Redirect_URL(Convert.ToString(AJ_url_to_view), "_blank", "menubar=0,width=600,height=600,scrollable=yes", Convert.ToString(AJ_EnrollCourseID), Convert.ToString(AJ_CoursePath), false, Convert.ToString(AJ_EnrollCourseID));
            }
            else
            {
                GetSessions();
                Int32 CID_ = Convert.ToInt32(AJ_EnrollCourseID);
                bool hasTakenSameScormCourse = DataHelper.CheckUserScormTaken(Convert.ToInt32(_employeeId), CID_);
                if (hasTakenSameScormCourse == false)
                {
                    //SaveToEnrollCourse(Convert.ToInt32(AJ_EnrollCourseID), Convert.ToString(AJ_CourseEndDate));
                }

                Redirect_URL(Convert.ToString(AJ_url_to_view), "_blank", "menubar=0,width=600,height=600,scrollable=yes", Convert.ToString(AJ_EnrollCourseID), Convert.ToString(AJ_CoursePath), true, Convert.ToString(AJ_EnrollCourseID) + "|" + _employeeId);
            }
        }
        else
        {
            lblRadConfirm_Res.Text = "Cancel";
        }
    }

    private void Redirect_URL(string url, string target, string windowFeatures, string CourseID, string cpath, bool isSorm, string ENC_ID)
    {
        HttpContext context = HttpContext.Current;
        DataTable dt = null;

        if ((String.IsNullOrEmpty(target) ||
            target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
            String.IsNullOrEmpty(windowFeatures))
        {

            context.Response.Redirect(url);
        }
        else
        {
            Page page = (Page)context.Handler;
            if (page == null)
            {
                throw new InvalidOperationException(
                    "Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);

            string script;
            if (!String.IsNullOrEmpty(windowFeatures))
            {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else
            {
                script = @"window.open(""{0}"", ""{1}"");";
            }

            GetSessions();
            string strURL;
            if (isSorm == false)
            {
                strURL = "CourseLauncher.aspx";
            }
            else
            {

                strURL = string.Format("CourseLaunch.aspx?CourseID={0}", ENC_ID);
            }

            if (cpath == string.Empty)
            {
                Session["CourseLaunch"] = url;
            }
            else
            {
                Session["CourseLaunch"] = cpath;
            }

            GetSessions();

            Session["USERID"] = _employeeId;
            Session["COURSEID"] = CourseID;

            script = String.Format(script, strURL, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page, typeof(Page),
                "Redirect",
                script,
                true);
        }
    }

    protected void cbSearchType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        SearchKeys = new[] { TwwId };

        cbDirectReports.Text = "";
        cbDirectReports.ClearCheckedItems();

        txtSearch.Text = "";

        SearchCriteria = Convert.ToInt32(cbSearchType.SelectedValue);

        switch (cbSearchType.SelectedValue)
        {
            case "0":
                cbDirectReports.Enabled = true;
                cbDirectReports.DataTextField = "Name";
                cbDirectReports.DataValueField = "TwwId";
                cbDirectReports.DataBind();
                break;
            case "1":
                cbDirectReports.Enabled = false;
                cbDirectReports.DataTextField = "Name";
                cbDirectReports.DataValueField = "TwwId";
                cbDirectReports.DataBind();
                break;
            case "2":
                cbDirectReports.Enabled = false;
                cbDirectReports.DataTextField = "Name";
                cbDirectReports.DataValueField = "TwwId";
                cbDirectReports.DataBind();
                break;
        }

        gridEmployee.Rebind();
    }

    private string GetCourseURL(string CourseID, string TestCatID, string twwid)
    {
        string sDecryptedTestCategoryId = null;
        string sCourseId = null;
        var sPath = "";

        try
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOG_TWWID");
            _employeeId = lblLNAME.Text;
            try
            {
                sDecryptedTestCategoryId = DecryptString(TestCatID);
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "FormatException")
                {
                    string _decrypt = TestCatID.Replace(" ", "+");
                    sDecryptedTestCategoryId = DecryptString(_decrypt);
                }
                else
                {
                    //Response.Redirect("Error.aspx");
                }
            }

            var db = new IntranetDBDataContext();
            var colNucommCourse = new List<tbl_NuCommUniversity_Lkp_Course>(db.tbl_NuCommUniversity_Lkp_Courses).Where(
                        p => p.CourseID == Convert.ToInt32(sDecryptedTestCategoryId)).ToList();

            if (colNucommCourse.Count > 0)
            {
                sCourseId = CourseID;
                var colTranscomCourse =
                           new List<tbl_TranscomUniversity_Cor_Course>(
                               db.tbl_TranscomUniversity_Cor_Courses.Where(p => p.CourseID == Convert.ToInt32(sCourseId))).ToList();
                if (colTranscomCourse.Count > 0)
                {

                    foreach (var item in colNucommCourse)
                    {
                        sPath = item.CoursePath;
                    }
                    var user = Registration.GetUser(_employeeId).SingleOrDefault();
                    if (user != null)
                    {
                        db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(_employeeId), Convert.ToInt32(sDecryptedTestCategoryId));
                        db.SubmitChanges();
                    }
                    else
                    {
                        // Response.Redirect("~/Login.aspx");
                    }
                }
            }
            else
            {
                //GetSessions();
                var user = Registration.GetUser(_employeeId).SingleOrDefault();
                sCourseId = CourseID;
                if (user != null)
                {
                    db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(_employeeId), Convert.ToInt32(sDecryptedTestCategoryId));
                    db.SubmitChanges();
                }
            }
        }
        catch (Exception)
        {
            // Response.Redirect("Error.aspx");
        }

        return sPath;
    }

    public string DecryptString(string inputString)
    {
        System.IO.MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = new byte[inputString.Length];
            byteInput = Convert.FromBase64String(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateDecryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }

        Encoding encoding1 = Encoding.UTF8;
        return encoding1.GetString(memStream.ToArray());
    }

    private void GetSessions()
    {
        try
        {
            var lv = (MultiView)Master.FindControl("mvMenu");
            string CurrIndx = CurrVwIndx.ToString();
            lv.ActiveViewIndex = Int32.Parse(CurrIndx);
        }
        catch
        {
            var lv = (MultiView)Master.FindControl("mvMenu");
            int CurrIndx = lv.ActiveViewIndex;
            lv.ActiveViewIndex = CurrIndx;
        }

        try
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            lblWel.Text = CurrTWWID.ToString();
            _employeeId = lblWel.Text;
        }
        catch
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            _employeeId = lblWel.Text;
        }

        try
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
            lblLNAME.Text = CurrLogName.ToString();
        }
        catch
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
            lblLNAME.Text = lblLNAME.Text;
        }
    }
}

public static class ResponseHelper_1
{
    public static void Redirect(string url, string target, string windowFeatures)
    {
        HttpContext context = HttpContext.Current;

        if ((String.IsNullOrEmpty(target) ||
            target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
            String.IsNullOrEmpty(windowFeatures))
        {

            context.Response.Redirect(url);
        }
        else
        {
            Page page = (Page)context.Handler;
            if (page == null)
            {
                throw new InvalidOperationException(
                    "Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);

            string script;
            if (!String.IsNullOrEmpty(windowFeatures))
            {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else
            {
                script = @"window.open(""{0}"", ""{1}"");";
            }

            script = String.Format(script, url, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page,
                typeof(Page),
                "Redirect",
                script,
                true);
        }
    }
}