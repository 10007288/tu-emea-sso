﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CourseLauncher.aspx.cs" Inherits="CourseLauncher" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112026746-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-112026746-1');
    </script>
    <style type="text/css">
        #CourseURL
        {
            height: 432px;
        }
         .detailTable 
        { 
          margin-left:40px !important; 
        } 
         .detailTable2 
        { 
          margin-left:80px !important; 
        } 
         .detailTable3 
        { 
          margin-left:120px !important; 
        } 
         .detailTable4 
        { 
          margin-left:160px !important; 
        } 
        .VwComment
        {    
                margin:0px 0px 0px 0px;
                background:url(../../images/comment.png) left center no-repeat;
                padding: 0em 1.2em;
                font: 8pt "tahoma";
                color: #336699;
                text-decoration: none;
                font-weight: normal;
                letter-spacing: 0px;
         }
    </style>
    
   
</head>
<body onbeforeunload="javascript: Popup()">
    <script type="text/javascript">
//     function Popup() {
//         var c_no = document.getElementById('<%=CourseNo_HF.ClientID %>').value;
//         var tww2 = document.getElementById('<%=TWWID_HF.ClientID %>').value;
//         window.open("Logout.aspx?CourseNo=" + c_no + "&" + "twwid=" + tww2, "win1", "dialogWidth:800px; dialogHeight:500px; dialogLeft:252px; dialogTop:120px; center:yes");
//         //window.open("Logout.aspx?CourseNo=" + CNo_HF + '&' + 'TWWID=' + TWW_HF, "win1", "dialogWidth:800px; dialogHeight:500px; dialogLeft:252px; dialogTop:120px; center:yes");
//     } 
    </script>
    <form id="form1" runat="server" visible="true">

    <asp:hiddenfield   ID="CourseNo_HF" runat="server" ></asp:hiddenfield>
    <asp:hiddenfield   ID="TWWID_HF" runat="server" ></asp:hiddenfield>
        
        <div style="width: 100%; height: 100%">
        <%--<iframe width="100%" height = "100%" id="CourseURL" runat="server" onload="AdjustIframeHeightOnLoad()"></iframe>--%>
        <%--src="http://localhost:53616/Images/courses/SMG/Smart_Goal_Settings_.htm"--%>
        <table style="width: 100%; height: 100%">
            <tr>
                <td>
                    <iframe id="CourseURL" runat="server" style="margin:0; width:100%; height:650px; border:none;" scrolling="yes" name="CourseURL"></iframe>
                </td>
            </tr>
            <tr>
                <td>
                        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" />
                        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                            
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="lvComments">
                                    <UpdatedControls>
                            
                                        <telerik:AjaxUpdatedControl ControlID="ListViewPanel1" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                                    </UpdatedControls>
                                </telerik:AjaxSetting>

                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                     <div style = "border: medium solid #01847E; background-color: #EDEDED; visibility:visible;"   >
                                            <table style="height: 140px; width: 100%">
                                                        <tr><td>
                                                            <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="98%" 
                                                                Height="100px" visible = "true" ForeColor="#001131"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            <asp:Button ID="btnComment" ForeColor="#001131" runat="server" Text="Comment" 
                                                                onclick="btnComment_Click" Font-Bold="True" />
                                                           <asp:Button ID="BtnReply" ForeColor="#001131"  runat="server" Text="Reply"
                                                                onclick="btnComment_Click" Visible = "false"/>
                                                            </td>
                                                        </tr>
                                                </table>
                                            <%--</asp:Panel>--%>
            </div>
                        <br />
                     <div class="demo-container no-bg" style="border: medium solid #2C3D5A visibility: visible;">
                <telerik:RadGrid ID="RadGrid1" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    PageSize="7" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                    OnDetailTableDataBind="RadGrid1_DetailTableDataBind" OnNeedDataSource="RadGrid1_NeedDataSource"
                    OnPreRender="RadGrid1_PreRender" OnItemCommand = "RadGrid1_ItemCommand" Width ="100%" OnColumnCreated ="Grid_ColumnCreated" Visible="true">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <ClientSettings > 
                       <Selecting  AllowRowSelect="true" /> 
                     </ClientSettings>
                    <MasterTableView DataKeyNames="CommID,rownumber" AllowMultiColumnSorting="True"> 
                                    <DetailItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text="Comment By:"/>
                                                    <asp:Label ID="txtRepComm_CommBy" runat="server" Text='<%#Bind("CommBy")%>' Font-Bold="True" ForeColor="#003366"></asp:Label> 
                                                    <asp:Label ID="txtRepComm_CommDate" runat="server" Text='<%#Bind("CommDate")%>'></asp:Label>
                                                    <asp:Label ID="txtRowNo" runat="server" Text='<%#Bind("rownumber")%>' Visible="False"></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td colspan = "2">
                                                    <asp:Label ID="txtRep_Comment" runat="server" Text='<%#Bind("CommentText")%>' Width="100%" Style="word-wrap: normal; word-break: break-all;" ForeColor="#006666" Font-Bold="True"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <%--<asp:Button ID="btnVwReply" runat="server" Text="Reply"  OnClick = "btnRepComment_Click"/> --%>
                                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/reply.png" Height="20" Width="20" AlternateText="TEXT" OnClick = "btnRepComment_Click" ToolTip="Reply" />
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/comment.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnVwComment_Click" ToolTip="View Comments"/>   
                                                    
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnDel" runat="server" ImageUrl="~/Images/delete-icon.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnDelComment_Click" ToolTip="Delete Comment" Visible= "<%# DelVisible %>" />   
                                                    <asp:Button ID="btnRepComment" runat="server" Text="Reply" OnClick = "btnRepComment_Click" Postback="true" Visible="false"/>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <asp:Panel ID="pnReply" runat="server" class="detailTable"  style = "border: medium solid #01847E; background-color: #EDEDED; visibility: hidden;" Visible="False">
                                                    <table style="height: 140px; width: 100%">
                                                            <tr><td>
                                                                <asp:TextBox ID="txtReply" runat="server" TextMode="MultiLine"  Width="98%" 
                                                                    Height="100px" ></asp:TextBox></td>
                                                            </tr>
                                                            <tr><td>
                                                                <asp:Button ID="btnReply" runat="server" Text="Reply" 
                                                                    onclick="btnReply_Click" />
                                                               <asp:Button ID="btnReplyDel" runat="server" Text="Delete" 
                                                                                    onclick="btnComment_Click" VISIBLE ="false"/>
                                                               <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                                                </td>
                                                            </tr> 
                                                    </table>
                                                </asp:Panel>
                                            </tr>
                                        </table>
                                 </DetailItemTemplate>
                    <NestedViewSettings >
                            <ParentTableRelation>
                                <telerik:GridRelationFields DetailKeyField="CommID" MasterKeyField="CommID" />
                            </ParentTableRelation>
                    </NestedViewSettings>
                        <NestedViewTemplate>
                        <asp:Panel ID="Panel1" runat="server" CssClass="detailTable" >
                           <div class="contactWrap">
                            <telerik:RadGrid ID="doc_grid" runat="server" OnNeedDataSource="RadGrid2_NeedDataSource" OnItemCommand = "RadGrid2_ItemCommand" OnColumnCreated ="Grid_ColumnCreated" Width = "100%">
                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="CommID">
                                <DetailItemTemplate>
                                        <table>

                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text="Replied By:"/>
                                                    <asp:Label ID="txtRepComm_CommBy" runat="server" Text='<%#Bind("CommBy")%>' Font-Bold="True" ForeColor="#003366"></asp:Label> 
                                                    <asp:Label ID="txtRepComm_CommDate" runat="server" Text='<%#Bind("CommDate")%>'></asp:Label>
                                                    <asp:Label ID="txtRowNo2" runat="server" Text='<%#Bind("rownumber")%>' Visible="False"></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="TextBox3" runat="server" ReadOnly="True" Text='<%#Bind("CommDate")%>'></asp:TextBox>--%>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td colspan = "2">
                                                    <asp:Label ID="txtRep_Comment" runat="server" Text='<%#Bind("CommentText")%>' Width="100%" Style="word-wrap: normal; word-break: break-all;" ForeColor="#006666" Font-Bold="True"></asp:Label>
                                                   <%-- <asp:TextBox ID="txtRep_Comment" runat="server" TextMode="MultiLine" Width="100%" 
                                                     Height="100px" ReadOnly="True" Text='<%#Bind("CommentText")%>'></asp:TextBox>--%>
                                                </td>
                                            </tr>
                                            <td>
                                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/reply.png" Height="20" Width="20" AlternateText="TEXT" OnClick = "btnRepComment2_Click" ToolTip="Reply"/>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/comment.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnVwComment2_Click" ToolTip="View Comments"/>   
                                                    
                                                </td>
                                                <td><asp:ImageButton ID="btnDel" runat="server" ImageUrl="~/Images/delete-icon.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnDelComment2_Click" ToolTip="Delete Comment"  Visible= "<%# DelVisible %>"/>   </td>
                                            <tr>
                                                <asp:Panel ID="pnReply2" runat="server" class="detailTable"  style = "border: medium solid #01847E; background-color: #EDEDED; visibility: hidden;" Visible="False">
                                                    <table style="height: 140px; width: 100%">
                                                            <tr><td>
                                                                <asp:TextBox ID="txtReplyLvl2" runat="server" TextMode="MultiLine"  Width="98%" 
                                                                    Height="100px" ></asp:TextBox></td>
                                                            </tr>
                                                            <tr><td>
                                                                <asp:Button ID="btnReplyLvl2" runat="server" Text="Reply" 
                                                                    onclick="btnReply2_Click" />
                                                               <asp:Button ID="btnReplyDelLvl2" runat="server" Text="Delete" 
                                                                                    onclick="btnComment_Click"  VISIBLE ="false"/>
                                                               <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                                                </td>
                                                            </tr> 
                                                    </table>
                                                </asp:Panel>
                                            </tr>
                                            
                                        </table>
                                        
                                        
                                 </DetailItemTemplate>
                                  
                                   <NestedViewSettings >
                                                <ParentTableRelation>
                                                    <telerik:GridRelationFields DetailKeyField="CommID" MasterKeyField="CommID" />
                                                </ParentTableRelation>
                                        </NestedViewSettings>
                                            <NestedViewTemplate>
                                            <asp:Panel ID="Panel1" runat="server" CssClass="detailTable2" >
                                               <div class="contactWrap">
                                                <telerik:RadGrid ID="gridLvl3" runat="server" OnNeedDataSource="RadGrid3_NeedDataSource" OnItemCommand = "RadGrid3_ItemCommand" OnColumnCreated ="Grid_ColumnCreated" Width = "100%">
                                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="CommID">
                                                    <DetailItemTemplate>
                                                            <table>

                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label8" runat="server" Text="Replied By:"/>
                                                                        <asp:Label ID="txtRepComm_CommBy3" runat="server" Text='<%#Bind("CommBy")%>' Font-Bold="True" ForeColor="#003366"></asp:Label> 
                                                                        <asp:Label ID="txtRepComm_CommDate3" runat="server" Text='<%#Bind("CommDate")%>'></asp:Label>
                                                                        <asp:Label ID="txtRowNo3" runat="server" Text='<%#Bind("rownumber")%>' Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:TextBox ID="TextBox3" runat="server" ReadOnly="True" Text='<%#Bind("CommDate")%>'></asp:TextBox>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr >
                                                                    <td colspan = "2">
                                                                        <asp:Label ID="txtRep_Comment3" runat="server" Text='<%#Bind("CommentText")%>' Width="100%" Style="word-wrap: normal; word-break: break-all;" ForeColor="#006666" Font-Bold="True"></asp:Label>
                                                                       <%-- <asp:TextBox ID="txtRep_Comment" runat="server" TextMode="MultiLine" Width="100%" 
                                                                         Height="100px" ReadOnly="True" Text='<%#Bind("CommentText")%>'></asp:TextBox>--%>
                                                                    </td>
                                                                </tr>
                                                                <td>
                                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/reply.png" Height="20" Width="20" AlternateText="TEXT" OnClick = "btnRepComment3_Click" ToolTip="Reply"/>
                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/comment.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnVwComment3_Click" ToolTip="View Comments"/>   
                                                                  
                                                                </td>
                                                                <td><asp:ImageButton ID="btnDel" runat="server" ImageUrl="~/Images/delete-icon.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnDelComment3_Click" ToolTip="Delete Comment"  Visible= "<%# DelVisible %>"/>   </td>
                                                                <tr>
                                                                    <asp:Panel ID="pnReply3" runat="server" class="detailTable"  style = "border: medium solid #01847E; background-color: #EDEDED; visibility: hidden;" Visible="False">
                                                                        <table style="height: 140px; width: 100%">
                                                                                <tr><td>
                                                                                    <asp:TextBox ID="txtReplyLvl3" runat="server" TextMode="MultiLine"  Width="98%" 
                                                                                        Height="100px" ></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr><td>
                                                                                    <asp:Button ID="btnReplyLvl3" runat="server" Text="Reply" 
                                                                                        onclick="btnReply3_Click" />
                                                                                   <asp:Button ID="btnReplyDellvl3" runat="server" Text="Delete" 
                                                                                                        onclick="btnComment_Click" VISIBLE ="false"/>
                                                                                   <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                                                                    </td>
                                                                                </tr> 
                                                                        </table>
                                                                    </asp:Panel>
                                                                </tr>
                                            
                                                            </table>
                                        
                                        
                                                     </DetailItemTemplate>
                                                                 <NestedViewSettings >
                                                                            <ParentTableRelation>
                                                                                <telerik:GridRelationFields DetailKeyField="CommID" MasterKeyField="CommID" />
                                                                            </ParentTableRelation>
                                                                    </NestedViewSettings>
                                                                        <NestedViewTemplate>
                                                                        <asp:Panel ID="Panel1" runat="server" CssClass="detailTable3" >
                                                                           <div class="contactWrap">
                                                                            <telerik:RadGrid ID="gridLvl4" runat="server" OnNeedDataSource="RadGrid4_NeedDataSource" OnItemCommand = "RadGrid4_ItemCommand" OnColumnCreated ="Grid_ColumnCreated" width = "100%">
                                                                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="CommID">
                                                                                <DetailItemTemplate>
                                                                                        <table>

                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="Label8" runat="server" Text="Replied By:"/>
                                                                                                    <asp:Label ID="txtRepComm_CommBy4" runat="server" Text='<%#Bind("CommBy")%>' Font-Bold="True" ForeColor="#003366"></asp:Label> 
                                                                                                    <asp:Label ID="txtRepComm_CommDate4" runat="server" Text='<%#Bind("CommDate")%>'></asp:Label>
                                                                                                    <asp:Label ID="txtRowNo4" runat="server" Text='<%#Bind("rownumber")%>' Visible="False"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <%--<asp:TextBox ID="TextBox3" runat="server" ReadOnly="True" Text='<%#Bind("CommDate")%>'></asp:TextBox>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr >
                                                                                                <td colspan = "2">
                                                                                                    <asp:Label ID="txtRep_Comment4" runat="server" Text='<%#Bind("CommentText")%>' Width="100%" Style="word-wrap: normal; word-break: break-all;" ForeColor="#006666" Font-Bold="True"></asp:Label>
                                                                                                   <%-- <asp:TextBox ID="txtRep_Comment" runat="server" TextMode="MultiLine" Width="100%" 
                                                                                                     Height="100px" ReadOnly="True" Text='<%#Bind("CommentText")%>'></asp:TextBox>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <td>
                                                                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/reply.png" Height="20" Width="20" AlternateText="TEXT" OnClick = "btnRepComment4_Click" ToolTip="Reply"/>
                                                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/comment.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnVwComment4_Click" ToolTip="View Comments"/> 
                                                                                              
                                                                                            </td>
                                                                                            <td><asp:ImageButton ID="btnDel" runat="server" ImageUrl="~/Images/delete-icon.png" Height="20" Width="20" AlternateText="TEXT" OnClick ="btnDelComment4_Click" ToolTip="Delete Comment"  Visible= "<%# DelVisible %>"/>   </td>
                                                                                            <tr>
                                                                                                    <asp:Panel ID="pnReply4" runat="server" class="detailTable"  style = "border: medium solid #01847E; background-color: #EDEDED; visibility: hidden;" Visible="False">
                                                                                                        <table style="height: 140px; width: 100%">
                                                                                                                <tr><td>
                                                                                                                    <asp:TextBox ID="txtReplyLvl4" runat="server" TextMode="MultiLine"  Width="98%" 
                                                                                                                        Height="100px" ></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr><td>
                                                                                                                    <asp:Button ID="btnReplyLvl4" runat="server" Text="Reply" 
                                                                                                                        onclick="btnReply4_Click" />
                                                                                                                   <asp:Button ID="btnReplyDelLvl4" runat="server" Text="Delete" 
                                                                                                                                        onclick="btnComment_Click" VISIBLE ="false"/>
                                                                                                                   <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr> 
                                                                                                        </table>
                                                                                                    </asp:Panel>
                                                                                                </tr>
                                                                                        </table>
                                        
                                        
                                                                                 </DetailItemTemplate>
                                                                                                   <NestedViewSettings >
                                                                                                            <ParentTableRelation>
                                                                                                                <telerik:GridRelationFields DetailKeyField="CommID" MasterKeyField="CommID" />
                                                                                                            </ParentTableRelation>
                                                                                                    </NestedViewSettings>
                                                                                                        <NestedViewTemplate>
                                                                                                        <asp:Panel ID="Panel1" runat="server" CssClass="detailTable4" >
                                                                                                           <div class="contactWrap">
                                                                                                            <telerik:RadGrid ID="gridLvl5" runat="server" OnNeedDataSource="RadGrid5_NeedDataSource" OnItemCommand = "RadGrid5_ItemCommand" OnColumnCreated ="Grid_ColumnCreated" width = "100%">
                                                                                                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="CommID">
                                                                                                                <DetailItemTemplate>
                                                                                                                        <table>

                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="Label8" runat="server" Text="Replied By:"/>
                                                                                                                                    <asp:Label ID="txtRepComm_CommBy5" runat="server" Text='<%#Bind("CommBy")%>' Font-Bold="True" ForeColor="#003366"></asp:Label> 
                                                                                                                                    <asp:Label ID="txtRepComm_CommDate5" runat="server" Text='<%#Bind("CommDate")%>'></asp:Label>
                                                                                                                                    <asp:Label ID="txtRowNo5" runat="server" Text='<%#Bind("rownumber")%>' Visible="False"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <%--<asp:TextBox ID="TextBox3" runat="server" ReadOnly="True" Text='<%#Bind("CommDate")%>'></asp:TextBox>--%>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr >
                                                                                                                                <td colspan = "2">
                                                                                                                                    <asp:Label ID="txtRep_Comment5" runat="server" Text='<%#Bind("CommentText")%>' Width="100%" Style="word-wrap: normal; word-break: break-all;" ForeColor="#006666" Font-Bold="True"></asp:Label>
                                                                                                                                   <%-- <asp:TextBox ID="txtRep_Comment" runat="server" TextMode="MultiLine" Width="100%" 
                                                                                                                                     Height="100px" ReadOnly="True" Text='<%#Bind("CommentText")%>'></asp:TextBox>--%>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <td>
                                                                                                                            <%--<asp:Button ID="btnVwReply5" runat="server" Text="Reply" OnClick = "btnRepComment5_Click"/>--%>
                                                                                                                            </td>
                                                                                                                            <tr>
                                                                                                                                    <asp:Panel ID="pnReply5" runat="server" class="detailTable"  style = "border: medium solid #01847E; background-color: #EDEDED; visibility: hidden;" Visible="False">
                                                                                                                                        <table style="height: 140px; width: 100%">
                                                                                                                                                <tr><td>
                                                                                                                                                    <asp:TextBox ID="txtReplyLvl5" runat="server" TextMode="MultiLine"  Width="98%" 
                                                                                                                                                        Height="100px" ></asp:TextBox></td>
                                                                                                                                                </tr>
                                                                                                                                                <tr><td>
                                                                                                                                                    <asp:Button ID="btnReplyLvl5" runat="server" Text="Reply" 
                                                                                                                                                         /><%-- onclick="btnReply_Click"--%>
                                                                                                                                                   <asp:Button ID="btnReplyDelLvl5" runat="server" Text="Delete" 
                                                                                                                                                                        onclick="btnComment_Click" VISIBLE ="false"/>
                                                                                                                                                   <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                                                                                                                                    </td>
                                                                                                                                                </tr> 
                                                                                                                                        </table>
                                                                                                                                    </asp:Panel>
                                                                                                                                </tr>
                                            
                                                                                                                        </table>
                                        
                                        
                                                                                                                 </DetailItemTemplate>
                               
                                                                                                                 <columns>
                                                                                                                    <telerik:gridboundcolumn sortexpression="unitprice" headertext="reply id" headerbuttontype="textbutton"
                                                                                                                        datafield="CommId" Visible="False">
                                                                                                                    </telerik:gridboundcolumn>
                                                                                                                    <%--<telerik:gridboundcolumn sortexpression="quantity" headertext="reply text" headerbuttontype="textbutton"
                                                                                                                        datafield="commenttext">
                                                                                                                    </telerik:gridboundcolumn>--%>
                                                                                                                </columns>
                                                                                                                </MasterTableView>
                                                                                                            </telerik:RadGrid>
                                                                                                            </div>
                                                                                                        </asp:Panel>
                                                                                                            <%--<asp:Panel ID="Panel2" runat="server" CssClass="detailTable--%>
                            
                                                                                                            <%--</asp:Panel>--%>
                                                                                                        </NestedViewTemplate>
                                                                                 <columns>
                                                                                    <telerik:gridboundcolumn sortexpression="unitprice" headertext="reply id" headerbuttontype="textbutton"
                                                                                        datafield="CommId" Visible="False">
                                                                                    </telerik:gridboundcolumn>
                                                                                    <%--<telerik:gridboundcolumn sortexpression="quantity" headertext="reply text" headerbuttontype="textbutton"
                                                                                        datafield="commenttext">
                                                                                    </telerik:gridboundcolumn>--%>
                                                                                </columns>
                                                                                </MasterTableView>
                                                                            </telerik:RadGrid>
                                                                            </div>
                                                                        </asp:Panel>
                                                                            <%--<asp:Panel ID="Panel2" runat="server" CssClass="detailTable--%>
                            
                                                                            <%--</asp:Panel>--%>
                                                                        </NestedViewTemplate>
                                                     <columns>
                                                        <telerik:gridboundcolumn sortexpression="unitprice" headertext="reply id" headerbuttontype="textbutton"
                                                            datafield="CommId" Visible="False">
                                                        </telerik:gridboundcolumn>
                                                        <%--<telerik:gridboundcolumn sortexpression="quantity" headertext="reply text" headerbuttontype="textbutton"
                                                            datafield="commenttext">
                                                        </telerik:gridboundcolumn>--%>
                                                    </columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                                </div>
                                            </asp:Panel>
                                                <%--<asp:Panel ID="Panel2" runat="server" CssClass="detailTable--%>
                            
                                                <%--</asp:Panel>--%>
                                            </NestedViewTemplate>
                                    
                                 <columns>
                                    <telerik:gridboundcolumn sortexpression="unitprice" headertext="reply id" headerbuttontype="textbutton"
                                        datafield="CommId" Visible="False">
                                    </telerik:gridboundcolumn>
                                    <%--<telerik:gridboundcolumn sortexpression="quantity" headertext="reply text" headerbuttontype="textbutton"
                                        datafield="commenttext">
                                    </telerik:gridboundcolumn>--%>
                                </columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            </div>
                        </asp:Panel>
                            <%--<asp:Panel ID="Panel2" runat="server" CssClass="detailTable--%>
                            
                            <%--</asp:Panel>--%>
                        </NestedViewTemplate>
                   
                           <Columns>
                                <telerik:GridBoundColumn SortExpression="CustomerID" HeaderText="Comment#" HeaderButtonType="TextButton"
                                    DataField="CommID" UniqueName="ColCommID" Resizable="False" Reorderable="False" Visible="False">
                                </telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn SortExpression="CustomerID" HeaderText="User Name" HeaderButtonType="TextButton"
                                    DataField="CommBy" UniqueName="ColCommID" Resizable="False" Reorderable="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="CustomerID" HeaderText="Date" HeaderButtonType="TextButton"
                                    DataField="CommDate" UniqueName="ColCommID" Resizable="False" Reorderable="False">
                                </telerik:GridBoundColumn>--%>
                                <%--<telerik:GridBoundColumn SortExpression="ContactName" HeaderText="Comment Text" HeaderButtonType="TextButton"
                                    DataField="CommentText">
                                </telerik:GridBoundColumn>--%>
                            </Columns>
                           
                    </MasterTableView>
                                   
                </telerik:RadGrid>
            </div>
                </td>
            </tr>
        </table>
        
            
        </div>
        
       
        </form></body></html>