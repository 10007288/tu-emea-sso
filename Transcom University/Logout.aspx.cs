﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;

public partial class Logout : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String CourseNo_QS = Request.QueryString["CourseNo"];
        String TWWID_QS = Request.QueryString["twwid"];

        //lblCourseNo.Text = CourseNo_QS;
        //lblTWWID.Text = TWWID_QS;

        var db = new IntranetDBDataContext();
        db.pr_NuCommUniversity_Sav_EmployeeCourseLogout(Convert.ToInt32(TWWID_QS), Convert.ToInt32(CourseNo_QS));
        db.SubmitChanges();

        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "ClosePage", "window.close();", true);

        //Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.Cache.SetNoStore();

        //var redirectUrl = FormsAuthentication.LoginUrl + "?ReturnUrl=Default.aspx";
        //FormsAuthentication.SignOut();
        //Session.Clear();
        //Session.RemoveAll();
        //Session.Abandon();
        //Response.Redirect(redirectUrl); 
    }
}