﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CalendarEvents.aspx.cs" Inherits="CalendarEvents" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExport") >= 0 ||
                args.get_eventTarget().indexOf("btnExportTRA") >= 0 ||
                args.get_eventTarget().indexOf("btnExportTabular") >= 0) {
                    args.set_enableAjax(false);
                }

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function Approve_CreateEvent(sender, args) {
                setTimeout(function () { $find("<%=windowApproveCreateEvent.ClientID %>").show(); }, 800);
            }
            function CloseEventForm(sender, args) {
                var window = $find('<%=windowApproveCreateEvent.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }
            function Cancel_Reservation(sender, args) {
                setTimeout(function () { $find("<%=windowCancelReservation.ClientID %>").show(); }, 800);
            }
            function CloseCancelForm(sender, args) {
                var window = $find('<%=windowCancelReservation.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }
            function PreviewDetails(sender, args) {
                setTimeout(function () { $find("<%=windowPreviewDetails.ClientID %>").show(); }, 800);
            }
            function ClosePreviewForm(sender, args) {
                var window = $find('<%=windowPreviewDetails.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }

            var popUp;
            function PopUpShowing(sender, eventArgs) {
                popUp = eventArgs.get_popUp();
                var gridWidth = sender.get_element().offsetWidth;
                var gridHeight = sender.get_element().offsetHeight;
                var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
                var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
                popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
                popUp.style.top = ((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
            }  
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridEventsForApproval">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEventsForApproval" />
                    <telerik:AjaxUpdatedControl ControlID="reservationFields" />
                    <telerik:AjaxUpdatedControl ControlID="previewFields" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridUpcomingEvents">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridUpcomingEvents" />
                    <telerik:AjaxUpdatedControl ControlID="previewFields" />
                    <telerik:AjaxUpdatedControl ControlID="hidQueryStr" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tsEvents">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="mpEvents" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnApprove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEventsForApproval" />
                    <telerik:AjaxUpdatedControl ControlID="reservationFields" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelEvent">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEventsForApproval" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCreateEvent">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnPreview">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnGotoReservations">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnClosePreview">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCloseCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="windowApproveCreateEvent" runat="server" Title="Approve & Create Event"
                Width="800px" Height="600px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="reservationFields" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Reservation Summary</h3>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Training Type
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblTrainingType" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Class Name
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblClassName" Font-Bold="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Date from
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblDate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Date to
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblDateTo" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Site
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblSite" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Time Slot
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblTimeSlot" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Trainer
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblTrainer" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Created by
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblCreatedby" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Status
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblStatus" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Approve & Create Event</h3>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Assigned Room
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbAssignedRoom" DataTextField="Room" DataValueField="RoomId"
                                    runat="server" EmptyMessage="- assigned room -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                    DataSourceID="dsRoomAsignment" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="cbAssignedRoom" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                    ValidationGroup="event" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Coordinator Advisory
                            </td>
                            <td class="tblfield">
                                <telerik:RadTextBox runat="server" ID="txtCoordinatorAdvisory" EmptyMessage="- coordinator advisory -"
                                    Width="300" Height="60px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                    SelectionOnFocus="SelectAll" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Enrolment Instructions
                            </td>
                            <td class="tblfield">
                                <telerik:RadEditor ID="txtInstruction" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                    AllowScripts="false" Height="200px" Width="500px" BorderColor="LightGray" BorderStyle="Solid"
                                    BorderWidth="1px" AutoResizeHeight="True">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                    </CssFiles>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Course Description
                            </td>
                            <td class="tblfield">
                                <telerik:RadEditor ID="txtCourseDesc" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                    AllowScripts="false" Height="200px" Width="500px" BorderColor="LightGray" BorderStyle="Solid"
                                    BorderWidth="1px" AutoResizeHeight="True">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                    </CssFiles>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <br />
                                <asp:LinkButton runat="server" ID="btnApprove" Text="Approve Reservation" CssClass="linkBtn"
                                    ValidationGroup="event" OnClick="btnApprove_Click" />
                                |
                                <asp:LinkButton runat="server" ID="btnCancel" Text="Close" CssClass="linkBtn"
                                    OnClientClick="CloseEventForm()" CausesValidation="False" />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowCancelReservation" runat="server" Title="Cancel Reservation"
                Width="560px" Height="200px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="tblCancel" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Cancel Reservation</h3>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Advisory
                            </td>
                            <td class="tblfield">
                                <telerik:RadTextBox runat="server" ID="txtCancelAdvisory" EmptyMessage="- cancel advisory -"
                                    Width="300" Height="60px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                    SelectionOnFocus="SelectAll" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <asp:LinkButton runat="server" ID="btnCancelEvent" Text="Cancel Reservation" CssClass="linkBtn"
                                    ValidationGroup="cancel" OnClick="btnCancelEvent_Click" />
                                |
                                <asp:LinkButton runat="server" ID="btnCloseCancel" Text="Close" CssClass="linkBtn"
                                    OnClientClick="CloseCancelForm()" CausesValidation="False" />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowPreviewDetails" runat="server" Title="Reservation & Event Details"
                Width="800px" Height="570px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="previewFields" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Reservation Details</h3>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Attrition Growth
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblAttrition" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                FTE Count
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblFteCount" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                RTP Tandim
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblRtpTandim" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Wave #
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblWaveNo" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Site
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblSite" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Client
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblClient" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Training Type
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblTrainType" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Class Name
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblClassname" Font-Bold="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Date from
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblDate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Date to
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblDateTo" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Time Slot
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblTime" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Trainer Name
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblTrainer" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Week #
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblWeek" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Advisory
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblAdvisory" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Created By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblCreate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Approved By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblApprove" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Updated By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblUpdatedReserv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Status
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblStat" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Event Details</h3>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Room
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblRoom" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Coordinator Advisory
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblCoorAdvise" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Enrolment Instruction
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblEnrolmentIns" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Course Description
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlvlCourseDesc" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Created By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblCreatedEvent" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Updated By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblUpdatedEvent" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <br />
                                <asp:LinkButton runat="server" ID="btnClosePreview" Text="Close" CssClass="linkBtn"
                                    OnClientClick="ClosePreviewForm()" CausesValidation="False" />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="containerdefault">
                <table width="100%">
                    <tr>
                        <td>
                            <telerik:RadTabStrip ID="tsEvents" runat="server" SelectedIndex="0" MultiPageID="mpEvents"
                                ShowBaseLine="true" Width="100%" CausesValidation="False" AutoPostBack="True"
                                OnTabClick="tsEvents_TabClick" RenderMode="Lightweight">
                                <Tabs>
                                    <telerik:RadTab Text="Room Reservations - For Approval" Selected="true" />
                                    <telerik:RadTab Text="Upcoming Events" />
                                    <telerik:RadTab Text="Generate Report" />
                                </Tabs>
                            </telerik:RadTabStrip>
                        </td>
                        <td align="right" style="width: 300px">
                            <asp:LinkButton runat="server" ID="btnGotoReservations" CssClass="linkBtn" Text="Go to Reservations"
                                PostBackUrl="Reservations.aspx" />
                        </td>
                    </tr>
                </table>
                <div class="insidecontainer">
                    <telerik:RadMultiPage ID="mpEvents" runat="server" SelectedIndex="0" Width="100%"
                        RenderSelectedPageOnly="True">
                        <telerik:RadPageView ID="pvForApproval" runat="server" Selected="true">
                            <div>
                                <telerik:RadGrid runat="server" ID="gridEventsForApproval" AutoGenerateColumns="False"
                                    AllowFilteringByColumn="True" AllowSorting="True" GridLines="None" CssClass="RadGrid1"
                                    Skin="Default" OnNeedDataSource="gridEventsForApproval_NeedDataSource">
                                    <GroupingSettings CaseSensitive="False" />
                                    <MasterTableView DataKeyNames="ReservationReqId" ShowHeader="true" AutoGenerateColumns="False"
                                        PageSize="10" EditMode="PopUp" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                                        NoMasterRecordsText="No reservations 'for approval' to display." AllowPaging="True"
                                        TableLayout="Auto">
                                        <EditFormSettings>
                                            <PopUpSettings Modal="true" />
                                        </EditFormSettings>
                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                        <Columns>
                                            <telerik:GridBoundColumn UniqueName="ReservationReqId" HeaderText="Reserv ID" DataField="ReservationReqId"
                                                AllowFiltering="False">
                                                <HeaderStyle ForeColor="Silver" />
                                                <ItemStyle ForeColor="Gray" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="TrainingClassName" HeaderText="Class Name" DataField="TrainingClassName"
                                                FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                                ShowFilterIcon="False" />
                                            <telerik:GridBoundColumn UniqueName="TrainingType" HeaderText="Training Type" DataField="TrainingType"
                                                AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="Date" HeaderText="Date from" DataField="Date"
                                                DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="DateTo" HeaderText="Date to" DataField="DateTo"
                                                DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="CompanySite" HeaderText="Site" DataField="CompanySite"
                                                AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="TimeSlot" HeaderText="Time Slot" DataField="TimeSlot"
                                                AllowFiltering="False" />
                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                UniqueName="ViewDetails">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnPreview" runat="server" ToolTip="View Complete Details" OnClientClick="PreviewDetails()"
                                                        OnClick="btnPreview_Click"><img style="border:0;" alt="" src="Images/more2.png"/></asp:LinkButton></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                UniqueName="Approve">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnCreateEvent" runat="server" ToolTip="Approve" OnClientClick="Approve_CreateEvent()"
                                                        OnClick="btnCreateEvent_Click"><img style="border:0;" alt="" src="Images/thumb.png"/></asp:LinkButton></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                UniqueName="CancelColumn">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnCancel" runat="server" ToolTip="Cancel" OnClientClick="Cancel_Reservation()"
                                                        OnClick="btnCancel_Click"><img style="border:0;" alt="" src="Images/discard2.png"/></asp:LinkButton></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="pvUpcomingEvents" runat="server">
                            <div>
                                <telerik:RadGrid runat="server" ID="gridUpcomingEvents" AutoGenerateColumns="False"
                                    AllowFilteringByColumn="True" AllowSorting="True" GridLines="None" CssClass="RadGrid1"
                                    Skin="Default" OnItemCommand="gridUpcomingEvents_ItemCommand" OnItemDataBound="gridUpcomingEvents_ItemDataBound"
                                    OnNeedDataSource="gridUpcomingEvents_NeedDataSource">
                                    <GroupingSettings CaseSensitive="False" />
                                    <MasterTableView DataKeyNames="ReservationReqId,StatusId,TrainingTypeID,TrainingType,TrainingClassName,TrainerName,CompanySite,TimeSlot,Date,DateTo,EventId,Client"
                                        ShowHeader="true" AutoGenerateColumns="False" PageSize="10" EditMode="PopUp"
                                        EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" NoMasterRecordsText="No events to display."
                                        AllowPaging="True" TableLayout="Auto">
                                        <EditFormSettings CaptionFormatString="Edit Reservation ID : {0}" CaptionDataField="ReservationReqId"
                                            PopUpSettings-Height="600px" PopUpSettings-Width="800px">
                                            <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                        </EditFormSettings>
                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                        <Columns>
                                            <telerik:GridBoundColumn UniqueName="EventId" HeaderText="Event ID" DataField="EventId"
                                                AllowFiltering="False">
                                                <HeaderStyle ForeColor="Silver" />
                                                <ItemStyle ForeColor="Gray" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="ReservationReqId" HeaderText="Reserv ID" DataField="ReservationReqId"
                                                AllowFiltering="False">
                                                <HeaderStyle ForeColor="Silver" />
                                                <ItemStyle ForeColor="Gray" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="TrainingClassName" HeaderText="Class Name" DataField="TrainingClassName"
                                                FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                                ShowFilterIcon="False" />
                                            <telerik:GridBoundColumn UniqueName="TrainingType" HeaderText="Training Type" DataField="TrainingType"
                                                AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="Date" HeaderText="Date from" DataField="Date"
                                                DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="DateTo" HeaderText="Date to" DataField="DateTo"
                                                DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="CompanySite" HeaderText="Site" DataField="CompanySite"
                                                AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="RoomAssignment" HeaderText="Room" DataField="RoomAssignment"
                                                AllowFiltering="False" />
                                            <telerik:GridBoundColumn UniqueName="TimeSlot" HeaderText="Time Slot" DataField="TimeSlot"
                                                AllowFiltering="False" />
                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                UniqueName="ViewDetails">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnPreview" runat="server" ToolTip="View Complete Details" OnClientClick="PreviewDetails()"
                                                        OnClick="btnPreview_Click"><img style="border:0;" alt="" src="Images/more2.png"/></asp:LinkButton></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                HeaderStyle-Width="50px">
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridEditCommandColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                UniqueName="CancelColumn">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnCancel" runat="server" ToolTip="Cancel" OnClientClick="Cancel_Reservation()"
                                                        OnClick="btnCancel_Click"><img style="border:0;" alt="" src="Images/discard2.png"/></asp:LinkButton></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings EditFormType="Template">
                                            <FormTemplate>
                                                <table id="tblEditMode" class="tabledefault">
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <h3>
                                                                Reservation Details</h3>
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:ValidationSummary ID="validationSummaryEditMode" ShowMessageBox="False" ShowSummary="True"
                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                runat="server" ValidationGroup="edit" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Attrition/Growth" ControlToValidate="cbAttritionGrowth_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />Attrition/Growth
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbAttritionGrowth_edit" DataTextField="Desc" DataValueField="Id"
                                                                runat="server" DropDownAutoWidth="Enabled" MaxHeight="300px" DataSourceID="dsAttritionGrowth" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="FTE Count" ControlToValidate="txtFteCount_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />FTE Count
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox runat="server" ID="txtFteCount_edit">
                                                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                                            </telerik:RadNumericTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="RTP Tandim" ControlToValidate="txtRtpTandim_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />RTP Tandim
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtRtpTandim_edit" SelectionOnFocus="SelectAll" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Wave #" ControlToValidate="txtWaveNo_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />Wave #
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox runat="server" ID="txtWaveNo_edit">
                                                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                                            </telerik:RadNumericTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Site" ControlToValidate="cbSite_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />Site
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbSite_edit" DataTextField="CompanySite" DataValueField="CompanySiteID"
                                                                runat="server" DropDownAutoWidth="Enabled" MaxHeight="300px" DataSourceID="dsSite" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Client" ControlToValidate="cbClient_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />Client
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbClient_edit" DataTextField="ClientName" DataValueField="Id"
                                                                runat="server" DropDownAutoWidth="Enabled" MaxHeight="300px" DataSourceID="dsClient"
                                                                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="cbClientEdit_SelectedIndexChanged">
                                                            </telerik:RadComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Training Type" ControlToValidate="cbTrainingType_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />
                                                            Type
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbTrainingType_edit" DataTextField="TrainingType" DataValueField="TrainingTypeId"
                                                                runat="server" DropDownAutoWidth="Enabled" MaxHeight="300px" AutoPostBack="True"
                                                                OnSelectedIndexChanged="cbTrainingTypeEdit_SelectedIndexChanged" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Class Name" ControlToValidate="cbClassName_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" EnableClientScript="False" />
                                                            Class Name
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbClassName_edit" runat="server" EmptyMessage="- select -"
                                                                DropDownAutoWidth="Enabled" MaxHeight="300px" DataTextField="ClassName" DataValueField="ClassName">
                                                            </telerik:RadComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Date from" ControlToValidate="dpDate_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />Date from
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpDate_edit" runat="server" Width="150" EnableTyping="False"
                                                                ShowPopupOnFocus="True">
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Date to" ControlToValidate="dpDateTo_edit"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                Text="*" />Date to
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpDateTo_edit" runat="server" Width="150" EnableTyping="False"
                                                                ShowPopupOnFocus="True">
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                            <asp:CompareValidator ID="dateCompareValidator" runat="server" ControlToValidate="dpDateTo_edit"
                                                                ControlToCompare="dpDate_edit" Operator="GreaterThanEqual" Type="Date" ErrorMessage="The second date must be after the first one"
                                                                CssClass="displayerror" ValidationGroup="edit" EnableClientScript="False">
                                                            </asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Time Slot"
                                                                ControlToValidate="cbTimeSlot_edit" SetFocusOnError="True" Display="Dynamic"
                                                                CssClass="displayerror" ValidationGroup="edit" Text="*" />Time Slot
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbTimeSlot_edit" DataTextField="TimeSlot" DataValueField="TimeSlotId"
                                                                runat="server" DropDownAutoWidth="Enabled" MaxHeight="300px" DataSourceID="dsTimeSlot" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Trainer Name"
                                                                ControlToValidate="txtTrainerName_edit" SetFocusOnError="True" Display="Dynamic"
                                                                CssClass="displayerror" ValidationGroup="edit" Text="*" />Trainer Name
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtTrainerName_edit" SelectionOnFocus="SelectAll"
                                                                Width="200" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Week #"
                                                                ControlToValidate="txtWeekNo_edit" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                ValidationGroup="edit" Text="*" />Week #
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox runat="server" ID="txtWeekNo_edit">
                                                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                                            </telerik:RadNumericTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Advisory
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAdvisory_edit" Width="300" Height="60px"
                                                                TextMode="MultiLine" CssClass="txtMultiline" Wrap="True" SelectionOnFocus="SelectAll" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trEventHeader">
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <h3>
                                                                Event Details</h3>
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trRoomAssign">
                                                        <td class="tblLabel">
                                                            Assigned Room
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbAssignedRoom_edit" DataTextField="Room" DataValueField="RoomId"
                                                                runat="server" EmptyMessage="- assigned room -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                                DataSourceID="dsRoomAsignment" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                                                                ControlToValidate="cbAssignedRoom_edit" SetFocusOnError="True" Display="Dynamic"
                                                                CssClass="displayerror" ValidationGroup="edit" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trCoorAdvisory">
                                                        <td class="tblLabel">
                                                            Coordinator Advisory
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtCoordinatorAdvisory_edit" EmptyMessage="- coordinator advisory -"
                                                                Width="300" Height="60px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                                                SelectionOnFocus="SelectAll" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trEnrollIns">
                                                        <td class="tblLabel">
                                                            Enrolment Instructions
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadEditor ID="txtInstruction_edit" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                                                AllowScripts="false" Height="200px" Width="500px" BorderColor="LightGray" BorderStyle="Solid"
                                                                BorderWidth="1px" AutoResizeHeight="True">
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                </CssFiles>
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trCourseDesc">
                                                        <td class="tblLabel">
                                                            Course Description
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadEditor ID="txtCourseDesc_edit" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                                                AllowScripts="false" Height="200px" Width="500px" BorderColor="LightGray" BorderStyle="Solid"
                                                                BorderWidth="1px" AutoResizeHeight="True">
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                </CssFiles>
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <br />
                                                            <asp:LinkButton ID="bntUpdate" ValidationGroup="edit" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                                            </asp:LinkButton>
                                                            |
                                                            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                CommandName="Cancel" />
                                                            <br />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </FormTemplate>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <ClientEvents OnPopUpShowing="PopUpShowing" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </div>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="pvEventReports" runat="server">
                            <div>
                                <telerik:RadTabStrip ID="tsReports" runat="server" SelectedIndex="0" MultiPageID="mpReports"
                                    Width="100%">
                                    <Tabs>
                                        <telerik:RadTab Text="TRA" runat="server" Selected="True" />
                                        <telerik:RadTab Text="Tabular Report" runat="server" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <hr />
                                <telerik:RadMultiPage ID="mpReports" runat="server" SelectedIndex="0" RenderSelectedPageOnly="False"
                                    Width="100%">
                                    <telerik:RadPageView ID="pvTRA" runat="server" Selected="True">
                                        <table style="padding-bottom: 5px">
                                            <tr>
                                                <td class="paramField">
                                                    Date Start
                                                    <telerik:RadDatePicker ID="dpDateStartTRA" runat="server" Width="150" EnableTyping="False"
                                                        ShowPopupOnFocus="True">
                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td class="paramField">
                                                    Date End
                                                    <telerik:RadDatePicker ID="dpDateEndTRA" runat="server" Width="150" EnableTyping="False"
                                                        ShowPopupOnFocus="True">
                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td class="paramField">
                                                    Time Slot
                                                    <telerik:RadComboBox ID="cbTimeSlot" DataTextField="TimeSlot" DataValueField="TimeSlotID"
                                                        runat="server" EmptyMessage="- select -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                        CausesValidation="False" DataSourceID="dsTimeSlot" Width="150px" ToolTip="Select Time Slot"
                                                        CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EnableLoadOnDemand="True" />
                                                    <asp:RequiredFieldValidator ID="rfvTRATimeSlot" runat="server" ErrorMessage="*" ControlToValidate="cbTimeSlot"
                                                        SetFocusOnError="True" Display="Dynamic" ValidationGroup="vgTRA" CssClass="displayerror" />
                                                </td>
                                                <td class="paramField">
                                                    Location
                                                    <telerik:RadComboBox ID="cbLocation" DataTextField="Room" DataValueField="RoomID"
                                                        runat="server" EmptyMessage="- select -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                        CausesValidation="False" DataSourceID="dsRoomAsignment" Width="150px" ToolTip="Select Room"
                                                        CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EnableLoadOnDemand="True" />
                                                    <asp:RequiredFieldValidator ID="rfvTRALocation" runat="server" ErrorMessage="*" ControlToValidate="cbLocation"
                                                        SetFocusOnError="True" Display="Dynamic" ValidationGroup="vgTRA" CssClass="displayerror" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="paramField" colspan="4">
                                                    <telerik:RadButton ID="btnViewTRA" runat="server" Text="View Report" OnClick="btnViewTRA_Click"
                                                        ValidationGroup="vgTRA" Style="margin-top: 5px" />
                                                    <telerik:RadButton ID="btnExportTRA" runat="server" Text="Export" OnClick="btnExportTRA_Click"
                                                        Visible="false" Style="margin-top: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div style="overflow: hidden !important">
                                            <rsweb:ReportViewer ID="rvTRA" runat="server" ProcessingMode="Remote" ShowParameterPrompts="False"
                                                ShowDocumentMapButton="False" ShowPromptAreaButton="False" AsyncRendering="false"
                                                CssClass="rptviewer" ShowExportControls="False" ShowRefreshButton="False" ShowZoomControl="False"
                                                ShowPrintButton="False" Width="100%" Height="100%" Visible="False">
                                                <ServerReport ReportServerUrl="http://172.16.20.25/reportserver" />
                                            </rsweb:ReportViewer>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvTabular" runat="server">
                                        <table style="padding-bottom: 5px">
                                            <tr>
                                                <td class="paramField">
                                                    Date Start
                                                    <telerik:RadDatePicker ID="dpDateStartTabular" runat="server" Width="150" EnableTyping="False"
                                                        ShowPopupOnFocus="True">
                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td class="paramField">
                                                    Date End
                                                    <telerik:RadDatePicker ID="dpDateEndTabular" runat="server" Width="150" EnableTyping="False"
                                                        ShowPopupOnFocus="True">
                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td class="paramField">
                                                    Client
                                                    <telerik:RadComboBox ID="cbClientTabular" DataTextField="ClientName" DataValueField="Id"
                                                        runat="server" EmptyMessage="- select -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                        CausesValidation="False" DataSourceID="dsClient" Width="150px" ToolTip="Select Time Slot"
                                                        CheckBoxes="true" EnableCheckAllItemsCheckBox="true" EnableLoadOnDemand="True" />
                                                    <asp:RequiredFieldValidator ID="rfvTabularClient" runat="server" ErrorMessage="*"
                                                        ControlToValidate="cbClientTabular" SetFocusOnError="True" Display="Dynamic"
                                                        ValidationGroup="vgTabular" CssClass="displayerror" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="paramField" colspan="3">
                                                    <telerik:RadButton ID="btnViewTabular" runat="server" Text="View Report" OnClick="btnViewTabular_Click"
                                                        ValidationGroup="vgTabular" Style="margin-top: 5px" />
                                                    <telerik:RadButton ID="btnExportTabular" runat="server" Text="Export" OnClick="btnExportTabular_Click"
                                                        Visible="false" ValidationGroup="vgTabular" Style="margin-top: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div>
                                            <rsweb:ReportViewer ID="rvTabular" runat="server" ProcessingMode="Remote" ShowParameterPrompts="False"
                                                ShowDocumentMapButton="False" ShowPromptAreaButton="False" AsyncRendering="false"
                                                CssClass="rptviewer" ShowExportControls="False" ShowRefreshButton="False" ShowZoomControl="False"
                                                ShowPrintButton="False" Width="100%" Height="100%" Visible="False">
                                                <ServerReport ReportServerUrl="http://172.16.20.25/reportserver" />
                                            </rsweb:ReportViewer>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </div>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidQueryStr" Value="0" />
    </div>
    <div>
        <%--<asp:SqlDataSource ID="dsTrainingType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT TOP 5 [TrainingTypeId], [TrainingType] FROM [ref_TranscomUniversity_TrainingType] WHERE ([Active] = 1) ORDER BY [TrainingTypeId]" />--%>
        <asp:SqlDataSource ID="dsAttritionGrowth" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [Id], [Desc] FROM [ref_TranscomUniversity_AttritionGrowth] WHERE ([Active] = 1) ORDER BY [Id]" />
        <asp:SqlDataSource ID="dsTimeSlot" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [TimeSlotId], [TimeSlot] FROM [ref_TranscomUniversity_TimeSlot] WHERE ([Active] = 1) ORDER BY [TimeSlotId]" />
        <asp:SqlDataSource ID="dsSite" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_PhilippineSites" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="CompanySiteID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [Id], [ClientName], [Active] FROM [INTRANET].[dbo].[ref_TranscomUniversity_Class_Client] WHERE [Active] = 1 ORDER BY [ClientName]"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsRoomAsignment" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [RoomId], [Room] FROM [ref_TranscomUniversity_RoomAssignment] WHERE ([Active] = 1) ORDER BY [Room]" />
        <asp:SqlDataSource ID="dsCampaign" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="
            SELECT 
                A.AccountID 'CampaignID', 
                B.Account 'Campaign'
            FROM  SUSL3PSQLDB02.CIMEnterprise.dbo.tbl_Business_Cor_CampaignStatus A 
            INNER JOIN SUSL3PSQLDB02.CIMEnterprise.dbo.tbl_Business_Lkp_CampaignStatusType E 
            ON E.CampaignStatusTypeID = A.CampaignStatusTypeID 
            INNER JOIN SUSL3PSQLDB02.CIMEnterprise.dbo.tbl_Business_Cor_Account B 
            ON A.AccountID = B.AccountID 
            INNER JOIN SUSL3PSQLDB04.CIMeReport.[dbo].[tbl_Hierarchy_Account] HA
            ON HA.CampaignID = A.AccountID
            WHERE (GetDate() BETWEEN A.StartDate AND A.EndDate) 
            AND A.CampaignStatusTypeID = 5" />
    </div>
</asp:Content>
