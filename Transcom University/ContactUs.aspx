<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <div align="center">
            <br />
            <span style="font-size: 16pt;">Contact Transcom University</span>
            <hr />
            <table style="width: 500px">
                <tr>
                    <td style="width: " align="right">
                        Email :
                    </td>
                    <td>
                        <a href="mailto:university@transcom.com" class="linkBtn">university@transcom.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Suggest a Course :
                    </td>
                    <td>
                        <a href="mailto:university@transcom.com" class="linkBtn">university@transcom.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Course Feedback :
                    </td>
                    <td>
                        <asp:HyperLink runat="server" ID="hplContactUs" Text="Click here" NavigateUrl="https://applications.transcom.com/transcomuniversity/EXTERNAL/Feedback/UnivFeedback.asp"
                            Target="_blank" CssClass="linkBtn" />
                    </td>
                </tr>
            </table>
            <br />
        </div>
    </div>
</asp:Content>
