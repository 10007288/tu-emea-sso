﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyReportQuiz.aspx.cs" Inherits="MyReportsQuiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderContent" Runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportExcel") >= 0) {
                    args.set_enableAjax(false);
                }
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }
                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function triggerValidation() {
                Page_ClientValidate();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cboCat">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReports" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cboSubCat">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReports" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cboCourseName">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReports" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReports" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div>
                        <div align="center">
                            <telerik:RadComboBox ID="cmbReportList" DataTextField="ReportName" DataValueField="ReportID" CausesValidation="false"
                                runat="server" EmptyMessage="- select report -" DropDownAutoWidth="Enabled" MaxHeight="300"
                                AutoPostBack="true" DataSourceID="dsReports" ToolTip="Select Report" OnSelectedIndexChanged="cmbReportList_SelectedIndexChanged" />
                        </div>
                        <table width="100%">
                            <tr align="center">
                                <td>
                                    <asp:Label runat="server" ID="lblRptName" Text="Report Name: Quiz" Font-Size="X-Large" />
                                </td>
                            </tr>
                            <tr id="trDesc" runat="server" visible="true" align="center">
                                <td>
                                    <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                    <br>
                                </td>
                            </tr>
                            <tr id="trHoriz" runat="server" visible="False">
                                <td>
                                    <hr />
                                </td>
                            </tr>
                        </table>
                        <table class="divReportParams">
                            <tr>
                                <td>
                                    Course Category
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cboCat" runat="server" DropDownAutoWidth="Enabled" DataTextField="Category"
                                        DataValueField="CategoryID" MarkFirstMatch="true" DataSourceID="dsCategory" EmptyMessage="- Select Category -"
                                        AppendDataBoundItems="true" AutoPostBack="true" CausesValidation="false" ValidationGroup="rfvGroup">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="cboCat" Text="*"
                                        ErrorMessage="*" Display="Dynamic" ValidationGroup="rfvGroup"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Course Sub-category
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cboSubCat" runat="server" DropDownAutoWidth="Enabled" DataTextField="SubCategory"
                                        EmptyMessage="- Select Sub-Category -" AppendDataBoundItems="true" DataValueField="SubCategoryID"
                                        DataSourceID="dsSubCategory" MarkFirstMatch="true" AutoPostBack="true" CausesValidation="false" ValidationGroup="rfvGroup">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboSubCat"
                                        Text="*" ErrorMessage="*" Display="Dynamic" ValidationGroup="rfvGroup"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Course
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cboCourseName" runat="server" DropDownAutoWidth="Enabled"
                                        EmptyMessage="- Select Course -" DataTextField="Title" DataValueField="CourseID" 
                                        DataSourceID="dsCourses" MarkFirstMatch="true" CheckBoxes="true" ValidationGroup="rfvGroup">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboCourseName"
                                        Text="*" ErrorMessage="*" Display="Dynamic" ValidationGroup="rfvGroup"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Department
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cboDepartment" runat="server" DropDownAutoWidth="Enabled"
                                        MarkFirstMatch="true" EmptyMessage="- Select Department -" DataSourceID="dsDepartment"
                                        DataTextField="Department" DataValueField="DepartmentID" CheckBoxes="true" ValidationGroup="rfvGroup">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cboDepartment"
                                        Text="*" ErrorMessage="*" Display="Dynamic" ValidationGroup="rfvGroup"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Start Date
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtpStartDate" runat="server">
                                    </telerik:RadDatePicker>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="dtpStartDate"
                                        Text="*" ErrorMessage="*" Display="Dynamic" ValidationGroup="rfvGroup"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    End Date
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtpEndDate" runat="server">
                                    </telerik:RadDatePicker>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="dtpEndDate"
                                        Text="*" ErrorMessage="*" Display="Dynamic" ValidationGroup="rfvGroup"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                        <table id="tblViewAndExport" width="100%" visible="true" runat="server" style="padding-bottom: 10px">
                            <tr>
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadButton ID="btnView" runat="server" Text="View Report" OnClick="btnView_Click" ValidationGroup="rfvGroup" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportExcel" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export this report to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <telerik:RadGrid runat="server" ID="gridReports" GridLines="None" OnNeedDataSource="gridReports_NeedDataSource"
                            AllowSorting="True" CssClass="RadGrid1" Skin="Metro">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" EnableVirtualScrollPaging="false" />
                            </ClientSettings>
                            <MasterTableView ShowHeader="true" NoMasterRecordsText="No result for selected report."
                                AllowPaging="True" PageSize="50" ShowHeadersWhenNoRecords="true" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                                TableLayout="Auto" Width="1000">
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="True" Mode="NextPrevAndNumeric" />
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="dsReports" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_MyReports" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="RoleName" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsCategory" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select CategoryID, Category from dbo.tbl_TranscomUniversity_Lkp_Category where HideFromList = 0 ORDER BY Category"
            SelectCommandType="Text">
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="dsSubCategory" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select SubCategoryID, SubCategory from dbo.tbl_TranscomUniversity_Lkp_SubCategory where CategoryID = @CategoryID AND HideFromList = 0 ORDER BY SubCategory"
            SelectCommandType="Text">
            <SelectParameters>
                <asp:ControlParameter ControlID="cboCat" PropertyName="SelectedValue" Name="CategoryID" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="dsCourses" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select CourseID, Title from intranet.dbo.tbl_TranscomUniversity_Cor_Course where SubcategoryID = @SubcategoryID AND HideFromList = 0 AND CourseTypeID = 2 ORDER BY Title"
            SelectCommandType="Text">
            <SelectParameters>
                <asp:ControlParameter ControlID="cboSubCat" PropertyName="SelectedValue" Name="SubcategoryID" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="dsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="SELECT [id] as DepartmentID, [description] as Department FROM TranscomUniv_local.dbo.refDepartments WHERE ([active] = 1) ORDER BY [description]" />
    </div>
</asp:Content>

