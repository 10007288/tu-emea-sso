﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="txtTWWID">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cbSecurityQuestion" />
                    <telerik:AjaxUpdatedControl ControlID="txtSecurityAnswer" />
                    <telerik:AjaxUpdatedControl ControlID="btnContinue" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="insidecontainer">
                    <h2>
                        <asp:Label ID="lblTitle" Text="Forgot Password" runat="server" /></h2>
                    <asp:Panel runat="server" ID="PanelNotification" Visible="false" CssClass="msgnotification" Width="100%">
                        <asp:Label Text="" runat="server" ID="lblMessage" />
                    </asp:Panel>
                    <p>
                        <a href="Login.aspx">Back</a></p>
                    <br />
                    <div style="position: relative; padding-bottom: 5px">
                        <div style="float: left; width: 120px">
                            <asp:Label ID="lblFP_TWWID" runat="server" Text="Label">TWW ID</asp:Label>
                        </div>
                        <div>
                            <telerik:RadNumericTextBox runat="server" ID="txtTWWID" AutoPostBack="True" MaxLength="10" OnTextChanged="TWWID_TextChanged"
                                ValidationGroup="reg" SelectionOnFocus="SelectAll">
                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                            </telerik:RadNumericTextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtTWWID"
                                    SetFocusOnError="True" ValidationGroup="forgot" ErrorMessage="* Please provide your TWW ID"
                                    Display="Dynamic" CssClass="displayerror" />
                                <div runat="server" id="UserIsEmployee" style="color: red">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="position: relative; padding-bottom: 5px">
                        <div style="float: left; width: 120px">
                            <asp:Label ID="lblFP_SecQues" runat="server" Visible="true" Text="Label">Security Question</asp:Label>
                        </div>
                        <div>
                            <telerik:RadComboBox runat="server" ID="cbSecurityQuestion" DataTextField="securityquestion" Visible="true"
                                DataValueField="id" MaxHeight="200px" EmptyMessage="- select question -" Width="350px" >
                                <%--DataSourceID="SqlDataSourceSecurityQuestion">--%>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>

                            <asp:SqlDataSource ID="SqlDataSourceSecurityQuestion" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
                                SelectCommand="SELECT SQ.ID, SQ.SecurityQuestion FROM refSecurityQuestions AS SQ INNER JOIN tblSQAnswers AS QA ON SQ.ID = QA.QuestionID WHERE SQ.BitActive = @bitactive AND QA.TWWID = @TWWID ORDER BY SQ.ID">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtTWWID" PropertyName="Text" Name="TWWID" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                            <div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cbSecurityQuestion"
                                    SetFocusOnError="True" ValidationGroup="forgot" ErrorMessage="* Please select a security question"
                                    Display="Dynamic" CssClass="displayerror" />
                            </div>
                        </div>
                    </div>
                    <div style="position: relative; padding-bottom: 5px">
                        <div style="float: left; width: 120px">
                            <asp:Label ID="lblFP_SecAns" runat="server" Visible="true" Text="Label">Security Answer</asp:Label>
                        </div>
                        <div>
                            <telerik:RadTextBox runat="server" ID="txtSecurityAnswer" Width="200" ValidationGroup="forgot" Visible="true"
                                SelectionOnFocus="SelectAll" />
                            <div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSecurityAnswer"
                                    SetFocusOnError="True" ValidationGroup="forgot" ErrorMessage="* Please provide your security answer"
                                    Display="Dynamic" CssClass="displayerror" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div id="managecontrol" style="width: 100%">
                        <asp:LinkButton Text="Continue" runat="server" ID="btnContinue" ValidationGroup="forgot" Visible="true"
                            OnClick="BtnContinueClick" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
