﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminCourses.aspx.cs" Inherits="Admin_AdminCourses" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Transcom University Admin</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112026746-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-112026746-1');
    </script>

    <link id="CSS" rel="stylesheet" type="text/css" href="~/Styles/styles_2.css" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridAdminCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAdminCourses" />
                    <telerik:AjaxUpdatedControl ControlID="hidShowAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnShowAllHidden">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAdminCourses" />
                    <telerik:AjaxUpdatedControl ControlID="hidShowAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
        </script>
        <style type="text/css">
            .RadGrid td
            {
                word-wrap: break-word;
                word-break: break-all;
            }
        </style>
    </telerik:RadCodeBlock>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="container">
                <div align="left">
                    <img src="../Images/tu_image_header_new3.png" alt="" />
                </div>
                <%--<div align="right">
                    <asp:LinkButton runat="server" ID="lnkBtnLogout" Text="Logout" OnClick="lnkBtnLogout_Click" />
                </div>--%>
                <div style="padding: 10px 10px 0 10px">
                    <telerik:RadTabStrip ID="tsAdminCourse" runat="server" MultiPageID="mpAdminCourse"
                        SelectedIndex="0" AutoPostBack="True" Width="100%">
                        <Tabs>
                            <telerik:RadTab Text="Course List" Value="1" Selected="True" />
                            <telerik:RadTab Text="Add Course" Value="2" />
                        </Tabs>
                    </telerik:RadTabStrip>
                </div>
                <div class="paneldefault">
                    <telerik:RadMultiPage ID="mpAdminCourse" runat="server" SelectedIndex="0" RenderSelectedPageOnly="true"
                        Width="100%">
                        <telerik:RadPageView ID="pvCourseList" runat="server">
                            <div align="center" style="padding-bottom: 10px">
                                <asp:LinkButton runat="server" ID="btnShowAllHidden" Text="Show Hidden Courses" OnClick="btnShowAllHidden_Click" />
                            </div>
                            <div>
                                <telerik:RadGrid runat="server" ID="gridAdminCourses" AutoGenerateColumns="False"
                                    OnNeedDataSource="gridAdminCourses_NeedDataSource" GridLines="None" OnItemDataBound="gridAdminCourses_ItemDataBound"
                                    OnUpdateCommand="gridAdminCourses_UpdateCommand" OnCancelCommand="gridAdminCourses_CancelCommand"
                                    CellSpacing="0" ShowStatusBar="true" AllowFilteringByColumn="True">
                                    <GroupingSettings CaseSensitive="False" />
                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="CourseID,ClientID"
                                        PageSize="25" EditMode="EditForms" AllowPaging="True" AllowSorting="True">
                                        <HeaderStyle Font-Names="Helvetica" BackColor="#001538" ForeColor="White" />
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn1 column" UniqueName="EditCommandColumn1">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderStyle-Width="50px"
                                                UniqueName="EditCommandColumn">
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                                            </telerik:GridEditCommandColumn>
                                            <telerik:GridBoundColumn AllowFiltering="False" DataField="CourseID" HeaderText="Course ID"
                                                ReadOnly="True" UniqueName="CourseID" SortExpression="CourseID">
                                                <HeaderStyle ForeColor="Silver" />
                                                <ItemStyle ForeColor="Gray" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="True" DataField="CourseName" HeaderText="Course Name"
                                                UniqueName="CourseName" SortExpression="CourseName" AutoPostBackOnFilter="true"
                                                CurrentFilterFunction="Contains" ShowFilterIcon="true" FilterControlWidth="200">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCourseName" runat="server" Text='<%# Eval("CourseName") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadTextBox ID="txtCourseName" runat="server" Text='<%# Bind("CourseName") %>'
                                                        Width="400" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCourseName"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="True" DataField="CoursePath" HeaderText="Course Path"
                                                UniqueName="CoursePath" SortExpression="CoursePath" AutoPostBackOnFilter="true"
                                                FilterControlWidth="200" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCoursePath" runat="server" Text='<%# Eval("CoursePath") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadTextBox ID="txtCoursePath" runat="server" EmptyMessage="- course path -"
                                                        Text='<%# Bind("CoursePath") %>' Width="400" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="False" DataField="EncryptedCourseID"
                                                HeaderText="Encrypted" UniqueName="EncryptedCourseID" ReadOnly="True">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEncryptedCourseID" runat="server" Text='<%# Eval("EncryptedCourseID") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadTextBox ID="txtEncryptedCourseID" runat="server" EmptyMessage="- encrypted course id -"
                                                        Text='<%# Bind("EncryptedCourseID") %>' Width="150" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
<%--                                            <telerik:GridBoundColumn DataField="ClientID" UniqueName="ClientID" Display="false"
                                                ReadOnly="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="true" DataField="ClientName" ForceExtractValue="Always"
                                                HeaderText="Client" UniqueName="ClientName" SortExpression="ClientName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblClient" runat="server" Text='<%# Eval("ClientName") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadComboBox ID="cbClient" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                        DataTextField="description" DataValueField="ID" EmptyMessage="- select client -"
                                                        EnableLoadOnDemand="True" EnableVirtualScrolling="True" Filter="Contains" HighlightTemplatedItems="True"
                                                        IsCaseSensitive="False" MarkFirstMatch="True" OnItemsRequested="cbClient_ItemsRequested"
                                                        ShowWhileLoading="False" Width="200" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cbClient"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                    <telerik:RadListBox ID="rlbClient" runat="server" CheckBoxes="True" DataSourceID="SqlDataSource3"
                                                        DataTextField="description" DataValueField="ID" Height="115" SelectionMode="Multiple"
                                                        Width="300" ShowCheckAll="true" Visible="false">
                                                    </telerik:RadListBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="rlbClient"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" Enabled="false" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>--%>
                                            <telerik:GridTemplateColumn AllowFiltering="False" DataField="PostRevisionDate" HeaderText="Post/Revision Date"
                                                UniqueName="PostRevisionDate" SortExpression="PostRevisionDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPostRevisionDate" runat="server" Text='<%# Eval("PostRevisionDate") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadDatePicker ID="dpPostRevisionDate" runat="server" DbSelectedDate='<%# Bind("PostRevisionDate") %>' />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="dpPostRevisionDate"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
<%--                                            <telerik:GridTemplateColumn AllowFiltering="False" DataField="CourseDuration" HeaderText="Duration"
                                                UniqueName="CourseDuration" SortExpression="CourseDuration">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCourseDuration" runat="server" Text='<%# Eval("CourseDuration") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadNumericTextBox ID="txtCourseDuration" runat="server" EmptyMessage="- duration -"
                                                        Text='<%# Bind("CourseDuration") %>' Width="100" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCourseDuration"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>--%>
                                            <telerik:GridTemplateColumn AllowFiltering="False" DataField="VersionNumber" HeaderText="Version No"
                                                UniqueName="VersionNumber" SortExpression="VersionNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVersionNumber" runat="server" Text='<%# Eval("VersionNumber") %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadNumericTextBox ID="txtVersionNumber" runat="server" EmptyMessage="- version -"
                                                        Text='<%# Bind("VersionNumber") %>' Width="100" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtVersionNumber"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="True" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"
                                                DataField="OutOfQueue" FilterControlToolTip="Enter 'Yes' or 'No'" FilterControlWidth="50"
                                                HeaderText="Off-Phone Queue" ShowFilterIcon="true" SortExpression="OutOfQueue"
                                                UniqueName="OutOfQueue">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOutOfQueue" runat="server" Text='<%# Convert.ToString(Eval("OutOfQueue")) == "Yes" ? "Yes" : "No" %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadComboBox ID="cbOutOfQueue" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                        EmptyMessage="- select -" HighlightTemplatedItems="True" SelectedValue='<%# Bind("OutOfQueue") %>'
                                                        ShowWhileLoading="False" Width="100">
                                                        <Items>
                                                            <telerik:RadComboBoxItem runat="server" Text="No" Value="No" />
                                                            <telerik:RadComboBoxItem runat="server" Text="Yes" Value="Yes" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cbOutOfQueue"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="True" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"
                                                DataField="HideFromList" FilterControlToolTip="Enter '0' for No, '1' for Yes"
                                                FilterControlWidth="50" HeaderText="Hide" ShowFilterIcon="true" SortExpression="HideFromList"
                                                UniqueName="HideFromList">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblHideFromList" runat="server" Text='<%# Convert.ToString(Eval("HideFromList")) == "1" ? "Yes" : "No" %>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadComboBox ID="cbHideFromList" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                        EmptyMessage="- select -" HighlightTemplatedItems="True" SelectedValue='<%# Bind("HideFromList") %>'
                                                        ShowWhileLoading="False" Width="100">
                                                        <Items>
                                                            <telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
                                                            <telerik:RadComboBoxItem runat="server" Text="Yes" Value="1" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="cbHideFromList"
                                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                        ValidationGroup="adminCourse" />
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings EditFormType="Template">
                                            <FormTemplate>
                                                <table id="Table2" cellspacing="2" cellpadding="1" width="100%" border="0" rules="none"
                                                    style="border-collapse: collapse;">
                                                    <tr class="EditFormHeader">
                                                        <td colspan="2">
                                                            <b>Course Info</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table id="Table3" width="450px" border="0" class="module">
                                                                <tr>
                                                                    <td>
                                                                        CourseID:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEdCourseID" runat="server" Text='<%# Bind("CourseID") %>' Enabled="false">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        CourseName:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEdCourseName" runat="server" Text='<%# Bind("CourseName") %>'>
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Course Path:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEdCoursePath" runat="server" Text='<%# Bind("CoursePath") %>'
                                                                            TabIndex="1" Width="450">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                    <%--<tr>
                                                                    <td>
                                                                        Course Duration:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEdCourseDuration" runat="server" Text='<%# Bind("CourseDuration") %>'
                                                                            TabIndex="2">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>
                                                                        Client:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cbEdClient" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                                            DataTextField="description" DataValueField="ID" DataSourceID="SqlDataSource1"
                                                                            EmptyMessage="- select client -" EnableLoadOnDemand="True" EnableVirtualScrolling="True"
                                                                            Filter="Contains" HighlightTemplatedItems="True" IsCaseSensitive="False" MarkFirstMatch="True"
                                                                            ShowWhileLoading="False" Width="200">
                                                                        </asp:DropDownList>
                                                                            <telerik:RadPanelBar ID="pbEDClient" runat="server" 
                                                                             Font-Names="Helvetica" ForeColor="White" ValidationGroup="view" Width="330px" ExpandMode="MultipleExpandedItems" >
                                                                             <Items>                                            
                                                                                 <telerik:RadPanelItem runat="server"  Text="select client/s"  
                                                                                     ToolTip="Click to Expand/Collapse" Expanded="False">
                                                                                     <ContentTemplate>
                                                                                         <div style="padding: 5px">
                                                                        <telerik:RadListBox ID="rlbedClient" runat="server" CheckBoxes="True" DataSourceID="SqlDataSource3"
                                                                            DataTextField="description" DataValueField="ID" Height="115" SelectionMode="Multiple"
                                                                            Width="300" ShowCheckAll="true" Visible="false">
                                                                        </telerik:RadListBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="rlbedClient"
                                                                            CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                                            ValidationGroup="adminCourse" Enabled="false" />
                                                                                 </div>
                                                                                     </ContentTemplate>
                                                                                 </telerik:RadPanelItem>
                                                                            </Items>
                                                                         </telerik:RadPanelBar>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="cbEdClient"
                                                                            CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                                            ValidationGroup="adminCourse" />
                                                                        <telerik:RadComboBox ID="cbEdClient" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                                            DataTextField="description" DataValueField="ID" DatasourceId = "SqlDataSource1" EmptyMessage="- select client -"
                                                                            EnableLoadOnDemand="True" EnableVirtualScrolling="True" Filter="Contains" HighlightTemplatedItems="True"
                                                                            IsCaseSensitive="False" MarkFirstMatch="True" 
                                                                            ShowWhileLoading="False" Width="200" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cbEdClient"
                                                                            CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                                            ValidationGroup="adminCourse" />
                                                                        OnItemsRequested="cbClient_ItemsRequested"
                                                                    </td>
                                                                </tr>--%>
                                                                <tr>
                                                                    <td>
                                                                        Version #:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEdversionNumber" runat="server" Text='<%# Bind("versionNumber") %>'
                                                                            TabIndex="2">
                                                                        </asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtEdversionNumber"
                                                                            CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                                            ValidationGroup="adminCourse" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Off Phone Queue:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cbEdOutOfQueue" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                                            EmptyMessage="- select -" HighlightTemplatedItems="True" SelectedValue='<%# Bind("OutOfQueue") %>'
                                                                            ShowWhileLoading="False" Width="100">
                                                                            <Items>
                                                                                <asp:ListItem runat="server" Text="No" Value="No" />
                                                                                <asp:ListItem runat="server" Text="Yes" Value="Yes" />
                                                                            </Items>
                                                                        </asp:DropDownList>
                                                                        <%--<telerik:RadComboBox ID="cbEdOutOfQueue" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                                            EmptyMessage="- select -" HighlightTemplatedItems="True" SelectedValue='<%# Bind("OutOfQueue") %>'
                                                                            ShowWhileLoading="False" Width="100">
                                                                            <Items>
                                                                                <telerik:RadComboBoxItem runat="server" Text="No" Value="No" />
                                                                                <telerik:RadComboBoxItem runat="server" Text="Yes" Value="Yes" />
                                                                            </Items>
                                                                        </telerik:RadComboBox>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Hide from List:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cbEdHideFromList" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                                            EmptyMessage="- select -" HighlightTemplatedItems="True" SelectedValue='<%# Bind("HideFromList") %>'
                                                                            ShowWhileLoading="False" Width="100">
                                                                            <Items>
                                                                                <asp:ListItem runat="server" Text="No" Value="0" />
                                                                                <asp:ListItem runat="server" Text="Yes" Value="1" />
                                                                            </Items>
                                                                        </asp:DropDownList>
                                                                        <%--<telerik:RadComboBox ID="cbEdHideFromList" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                                                                EmptyMessage="- select -" HighlightTemplatedItems="True" SelectedValue='<%# Bind("HideFromList") %>'
                                                                                ShowWhileLoading="False" Width="100">
                                                                                <Items>
                                                                                    <telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
                                                                                    <telerik:RadComboBoxItem runat="server" Text="Yes" Value="1" />
                                                                                </Items>
                                                                            </telerik:RadComboBox>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadButton ID="btnAdCourseUpdate" runat="server" CommandName="Update" Text="Update">
                                                                        </telerik:RadButton>
                                                                        <%--<button id="btnAdCourseUpdate">Update</button>--%>
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadButton ID="btnCancelCUpdate" runat="server" CommandName="Cancel" Text="Cancel">
                                                                        </telerik:RadButton>
                                                                        <%--<button id="btnAdCourseUpdate">Update</button>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                            </FormTemplate>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </div>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="pvAddCourse" runat="server">
                            <div>
                                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" ></asp:Label>
                                <br>
                                <br>
                                <div style="padding-bottom: 10px">
                                    <label style="padding-right: 15px">
                                        Course Name</label>
                                    <telerik:RadTextBox ID="txtAddCourseName" runat="server" EmptyMessage="- course name -"
                                        Width="400" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAddCourseName"
                                        CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                        ValidationGroup="addAdminCourse" />
                                </div>
                                <div style="padding-bottom: 10px">
                                    <label style="padding-right: 22px">
                                        Course Path</label>
                                    <asp:Label ID="lbllink" runat="server" Text="Label">http://university.transcom.com/</asp:Label>
                                    <%--http://10.252.94.6/--%>
                                    <telerik:RadTextBox ID="txtAddCoursePath" runat="server" EmptyMessage="- course path -"
                                        Width="400" />
                                </div>
                                <%--<div style="float: left">
                                        <label style="padding-right: 57px">
                                            Clients</label>
                                    </div>
                                    <div style="float: left; width: 330px">
                                        <telerik:RadPanelBar ID="pbClient" runat="server" Font-Names="Helvetica" ForeColor="White"
                                            ValidationGroup="view" Width="330px" ExpandMode="MultipleExpandedItems">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Text="select client/s" ToolTip="Click to Expand/Collapse"
                                                    Expanded="False">
                                                    <ContentTemplate>
                                                        <div style="padding: 5px">
                                                            <telerik:RadListBox ID="rlbClient" runat="server" CheckBoxes="True" DataSourceID="SqlDataSource2"
                                                                DataTextField="description" DataValueField="ID" Height="115" SelectionMode="Multiple"
                                                                Width="300" ShowCheckAll="true">
                                                            </telerik:RadListBox>
                                                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
                                                                SelectCommand="SELECT DISTINCT ID, description FROM TranscomUniv_local.dbo.Clients where Inactivedate is null and active = 1 order by [description]" />
                                                        </div>
                            </ContentTemplate> </telerik:RadPanelItem> </Items> </telerik:RadPanelBar> </label>
                            <telerik:RadComboBox ID="cbAddClient" runat="server" ChangeTextOnKeyBoardNavigation="True"
                                DropDownAutoWidth="Enabled" DataTextField="description" DataValueField="ID" EmptyMessage="- select client -"
                                EnableLoadOnDemand="True" EnableVirtualScrolling="True" Filter="Contains" HighlightTemplatedItems="True"
                                IsCaseSensitive="False" MarkFirstMatch="True" MaxHeight="300" OnItemsRequested="cbClient_ItemsRequested"
                                ShowWhileLoading="False" SortCaseSensitive="False" Width="200" Visible="false" />
                            </div>
                            <br />
                            <br />
                            <br />
                            <div style="padding-bottom: 10px">
                                <label style="padding-right: 42px">
                                    Duration</label>
                                <telerik:RadNumericTextBox ID="txtAddCourseDuration" runat="server" EmptyMessage="- duration -"
                                    Width="100" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtAddCourseDuration"
                                    CssClass="displayerror" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                    ValidationGroup="addAdminCourse" />
                            </div>
                            <div style="padding-bottom: 10px">
                                <div style="float: left">
                                    <label style="padding-right: 33px">
                                        Campaigns</label>
                                </div>
                                <div style="float: left; width: 330px">
                                    <telerik:RadPanelBar ID="pbCampaign" runat="server" ExpandMode="MultipleExpandedItems"
                                        Width="330" ForeColor="White" Font-Names="Helvetica" ValidationGroup="view" CausesValidation="True">
                                        <Items>
                                            <telerik:RadPanelItem runat="server" Text="select campaign/s" Expanded="False" ToolTip="Click to Expand/Collapse">
                                                <ContentTemplate>
                                                    <div style="padding: 5px">
                                                        <telerik:RadListBox ID="rlbAddCampaign" runat="server" Height="115" SelectionMode="Multiple"
                                                            Width="300" DataSourceID="Campaign" DataTextField="campaign_desc" DataValueField="ID"
                                                            CheckBoxes="True" />
                                                        <asp:SqlDataSource ID="Campaign" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
                                                            SelectCommand="SELECT ID, campaign_desc FROM [dbo].[refCampaigns] ORDER BY campaign_desc" />
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </div>
                            </div>--%>
                            </div>
                            <div>
                                <asp:LinkButton runat="server" ID="btnAddAdminCourse" Text="Add Course" ValidationGroup="addAdminCourse"
                                    OnClick="btnAddAdminCourse_Click" />
                            </div>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT DISTINCT ID, description FROM TranscomUniv_local.dbo.Clients where Inactivedate is null and active = 1 order by [description]" />
        <%-- SELECT ID, [description] FROM [dbo].[ClientsLocation] where active = 1 ORDER BY description--%>
        <asp:HiddenField runat="server" ID="hidShowAll" Value="" />
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT DISTINCT ID, description FROM TranscomUniv_local.dbo.Clients where Inactivedate is null and active = 1 order by [description]" />
        <%-- SelectCommand="select  id, [description] from [dbo].[ClientsLocation] order by [description]--%>
    </div>
    </form>
</body>
</html>
