﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Text;
using System.IO;using System.Linq;

public partial class Admin_AdminCourses : BasePage
{
    string[] stringExtension = { ".pdf", ".html", ".swf", ".pptx", ".xlxs", ".doc", ".htm", ".mpeg", ".flv", ".swf", ".mp4", ".mov", ".avi", ".wav", ".mp3" };

    protected void Page_Load(object sender, EventArgs e)
    {
        var UserRoleID = Registration.GetRoleForUser(CurrTWWID);
        if (UserRoleID == "Normal User" || UserRoleID == "Super User")
        {
            Response.Redirect("NoAccess.aspx");
        }

        if (IsPostBack) return;
        hidShowAll.Value = "0";

        var CurrLog = HttpContext.Current.Session["CurrentUser"];
        if (CurrLog == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void lnkBtnLogout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        FormsAuthentication.RedirectToLoginPage();
    }

    protected void btnACourseEdit_Click(object sender, EventArgs e)
    {
        //txtEdCourseID.text
        //    txtEdCourseName
        //    txtEdCoursePath
        //        txtEdCourseDuration

    }

    protected void cbClient_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        var cbClient = sender as RadComboBox;

        var db = new UserDBDataContext();
        var ds = (from a in db.ClientsLocations where a.description.ToUpper() == "TRANSCOM" select a);

        cbClient.Items.Clear();

        foreach (var item in ds)
        {
            cbClient.Items.Add(new RadComboBoxItem { Text = item.description, Value = item.id.ToString() });
        }

        var ds2 = (from a in db.ClientsLocations where a.description.ToUpper() != "TRANSCOM" orderby a.description select a).OrderBy(p => p.description).ToList();

        foreach (var item2 in ds2)
        {
            cbClient.Items.Add(new RadComboBoxItem { Text = item2.description, Value = item2.id.ToString() });
        }

        //if (cbClient == null) return;
        //cbClient.DataSource = ds;
        //cbClient.DataBind();
    }

    protected void gridAdminCourses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        BindGrid(Convert.ToInt32(hidShowAll.Value));
    }

    private void BindGrid(int showAll)
    {
        var result = AdminDataHelper.GetAdminCourseResult(0, showAll);
        gridAdminCourses.DataSource = result;
    }

    protected void gridAdminCourses_UpdateCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (e.Item is GridEditFormItem)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                int id = Convert.ToInt32(item.GetDataKeyValue("CourseID"));
                if (id != 0)
                {
                    TextBox txtTempCName = (TextBox)item.FindControl("txtEdCourseName");
                    TextBox txtTempCPath = (TextBox)item.FindControl("txtEdCoursePath");
                    //TextBox txtTempCDur = (TextBox)item.FindControl("txtEdCourseDuration");
                    TextBox txtTempVerNum = (TextBox)item.FindControl("txtEdversionNumber");
                    DropDownList rdOutofQ = (DropDownList)item.FindControl("cbEdOutOfQueue");
                    //DropDownList rdClient = (DropDownList)item.FindControl("cbEdClient");
                    //RadListBox rlbedClient = (RadListBox)item.FindControl("rlbedClient");
                    DropDownList rdHideList = (DropDownList)item.FindControl("cbEdHideFromList");

                    //var clients = "";
                    //var clientcollection = rlbedClient.CheckedItems;
                    //if (clientcollection.Count > 0)
                    //{
                    //      if (clientcollection.Count > 1)
                    //        {
                    //            clients = clientcollection.Aggregate(clients, (current, parent) => current + (parent.Value + ","));
                    //            clients = clients.Substring(0, clients.Length - 1);
                    //        }
                    //      if (clientcollection.Count == 1)
                    //      {
                    //          clients = clientcollection.Aggregate(clients, (current, parent) => current + (parent.Value + ","));
                    //      }
                    //}
                   
                    var queue = rdOutofQ.SelectedValue == "Yes" ? 1 : 0;
                    //var TClient = rdClient.SelectedValue == "" ? "339" : "339";
                    int resInt = AdminDataHelper.SaveCourse(txtTempCName.Text, txtTempCPath.Text, 0, txtTempVerNum.Text, DateTime.Now.ToShortDateString(), Convert.ToByte(queue), id, 0, Convert.ToByte(rdHideList.SelectedValue), "0"); //int.Parse(TClient), Convert.ToByte(rdHideList.SelectedValue), "0");
                    //int resInt = AdminDataHelper.SaveCourse_client(txtTempCName.Text, txtTempCPath.Text, int.Parse(txtTempCDur.Text), txtTempVerNum.Text, DateTime.Now.ToShortDateString(), Convert.ToByte(queue), id, Convert.ToByte(rdHideList.SelectedValue), "0" , clients);  
                    //CategoriesEntity c = new CategoriesEntity(id);
                    //c.CategoryName = txtCategoryName.Text;
                    //c.Description = txtDescription.Text;
                    //c.Save();

                    gridAdminCourses.Rebind();
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", resInt != 0
                                                                                     ? "alert('The Course has been updated.');"
                                                                                     : "alert('Course update failed.');",
                                                    true);

                }
            }
        }
    }

    protected void gridAdminCourses_CancelCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.CancelCommandName)
        {

            gridAdminCourses.MasterTableView.ClearEditItems();
            gridAdminCourses.Rebind();

        }
    }

    protected void gridAdminCourses_ItemCommand(object sender, GridCommandEventArgs e)
    {
        //RadGrid.UpdateCommandName
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editItem = e.Item as GridEditableItem;

            var courseID = editItem.GetDataKeyValue("CourseID").ToString();

            var txtCourseName = (RadTextBox)editItem.FindControl("txtCourseName");
            var txtCoursePath = (RadTextBox)editItem.FindControl("txtCoursePath");
            var cbClient = (RadComboBox)editItem.FindControl("cbClient");
            var rlClient = (RadListBox)editItem.FindControl("rlbClient");
            var dpPostRevisionDate = (RadDatePicker)editItem.FindControl("dpPostRevisionDate");
            var txtCourseDuration = (RadNumericTextBox)editItem.FindControl("txtCourseDuration");
            var txtVersionNumber = (RadNumericTextBox)editItem.FindControl("txtVersionNumber");
            var cbOutOfQueue = (RadComboBox)editItem.FindControl("cbOutOfQueue");
            var cbHideFromList = (RadComboBox)editItem.FindControl("cbHideFromList");
            var queue = cbOutOfQueue.SelectedValue == "Yes" ? 1 : 0;

            var coursePath = !string.IsNullOrEmpty(txtCoursePath.Text) ? txtCoursePath.Text : "n/a";

            var retVal = AdminDataHelper.SaveCourse(txtCourseName.Text, coursePath,
                                                      Convert.ToInt32(txtCourseDuration.Text),
                                                      txtVersionNumber.Text, dpPostRevisionDate.SelectedDate.ToString(), (byte)queue,
                                                      Convert.ToInt32(courseID), Convert.ToInt32(cbClient.SelectedValue), Convert.ToByte(cbHideFromList.SelectedValue),
                // Convert.ToInt32(courseID), Convert.ToInt32(rlClient.SelectedValue), Convert.ToByte(cbHideFromList.SelectedValue),
                                                     "");


            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", retVal != 0
                                                                                     ? "alert('The Course has been updated.');"
                                                                                     : "alert('Course update failed.');",
                                                    true);
            gridAdminCourses.Rebind();
        }
        else if (e.CommandName == "Edit")
        {
            //var editItem = e.Item as GridItem;
            GridDataItem editItem = (GridDataItem)e.Item;
            var courseID = editItem["CourseID"].Text;
            //var cn1 = (label) editItem.FindControl("lblCourseName");
            //txtAddCourseName.Text = (Label)editItem.FindControl("lblCourseName");
            //txtAddCoursePath.Text = editItem["CoursePath"].Text;
            //cbAddClient.SelectedValue = editItem["ClientID"].Text;
            //txtAddCourseDuration.Text = editItem["CourseDuration"].Text;

            //txtAddCourseName.Text = TAddCourseName.Text;
            //txtAddCoursePath.Text = TAddCoursePath.Text;
            //cbAddClient.SelectedValue = TAddClient.SelectedValue;
            //txtAddCourseDuration.Text = TAddCourseDuration.Text;
            //RadTab tab1 = tsAdminCourse.Tabs.FindTabByValue("2");
            ////tab1.Selected = true;
            ////RadTab tab1 = tsCategory.Tabs.FindTabByText("Category List");
            //tab1.Selected = true;
            //RadMultiPage RM = (RadMultiPage)mpAdminCourse;
            //RM.SelectedIndex = 2;
            //pvAddCourse.Selected = true;
            //pvAddCourse.Visible = true;
            //pvCourseList.Selected = false;
            //pvAddCourse.Selected = true;
            //var item = (GridEditableItem)e.Item;
            //var cbClient = (RadComboBox)item.FindControl("cbClient2");
            //var db = new UserDBDataContext();
            //var ds = (from a in db.ClientsLocations select a);
            //cbClient.DataSource = ds;
            //cbClient.DataBind();

        }
    }

    protected void gridAdminCourses_ItemDataBound(object sender, GridItemEventArgs e)
    {
        //if (e.Item.IsInEditMode)
        //{
        //    var item = (GridEditableItem)e.Item;

        //    if (!(e.Item is IGridInsertItem))
        //    {
        //        var clientID = item.GetDataKeyValue("ClientID").ToString();
        //        var cbClient = (RadComboBox)item.FindControl("cbClient");

        //        var db = new UserDBDataContext();
        //        var ds = (from a in db.ClientsLocations select a);
        //        cbClient.DataSource = ds;
        //        cbClient.DataBind();
        //        cbClient.SelectedValue = clientID;
        //    }
        //}
    }

    protected void btnShowAllHidden_Click(object sender, EventArgs e)
    {
        hidShowAll.Value = "1";
        BindGrid(Convert.ToInt32(hidShowAll.Value));
        gridAdminCourses.Rebind();
    }

    protected void btnAddAdminCourse_Click(object sender, EventArgs e)
    {
        foreach (string hasExtension in stringExtension)
        {
            if (txtAddCoursePath.Text.Contains(hasExtension))
            {
                var courselink = lbllink.Text;
                var path = txtAddCoursePath.Text;
                var coursePath = courselink + path;

                if (txtAddCoursePath.Text.Length == 0)
                {
                    coursePath = "n/a";
                }

                var retVal = AdminDataHelper.SaveCourse_client(txtAddCourseName.Text, coursePath, 0, "", "", 0, 0, 0, "0", "0");

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", retVal != 0
                                                                                         ? "alert('The Course has been added.');"
                                                                                         : "alert('Course insert failed.');",
                                                        true);

                txtAddCourseName.Text = string.Empty;
                txtAddCoursePath.Text = string.Empty;
                lblMessage.Visible = false;
                gridAdminCourses.Rebind();
            }

            else
            {
                lblMessage.Text = "Please specify the correct path, including the Filename and Extension Name.";
            }
        }
    }

}