using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using NuComm.Security.Encryption;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;

public partial class Courses : BasePage
{
    private DbHelper _db;
    private MasterPage _master;
    String connStr = ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString;
    string _Role;
    string _RoleDesc;

    private string _employeeId;

    public static string AccessMode
    {
        get
        {
            var value = HttpContext.Current.Session["AccessModeSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["AccessModeSession"] = value;
        }
    }

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["TwwIdSession"] = value; }
    }

    public static int CourseId
    {
        get
        {
            var value = HttpContext.Current.Session["CourseIdSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["CourseIdSession"] = value; }
    }

    public static string StrCourseIDs
    {
        get
        {
            var val = HttpContext.Current.Session["strCourseIDs"] == null ? string.Empty : HttpContext.Current.Session["strCourseIDs"].ToString();
            return val;
        }
        set { HttpContext.Current.Session["strCourseIDs"] = value; }
    }

    public static bool IsSearching
    {
        get
        {
            var val = HttpContext.Current.Session["IsSearching"] != null && Convert.ToBoolean(HttpContext.Current.Session["IsSearching"]);
            return val;
        }
        set { HttpContext.Current.Session["IsSearching"] = value; }
    }

    public static string CourseLinkTitleToLearner
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToLearnerSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToLearnerSession"] = value; }
    }

    public static string CourseLinkTitleToApprover
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToApproverSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToApproverSession"] = value; }
    }

    public static string CourseDescription
    {
        get
        {
            var value = HttpContext.Current.Session["CourseDescriptionSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseDescriptionSession"] = value; }
    }

    private DataSet GetSubcategory_Courses_Active(int? categoryId, string accessmode2)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetSubcategory_CoursesActive(categoryId, accessmode2);
        return ds;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!IsPostBack)
        {
            GetSessions();
            PopulateComboBoxCategory(cbCategory);
            StrCourseIDs = Request.QueryString["c"] ?? string.Empty;
            btnExport.Visible = false;

            if (StrCourseIDs != string.Empty)
            {
                gridCourse.Rebind();
                StrCourseIDs = string.Empty;
            }

            if (AccessMode == "External")
            {
                RestrictExternalUser();
            }

            GetLang();
            GetUserRole();

            if (_RoleDesc == "Admin" || _RoleDesc == "Course Admin")
            {
                (gridCourse.MasterTableView.GetColumn("PrereqProgram") as GridBoundColumn).Display = true;
            }
            else
            {
                (gridCourse.MasterTableView.GetColumn("PrereqProgram") as GridBoundColumn).Display = false;
            }
        }
    }

    protected void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = @"
                SELECT R.Role_Desc AS Role
                FROM TranscomUniv_Local.dbo.refRoles R
                INNER JOIN TranscomUniv_Local.dbo.Users U
                    ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";

            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = CurrTWWID;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _RoleDesc = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    private void GetGlobals()
    {
        if (!string.IsNullOrEmpty(Context.User.Identity.Name))
        {
            TwwId = Context.User.Identity.Name;
            CourseLinkTitleToLearner = "";
            CourseLinkTitleToApprover = "";
            CourseDescription = "";
            AccessMode = "";

            if (Page.User.Identity.Name.Contains("@"))
            {
                AccessMode = "External";
            }
            else
            {
                AccessMode = System.Configuration.ConfigurationManager.AppSettings["AccessMode"] == "Admin" ? "Admin" : "Internal";
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    private void GetSessions()
    { 
        try
        {
            var lv = (MultiView)Master.FindControl("mvMenu");
            string CurrIndx = CurrVwIndx.ToString();
            lv.ActiveViewIndex = Int32.Parse(CurrIndx);
        }
        catch
        {
            var lv = (MultiView)Master.FindControl("mvMenu");
            int CurrIndx = lv.ActiveViewIndex;
            lv.ActiveViewIndex = CurrIndx;
        }

        try
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            lblWel.Text = CurrTWWID.ToString();
            _employeeId = lblWel.Text;
        }
        catch
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            // lblWel.Text = CurrTWWID.ToString();
            _employeeId = lblWel.Text;
        }

        //CurrLogName = lblLOGNAME.Text;
        try
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
            lblLNAME.Text = CurrLogName.ToString();
            //_employeeId = lblLNAME.Text;
        }
        catch
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
            lblLNAME.Text = lblLNAME.Text;
            //_employeeId = lblWel.Text;
        }
    }

    private void GetLang()
    {
        try
        {
            var masterTableView = gridCourse.MasterTableView;
            var column = masterTableView.GetColumn("Title");
            column.HeaderText = Resources.LocalizedResource.rgcol_Title.ToString();
            var column2 = masterTableView.GetColumn("Description");
            column2.HeaderText = Resources.LocalizedResource.rgcol_Desc.ToString();
            var column3 = masterTableView.GetColumn("Duration");
            column3.HeaderText = Resources.LocalizedResource.rgcol_Duration.ToString();
            var column4 = masterTableView.GetColumn("TargetAudience");
            column4.HeaderText = Resources.LocalizedResource.rgcol_TargetAud.ToString();
            var column5 = masterTableView.GetColumn("LastUpdated");
            column5.HeaderText = Resources.LocalizedResource.rgcol_LastUp.ToString();
            var column6 = masterTableView.GetColumn("PrereqProgram");
            column6.HeaderText = Resources.LocalizedResource.ReqProg.ToString();
            var column7 = masterTableView.GetColumn("Subcategory");
            column7.HeaderText = Resources.LocalizedResource.rgcol_CoursePath.ToString();

            masterTableView.Rebind();
            GridHeaderItem headerItem = gridCourse.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            gridCourse.Columns[0].HeaderText = Resources.LocalizedResource.rgcol_Title.ToString();
            headerItem["Title"].Text = Resources.LocalizedResource.rgcol_Title.ToString();
            headerItem["Description"].Text = Resources.LocalizedResource.rgcol_Desc.ToString();
            headerItem["Duration"].Text = Resources.LocalizedResource.rgcol_Duration.ToString();
            headerItem["TargetAudience"].Text = Resources.LocalizedResource.rgcol_TargetAud.ToString();
            headerItem["LastUpdated"].Text = Resources.LocalizedResource.rgcol_LastUp.ToString();
            headerItem["PrereqProgram"].Text = Resources.LocalizedResource.ReqProg.ToString();
            headerItem["Subcategory"].Text = Resources.LocalizedResource.rgcol_CoursePath.ToString();

            cbCategory.EmptyMessage = Resources.LocalizedResource.SelectCategory.ToString();
            ddtSubcategory.DefaultMessage = Resources.LocalizedResource.SelectSubcategory.ToString();
            Label1.Text = Resources.LocalizedResource.TodayCC.ToString();
            txtSearch.EmptyMessage = Resources.LocalizedResource.SearchCourses.ToString();
            gridCourse.MasterTableView.NoMasterRecordsText = Resources.LocalizedResource.NoMasterRecordsText.ToString();
        }
        catch (Exception e)
        {

        }
    }

    protected void PopulateComboBoxCategory(RadComboBox combo)
    {
        combo.Text = "";
        combo.Items.Clear();

        var ds = DataHelper.GetCategories(AccessMode);
        combo.DataSource = ds;
        combo.DataBind();
    }

    protected void PopulateComboBoxSubCategory(RadComboBox combo, string selectedVal)
    {
        combo.Text = "";
        combo.Items.Clear();

        var categoryId = !string.IsNullOrEmpty(selectedVal) ? Convert.ToInt32(selectedVal) : (int?)null;

        var ds = DataHelper.GetSubCategoriesByCategoryId(categoryId, AccessMode);
        combo.DataSource = ds;
        combo.DataBind();
    }

    public void PopulateDropdownTreeSubcategories(RadComboBox cbCategory1, RadDropDownTree ddtSubcategory1)
    {
        String AccMode = GetAccessMode();
        var categoryId = !string.IsNullOrEmpty(cbCategory1.SelectedValue)
                             ? Convert.ToInt32(cbCategory1.SelectedValue)
                             : (int?)null;
        ddtSubcategory.DataSource = GetSubcategory_Courses_Active(categoryId, AccMode);
        ddtSubcategory.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsSearching = true;
        gridCourse.Rebind();

        cbCategory.Text = "";
        cbCategory.ClearSelection();
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
    }

    protected void CbCategorySelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
    }

    protected void ddtSubcategory_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        IsSearching = false;
        gridCourse.Rebind();

        txtSearch.Text = "";
    }

    protected void GridCourseNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == GridRebindReason.ExplicitRebind)
        {
            if (IsSearching)
            {
                var val = Server.HtmlEncode(Microsoft.Security.Application.AntiXss.HtmlEncode(txtSearch.Text.Trim()));

                if (!string.IsNullOrEmpty(val))
                {
                    var ds = DataHelper.DoSearch(val, AccessMode);
                    gridCourse.DataSource = ds;
                    gridCourse.MasterTableView.GetColumn("Subcategory").Display = true;

                    btnExport.Visible = ds.Any();
                }
            }
            else
            {
                var categoryId = !string.IsNullOrEmpty(cbCategory.SelectedValue)
                         ? Convert.ToInt32(cbCategory.SelectedValue)
                         : (int?)null;
                var subcategoryId = !string.IsNullOrEmpty(ddtSubcategory.SelectedValue)
                                        ? Convert.ToInt32(ddtSubcategory.SelectedValue)
                                        : (int?)null;

                var ds = DataHelper.GetCourses(categoryId, subcategoryId, AccessMode);
                gridCourse.DataSource = ds;
                gridCourse.MasterTableView.GetColumn("Subcategory").Display = false;

                btnExport.Visible = ds.Any();
            }
        }
    }

    protected void gridCourse_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = (GridDataItem)e.Item;

            var courseId = dataItem.GetDataKeyValue("CourseID").ToString();
            var encCourseId = Server.UrlEncode(UTF8.EncryptText(dataItem.GetDataKeyValue("CourseID").ToString()));
            var sTestCategoryId = dataItem.GetDataKeyValue("EncryptedCourseID").ToString();
            var enrolmentRequired = dataItem.GetDataKeyValue("EnrolmentRequired") ?? string.Empty;
            var title = dataItem.GetDataKeyValue("Title").ToString();
            var courseTypeID = dataItem.GetDataKeyValue("CourseTypeID") ?? 1;

            //TrapID = Convert.ToInt32(courseId);
            string urlWithParams;

            if (Convert.ToInt32(courseTypeID) == 1)
            {
                urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId,
                                              sTestCategoryId);
            }
            else
            {
                urlWithParams = string.Format("CourseLaunch.aspx?CourseID={0}", encCourseId);
            }

            HttpContext.Current.Session["ENROLL_REQ"] = enrolmentRequired;

            //Check user role of the current user
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
            {
                connection.Open();

                string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
                SqlCommand cmd = new SqlCommand(sql, connection);

                cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = CurrTWWID;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        _Role = String.Format("{0}", reader["Role"].ToString());
                    }
                }
            }

            var _trapXml = "";
            var trapPreReq = DataHelper.ArePrerequisiteMet(Convert.ToInt32(CurrTWWID), Convert.ToInt32(courseId), _trapXml);
            string _trapMsg = trapPreReq.MSG.ToString();

            if (_trapMsg != "")
            {
                if (dataItem.GetDataKeyValue("CourseID").ToString() == courseId)
                {
                    dataItem.Display = false;
                }

                if (_Role == "Admin")
                {
                    dataItem.Display = true;
                }
            }
        }
    }

    protected void RestrictExternalUser()
    {
        var list = DataHelper.GetCourseAccess(Page.User.Identity.Name);
        cbCategory.SelectedValue = list[0].CategoryIDAccess.ToString();
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
        ddtSubcategory.SelectedValue = list[0].SubCategoryIDAccess.ToString();

        tblDefault2.Visible = false;
        tblExternal.Visible = true;

        lblCourse.Text = cbCategory.Text + " > " + ddtSubcategory.SelectedText;
    }

    protected void btnEnrolCourse_Click(object sender, EventArgs e)
    {
        GetSessions();
        var EnCourseID = HttpContext.Current.Session["EnrolCourseID"];

        var enrolmentStatus = DataHelper.ChkUserEnrolmentStatusToCourse(Convert.ToInt32(EnCourseID), Convert.ToInt32(_employeeId));
        _master = Master;

        switch (enrolmentStatus)
        {
            //0 = New, 3 = Denied, 4 = Cancelled by user
            case 0:
                var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(_employeeId));

                if (supQry != null)
                {
                    var retVal = DataHelper.InsertCourseEnrolment(Convert.ToInt32(EnCourseID),
                        Convert.ToInt32(_employeeId), Convert.ToInt32(supQry.Cim1));

                    if (retVal)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                            "alert('Your request for enrollment has been sent to your manager for approval.');CloseEnrollCourseWindow();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Request failed. Please try again.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Request failed. Please try again.');", true);
                }

                break;
            case 1: //For Approval
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    "alert('Your enrollment request is still subject for approval.');CloseEnrollCourseWindow();", true);
                break;
            case 2: //Approved
                var url = hidUrlWithParams.Value;
                var s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=yes');CloseEnrollCourseWindow();";
                ClientScript.RegisterStartupScript(GetType(), "script", s, true);

                break;
            case 3:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    "alert('Your request for enrollment has been denied by your manager.');CloseEnrollCourseWindow();", true);

                break;
            case 4: 
                //var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(_employeeId));

                //if (supQry != null)
                //{
                //    var retVal = DataHelper.InsertCourseEnrolment(Convert.ToInt32(EnCourseID),
                //        Convert.ToInt32(_employeeId),Convert.ToInt32(supQry.Cim1));

                //    if (retVal)
                //    {
                //        //const string msgToLearner = "Your course enrollment has been received and is going through approval process.";
                //        //const string msgToApprover = "Course registration for your approval.";
                //        //const string alertMsg = "Your request for enrollment has been sent to your manager for approval.";
                //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //            "alert('Your request for enrollment has been sent to your manager for approval.');CloseEnrollCourseWindow();", true);
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Request failed. Please try again.');", true);
                //    }
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Request failed. Please try again.');", true);
                //}

                //break;
            default:
                break;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        var categoryId = !string.IsNullOrEmpty(cbCategory.SelectedValue)
                   ? Convert.ToInt32(cbCategory.SelectedValue)
                   : (int?)null;
        var subcategoryId = !string.IsNullOrEmpty(ddtSubcategory.SelectedValue)
                                ? Convert.ToInt32(ddtSubcategory.SelectedValue)
                                : (int?)null;
        DataTable dsF = RemoveHtmlTags(categoryId, subcategoryId, AccessMode);
        gridCourse.DataSource = dsF;
        gridCourse.DataBind();

        gridCourse.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridCourse.ExportSettings.ExportOnlyData = true;
        gridCourse.ExportSettings.OpenInNewWindow = true;
        gridCourse.ExportSettings.FileName = "CoursesCatalog_" + DateTime.Now;
        gridCourse.MasterTableView.ExportToExcel();

        #region Hide
        //if (dt.Rows.Count > 0)
        //{
        //    dt.Columns.Remove("CategoryID");
        //    dt.Columns.Remove("SubcategoryID");
        //    dt.Columns.Remove("Duration");
        //    dt.Columns.Remove("UpdatedBy");
        //    dt.Columns.Remove("DateTimeUpdated");
        //    dt.Columns.Remove("EncryptedCourseID1");
        //    dt.Columns.Remove("Category");
        //    dt.Columns["Subcategory"].ColumnName = "Course Path";
        //    var sbDocBody = new StringBuilder();

        //    try
        //    {
        //        //sbDocBody.Append("<HTML><HEAD>");
        //        sbDocBody.Append("<style>");
        //        sbDocBody.Append(".Header {  background-color:Navy; color:#ffffff; font-weight:bold;font-family:Verdana; font-size:12px;}");
        //        sbDocBody.Append(".SectionHeader { background-color:#8080aa; color:#ffffff; font-family:Verdana; font-size:12px;font-weight:bold;}");
        //        sbDocBody.Append(".Content { background-color:#ccccff; color:#000000; font-family:Verdana; font-size:12px;text-align:left}");
        //        sbDocBody.Append(".Label { background-color:#ccccee; color:#000000; font-family:Verdana; font-size:12px; text-align:right;}");
        //        sbDocBody.Append("</style>");

        //        sbDocBody.Append("<br><table align=\"center\" cellpadding=1 cellspacing=0 style=\"background-color:#000000;\">");
        //        sbDocBody.Append("<tr><td width=\"500\">");
        //        sbDocBody.Append("<table width=\"100%\" cellpadding=1 cellspacing=2 style=\"background-color:#ffffff;\">");

        //        if (dt.Rows.Count > 0)
        //        {
        //            sbDocBody.Append("<tr><td>");
        //            sbDocBody.Append("<table width=\"600\" cellpadding=\"0\" cellspacing=\"2\"><tr><td>");

        //            sbDocBody.Append("<tr><td width=\"25\"> </td></tr>");
        //            sbDocBody.Append("<tr>");
        //            sbDocBody.Append("<td> </td>");
        //            for (var i = 0; i < dt.Columns.Count; i++)
        //            {
        //                sbDocBody.Append("<td class=\"Header\" width=\"120\">" + dt.Columns[i].ToString().Replace(".", "<br>") + "</td>");
        //            }
        //            sbDocBody.Append("</tr>");

        //            for (var i = 0; i < dt.Rows.Count; i++)
        //            {
        //                sbDocBody.Append("<tr>");
        //                sbDocBody.Append("<td> </td>");
        //                for (var j = 0; j < dt.Columns.Count; j++)
        //                {
        //                    sbDocBody.Append("<td class=\"Content\">" + dt.Rows[i][j] + "</td>");
        //                }
        //                sbDocBody.Append("</tr>");
        //            }
        //            sbDocBody.Append("</table>");
        //            sbDocBody.Append("</td></tr></table>");
        //            sbDocBody.Append("</td></tr></table>");
        //            //sbDocBody.Append("</BODY></HTML>");
        //        }

        //        //HttpContext.Current.Response.Clear();
        //        //HttpContext.Current.Response.Buffer = true;

        //        //HttpContext.Current.Response.AppendHeader("Content-Type", "application/ms-excel");
        //        //HttpContext.Current.Response.AppendHeader("Content-disposition", "attachment; filename=Courses.xls");
        //        //HttpContext.Current.Response.Write(sbDocBody.ToString());
        //        //HttpContext.Current.Response.Flush();

        //        Response.Clear();
        //        Response.Buffer = true;

        //        Response.AppendHeader("Content-Type", "application/ms-excel");
        //        Response.AppendHeader("Content-disposition", "attachment; filename=Courses.xls");
        //        Response.Write(sbDocBody.ToString());
        //        Response.Output.Write(sbDocBody.ToString());
        //        Response.Flush();
        //        Response.Close();
        //        Response.End();

        //        //Response.ContentType = "application/force-download";
        //        //Response.AddHeader("content-disposition", "attachment; filename=Courses.xls");
        //        //Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        //        //Response.Write("<head>");
        //        //Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        //        //Response.Write("<!--[if gte mso 9]><xml>");
        //        //Response.Write("<x:ExcelWorkbook>");
        //        //Response.Write("<x:ExcelWorksheets>");
        //        //Response.Write("<x:ExcelWorksheet>");
        //        //Response.Write("<x:Name>Report Data</x:Name>");
        //        //Response.Write("<x:WorksheetOptions>");
        //        //Response.Write("<x:Print>");
        //        //Response.Write("<x:ValidPrinterInfo/>");
        //        //Response.Write("</x:Print>");
        //        //Response.Write("</x:WorksheetOptions>");
        //        //Response.Write("</x:ExcelWorksheet>");
        //        //Response.Write("</x:ExcelWorksheets>");
        //        //Response.Write("</x:ExcelWorkbook>");
        //        //Response.Write("</xml>");
        //        //Response.Write("<![endif]--> ");
        //        //Response.Write(sbDocBody.ToString()); // give ur html string here
        //        //Response.Write("</head>");
        //        //Response.Flush();
        //    }
        //    catch (Exception ex)
        //    {
        //        string Err = ex.ToString() + " " + ex.InnerException.ToString();
        //    }
        //}
        #endregion

    }

    protected DataTable RemoveHtmlTags(int? categoryId, int? subCategoryId, string accessMode)
    {
        DataSet ds = new DataSet();
        try
        {
#region hide
            SqlConnection con;
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Intranet"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("pr_TranscomUniversity_Lkp_Course", con);
            cmd.Parameters.Add("@CategoryID", SqlDbType.Int, 10).Value = categoryId;
            cmd.Parameters.Add("@SubcategoryID", SqlDbType.Int, 10).Value = subCategoryId;
            cmd.Parameters.Add("@AccessMode", SqlDbType.VarChar, 100).Value = accessMode;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            con.Close();
            foreach (DataTable table in ds.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    dr["Title"] = dr["Title"].ToString();
                    string HtmlToData = dr["Description"].ToString();
                    string noHTML = Regex.Replace(HtmlToData, @"<[^>]+>|&nbsp;", "").Trim();
                    string noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");
                    dr["Description"] = noHTMLNormalised;

                }
            }

#endregion
        }
        catch (Exception ex)
        {

        }

        return ds.Tables[0];
    }

    protected void linkCourseTitle_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;
        var title = item.GetDataKeyValue("Title").ToString();
        var desc = item.GetDataKeyValue("Description").ToString();
        string EnrolReq;

        if (item.GetDataKeyValue("EnrolmentRequired").ToString() == "False")
        {
            EnrolReq = "False";
        }
        else
        {
            EnrolReq = "True";
        }

        string url_to_view;

        const string url1 = "~/CourseRedirect.aspx";
        var titleLink1 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url1, title);

        const string url2 = "http://transcomuniversity.com/MyPlan.aspx";
        var titleLink2 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url2, title);

        var sCourseID = Server.UrlEncode(NuComm.Security.Encryption.UTF8.EncryptText(item.GetDataKeyValue("CourseID").ToString()));
        var sCourseID_1 = item.GetDataKeyValue("CourseID").ToString();
        var sTestCategoryID = item.GetDataKeyValue("EncryptedCourseID").ToString();
        HttpContext.Current.Session["EnrolCourseID"] = sCourseID_1;

        GetSessions();

        string Course_Path = GetCourseURL(sCourseID_1, sTestCategoryID, _employeeId);
        var courseTypeID = item.GetDataKeyValue("CourseTypeID") ?? 1;

        //var status = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(CurrTWWID));

        var _xml = "<DocumentElement>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>1073</PrereqCourseID>\r\n    <EncryptedCourseID />\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>984</PrereqCourseID>\r\n    <EncryptedCourseID>1382</EncryptedCourseID>\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>987</PrereqCourseID>\r\n    <EncryptedCourseID>1385</EncryptedCourseID>\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>979</PrereqCourseID>\r\n    <EncryptedCourseID>1377</EncryptedCourseID>\r\n  </encCourseIDs>\r\n  <encCourseIDs>\r\n    <PrereqCourseID>980</PrereqCourseID>\r\n    <EncryptedCourseID>1378</EncryptedCourseID>\r\n  </encCourseIDs>\r\n</DocumentElement>";
        var preReq = DataHelper.ArePrerequisiteMet(Convert.ToInt32(CurrTWWID), Convert.ToInt32(sCourseID_1), _xml);
        var statusChecker = DataHelper.StatusChecker(Convert.ToInt32(preReq.ReqCourseID), Convert.ToInt32(CurrTWWID));

        string _msg = preReq.MSG.ToString();
        string _reqCourseID = "";

        if (statusChecker != null)
            _reqCourseID = statusChecker.Status.ToString();

        if (_msg != "" && _reqCourseID != "Completed")
        {
            string script = string.Format(@"ShowNote('<span>Thank you for your interest in taking this course. The following requirement/s must be fulfilled:</span><br/><br/><br/><span>{0}</span>')", _msg);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

            //string script = "function f(){$find(\"" + windowPrereq.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //(windowPrereq.ContentContainer.FindControl("lblPrerequisiteMessage") as Label).Text = _msg;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
        }
        else
        {
            if (EnrolReq == "False")
            {
                //Non SCORM
                if (Convert.ToInt32(courseTypeID) == 1)
                {
                    url_to_view = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", sCourseID, sTestCategoryID);
                    Int32 CID_ = Convert.ToInt32(sCourseID_1);
                    string CourseEnddate = DataHelper.GetCourseEndDate(CID_);
                    HttpContext.Current.Session["EnrolCourseID_EndDate"] = CourseEnddate;
                    HttpContext.Current.Session["EnrolCourse_Path"] = Course_Path;
                    HttpContext.Current.Session["EnrolUrl_to_view"] = url_to_view;
                    HttpContext.Current.Session["EnrolCourseType"] = Convert.ToInt32(courseTypeID);
                    rmPopup.RadConfirm("Are you sure you want to take this course?", "confirmCallBackFn", 350, 180, null, "Launch Course");
                }
                else
                {
                    //SCORM
                    url_to_view = string.Format("CourseLaunch.aspx?CourseID={0}", sCourseID);
                    rmPopup.RadConfirm("Are you sure you want to take this course?", "confirmCallBackFn", 350, 180, null, "Launch Course");
                    Int32 CID_ = Convert.ToInt32(sCourseID_1);
                    string CourseEnddate = DataHelper.GetCourseEndDate(CID_);
                    HttpContext.Current.Session["EnrolCourseID_EndDate"] = CourseEnddate;
                    HttpContext.Current.Session["EnrolCourse_Path"] = Course_Path;
                    HttpContext.Current.Session["EnrolUrl_to_view"] = url_to_view;
                    HttpContext.Current.Session["EnrolCourseType"] = Convert.ToInt32(courseTypeID);
                }
            }
            else
            {
                var status = DataHelper.GetEnrollmentApprovalStatus(Convert.ToInt32(CurrTWWID), Convert.ToInt32( item.GetDataKeyValue("CourseID")));

                //Approved Enrollment
                if (status == 2)
                {
                    //Non SCORM
                    if (Convert.ToInt32(courseTypeID) == 1)
                    {
                        url_to_view = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", sCourseID, sTestCategoryID);
                        Int32 CID_ = Convert.ToInt32(sCourseID_1);
                        string CourseEnddate = DataHelper.GetCourseEndDate(CID_);
                        HttpContext.Current.Session["EnrolCourseID_EndDate"] = CourseEnddate;
                        HttpContext.Current.Session["EnrolCourse_Path"] = Course_Path;
                        HttpContext.Current.Session["EnrolUrl_to_view"] = url_to_view;
                        HttpContext.Current.Session["EnrolCourseType"] = Convert.ToInt32(courseTypeID);
                        rmPopup.RadConfirm("Are you sure you want to take this course?", "confirmCallBackFn", 350, 180, null, "Launch Course");
                    }
                    else
                    {
                        //SCORM
                        url_to_view = string.Format("CourseLaunch.aspx?CourseID={0}", sCourseID);
                        rmPopup.RadConfirm("Are you sure you want to take this course?", "confirmCallBackFn", 350, 180, null, "Launch Course");
                        Int32 CID_ = Convert.ToInt32(sCourseID_1);
                        string CourseEnddate = DataHelper.GetCourseEndDate(CID_);
                        HttpContext.Current.Session["EnrolCourseID_EndDate"] = CourseEnddate;
                        HttpContext.Current.Session["EnrolCourse_Path"] = Course_Path;
                        HttpContext.Current.Session["EnrolUrl_to_view"] = url_to_view;
                        HttpContext.Current.Session["EnrolCourseType"] = Convert.ToInt32(courseTypeID);
                    }
                }
                else
                {
                    string script = "function f(){$find(\"" + windowEnrollCourse.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
                }
            }
        }
    }

    protected void RadAjaxManager2_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument.ToString() == "ok")
        {
            lblRadConfirm_Res.Text = "OK";
            var AJ_CourseEndDate = HttpContext.Current.Session["EnrolCourseID_EndDate"];
            var AJ_EnrollCourseID = HttpContext.Current.Session["EnrolCourseID"];
            var AJ_CoursePath = HttpContext.Current.Session["EnrolCourse_Path"];
            var AJ_url_to_view = HttpContext.Current.Session["EnrolUrl_to_view"];
            var AJ_Course_Type = HttpContext.Current.Session["EnrolCourseType"];

            if (Convert.ToString(AJ_Course_Type) == "1")
            {
                //SaveToEnrollCourse(Convert.ToInt32(AJ_EnrollCourseID), Convert.ToString(AJ_CourseEndDate));
                Redirect_URL(Convert.ToString(AJ_url_to_view), "_blank", "width=800,height=600,resizable=yes,scrollbars=yes", Convert.ToString(AJ_EnrollCourseID), Convert.ToString(AJ_CoursePath), false, Convert.ToString(AJ_EnrollCourseID));
            }
            else
            {
                GetSessions();
                Int32 CID_ = Convert.ToInt32(AJ_EnrollCourseID);
                bool hasTakenSameScormCourse = DataHelper.CheckUserScormTaken(Convert.ToInt32(_employeeId), CID_);
                if (hasTakenSameScormCourse == false)
                {
                    //SaveToEnrollCourse(Convert.ToInt32(AJ_EnrollCourseID), Convert.ToString(AJ_CourseEndDate));
                }
                Redirect_URL(Convert.ToString(AJ_url_to_view), "_blank", "width=800,height=600,resizable=yes,scrollbars=yes", Convert.ToString(AJ_EnrollCourseID), Convert.ToString(AJ_CoursePath), true, Convert.ToString(AJ_EnrollCourseID) + "|" + _employeeId);
            }
          
        }
        else
        {
            lblRadConfirm_Res.Text = "Cancel";
        }
    }

    private string GetCourseURL(string CourseID, string TestCatID, string twwid)
    {
        string sDecryptedTestCategoryId = null;
        string sCourseId = null;
        var sPath = "";
        try
        {
            GetSessions();
            try
            {
                sDecryptedTestCategoryId = DecryptString(TestCatID);
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "FormatException")
                {
                    string _decrypt = TestCatID.Replace(" ", "+");
                    sDecryptedTestCategoryId = DecryptString(_decrypt);
                }
                else
                {
                    //Response.Redirect("Error.aspx");
                }
            }

            var db = new IntranetDBDataContext();

            //var colTranscomCourse =
            //                new List<tbl_TranscomUniversity_Cor_Course1>(
            //                    db.tbl_TranscomUniversity_Cor_Course1s.Where(p => p.CourseID == Convert.ToInt32(sCourseId))).ToList();


            var colNucommCourse = new List<tbl_NuCommUniversity_Lkp_Course>(db.tbl_NuCommUniversity_Lkp_Courses).Where(
                        p => p.CourseID == Convert.ToInt32(sDecryptedTestCategoryId)).ToList();

            if (colNucommCourse.Count > 0)
            {
                sCourseId = CourseID;
                var colTranscomCourse =
                           new List<tbl_TranscomUniversity_Cor_Course>(
                               db.tbl_TranscomUniversity_Cor_Courses.Where(p => p.CourseID == Convert.ToInt32(sCourseId))).ToList();
                if (colTranscomCourse.Count > 0)
                {

                    foreach (var item in colNucommCourse)
                    {
                        sPath = item.CoursePath;
                    }
                    var user = Registration.GetUser(_employeeId).SingleOrDefault();
                    if (user != null)
                    {
                        //db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(user.Twwid), Convert.ToInt32(sDecryptedTestCategoryID));
                        db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(_employeeId), Convert.ToInt32(sDecryptedTestCategoryId));
                        db.SubmitChanges();
                    }
                    else
                    {
                        // Response.Redirect("~/Login.aspx");
                    }
                }
            }
            else
            {
                GetSessions();
                var user = Registration.GetUser(_employeeId).SingleOrDefault();
                sCourseId = CourseID;
                if (user != null)
                {
                    //db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(user.Twwid), Convert.ToInt32(sDecryptedTestCategoryID));
                    db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(_employeeId), Convert.ToInt32(sDecryptedTestCategoryId));
                    db.SubmitChanges();
                }
            }

            //Goto course page
            //Response.Redirect(sPath, true);
        }
        catch (Exception)
        {
            // Response.Redirect("Error.aspx");
        }

       // sPath = "C:/Users/Percival.ponte/Documents/WORK/SYSTEMS/fileuploader/courses/SMG/Smart_Goal_Settings_.htm";
        // END === disabled this line of codes === END//
        return sPath;
    }

    private void Redirect_URL(string url, string target, string windowFeatures, string CourseID, string cpath, bool isSorm, string ENC_ID)
    {
        HttpContext context = HttpContext.Current;


        DataTable dt = null;
        

        if ((String.IsNullOrEmpty(target) ||
            target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
            String.IsNullOrEmpty(windowFeatures))
        {

            context.Response.Redirect(url);
        }
        else
        {
            Page page = (Page)context.Handler;
            if (page == null)
            {
                throw new InvalidOperationException(
                    "Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);

            string script;
            if (!String.IsNullOrEmpty(windowFeatures))
            {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else
            {
                script = @"window.open(""{0}"", ""{1}"");";
            }

            GetSessions();
            string strURL;
            if (isSorm == false)
            {
                strURL = "CourseLauncher.aspx";
            }
            else
            {
                strURL = string.Format("CourseLaunch.aspx?CourseID={0}", ENC_ID);
            }

            //strURL = "CourseLauncher.aspx";

            if (cpath == string.Empty)
            {
                Session["CourseLaunch"] = url;
            }
            else
            {
                Session["CourseLaunch"] = cpath;
            }

            GetSessions();

            Session["USERID"] = _employeeId;
            Session["COURSEID"] = CourseID;
            
            script = String.Format(script, strURL, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page, typeof(Page),
                "Redirect",
                script,
                true);
        }
    }

    private String GetAccessMode()
    { 
        String RetVal =  String.Empty;
        GetSessions();
        String StrSql  = "Select count(*) from users where twwid = " + _employeeId ;
        Int32 IntUser = Convert.ToInt32(GetDataValue(StrSql));
        if (IntUser >= 1 )
        {
            RetVal = "Internal";
        }
        else
        {
            RetVal = "External";
        }

        return RetVal;
    }

    public String GetDataValue(string query)
    {
        //string RetVal = string.Empty;
        String ConnString = connStr;
        SqlConnection conn = new SqlConnection(ConnString);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        var RetVal = cmd.ExecuteScalar();


        return (RetVal == null) ? string.Empty : RetVal.ToString();
    }

    public string DecryptString(string inputString)
    {
        System.IO.MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = new byte[inputString.Length];
            byteInput = Convert.FromBase64String(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateDecryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }

        Encoding encoding1 = Encoding.UTF8;
        return encoding1.GetString(memStream.ToArray());
    }

   private void SaveToEnrollCourse(int courseID,string courseDueDate)
    {
        var msg = string.Empty;
        DataTable DtToAssign = new DataTable();
        DtToAssign.Columns.Add("CourseID", typeof(int));
        DtToAssign.Columns.Add("DueDate", typeof(DateTime));

        GetSessions();

        var dr = DtToAssign.NewRow();

        dr["CourseID"] = courseID;
        dr["DueDate"] = Convert.ToDateTime(courseDueDate);
        DtToAssign.Rows.Add(dr);


        //var retVal = DataHelper.BulkAddAssignCourse(Convert.ToInt32(cbAssignmentType.SelectedValue),
        //                                             Convert.ToInt32(cbAssignmentType.SelectedValue) == 3
        //                                                 ? Convert.ToInt32(TwwId)
        //                                                 : Convert.ToInt32(cbDirectReports.SelectedValue),
        //                                             Convert.ToInt32(TwwId), DtToAssign);

        var retVal = DataHelper.BulkAddAssignCourse(5,Convert.ToInt32(_employeeId),Convert.ToInt32(_employeeId), DtToAssign);
    }
}

public static class ResponseHelper
{
    public static void Redirect(string url, string target, string windowFeatures)
    {
        HttpContext context = HttpContext.Current;

        if ((String.IsNullOrEmpty(target) ||
            target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
            String.IsNullOrEmpty(windowFeatures))
        {

            context.Response.Redirect(url);
        }
        else
        {
            Page page = (Page)context.Handler;
            if (page == null)
            {
                throw new InvalidOperationException(
                    "Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);

            string script;
            if (!String.IsNullOrEmpty(windowFeatures))
            {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else
            {
                script = @"window.open(""{0}"", ""{1}"");";
            }

            script = String.Format(script, url, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page,
                typeof(Page),
                "Redirect",
                script,
                true);
        }
    }
}
    
