﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CourseHistory.aspx.cs" Inherits="CourseHistory"
    MasterPageFile="~/MasterPage.master" %>

<%@ OutputCache Location="None" VaryByParam="None" %>
 <asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            Sys.Application.add_load(function () {
                $('.progressBarContainer').each(function (i, obj) {
                    var hidValue = $('input[type=hidden]', this).val();

                    jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                    var progressBarWidth = hidValue * $('#progressBar').width() / 100;
                    jQuery('[id=progress]', this).animate({ width: progressBarWidth }, 3000).html("&nbsp;");

                    if (hidValue >= 0 && hidValue <= 50) {
                        jQuery('[id=progress]', this).addClass('red');
                    }
                    if (hidValue >= 50 && hidValue <= 75) {
                        jQuery('[id=progress]', this).addClass('yellow');
                    }
                    if (hidValue >= 75 && hidValue <= 100) {
                        jQuery('[id=progress]', this).addClass('green');
                    }
                });
            });

            function RequestFullScreen(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportExcel") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0) {
                    args.set_enableAjax(false);
                }
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollLeftOffset;
                var scrollTopOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }

            function PreviousScores(sender, args) {
                $find("<%=windowPreviousScores.ClientID %>").show();
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridCourseHistory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCourseHistory" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourseName" />
                    <telerik:AjaxUpdatedControl ControlID="gridPreviousScores" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCourseHistory" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbDirectReports">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="windowPreviousScores" runat="server" Title="Previous Scores"
                Width="600px" Height="400px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div style="vertical-align: top">
                        <div style="padding: 4px;" align="center">
                            <asp:Label runat="server" ID="lblCourseName" />
                        </div>
                        <telerik:RadGrid ID="gridPreviousScores" runat="server" AutoGenerateColumns="False"
                            Skin="Metro" CssClass="RadGrid2_detailtable" BorderStyle="None">
                            <MasterTableView EnableNoRecordsTemplate="False" ShowHeadersWhenNoRecords="True"
                                GridLines="None">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="DateStartTaken" HeaderText="Date Taken" UniqueName="DateStartTaken"
                                        DataType="System.DateTime">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score" UniqueName="Score">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" UniqueName="Percentage">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="DateStartTaken" SortOrder="Descending" />
                                </SortExpressions>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnSearch">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div class="insidecontainer">
                        <table width="100%" id="tblAdmin" runat="server" cellpadding="0" cellspacing="0"
                            style="padding-bottom: 5px">
                            <tr>
                                <td id="tdMode" runat="server" style="width: 150px" visible="False">
                                    <asp:RadioButtonList ID="radiolist1" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                        CssClass="radio">
                                        <asp:ListItem Selected="true">Internal</asp:ListItem>
                                        <asp:ListItem>External</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td id="tdMngr" runat="server" align="left" style="width: 154px" visible="False">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                        <telerik:RadComboBox ID="cbDirectReports" runat="server" AutoPostBack="True" CausesValidation="False"
                                            CheckBoxes="true" DataTextField="Name" DataValueField="TwwId" DataSourceID="ds_DirectReports"
                                            DropDownAutoWidth="Enabled" EmptyMessage="- direct reports -" EnableCheckAllItemsCheckBox="true"
                                            MaxHeight="300px" OnSelectedIndexChanged="cbDirectReports_SelectedIndexChanged"
                                            ToolTip="Select Cim # of your direct reports" Width="150px" />
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td id="tdAdminSearch" runat="server" style="width: 154px" visible="False">
                                    <telerik:RadTextBox ID="txtSearch" runat="server" EmptyMessage="- search key -" Width="150px"
                                        SelectionOnFocus="SelectAll" />
                                </td>
                                <td id="tdBtnSearch" runat="server" style="width: 40px" visible="False">
                                    <telerik:RadButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btnSearchImage"
                                        ValidationGroup="go">
                                        <Image EnableImageButton="true" />
                                    </telerik:RadButton>
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportExcel" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Employee/s Transcript to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton></div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid ID="gridCourseHistory" runat="server" OnNeedDataSource="GridCourseHistoryNeedDataSource"
                            GridLines="None" AutoGenerateColumns="False" CssClass="RadGrid1" Skin="Metro"
                            AllowFilteringByColumn="True" AllowSorting="True" OnExcelMLExportRowCreated="GridCourseHistoryExcelMlExportRowCreated"
                            OnGridExporting="GridCourseHistoryGridExporting" OnItemDataBound="gridCourseHistory_ItemDataBound"
                            OnExcelExportCellFormatting="gridCourseHistory_ExcelExportCellFormatting">
                            <ExportSettings ExportOnlyData="true" IgnorePaging="True" UseItemStyles="True">
                                <Excel Format="ExcelML" />
                            </ExportSettings>
                            <FilterItemStyle HorizontalAlign="Center" />
                            <GroupingSettings CaseSensitive="False" />
                            <MasterTableView AutoGenerateColumns="False" AllowSorting="true" DataKeyNames="CourseID,TestCategoryID,CourseName,DateCourseTaken,Passed"
                                NoDetailRecordsText="Transcript is empty." NoMasterRecordsText="Transcript is empty."
                                EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="CIMNumber" HeaderText="CIM" DataField="CIMNumber"
                                        AllowFiltering="False" Display="False">
                                        <HeaderStyle ForeColor="Silver" />
                                        <ItemStyle ForeColor="Gray" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="CourseID" HeaderText="Course ID" DataField="CourseID"
                                        AllowFiltering="False" Display="False" />
                                    <telerik:GridBoundColumn UniqueName="TestCategoryID" HeaderText="Test Category ID"
                                        DataField="TestCategoryID" AllowFiltering="False" Display="False" />
                                    <telerik:GridBoundColumn UniqueName="CourseName" HeaderText="Course Name" DataField="CourseName"
                                        FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                        ShowFilterIcon="False" FilterControlToolTip="Search by Course Name" />
                                    <telerik:GridDateTimeColumn UniqueName="DateCourseTaken" HeaderText="Date Course Taken"
                                        DataField="DateCourseTaken" AllowFiltering="False" DataType="System.DateTime"
                                        DataFormatString="{0:yyyy-MM-dd}" />
                                    <telerik:GridTemplateColumn UniqueName="Progress" HeaderText="Progress" DataField="Progress"
                                        AllowFiltering="false">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle Width="120" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <div class='progressBarContainer'>
                                                <asp:HiddenField ID="hidProgressVal" runat="server" Value="" />
                                                <div id='progressBar'>
                                                    <span id="spanval"></span>
                                                    <div id="progress">
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="ViewDetails"
                                        HeaderText="Previous Scores">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnPreviousScore" runat="server" ToolTip="View Previous Scores"
                                                OnClientClick="PreviousScores()" OnClick="btnPreviousScore_Click" Text="View" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn UniqueName="Score" HeaderText="Highest Score" DataField="Score"
                                        AllowFiltering="False" />
                                    <telerik:GridDateTimeColumn UniqueName="DateTestTaken" HeaderText="Date Test Taken"
                                        DataField="DateTestTaken" AllowFiltering="False" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" />
                                </Columns>
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="DateCourseTaken" SortOrder="Descending" />
                                </SortExpressions>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidCIM" />
    </div>
    <div>
        <asp:SqlDataSource ID="ds_DirectReports" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="pr_TranscomUniversity_Lkp_GetDirectReports" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="hidCIM" DefaultValue="0" Name="CIM" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
