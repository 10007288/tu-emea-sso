﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

public partial class Calendar : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MainPane.ContentUrl = "http://transcomuniversity.com/Transcom/TUCalendar/calendar.htm";
        }
    }
}