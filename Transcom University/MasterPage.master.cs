﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

public partial class MasterPage2 : System.Web.UI.MasterPage
{
    public string LoginName
    {
        get
        {
            var value = HttpContext.Current.Session["LoginNameSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["LoginNameSession"] = value;
        }
    }

    public string AccessMode
    {
        get
        {
            var value = HttpContext.Current.Session["AccessModeSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["AccessModeSession"] = value;
        }
    }

    public string LoginFirstName
    {
        get
        {
            var value = HttpContext.Current.Session["LoginFirstNameSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["LoginFirstNameSession"] = value;
        }
    }

    public int NotifyEventForApprovalCnt
    {
        get
        {
            var value = HttpContext.Current.Session["EventForApprovalSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["EventForApprovalSession"] = value;
        }
    }

    public int NotifyEnrollmentCnt
    {
        get
        {
            var value = HttpContext.Current.Session["NotifyEnrollmentSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["NotifyEnrollmentSession"] = value;
        }
    }

    public int NotifyEnrolApprovedCnt
    {
        get
        {
            var value = HttpContext.Current.Session["NotifyEnrolApprovedSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["NotifyEnrolApprovedSession"] = value;
        }
    }

    public int NotifyEnrollDeniedCnt
    {
        get
        {
            var value = HttpContext.Current.Session["NotifyEnrollDeniedSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["NotifyEnrollDeniedSession"] = value;
        }
    }

    protected void GetGlobals()
    {
        AccessMode = ConfigurationManager.AppSettings["AccessMode"];
        LoginName = Context.User.Identity.Name;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LabelDate.Text = DateTime.Now.ToString("D");
            try
            {
                String CurrIndx = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[3];
                mvMenu.ActiveViewIndex = Int32.Parse(CurrIndx);
                lblLOG_TWWID.Text = HttpContext.Current.User.Identity.Name.Split('|')[0].ToString();
                if (lblLOG_TWWID.Text != "")
                {
                    lblWelcome.Text = "Welcome back,";
                    lblExc.Text = "!";
                }

                lblLOGNAME.Text = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[1];
                String CurrLang = Session["CurrLang"].ToString();
                ddLangSel.SelectedValue = CurrLang;

                HttpContext.Current.Session["TWWID_MS"] = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[0];
            }
            catch
            {

            }
        }
        SetTopMenu();
        GetLang();
        SetNotifications();
    }

    private void GetLang()
    {
        //Learner
        lnkNU_Home.Text = Resources.LocalizedResource.Home.ToString();
        lnkNU_Prof.Text = Resources.LocalizedResource.MyProfile.ToString();
        lnkNU_CHist.Text = Resources.LocalizedResource.MyTranscript.ToString();
        lnkNU_MyPlan.Text = Resources.LocalizedResource.MyLearningPlan.ToString();
        lnkLogOut_User.Text = Resources.LocalizedResource.LogOut.ToString();

        //Team Leader/Super User
        LinkButton4.Text = Resources.LocalizedResource.Home.ToString();
        LinkButton5.Text = Resources.LocalizedResource.MyProfile.ToString();
        LinkButton6.Text = Resources.LocalizedResource.MyTranscript.ToString();
        LinkButton7.Text = Resources.LocalizedResource.MyLearningPlan.ToString();
        LinkButton8.Text = Resources.LocalizedResource.MyTeam.ToString();

        //Admin
        lnkAD_Home.Text = Resources.LocalizedResource.Home.ToString();
        lnkAD_Prof.Text = Resources.LocalizedResource.MyProfile.ToString();
        lnkAD_CHist.Text = Resources.LocalizedResource.MyTranscript.ToString();
        lnkAD_MyPlan.Text = Resources.LocalizedResource.MyLearningPlan.ToString();
        lnkAD_MyTeam.Text = Resources.LocalizedResource.MyTeam.ToString();
        lnkAD_MyReps.Text = Resources.LocalizedResource.MyReports.ToString();
        lnkAD_Admin.Text = Resources.LocalizedResource.Admin.ToString();
        lnkLogOut.Text = Resources.LocalizedResource.LogOut.ToString();

        //CourseAdmin
        LinkButton9.Text = Resources.LocalizedResource.Home.ToString();
        LinkButton10.Text = Resources.LocalizedResource.MyProfile.ToString();
        LinkButton12.Text = Resources.LocalizedResource.MyTranscript.ToString();
        LinkButton13.Text = Resources.LocalizedResource.MyLearningPlan.ToString();
        LinkButton14.Text = Resources.LocalizedResource.MyTeam.ToString();
        LinkButton15.Text = Resources.LocalizedResource.MyReports.ToString();
        LinkButton16.Text = Resources.LocalizedResource.Admin.ToString();
        LinkButton17.Text = Resources.LocalizedResource.LogOut.ToString();

        lblWelcome.Text = Resources.LocalizedResource.WelcomeBack.ToString();
        hlpCourseCatalog.Text = Resources.LocalizedResource.CourseCatalog.ToString();
        hplSupport.Text = Resources.LocalizedResource.Support.ToString();
        hplNuskillCheck.Text = Resources.LocalizedResource.Testing.ToString();
        hplTUReporting.Text = Resources.LocalizedResource.TUReporting.ToString();
        hplTUTestingAdmin.Text = Resources.LocalizedResource.TUTestingAdmin.ToString();
    }

    private void SetTopMenu()
    {
        try
        {
            string RoleID = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2];
            if (RoleID == "Super User")
            {
                hplTUReporting.Visible = false;
                hplTUTestingAdmin.Visible = true;
            }
            else if (RoleID == "Admin")
            {
                hplTUReporting.Visible = false;
                hplTUTestingAdmin.Visible = true;
            }
            else if (RoleID == "Course Admin")
            {
                hplTUReporting.Visible = false;
                hplTUTestingAdmin.Visible = true;
            }
            else
            {
                hplTUReporting.Visible = false;
                hplTUTestingAdmin.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddLangSel_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["CurrLang"] = ddLangSel.SelectedValue.ToString();
        Response.Redirect(Request.Path);
    }

    private void SetNotifications()
    {
        var url = Request.Url.AbsolutePath;
        string UserRole = string.Empty;
        try
        {
            UserRole = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2] != null ? System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2] : "";
        }
        catch (Exception ex)
        {

        }

        if (!string.IsNullOrEmpty(lblLOG_TWWID.Text) && !LoginName.Contains("@"))
        {
            //NOTIFICATIONS
            var lv = (MultiView)FindControl("mvMenu");

            if (lv != null)
            {
                //SUPERVISOR - MY TEAM
                if (UserRole == "Super User" || UserRole == "Admin" || UserRole == "Course Admin")
                {
                    var vw = (View)lv.FindControl("vwSuper_User");
                    // all enrollment under supervisor
                    NotifyEnrollmentCnt = DataHelper.CountEnrollments_Sup_ForApproval(Convert.ToInt32(lblLOG_TWWID.Text));

                    var notificationMyTeam = (HtmlGenericControl)vw.FindControl("notification_myteamSU") == null ? (HtmlGenericControl)vw.FindControl("notification_myteamAD") : (HtmlGenericControl)vw.FindControl("notification_myteamSU");

                    if (NotifyEnrollmentCnt > 0)
                    {
                        if (!url.Contains("MyTeam.aspx"))
                        {
                            notificationMyTeam.InnerHtml = NotifyEnrollmentCnt.ToString();
                            notificationMyTeam.Visible = true;
                        }
                        else
                        {
                            notificationMyTeam.Visible = false;
                        }
                    }
                    else
                    {
                        notificationMyTeam.Visible = false;
                    }
                }

                //USER - COURSE ENROLMENT
                // all enrollment under supervisor
                NotifyEnrolApprovedCnt = DataHelper.CountEnrollments_Approved_NotChecked(Convert.ToInt32(lblLOG_TWWID.Text));
                // all enrollment under supervisor
                NotifyEnrollDeniedCnt = DataHelper.CountEnrollments_Denied_NotChecked(Convert.ToInt32(lblLOG_TWWID.Text));

                var notificationMyPlan = (HtmlGenericControl)lv.FindControl("notification_myplanUS") != null ? (HtmlGenericControl)lv.FindControl("notification_myplanUS") : (HtmlGenericControl)lv.FindControl("notification_myplanSU") != null ? (HtmlGenericControl)lv.FindControl("notification_myplanSU") : (HtmlGenericControl)lv.FindControl("notification_myplanAD");

                int assignmentCnt = DataHelper.GetAssignCourseNotificationCount(Convert.ToInt32(lblLOG_TWWID.Text)).AssignedCount;

                if (NotifyEnrolApprovedCnt > 0 || NotifyEnrollDeniedCnt > 0 || assignmentCnt > 0)
                {
                    notificationMyPlan.InnerHtml = (NotifyEnrolApprovedCnt + NotifyEnrollDeniedCnt + assignmentCnt).ToString();
                    notificationMyPlan.Visible = true;
                }
                else
                {
                    notificationMyPlan.Visible = false;
                }
            }
        }
    }

    public void AlertMessage(bool success, string msg)
    {
        if (success)
        {
            lblNote.Text = msg;
            lblNote.ForeColor = System.Drawing.Color.FromArgb(255, 255, 255);
            lblNote.Font.Size = new FontUnit(Unit.Point(11));
            notify.TitleIcon = "ok";
            notify.Title = "Success!";
            notify.Opacity = 100;
            notify.Show();
        }
        else
        {
            lblNote.Text = msg;
            lblNote.ForeColor = System.Drawing.Color.FromArgb(255, 215, 0);
            lblNote.Font.Size = new FontUnit(Unit.Point(10));
            notify.TitleIcon = "warning";
            notify.Title = "Error!";
            notify.Opacity = 90;
            notify.Show();
        }
    }

    protected void lgnStatus_Click(Object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        FormsAuthentication.SignOut();
        Response.Redirect("~/LogIn.aspx");
    }

    protected void lnkAD_Home_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }

    protected void lnkAD_Prof_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ViewProfile.aspx");
    }

    protected void lnkAD_CHist_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/CourseHistory.aspx");
    }

    protected void lnkAD_MyPlan_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/MyPlan.aspx");
    }

    protected void lnkAD_MyTeam_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/MyTeam.aspx");
    }

    protected void lnkAD_MyReps_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/MyReports.aspx");
    }

    protected void lnkAD_Admin_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin.aspx");
    }

    protected void lnkCourse_Catalog_Click(object sender, EventArgs e)
    {
        if (lblLOG_TWWID.Text == null || lblLOG_TWWID.Text == string.Empty)
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            Response.Redirect("~/Courses.aspx");
        }
    }
}

