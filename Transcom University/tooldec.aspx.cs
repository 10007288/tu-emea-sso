﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class tooldec : System.Web.UI.Page
{
    //String connStr = ConfigurationManager.ConnectionStrings["LOKI"].ConnectionString;
    //protected void Page_Load(object sender, EventArgs e)
    //{

    //}
    protected void btnDelComment_Click(object sender, EventArgs e)
    {

        string ReSTR = DecryptString(TextBox1.Text);
        TextBox2.Text = ReSTR;
    }

    protected void btnDelComment2_Click(object sender, EventArgs e)
    {

        string ReSTR = EncryptString(Convert.ToInt32(TextBox1.Text));
        TextBox2.Text = ReSTR;
    }
    //protected void btnDelComment3_Click(object sender, EventArgs e)
    //{

    //    Enc_Str_Save_DB();
    //}
    //protected void Enc_Str_Save_DB()
    //{
    //    string xtr = "Select C_ID from COURSE_ENC";
    //    DataTable dx = GetDataTable(xtr);
    //    foreach (DataRow row in dx.Rows)
    //    {
    //        string ReSTR = EncryptString(Convert.ToInt32(row["C_ID"].ToString()));
    //        string xtr2 = "Update COURSE_ENC set ENC_STR = '" + ReSTR + "' where C_ID = '" + row["C_ID"].ToString() + "'";
    //        RunQuery(xtr2);
    //    }
    //    TextBox2.Text = "DONE ALL";
    //}
    public string DecryptString(string inputString)
    {
        System.IO.MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = new byte[inputString.Length];
            byteInput = Convert.FromBase64String(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateDecryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }

        Encoding encoding1 = Encoding.UTF8;
        return encoding1.GetString(memStream.ToArray());
    }
    public string EncryptString(int ConvVal)
    {
        string inputString = Convert.ToString(ConvVal);
        MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = Encoding.UTF8.GetBytes(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateEncryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();

        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
        return Convert.ToBase64String(memStream.ToArray());
    }
    //public void RunQuery(string query)
    //{
    //    //string RetVal = string.Empty;
    //    String ConnString = connStr;
    //    SqlConnection conn = new SqlConnection(ConnString);
    //    conn.Open();
    //    SqlCommand cmd = new SqlCommand(query, conn);
    //    cmd.ExecuteNonQuery();
    //    // return (RetVal == null) ? string.Empty : RetVal.ToString();
    //}
    //public DataTable GetDataTable(string query)
    //{
    //    String ConnString = connStr;
    //    SqlConnection conn = new SqlConnection(ConnString);
    //    SqlDataAdapter adapter = new SqlDataAdapter();
    //    adapter.SelectCommand = new SqlCommand(query, conn);

    //    DataTable myDataTable = new DataTable();
    //    if (conn.State != ConnectionState.Open)
    //    {
    //        conn.Open();
    //    }
    //    else
    //    {
    //        conn.Close();
    //        conn.Open();
    //    }

    //    try
    //    {
    //        adapter.Fill(myDataTable);
    //    }
    //    catch (Exception ex)
    //    {
    //        conn.Close();
    //    }
    //    adapter.Dispose();
    //    conn.Close();


    //    return myDataTable;
    //}
}