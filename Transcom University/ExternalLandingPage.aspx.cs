﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;
using DotNetOpenAuth.AspNet;
using System.Web.Security;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class ExternalLandingPage : BasePage
{
    string twwId;

    string ProviderName
    {
        get { return (string)ViewState["ProviderName"] ?? String.Empty; }
        set { ViewState["ProviderName"] = value; }
    }

    string ProviderDisplayName
    {
        get { return (string)ViewState["ProviderDisplayName"] ?? String.Empty; }
        set { ViewState["ProviderDisplayName"] = value; }
    }

    string ProviderUserId
    {
        get { return (string)ViewState["ProviderUserId"] ?? String.Empty; }
        set { ViewState["ProviderUserId"] = value; }
    }

    public string ProviderUserName
    {
        get { return (string)ViewState["ProviderUserName"] ?? String.Empty; }
        set { ViewState["ProviderUserName"] = value; }
    }

    protected void Page_Load()
    {
        if (!IsPostBack)
        {
            ProcessProviderResult();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        /*if (!IsPostBack)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();
        }*/
    }

    protected void logIn_Click(object sender, EventArgs e)
    {
        CreateAndLoginUser();
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        RedirectToReturnUrl();
    }

    private void ProcessProviderResult()
    {
        DotNetOpenAuth.GoogleOAuth2.GoogleOAuth2Client.RewriteRequest();

        // Process the result from an auth provider in the request
        ProviderName = OpenAuth.GetProviderNameFromCurrentRequest();

        if (String.IsNullOrEmpty(ProviderName))
        {
            Response.Redirect("~/Login.aspx");
        }

        // Build the redirect url for OpenAuth verification
        var redirectUrl = "~/ExternalLandingPage.aspx";
        var returnUrl = Request.QueryString["ReturnUrl"];
        if (!String.IsNullOrEmpty(returnUrl))
        {
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl);
        }

        // Verify the OpenAuth payload
        AuthenticationResult authResult = OpenAuth.VerifyAuthentication(redirectUrl);
        ProviderDisplayName = OpenAuth.GetProviderDisplayName(ProviderName);

        if (!authResult.IsSuccessful)
        {
            Title = "External login failed";
            Trace.Warn("OpenAuth", String.Format("There was an error verifying authentication with {0})", ProviderDisplayName), authResult.Error);

            return;
        }

        // Strip the query string from action
        Form.Action = ResolveUrl(redirectUrl);
        var extraData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(authResult.ExtraData));

        // Store the provider details in ViewState
        ProviderName = authResult.Provider;
        ProviderUserId = authResult.ProviderUserId;
        ProviderUserName = extraData.email;

        //Session["GoogleName"] = authResult.UserName;
        lblUsername.Text = ProviderUserName;
        lblEmail.Text = ProviderUserName;
        lblGoogleName.Text = authResult.UserName;
        lblProfileImage.Text = extraData.picture;

        if (extraData.hd != null && extraData.hd.ToString() == "transcom.com")
        {
            //original_message.Visible = true;

            if (OpenAuth.Login(authResult.Provider, authResult.ProviderUserId, createPersistentCookie: true))
            {
                RedirectToReturnUrl(ProviderUserName + "|" + authResult.UserName + "|" + lblProfileImage.Text);
            }

            // Check if user is already registered locally
            if (User.Identity.IsAuthenticated)
            {
                // User is already authenticated, add the external login and redirect to return url
                OpenAuth.AddAccountToExistingUser(ProviderName, ProviderUserId, ProviderUserName, User.Identity.Name);
                RedirectToReturnUrl(ProviderUserName + "|" + authResult.UserName + "|" + lblProfileImage.Text);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
            "window.location='https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
            HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/Login.aspx';", true);

            FormsAuthentication.SignOut();
            Button1.Visible = false;
            Button2.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Your domain is not allowed!');", true);
            //ModelErrorMessage1.Text = "Your domain is not allowed";
        }
    }

    private void CreateAndLoginUser()
    {
        if (!IsValid)
        {
            return;
        }

        var createResult = OpenAuth.CreateUser(ProviderName, ProviderUserId, ProviderUserName, lblUsername.Text);
        if (!createResult.IsSuccessful)
        {
            //ModelState.AddModelError("UserName", createResult.ErrorMessage);
        }
        else
        {
            // User created & associated OK
            if (OpenAuth.Login(ProviderName, ProviderUserId, createPersistentCookie: false))
            {
                RedirectToReturnUrl(lblUsername.Text + "|" + lblGoogleName.Text + "|" + lblProfileImage.Text);
            }
        }
    }

    private void RedirectToReturnUrl() //Cancel
    {
        var returnUrl = Request.QueryString["ReturnUrl"];

        if (!String.IsNullOrEmpty(returnUrl) && OpenAuth.IsLocalUrl(returnUrl))
        {
            Response.Redirect(returnUrl);
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    public void RedirectToReturnUrl(string userID)
    {
        var returnUrl = Request.QueryString["ReturnUrl"];
        FormsAuthentication.SetAuthCookie(userID, true);

        string serviceUrl = WebConfigurationManager.AppSettings["LokiV2RestApi"] + lblEmail.Text + "&fields=employeeid";

        HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(new Uri(serviceUrl));
        httpRequest.Accept = "application/json";
        httpRequest.ContentType = "application/json";
        httpRequest.Method = "GET";

        try
        {
            using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
            {
                using (Stream stream = httpResponse.GetResponseStream())
                {
                    string output = (new StreamReader(stream)).ReadToEnd();
                    string[] retApi = output.Split(new string[] { "\"" }, StringSplitOptions.None);
                    twwId = retApi[2].ToString().Replace(":", "").Replace("}", "");
                }
            }
        }
        catch (WebException ex)
        {
            var response = ex.Response as HttpWebResponse;

            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                "window.location='https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/Login.aspx';", true);

                FormsAuthentication.SignOut();

                Button1.Visible = false;
                Button2.Visible = false;

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Your Email has no Employee ID. For support please contact ServiceNow or ask your Teamleader to raise a ticket for you.');", true);
            }
        }

        if (twwId != null)
        {
            if (Registration.GetRoleForUser(twwId) == "")
            {
                using (SqlConnection oconn = new SqlConnection(ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
                {
                    oconn.Open();
                    using (SqlCommand ocomm = new SqlCommand("pr_TranscomUniversity_InsertNewUser", oconn))
                    {
                        ocomm.CommandType = CommandType.StoredProcedure;
                        ocomm.Parameters.AddWithValue("@TWWID", twwId);
                        ocomm.Parameters.AddWithValue("@Name", lblGoogleName.Text);
                        ocomm.ExecuteNonQuery();
                    }
                    oconn.Close();
                }
            }

            var UserRoleID = Registration.GetRoleForUser(twwId) != "" ? Registration.GetRoleForUser(twwId) : "Normal User";
            var lv = (MultiView)Master.FindControl("mvMenu");

            if (UserRoleID == "Admin")
            {
                var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
                hpTURep.Visible = false;
                var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
                hpTUTestAdmin.Visible = true;
                var vw = (View)lv.FindControl("vwAdmin");
                lv.ActiveViewIndex = 2;
                lv.SetActiveView(vw);
                lv.Visible = true;
            }
            else if (UserRoleID == "Normal User")
            {
                var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
                hpTURep.Visible = false;
                var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
                hpTUTestAdmin.Visible = false;
                var vw = (View)lv.FindControl("vwNormal_User");
                lv.ActiveViewIndex = 1;
                lv.SetActiveView(vw);
                lv.Visible = true;
            }
            else if (UserRoleID == "Super User")
            {
                var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
                hpTURep.Visible = false;
                var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
                hpTUTestAdmin.Visible = true;
                var vw = (View)lv.FindControl("vwSuper_User");
                lv.ActiveViewIndex = 3;
                lv.SetActiveView(vw);
                lv.Visible = true;
            }
            else if (UserRoleID == "Course Admin")
            {
                var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
                hpTURep.Visible = false;
                var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
                hpTUTestAdmin.Visible = true;
                var vw = (View)lv.FindControl("vwCourse_Admin");
                lv.ActiveViewIndex = 2;
                lv.SetActiveView(vw);
                lv.Visible = true;
            }

            string persistUser = string.Empty;
            persistUser = twwId + "|" + lblGoogleName.Text + "|" + UserRoleID + "|" + lv.ActiveViewIndex + "|" + "";

            FormsAuthentication.SetAuthCookie(persistUser, true);
            Response.Redirect("~/Default.aspx");
        }
    }
}