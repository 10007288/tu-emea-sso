﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Users" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Users Management</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="scriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="ajaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdUsers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdUsers" LoadingPanelID="loadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="filter1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="filter1" />
                    <telerik:AjaxUpdatedControl ControlID="grdUsers" LoadingPanelID="loadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div>
        <telerik:RadFilter ID="filter1" runat="server" FilterContainerID="grdUsers" ShowApplyButton="true">
        </telerik:RadFilter>
        <hr />
        <telerik:RadGrid ID="grdUsers" runat="server" AutoGenerateColumns="false" Width="100%"
            OnNeedDataSource="grdUsers_NeedDataSource" AllowPaging="true">
            <ClientSettings>
                <Resizing AllowColumnResize="true" AllowResizeToFit="true" EnableRealTimeResize="true" />
                <Scrolling UseStaticHeaders="true" ScrollHeight="550px" AllowScroll="true" />
            </ClientSettings>
            <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" AllowPaging="true"
                AllowSorting="true" EnableHeaderContextMenu="true" EditMode="Batch" PageSize="20">
                <PagerStyle AlwaysVisible="true" PageSizes="20" Mode="Advanced" />
                <BatchEditingSettings OpenEditingEvent="Click" />
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="CreateDate" SortOrder="Descending" />
                </SortExpressions>
                <Columns>
                    <telerik:GridBoundColumn DataField="TWWID" HeaderText="TWWID" SortExpression="TWWID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator Text="*" ForeColor="Red">
                            </RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="supervisorid" HeaderText="supervisorid" SortExpression="supervisorid">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Password" HeaderText="Password" SortExpression="Password"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FailedPasswordAttempCount" HeaderText="FailedPasswordAttempCount"
                        SortExpression="FailedPasswordAttempCount" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IsLockedOut" HeaderText="IsLockedOut" SortExpression="IsLockedOut"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Active" HeaderText="Active" SortExpression="Active">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="Role" HeaderText="Role" SortExpression="Role">
                        <ItemTemplate>
                            <%# Eval("Role") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboRole" runat="server" EmptyMessage="Select Role" AppendDataBoundItems="true"
                                DataValueField="RoleID" DataTextField="Role" DataSourceID="dsRoles" Width="100px"
                                DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Client" HeaderText="Client" SortExpression="Client">
                        <ItemTemplate>
                            <%# Eval("Client") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboClient" runat="server" EmptyMessage="Select Client" AppendDataBoundItems="true"
                                DataValueField="ClientID" DataTextField="Client" DataSourceID="dsClient" Width="100px"
                                DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="OtherClient" HeaderText="OtherClient" SortExpression="OtherClient">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="Region" HeaderText="Region" SortExpression="Region">
                        <ItemTemplate>
                            <%# Eval("Region")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboRegion" runat="server" EmptyMessage="Select Region" AppendDataBoundItems="true"
                                DataValueField="RegionID" DataTextField="Region" DataSourceID="dsRegion" Width="100px"
                                DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Country" HeaderText="Country" SortExpression="Country">
                        <ItemTemplate>
                            <%# Eval("Country") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboCountry" runat="server" EmptyMessage="Select Country"
                                AppendDataBoundItems="true" DataValueField="CountryID" DataTextField="Country"
                                DataSourceID="dsCountries" Width="100px" DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Site" HeaderText="Site" SortExpression="Site">
                        <ItemTemplate>
                            <%# Eval("Site") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboSite" runat="server" EmptyMessage="Select Site" AppendDataBoundItems="true"
                                DataValueField="SiteID" DataTextField="Site" DataSourceID="dsSite" Width="100px"
                                DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Language" HeaderText="Language" SortExpression="Language">
                        <ItemTemplate>
                            <%# Eval("Language")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboLanguage" runat="server" EmptyMessage="Select Language"
                                AppendDataBoundItems="true" DataValueField="LanguageID" DataTextField="Language"
                                DataSourceID="dsLanguage" Width="100px" DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="MobileNo" HeaderText="MobileNo" SortExpression="MobileNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Specialization_Skills" HeaderText="Specialization_Skills"
                        SortExpression="Specialization_Skills">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="EducationLevel" HeaderText="Education Level"
                        SortExpression="EducationLevel">
                        <ItemTemplate>
                            <%# Eval("EducationLevel")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboEducationLevel" runat="server" EmptyMessage="Select Education Level"
                                AppendDataBoundItems="true" DataValueField="EducationLevelID" DataTextField="EducationLevel"
                                DataSourceID="dsEducLevel" Width="100px" DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="CourseMajor" HeaderText="CourseMajor" SortExpression="CourseMajor">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AdditionalCourses_Trainings" HeaderText="AdditionalCourses_Trainings"
                        SortExpression="AdditionalCourses_Trainings">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="CareerPath" HeaderText="CareerPath" SortExpression="CareerPath">
                        <ItemTemplate>
                            <%# Eval("CareerPath")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cboCareerPath" runat="server" EmptyMessage="Select Career Path"
                                AppendDataBoundItems="true" DataValueField="CareerPathID" DataTextField="CareerPath"
                                DataSourceID="dsCareerPath" Width="100px" DropDownAutoWidth="Enabled">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="OtherCareerPath" HeaderText="OtherCareerPath"
                        SortExpression="OtherCareerPath">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CourseInterests" HeaderText="CourseInterests"
                        SortExpression="CourseInterests">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UploadedPhoto" HeaderText="UploadedPhoto" SortExpression="UploadedPhoto"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UpdateVia" HeaderText="UpdateVia" SortExpression="UpdateVia"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="departmentid" HeaderText="departmentid" SortExpression="departmentid"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OtherDepartment" HeaderText="OtherDepartment"
                        SortExpression="OtherDepartment">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="supporttype" HeaderText="supporttype" SortExpression="supporttype"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate">
                    </telerik:GridDateTimeColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsRoles" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT Id as RoleID, role_desc as Role FROM [TranscomUniv_local].dbo.refRoles WHERE active = 1 ORDER BY 2">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id] as RegionID, [description] as Region FROM [TranscomUniv_local].[dbo].[Regions] WHERE [Active] = 1 ORDER BY 2"
            SelectCommandType="Text"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsLanguage" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id] as LanguageID, Language FROM [TranscomUniv_local].[dbo].[refLanguage] WHERE [Active] = 1 ORDER BY 2"
            SelectCommandType="Text"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsClient" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id] as ClientID, [description] as Client FROM [TranscomUniv_local].[dbo].[Clients] WHERE [Active] = 1 ORDER BY 2"
            SelectCommandType="Text"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand=" SELECT ID as CountryID, Country FROM [dbo].[Countries] WHERE [Active] = 1 ORDER BY 2" />
        <asp:SqlDataSource ID="refCareerPath" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand=" SELECT ID as CareerPathID, CareerPath FROM [dbo].[refCareerPath] WHERE [Active] = 1 ORDER BY 2" />
        <asp:SqlDataSource ID="dsSite" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT ID as SiteID, Description as Site FROM TranscomUniv_local.dbo.Locations WHERE active = 1 ORDER BY 2" />
        <asp:SqlDataSource ID="dsCareerPath" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID] as CareerPathID, [Description] as CareerPath FROM [refCareerPath] WHERE Active = 1 ORDER BY 2">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsEducLevel" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand=" SELECT ID as EducationLevelID, Title AS EducationLevel FROM [dbo].[refEducationLevel] WHERE [Active] = 1 ORDER BY 2" />
    </div>
    <telerik:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function BestFit() {
                var grid = $find("<%= grdUsers.ClientID %>");
                var columns = grid.get_masterTableView().get_columns();
                for (var i = 0; i < columns.length; i++) {
                    if (i != 0)
                        columns[i].resizeToFit(false, true);
                }
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadCodeBlock ID="CodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                BestFit();
            }
        </script>
    </telerik:RadCodeBlock>
    </form>
</body>
</html>
