﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Calendar.aspx.cs" Inherits="Calendar" %>

<asp:Content ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="overflow: hidden; background: #FFF; height: 725px">
        <telerik:RadSplitter ID="MainSplitter" runat="server" Height="830px" Width="100%"
            Orientation="Horizontal">
            <telerik:RadPane ID="MainPane" runat="server" Scrolling="None" CssClass="calendarPane" />
        </telerik:RadSplitter>
    </div>
</asp:Content>
