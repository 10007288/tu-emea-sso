﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="styles.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= btnSave.UniqueID %>") {
                    args.set_enableAjax(false);
                }
            }

            function RequestFullScreen(sender, args) {
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptBlock ID="scriptBlock1">
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbRegion">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cbCountry" />
                    <telerik:AjaxUpdatedControl ControlID="cbSite" />
                    <telerik:AjaxUpdatedControl ControlID="cbClient" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbCountry">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cbSite" />
                    <telerik:AjaxUpdatedControl ControlID="cbClient" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtTWWID">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlEmployeeInfo" />
                    <telerik:AjaxUpdatedControl ControlID="divAutoPopulateFields" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                    <telerik:AjaxUpdatedControl ControlID="PanelNotification" />
                    <telerik:AjaxUpdatedControl ControlID="lblMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel2" runat="server" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="insidecontainer">
                    <h2>
                        <asp:Label ID="lblTitle" Text="Account Registration" runat="server" /></h2>
                    <p>
                        <a href="Login.aspx">Back to Login</a></p>
                    <asp:Panel runat="server" ID="PanelNotification" Visible="false" CssClass="msgnotification">
                        <asp:Label Text="" runat="server" ID="lblMessage" />
                    </asp:Panel>
                    <a runat="server" id="divNotifyError" style="color: red"></a>
                    <div>
                        <div class="divFieldLabel">
                            <label>
                                TWW ID</label>
                            <asp:RequiredFieldValidator ID="rqTTWID" runat="server" ControlToValidate="txtTWWID"
                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                CssClass="displayerror" />
                        </div>
                        <div class="divField">
                            <telerik:RadNumericTextBox runat="server" ID="txtTWWID" AutoPostBack="True" OnTextChanged="TxtTwwidTextChanged"
                                MaxLength="10" SelectionOnFocus="SelectAll">
                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                            </telerik:RadNumericTextBox>
                            <div runat="server" id="UserIsEmployee" style="color: red">
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlEmployeeInfo" runat="server" >
                        <div runat="server" class="insidecontainer" id="divAutoPopulateFields" visible="False"
                            style="padding-top: 5px; padding-bottom: 5px">
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <label>
                                                Full Name :
                                            </label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtFName" runat="server" Text="" ForeColor="DodgerBlue"> </asp:Label>
                                            <asp:Label ID="txtLName" runat="server" Text="" ForeColor="DodgerBlue"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Gender :</label>
                                        </td>
                                        <td>
                                            <a runat="server" id="divGender" style="color: dodgerblue"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Date of Birth :</label>
                                        </td>
                                        <td>
                                            <a runat="server" id="divBirthDate" style="color: dodgerblue"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Start Date w/ Transcom :</label>
                                        </td>
                                        <td>
                                            <a runat="server" id="divStartDate" style="color: dodgerblue"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Role :</label>
                                        </td>
                                        <td>
                                            <a runat="server" id="divRole" style="color: dodgerblue"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Region :
                                            </label>
                                            <asp:RequiredFieldValidator ID="rqRegion" runat="server" ControlToValidate="cbRegion"
                                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                                CssClass="displayerror" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRegion" runat="server" Text="Label" ForeColor="DodgerBlue"></asp:Label>
                                            <telerik:RadComboBox runat="server" ID="cbRegion" DataTextField="Description" DataValueField="ID"
                                                AutoPostBack="True" EmptyMessage="- select region -" OnSelectedIndexChanged="CbRegionSelectedIndexChanged"
                                                AppendDataBoundItems="true" MaxHeight="250" DropDownAutoWidth="Enabled">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Country :
                                            </label>
                                            <asp:RequiredFieldValidator ID="rqCountry" runat="server" ControlToValidate="cbCountry"
                                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                                CssClass="displayerror" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCountry" runat="server" Text="Label" ForeColor="DodgerBlue"></asp:Label>
                                            <telerik:RadComboBox runat="server" ID="cbCountry" AutoPostBack="True" DataTextField="Country1"
                                                DataValueField="ID" EmptyMessage="- select country -" OnSelectedIndexChanged="CbCountrySelectedIndexChanged"
                                                MaxHeight="250" DropDownAutoWidth="Enabled" AppendDataBoundItems="True">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Site :
                                            </label>
                                            <asp:RequiredFieldValidator ID="rqSite" runat="server" ControlToValidate="cbSite"
                                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                                CssClass="displayerror" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSite" runat="server" Text="Label" ForeColor="DodgerBlue"></asp:Label>
                                            <telerik:RadComboBox runat="server" ID="cbSite" AutoPostBack="True" DataTextField="Description"
                                                DataValueField="ID" MaxHeight="250" DropDownAutoWidth="Enabled" OnSelectedIndexChanged="cbSite_SelectedIndexChanged"
                                                EmptyMessage="- select site -" AppendDataBoundItems="True">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Reporting Manager/Supervisor :
                                            </label>
                                            <asp:RequiredFieldValidator ID="rqReptMgr" runat="server" ControlToValidate="cbSupervisor"
                                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                                CssClass="displayerror" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSupervisor" runat="server" Text="Label" ForeColor="DodgerBlue"></asp:Label>
                                            <telerik:RadComboBox ID="cbSupervisor" runat="server" DataSourceID="SqlDataSource_Supervisor"
                                                DataTextField="supervisorname" DataValueField="twwid" EmptyMessage="- select supervisor -"
                                                Height="150px" ItemsPerRequest="10" ShowMoreResultsBox="true" EnableVirtualScrolling="true"
                                                AppendDataBoundItems="True">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Department :
                                            </label>
                                            <asp:RequiredFieldValidator ID="rqDept" runat="server" ControlToValidate="cbDepartment"
                                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                                CssClass="displayerror" />
                                        </td>
                                        <td>
                                            <telerik:RadTextBox runat="server" ID="txtDepartment" Visible="False" Width="120"
                                                EmptyMessage="Please specify" ForeColor="DodgerBlue" />
                                            <telerik:RadComboBox ID="cbDepartment" runat="server" DataTextField="description"
                                                DataValueField="id" EmptyMessage="- select department -" DataSourceID="SqlDataSource_Department"
                                                MaxHeight="300" AppendDataBoundItems="True" DropDownAutoWidth="Enabled" OnSelectedIndexChanged="cbDepartment_SelectedIndexChanged"
                                                AutoPostBack="True" Height="16px">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="" Value="" />
                                                    <telerik:RadComboBoxItem Text="Others" Value="0" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Client :
                                            </label>
                                            <asp:RequiredFieldValidator ID="rqClient" runat="server" ControlToValidate="cbClient"
                                                SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                                                CssClass="displayerror" />
                                        </td>
                                        <td>
                                            <telerik:RadTextBox runat="server" ID="txtClient" Visible="False" Width="120" EmptyMessage="Please specify"
                                                ForeColor="DodgerBlue" />
                                            <telerik:RadComboBox runat="server" ID="cbClient" DataTextField="description" DataValueField="ID"
                                                EmptyMessage="- select client -" MaxHeight="300" DropDownAutoWidth="Enabled"
                                                AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="cbClient_SelectedIndexChanged">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="Others" Value="0" />
                                                    <telerik:RadComboBoxItem Text="Shared" Value="-1" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="divFieldLabel">
                        <label>
                            Transcom Email (<span class="noteLabel">optional</span>)</label>
                    </div>
                    <div class="divField">
                        <telerik:RadTextBox runat="server" ID="txtEmailAddress" Width="250" SelectionOnFocus="SelectAll" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Mobile No. (IC-PN) (<span class="noteLabel">optional</span>)</label>

                    </div>
                    <div class="insidecontainer">
                        <label>
                            International Code</label>
                        <telerik:RadTextBox runat="server" ID="txtMobileNoIC" SelectionOnFocus="SelectAll"
                            Width="40" MaxLength="5" EmptyMessage="00">
                        </telerik:RadTextBox>

                        <label>
                            -</label>

                        <telerik:RadTextBox runat="server" ID="txtMobileNo" SelectionOnFocus="SelectAll"
                            Width="100" MaxLength="15">
                        </telerik:RadTextBox>
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Type of Support</label>
                    </div>
                    <div class="divField">
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSourceID="SqlDataSource_TypeOfSupport"
                            DataTextField="description" DataValueField="id" RepeatColumns="2" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Primary Language</label>
                        <asp:RequiredFieldValidator ID="rqPrimLang" runat="server" ControlToValidate="cbLanguage"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadComboBox runat="server" ID="cbLanguage" DataTextField="Language" DataValueField="Id"
                            EmptyMessage="- select language -" MaxHeight="250" DataSourceID="DataSource_Language"
                            DropDownAutoWidth="Enabled" AppendDataBoundItems="True">
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                                <telerik:RadComboBoxItem Text="Others" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Current Specialization and Skills</label>
                        <asp:RequiredFieldValidator ID="rqCurrSpecSkills" runat="server" ControlToValidate="txtSpecializationSkills"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadTextBox runat="server" ID="txtSpecializationSkills" Width="300" Height="50"
                            SelectionOnFocus="SelectAll" TextMode="MultiLine" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Education Level</label>
                        <asp:RequiredFieldValidator ID="rqEducLvl" runat="server" ControlToValidate="cbEducLevel"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadComboBox runat="server" ID="cbEducLevel" DataValueField="Id" DataTextField="Title"
                            EmptyMessage="- select education level -" DataSourceID="DataSourse_EducLevel"
                            MaxHeight="250" DropDownAutoWidth="Enabled" AppendDataBoundItems="True">
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Major Subject</label>
                        <asp:RequiredFieldValidator ID="rqMajSubj" runat="server" ControlToValidate="txtCourseMajor"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadTextBox runat="server" ID="txtCourseMajor" Width="120" SelectionOnFocus="SelectAll" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Additional Courses and Training Attended (Internal & External)</label>
                        <asp:RequiredFieldValidator ID="reqAddCourse" runat="server" ControlToValidate="txtCoursesTrainings"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadTextBox runat="server" ID="txtCoursesTrainings" Width="300" Height="50"
                            SelectionOnFocus="SelectAll" TextMode="MultiLine" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            I am interested in pursuing the following career path in Transcom</label>
                        <asp:RequiredFieldValidator ID="rqCareerPath" runat="server" ControlToValidate="cbCareerPath"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                        <asp:RequiredFieldValidator ID="rfvOtherCareerPath" runat="server" ControlToValidate="txtCareerPath"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" Enabled="False" />
                    </div>
                    <div class="insidecontainer">
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadComboBox ID="cbCareerPath" runat="server" DataSourceID="SqlDataSource_CareerPath"
                                        DataTextField="Description" DataValueField="ID" DropDownAutoWidth="Enabled" EmptyMessage="- select career -"
                                        MaxHeight="300px" AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="cbCareerPath_SelectedIndexChanged">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                            <telerik:RadComboBoxItem Text="Others" Value="0" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtCareerPath" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Please tell me what courses you are interested in taking for your professional growth
                            and development</label>
                        <asp:RequiredFieldValidator ID="rqCourseInt" runat="server" ControlToValidate="txtCoursesInterested"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadTextBox runat="server" ID="txtCoursesInterested" Width="300" Height="50"
                            SelectionOnFocus="SelectAll" TextMode="MultiLine" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Upload a Photograph (<span class="noteLabel">optional</span>)
                        </label>
                    </div>
                    <div class="insidecontainer">
                        <asp:FileUpload ID="fileUpload" runat="server" />
                        <div runat="server" id="divUploader" style="color: red">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            I am interested in receiving updates via</label>
                        <asp:RequiredFieldValidator ID="rqReceiveUpdates" runat="server" ControlToValidate="cbReceiveUpdates"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadComboBox ID="cbReceiveUpdates" runat="server" DataSourceID="SqlDataSource_UpdateVia"
                            DataTextField="Description" DataValueField="ID" CheckBoxes="True" DropDownAutoWidth="Enabled"
                            EmptyMessage="- select item/s -" MaxHeight="300px">
                        </telerik:RadComboBox>
                    </div>
                </div>
                <p>
                </p>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Password</label>
                        <asp:RequiredFieldValidator ID="rqPassword" runat="server" ControlToValidate="txtPassword"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                        <asp:RegularExpressionValidator ID="reFortxtPassword" runat="server" ValidationExpression="^.*(?=.{8,})(?=.*[\d])(?=.*[\W]).*$"
                            ControlToValidate="txtPassword" SetFocusOnError="True" ValidationGroup="reg"
                            ErrorMessage="* Atleast 8 character with number and special character" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <label class="noteLabel">
                            Note: Atleast 8 character with number and special character</label>
                        <br />
                        <telerik:RadTextBox runat="server" ID="txtPassword" TextMode="Password" MaxLength="50"
                            Width="200" SelectionOnFocus="SelectAll" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Confirm Password</label>
                        <asp:RequiredFieldValidator ID="rqConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                        <asp:CompareValidator ID="cvFortxtPassword" runat="server" ErrorMessage="* Password and Confirm password do not match"
                            Type="String" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                            SetFocusOnError="True" ValidationGroup="reg" Display="Dynamic" CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadTextBox runat="server" ID="txtConfirmPassword" TextMode="Password" Width="200"
                            SelectionOnFocus="SelectAll" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Security Question</label>
                        <asp:RequiredFieldValidator ID="rqSecurityQuestion" runat="server" ControlToValidate="cbSecurityQuestion"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadComboBox runat="server" ID="cbSecurityQuestion" DataTextField="securityquestion"
                            DataValueField="id" EmptyMessage="- select question -" DataSourceID="SqlDataSourceSecurityQuestion"
                            MaxHeight="300" DropDownAutoWidth="Enabled" />
                    </div>
                </div>
                <div>
                    <div class="insidecontainer">
                        <label>
                            Security Answer</label>
                        <asp:RequiredFieldValidator ID="rqSecurityAnswer" runat="server" ControlToValidate="txtSecurityAnswer"
                            SetFocusOnError="True" ValidationGroup="reg" ErrorMessage="*" Display="Dynamic"
                            CssClass="displayerror" />
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadTextBox runat="server" ID="txtSecurityAnswer" Width="200" SelectionOnFocus="SelectAll" />
                    </div>
                </div>
                <br />
                <div id="managecontrol" style="width: 100%" class="insidecontainer">
                    <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" ClientEvents-OnRequestStart="conditionalPostback"
                        Width="100%">
                        <asp:LinkButton Text="Register" runat="server" ID="btnSave" ValidationGroup="reg"
                            OnClick="BtnSaveClick" />
                    </telerik:RadAjaxPanel>
                </div>
                <br />
                <br />
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="SqlDataSource_Regions" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [ID], [Description] FROM [Regions] WITH (NOLOCK) WHERE ([ACTIVE] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="SqlDataSourceSecurityQuestion" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [id], [securityquestion] FROM [refSecurityQuestions] WITH (NOLOCK) WHERE ([bitactive] = 1) ORDER BY [id]" />
        <asp:SqlDataSource ID="DataSourse_EducLevel" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id], [Title] FROM [refEducationLevel] WITH (NOLOCK) WHERE ([Active] = 1) ORDER BY [Id]" />
        <asp:SqlDataSource ID="DataSource_Language" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id], [Language] FROM [refLanguage] WITH (NOLOCK) WHERE ([Active] = 1) ORDER BY [Language]" />
        <asp:SqlDataSource ID="SqlDataSource_Supervisor" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="select * from dbo.vw_AllActiveSupervisors order by supervisorname" />
        <asp:SqlDataSource ID="SqlDataSource_TypeOfSupport" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [id], [description] FROM [refSupportType] WITH (NOLOCK) WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="SqlDataSource_Department" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [id], [description] FROM [refDepartments] WITH (NOLOCK) WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="SqlDataSource_CareerPath" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refCareerPath] WITH (NOLOCK) WHERE ([Active] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="SqlDataSource_UpdateVia" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refUpdateVia] WITH (NOLOCK) WHERE ([Active] = 1) ORDER BY [Description]" />
    </div>
</asp:Content>
