﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;

public partial class ChangePassword : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        var user = Context.User.Identity.Name;

        if (user != null)
        {
            Label lblLOG_TWWID = (Label)Master.FindControl("lblLOG_TWWID");

            var authUser = Registration.GetUser(lblLOG_TWWID.Text).FirstOrDefault();

            var sSafeNewPassword = Server.HtmlEncode(txtNewPassword.Text);
            var sSafeOldPassword = Server.HtmlEncode(txtOldPassword.Text);

            if (Password.ValidatePassword(txtOldPassword.Text, authUser.Password))
            {
                try
                {
                    Registration.UserChangePassword(lblLOG_TWWID.Text, sSafeNewPassword);
                    Registration.InsertSecurityAnswer(Convert.ToInt32(cboSQ.SelectedValue), lblLOG_TWWID.Text, cboSQ.Text);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Password change success!');window.location.href = 'ViewProfile.aspx';", true);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", string.Format("alert('Error: {0}')", ex.Message.Replace("'", "")), true);
                }
            }
            else
            {
                txtOldPassword.Text = string.Empty;
                txtNewPassword.Text = string.Empty;
                txtRetypePassword.Text = string.Empty;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Invalid old password');", true);
            }      
        }
        else
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            var redirectUrl = FormsAuthentication.LoginUrl + "?ReturnUrl=Default.aspx";
            FormsAuthentication.SignOut();
            Response.Redirect(redirectUrl);
        }
    }
}