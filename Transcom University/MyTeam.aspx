﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MyTeam.aspx.cs" Inherits="MyTeam" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function onRequestStart(sender, args) {
                if ((args.get_eventTarget().indexOf("btnExport") >= 0) || (args.get_eventTarget().indexOf("btnExportTranscript") >= 0) || (args.get_eventTarget().indexOf("btnExportPlan") >= 0)) {
                    args.set_enableAjax(false);
                }

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            //NEW JAY
            //OPEN/CLOSE WINDOW
            function Show_ProfileWindow() {
                $find("<%=windowViewProfile.ClientID %>").show();
            }
            function Show_TranscriptWindow() {
                $find("<%=windowViewTranscript.ClientID %>").show();
            }
            function Show_ScoreHistory() {
                $find("<%=windowScoreHistory.ClientID %>").show();
            }
            function Show_CourseEnrollment(sender, args) {
                $find("<%=windowCourseEnrollment.ClientID %>").show();
            }
            function Show_LearningPlanWindow(sender, args) {
                $find("<%=windowViewLearningPlan.ClientID %>").show();
            }
            function ApproveEnrolment(sender, args) {
                //$find("<%=windowApproveEnrolment.ClientID %>").show();
                var oWindow = window.radopen(null, "windowApproveEnrolment");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function DenyEnrolment(sender, args) {
                //$find("<%=windowDenyEnrolment.ClientID %>").show();
                var oWindow = window.radopen(null, "windowDenyEnrolment");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            //END OPEN/CLOSE WINDOW
            //MATTHEW
            function OnClientEntryAdding(sender, eventArgs) {
                setTimeout(function () { sender.closeDropDown(); }, 10);
            }
            function OnClientClose(sender, args) {
                var masterTable = $find("<%= gridMyTeam.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function CloseAssignForm(save, msg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("RebindAssign");
                var window = $find('<%=AssignCourseWindow.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }
            function Close(msg) {
                var masterTable = $find("<%= gridAssignedCourse.ClientID %>").get_masterTableView();
                masterTable.rebind();
                var window = $find('<%=AssignCourseWindow.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
                if (msg != '') {
                    alert(msg);
                }
            }
            function OpenAssignForm(sender, args) {
                setTimeout(function () { $find("<%=AssignCourseWindow.ClientID %>").show(); }, 800);
                //var oWindow = window.radopen(null, "AssignCourseWindow");
                //oWindow.add_beforeClose(onBeforeClose);
                //oWindow.center();
            }
            //MATT NEW
            //List box Checkboxes------------------------------------------------------
            function OnClientSelectedIndexChanged(sender, eventArgs) {

                eventArgs.get_item().set_checked(eventArgs.get_item().get_selected());

            }

            function OnClientItemChecked(sender, eventArgs) {
                var item_g = eventArgs.get_item();
                item_g.set_selected(item_g.get_checked());

                var lstBoxControl;

                var ctr = 0;
                lstBoxControl = $find(sender.get_id());
                var count = lstBoxControl.get_items().get_count();
                var items = lstBoxControl.get_items();

                for (var i = 0; i < count; i++) {

                    if (items.getItem(i).get_checked() == true) {
                        items.getItem(i).set_selected(true);
                        ctr++;
                    }
                }

                if (count == ctr) {
                    if (sender.get_id().substr(sender.get_id().length - ('lbUnassignedCourses').length) == 'lbUnassignedCourses') {
                        $("input[id$=chkAllAssignCourse]").prop('checked', true);
                    }
                    else {
                        $("input[id$=chkAllPrerequisite]").prop('checked', true);
                    }
                }
                else {
                    if (sender.get_id().substr(sender.get_id().length - ('lbUnassignedCourses').length) == 'lbUnassignedCourses') {

                        $("input[id$=chkAllAssignCourse]").prop('checked', false);
                    }
                    else {
                        $("input[id$=chkAllPrerequisite]").prop('checked', false);
                    }
                }

            }

            function OnTransferring(sender, eventArgs) {

                var lstBoxControl;
                lstBoxControl = $find(sender.get_id());

                if (sender.get_id().substr(sender.get_id().length - ('lbUnassignedCourses').length) == 'lbUnassignedCourses') {
                    $("input[id$=chkAllAssignCourse]").prop('checked', false);
                }
                else {
                    $("input[id$=chkAllPrerequisite]").prop('checked', false);
                }

                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {

                    if (items.getItem(i).get_checked() == true) {
                        items.getItem(i).set_selected(true);
                        items.getItem(i).set_checked(false)
                    }
                }
            }

            function chk(Event) {

                var chkctl;
                var lstBoxControl;
                if (Event == 'AssignCourse') {
                    chkctl = $("input[id$=chkAllAssignCourse]")[0].checked;
                    lstBoxControl = $find($("[id$=lbUnassignedCourses]").attr("id"));
                }
                else {
                    chkctl = $("input[id$=chkAllPrerequisite]")[0].checked;
                    lstBoxControl = $find($("[id$=lbAvailableCourses]").attr("id"));
                }
                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {
                    items.getItem(i).set_selected(chkctl);
                }
            }

            //NEW 11-18-2014
            function chkprerequisite() {
                if ($("input:hidden[id$=ctrPrereq]").val() != "0") {
                    var text = "Course/s selected have prerequisites.</br>Do you want to continue?";
                    return radconfirm(text, callbackFn, 350, 180, null, "Course with Prerequisite");
                }
                else {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                }
            }

            function callbackFn(args) {
                if (args == true) {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                }
            }

            function confirmCallBackFn(arg) {
                if (arg == true) {
                    chkprerequisite();
                }
            }

            //CONFIRM CLOSING WINDOW
            var from;
            function onBeforeClose(sender, arg) {
                function callbackFunction(arg) {
                    if (arg) {
                        sender.remove_beforeClose(onBeforeClose);
                        sender.close();
                    }
                }
                arg.set_cancel(true);
                if (from == '1') {
                    sender.remove_beforeClose(onBeforeClose);
                    sender.close();
                }
                else {
                    radconfirm("Are you sure you want to close this window?", callbackFunction, 350, 180, null, "Close Window");
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridMyTeam">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridMyTeam" />
                    <telerik:AjaxUpdatedControl ControlID="DetailsView_Profile" />
                    <telerik:AjaxUpdatedControl ControlID="gridPreviousScores" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourseName" />
                    <telerik:AjaxUpdatedControl ControlID="gridEnrolmentForApproval" />
                    <telerik:AjaxUpdatedControl ControlID="notificationMyteam" />
                    <telerik:AjaxUpdatedControl ControlID="lblTranscriptName" />
                    <telerik:AjaxUpdatedControl ControlID="gridTranscript" />
                    <telerik:AjaxUpdatedControl ControlID="lblNameEnrollment" />
                    <telerik:AjaxUpdatedControl ControlID="gridLearningPlan" />
                    <telerik:AjaxUpdatedControl ControlID="lblPlanName" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridTranscript">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridTranscript" />
                    <telerik:AjaxUpdatedControl ControlID="gridPreviousScores" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourseName" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridLearningPlan">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridLearningPlan" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridEnrolmentForApproval">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEnrolmentForApproval" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourse1" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourse2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnApproveEnrolment">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="notificationMyteam" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelEnrolment">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="notificationMyteam" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--FOR ASSIGN COURSE--%>
            <telerik:AjaxSetting AjaxControlID="lbUnassignedCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbAssignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddtSubcategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAssignedCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="lbAssignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="gridAssignedCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnAssignCourseAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="btnAssignCourseAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAssignCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnYes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowPrereq" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel4" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowPrereq" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel4" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridAssignedCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END ASSIGN COURSE--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="windowViewProfile" runat="server" Title="Profile Viewer" Width="650px"
                Height="660px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:DetailsView ID="DetailsView_Profile" DataKeyNames="TWWID" runat="server" AutoGenerateRows="False"
                        GridLines="None" Width="100%">
                        <Fields>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <table width="100%" style="padding-top: 5px">
                                        <tr>
                                            <td style="width: 166px; padding-right: 15px">
                                                <div style="width: 165px; height: 166px; border: 1px solid #bfbfbf">
                                                    <asp:Image runat="server" ID="profilePhoto" alt="" Height="166" Width="165" ImageUrl='<%# Eval("TWWID", "~/Resources/PhotoHandler.ashx?id={0}") %>' />
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <asp:Label runat="server" ID="lblName" Font-Size="X-Large" ForeColor="Black" Text='<%# (Eval("Gender").ToString() == "Male") ? "Mr " + Eval("FirstName") + " " + Eval("LastName") : "Ms " + Eval("FirstName") + " " + Eval("LastName")%>' />
                                                </div>
                                                <div>
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' />
                                                </div>
                                                <div>
                                                    <asp:Label runat="server" ID="lblSite" Text='<%# Eval("Site") %>' />
                                                </div>
                                                <div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <hr />
                                    <table runat="server" id="tblProfile" class="tableProfile">
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_CIMNo" runat="server" Text="Label">CIM Number</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTWWID" Text='<%# Eval("TWWID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Gender" runat="server" Text="Label">Gender</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblGender" runat="server" Text='<%# Eval("gender") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_DOB" runat="server" Text="Label">Date of Birth</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDateOfBirth" runat="server" Text='<%# Eval("birthday") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_StartDate" runat="server" Text="Label">Start Date with Transcom</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblStartDateWithTranscom" runat="server" Text='<%# Eval("startdate") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Eadd" runat="server" Text="Label">Email Address</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblEmailAddress" Text='<%# Eval("email") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_MobNo" runat="server" Text="Label">Mobile No</asp:Label>Mobile No
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMobileNo" Text='<%# Eval("mobileno") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Region" runat="server" Text="Label">Region</asp:Label>Region
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblRegion" Text='<%# Eval("region") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Country" runat="server" Text="Label">Country</asp:Label>Country
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCountry" Text='<%# Eval("country") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Mgr" runat="server" Text="Label">Reporting Manager/Supervisor</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSupervisor" Text='<%# Eval("TLName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Dept" runat="server" Text="Label">Department</asp:Label>Department
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblDepartment" Text='<%# Eval("DepartmentName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Client" runat="server" Text="Label">Client</asp:Label>Client
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblClient" Text='<%# Eval("ClientName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_Campaign" runat="server" Text="Label">Campaign</asp:Label>Campaign
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCampaign" Text='<%# Eval("CampaignName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_PrimLang" runat="server" Text="Label">Primary Language</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLanguage" Text='<%# Eval("LanguageName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_CareerPath" runat="server" Text="Label">I am interested in pursuing the following career path in Transcom</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCareerPath" Text='<%# Eval("careerpath") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_CourseInterested" runat="server" Text="Label">Please tell me what courses you are interested in taking for your professional growth
                                                and development</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCoursesInterested" Text='<%# Eval("courseinterests") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelMyTeam">
                                                <asp:Label ID="lblMT_UpdateVia" runat="server" Text="Label">I am interested in receiving updates via</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblReceiveUpdates" Text='<%# Eval("updatevia_name") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewTranscript" runat="server" Title="Transcript Viewer"
                Width="900px" Height="550px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table width="100%" style="padding: 2px 10px 2px 10px">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblTranscriptName" Text="Employee Name" />
                            </td>
                            <td align="right" style="line-height: 20px; vertical-align: bottom">
                                <div style="vertical-align: bottom">
                                    <asp:LinkButton runat="server" ID="btnExportTranscript" CssClass="linkBtn" OnClick="btnExportTranscript_Click"
                                        Height="100%" OnClientClick="if(!confirm('Are you sure you want to export this employee transcript to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <telerik:RadGrid ID="gridTranscript" runat="server" OnNeedDataSource="GridTranscriptNeedDataSource"
                        GridLines="None" AutoGenerateColumns="False" CssClass="RadGrid2_detailtable"
                        Skin="Metro" AllowFilteringByColumn="True" AllowSorting="True" OnItemDataBound="GridTranscriptItemDataBound">
                        <GroupingSettings CaseSensitive="False" />
                        <FilterItemStyle HorizontalAlign="Center" />
                        <MasterTableView AutoGenerateColumns="False" AllowSorting="true" DataKeyNames="CourseID,TestCategoryID,CourseName"
                            NoDetailRecordsText="Transcript is empty." NoMasterRecordsText="Transcript is empty."
                            EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                            BorderStyle="None">
                            <HeaderStyle CssClass="gridHeaderWindow" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="CourseName" HeaderText="Course Name" DataField="CourseName"
                                    FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                    ShowFilterIcon="False" FilterControlToolTip="Search by Course Name">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn UniqueName="DateCourseTaken" HeaderText="Date Course Taken"
                                    DataField="DateCourseTaken" AllowFiltering="False" DataType="System.DateTime"
                                    DataFormatString="{0:yyyy-MM-dd}">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="ViewDetails"
                                    HeaderText="Previous Scores">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnPreviousScore" runat="server" ToolTip="View Previous Scores"
                                            OnClientClick="Show_ScoreHistory()" OnClick="btnPreviousScore_Click" Text="View" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn UniqueName="Score" HeaderText="Highest Score" DataField="Score"
                                    AllowFiltering="False">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn UniqueName="DateTestTaken" HeaderText="Date Test Taken"
                                    DataField="DateTestTaken" AllowFiltering="False" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridDateTimeColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="True">
                            <Selecting AllowRowSelect="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewLearningPlan" runat="server" Title="Learning Plan Viewer"
                Width="900px" Height="550px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table width="100%" style="padding: 2px 10px 2px 10px">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblPlanName" Text="Employee Name" />
                            </td>
                            <td align="right" style="line-height: 20px; vertical-align: bottom">
                                <div style="vertical-align: bottom">
                                    <asp:LinkButton runat="server" ID="btnExportPlan" CssClass="linkBtn" OnClick="btnExportPlan_Click"
                                        Height="100%" OnClientClick="if(!confirm('Are you sure you want to export this employee learning plan to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <telerik:RadGrid ID="gridLearningPlan" runat="server" OnNeedDataSource="GridLearningPlanNeedDataSource"
                        GridLines="None" AutoGenerateColumns="False" CssClass="RadGrid2_detailtable"
                        Skin="Metro" AllowFilteringByColumn="True" AllowSorting="True" OnItemDataBound="GridLearningPlanItemDataBound">
                        <GroupingSettings CaseSensitive="False" />
                        <FilterItemStyle HorizontalAlign="Center" />
                        <MasterTableView AutoGenerateColumns="False" AllowSorting="true" DataKeyNames="EnrollmentId,CourseID,EnrolleeCim,Title,Description,StatusId,EncryptedCourseID"
                            NoDetailRecordsText="Learning Plan is empty." NoMasterRecordsText="Learning Plan is empty."
                            EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                            BorderStyle="None">
                            <HeaderStyle CssClass="gridHeaderWindow" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Title" HeaderText="Course" DataField="Title"
                                    FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                    ShowFilterIcon="False" FilterControlToolTip="Search by Course name" DataType="System.String">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn UniqueName="CreateDate" HeaderText="Enrollment/Assigned Date"
                                    DataField="CreateDate" AllowFiltering="False" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                    DataType="System.String" DataFormatString="{0:yyyy-MM-dd}" AllowFiltering="False">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="CourseType" HeaderText="Course Type" DataField="CourseType"
                                    FilterControlWidth="100" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                    ShowFilterIcon="False">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="True" UniqueName="Status" HeaderText="Status"
                                    SortExpression="Status" DataField="Status" FilterControlWidth="100" CurrentFilterFunction="Contains"
                                    AutoPostBackOnFilter="True" ShowFilterIcon="False">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="True">
                            <Selecting AllowRowSelect="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowScoreHistory" runat="server" Title="Previous Scores"
                Width="600px" Height="400px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div style="vertical-align: top">
                        <div align="center" style="padding: 5px 0 5px 0">
                            <asp:Label runat="server" ID="lblCourseName" Text="Course Name" />
                        </div>
                        <telerik:RadGrid ID="gridPreviousScores" runat="server" AutoGenerateColumns="False"
                            Skin="Metro" CssClass="RadGrid2_detailtable" BorderStyle="None">
                            <MasterTableView EnableNoRecordsTemplate="False" ShowHeadersWhenNoRecords="True"
                                GridLines="None">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="DateStartTaken" HeaderText="Date Taken" UniqueName="DateStartTaken">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score" UniqueName="Score">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" UniqueName="Percentage">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowCourseEnrollment" runat="server" Title="Course Enrollment Viewer"
                Width="900px" Height="550px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True" OnClientClose="OnClientClose">
                <ContentTemplate>
                    <div align="center" style="padding: 5px 0 5px 0">
                        <asp:Label runat="server" ID="lblNameEnrollment" Text="Employee Name" />
                    </div>
                    <telerik:RadGrid runat="server" ID="gridEnrolmentForApproval" AutoGenerateColumns="False"
                        AllowPaging="False" AllowFilteringByColumn="False" AllowSorting="True" GridLines="None"
                        CssClass="RadGrid2_detailtable" Skin="Metro" OnNeedDataSource="gridEnrolmentForApproval_NeedDataSource"
                        BorderStyle="None">
                        <MasterTableView DataKeyNames="EnrollmentId,EnrolleeCim,Title,Description" ShowHeader="true"
                            AutoGenerateColumns="False" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                            NoMasterRecordsText="No enrollment record." BorderStyle="None" GridLines="None">
                            <HeaderStyle CssClass="gridHeaderWindow" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Title" HeaderText="Course Title" DataField="Title">
                                    <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Description" HeaderText="Description" DataField="Description"
                                    ItemStyle-Font-Size="90%">
                                    <HeaderStyle HorizontalAlign="Center" Width="250px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Status" HeaderText="Status" DataField="Status"
                                    ItemStyle-Font-Size="90%" Display="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="CreateDate" HeaderText="Date" DataField="CreateDate"
                                    DataFormatString="{0:M/d/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="Approve">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnApprove" runat="server" ToolTip="Approve" OnClientClick="ApproveEnrolment()"
                                            OnClick="btnApproveCancel_Click"><img style="border:0;" alt="" src="Images/thumb.png"/></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="CancelColumn">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnCancel" runat="server" ToolTip="Cancel" OnClientClick="DenyEnrolment()"
                                            OnClick="btnApproveCancel_Click"><img style="border:0;" alt="" src="Images/discard2.png"/></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="True">
                            <Selecting AllowRowSelect="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowApproveEnrolment" runat="server" Title="Approve Request"
                Width="500px" Height="300px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="center">
                        <div align="center" style="padding: 5px 0 5px 0">
                            <asp:Label runat="server" ID="lblCourse1" Text="Course Name" />
                        </div>
                        <div style="padding-bottom: 10px">
                            <telerik:RadTextBox runat="server" ID="txtAdvisory_Approve" EmptyMessage="- advisory -"
                                Width="350" Height="120px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                SelectionOnFocus="SelectAll" />
                        </div>
                        <div style="padding-bottom: 15px">
                            <asp:LinkButton runat="server" ID="btnApproveEnrolment" Text="Approve Request" CssClass="linkBtn"
                                OnClick="btnApproveEnrolment_Click" OnClientClick="if(!confirm('Are you sure you want to Approve this request?')) return false;" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowDenyEnrolment" runat="server" Title="Decline Request"
                Width="500px" Height="300px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="center">
                        <div align="center" style="padding: 5px 0 5px 0">
                            <asp:Label runat="server" ID="lblCourse2" Text="Course Name" />
                        </div>
                        <div style="padding-bottom: 10px">
                            <telerik:RadTextBox runat="server" ID="txtAdvisory_Deny" EmptyMessage="- advisory -"
                                Width="350" Height="120px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                SelectionOnFocus="SelectAll" />
                            <asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="txtAdvisory_Deny"
                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="decline" />
                        </div>
                        <div style="padding-bottom: 15px">
                            <asp:LinkButton runat="server" ID="btnCancelEnrolment" Text="Decline Request" CssClass="linkBtn"
                                OnClick="btnCancelEnrolment_Click" ValidationGroup="decline" OnClientClick="if(!confirm('Are you sure you want to Decline this request?')) return false;" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="AssignCourseWindow" runat="server" Title="Select Course/s"
                Width="700px" Height="530px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                ReloadOnShow="true" VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel3" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnAssignCourseAdd">
                        <div class="insidecontainer2">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="background: #e4e4e4; height: 23px; border-style: solid; border-color: #C6C7D2;
                                            border-width: 1px 1px 0 1px">
                                            <asp:CheckBox runat="server" ID="chkAllAssignCourse" Text="All" Checked="false" OnClick="chk('AssignCourse');"
                                                CssClass="lbChk" />
                                        </div>
                                        <telerik:RadListBox ID="lbUnassignedCourses" runat="server" Height="200px" Width="100%"
                                            AllowTransfer="true" AutoPostBackOnTransfer="true" TransferMode="Move" EmptyMessage="No Unassigned Course"
                                            SelectionMode="Multiple" TransferToID="lbAssignedCourses" OnItemDataBound="lbUnassignedCourses_ItemDataBound"
                                            OnTransferred="lbUnassignedCourses_Transferred" OnInserted="lbUnassignedCourses_Inserted"
                                            OnClientItemChecked="OnClientItemChecked" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                                            OnClientTransferring="OnTransferring" CheckBoxes="true">
                                            <ButtonSettings Position="Bottom" HorizontalAlign="Center" RenderButtonText="true" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourseUnassigned" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Title\"]")%>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadListBox ID="lbAssignedCourses" runat="server" Height="200px" Width="100%"
                                            AutoPostBackOnTransfer="true" EmptyMessage="No Course Selected" CheckBoxes="false">
                                            <ButtonSettings ShowDelete="false" ShowReorder="false" />
                                            <HeaderTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="60%" align="center">
                                                            <asp:Label runat="server" Text="Course Name" Font-Size="9pt" />
                                                        </td>
                                                        <td width="25%" align="center">
                                                            <asp:Label runat="server" Text="Due Date" Font-Size="9pt" />
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:Label runat="server" Text="Prerequisite?" Font-Size="9pt" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="60%" align="center">
                                                            <asp:Label ID="lblCourseAssigned" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Title\"]")%>'></asp:Label>
                                                        </td>
                                                        <td width="30%" align="center">
                                                            <telerik:RadDatePicker ID="dpCourseAssigned" runat="server" EnableTyping="False"
                                                                SelectedDate='<%# DateTime.Today.AddMonths(1) %>' ShowPopupOnFocus="True" Width="140">
                                                                <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td id="Td1" width="10%" runat="server" align="center">
                                                            <asp:Label ID="lblAssignedPrerequisite" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Prerequisite\"]")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:LinkButton runat="server" ID="btnAssignCourseAdd" Text="Add" CssClass="linkBtn"
                                            OnClientClick="radconfirm('Are you sure you want to add this collection?', confirmCallBackFn, 350, 180, null, 'Close Window'); return false;" />
                                        |
                                        <asp:LinkButton runat="server" ID="btnAssignCourseCancel" Text="Cancel" CssClass="linkBtn"
                                            CausesValidation="False" OnClick="btnAssignCourseCancel_Click" OnClientClick="if(!confirm('Are you sure you want to close this window?')) return false;" />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="containerdefault">
                <table width="100%">
                    <tr>
                        <td>
                            <telerik:RadTabStrip ID="tabStrip1" runat="server" MultiPageID="mpMyTeam"
                                ShowBaseLine="True" Width="100%" CausesValidation="False" OnTabClick="tsMyTeam_TabClick">
                                <Tabs>
                                    <telerik:RadTab Text="My Team" Selected ="true" value ="1"/>
                                    <telerik:RadTab Text="Assign Course"  value ="2"/>
                                </Tabs>
                            </telerik:RadTabStrip>
                        </td>
                        <td align="right" style="line-height: 20px; vertical-align: bottom">
                            <div style="vertical-align: bottom">
                                <asp:LinkButton runat="server" ID="btnExport" CssClass="linkBtn" OnClick="btnExport_Click"
                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Team records to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="insidecontainer">
                    <telerik:RadMultiPage ID="mpMyTeam" runat="server" Width="100%">
                        <telerik:RadPageView ID="pvTeamList" runat="server" Selected="true">
                            <telerik:RadGrid runat="server" ID="gridMyTeam" AutoGenerateColumns="False" AllowSorting="True"
                                AllowFilteringByColumn="False" CssClass="RadGrid1" Skin="Metro" OnNeedDataSource="gridMyTeam_NeedDataSource"
                                EnableLinqExpressions="False" OnItemCommand="gridMyTeam_ItemCommand" OnItemDataBound="gridMyTeam_ItemDataBound">
                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="TwwId,Firstname,Lastname"
                                    NoMasterRecordsText="No Team Record." EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                                    TableLayout="Auto">
                                    <CommandItemSettings ShowExportToExcelButton="true" />
                                    <Columns>
                                        <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Width="60" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnProfile" runat="server" ToolTip="View Profile" OnClientClick="Show_ProfileWindow()"
                                                    OnClick="btnProfile_Click">
                                                                            <img style="border:0;" alt="" src="Images/profile.png"/>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Width="60" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnTranscript" runat="server" ToolTip="View Transcript" OnClientClick="Show_TranscriptWindow()"
                                                    OnClick="btnTranscript_Click">
                                                                            <img style="border:0;"  alt="" src="Images/transcript.png"/>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Width="60" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnViewTUTest" runat="server" ToolTip="View Learning Plan" OnClientClick="Show_LearningPlanWindow()"
                                                    OnClick="btnLearningPlan_Click">
                                                                            <img style="border:0;"  alt="" src="Images/learning.png"/>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn UniqueName="TwwId" HeaderText="CIM#" DataField="TwwId">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn UniqueName="Firstname" HeaderText="First Name" DataField="Firstname">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn UniqueName="Lastname" HeaderText="Last Name" DataField="Lastname">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="PendingEnrolments"
                                            HeaderText="Pending Enrollment">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEnrolmentCount" runat="server" Text="0" OnClientClick="Show_CourseEnrollment()"
                                                    OnClick="btnEnrolmentCount_Click" Font-Underline="False" ForeColor="red" Font-Names="Arial"
                                                    Font-Size="8pt" CssClass="btnRounded" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="True">
                                    <Selecting AllowRowSelect="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="pvAssignCourse" runat="server">
                            <div class="paneldefault">
                                <div class="insidecontainer">
                                    <table class="tabledefault">
                                        <tr>
                                            <td class="tblLabel" width="14%">
                                            </td>
                                            <td class="tblfield" width="48%">
                                            </td>
                                            <td class="tblfield" width="38%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tblLabel">
                                            </td>
                                            <td class="tblfield" colspan="2">
                                                <asp:ValidationSummary ID="vsAssign" ShowMessageBox="True" ShowSummary="True" DisplayMode="BulletList"
                                                    HeaderText="Please complete the following field/s:" runat="server" ValidationGroup="vgAssign"
                                                    CssClass="displayerror" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tblLabel">
                                                <asp:Label ID="lblAssType" runat="server" Text="Assignment Type"></asp:Label>
                                                
                                            </td>
                                            <td class="tblfield" colspan="2">
                                                <telerik:RadComboBox ID="cbAssignmentType" runat="server" AutoPostBack="True" EmptyMessage="- select type -"
                                                    DropDownAutoWidth="Enabled" MaxHeight="300px" CausesValidation="False" DataSourceID="dsAssignmentTypes"
                                                    DataTextField="CourseAssignmentType" DataValueField="CourseAssignmentTypeID"
                                                    OnSelectedIndexChanged="cbAssignmentType_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <telerik:RadComboBox ID="cbDirectReports" runat="server" AutoPostBack="True" CausesValidation="False"
                                                    DataSourceID="ds_DirectReports" DataTextField="Name" DataValueField="TwwId" DropDownAutoWidth="Enabled"
                                                    EmptyMessage="- select emp -" MaxHeight="300px" OnSelectedIndexChanged="cbDirectReports_SelectedIndexChanged"
                                                    ToolTip="Select Cim # of your direct reports" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tblLabel">
                                                <asp:Label ID="lblAssFil" runat="server" Text="Assignment Filters"></asp:Label>
                                               
                                            </td>
                                            <td class="tblfield">
                                                <telerik:RadComboBox ID="cbCategory" DataTextField="Category" DataValueField="CategoryID" AutoPostBack="True"
                                                    runat="server"  EmptyMessage="- select category -" OnSelectedIndexChanged="CbCategorySelectedIndexChanged"
                                                    CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                    <%--AutoPostBack="True"--%>
                                                <telerik:RadDropDownTree runat="server" ID="ddtSubcategory" DefaultMessage="- select subcategory -"
                                                    DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                    DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" AutoPostBack="True"
                                                    OnClientEntryAdding="OnClientEntryAdding" Skin="Default" OnEntryAdded="ddtSubcategory_EntryAdded">
                                                    <DropDownSettings Width="400px" Height="250px" />
                                                    <%--AutoPostBack="True"--%>
                                                </telerik:RadDropDownTree>
                                                <asp:LinkButton runat="server" ID="btnAssignCourse" Text="Choose Course/s" CssClass="linkBtn"
                                                    OnClick="btnAssignCourse_Click" Font-Size="8.5" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tblLabel">
                                            <asp:Label ID="lblCurrAss" runat="server" Text="Currently Assigned"></asp:Label>
                                                
                                            </td>
                                            <td class="tblfield" colspan="2">
                                                <telerik:RadGrid runat="server" ID="gridAssignedCourse" AutoGenerateColumns="False"
                                                    GridLines="None" OnNeedDataSource="gridAssignedCourse_NeedDataSource" OnItemCommand="gridAssignedCourse_ItemCommand"
                                                    EnableLinqExpressions="True" ValidationSettings-EnableValidation="True" CssClass="RadGrid1" OnItemDataBound="gridAssignedCourse_ItemDataBound"
                                                    Skin="Metro">
                                                    <GroupingSettings CaseSensitive="False" />
                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="CourseAssignmentID,CIMName"
                                                        PageSize="10" EditMode="InPlace" NoMasterRecordsText="No Assigned Course for this team."
                                                        EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" AllowPaging="True"
                                                        TableLayout="Auto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        AlternatingItemStyle-HorizontalAlign="Center">
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="70px" EditImageUrl="Images/editpen2.png">
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                            </telerik:GridButtonColumn>
                                                            <telerik:GridBoundColumn UniqueName="CourseID" HeaderText="ID" DataField="CourseID"
                                                                AllowFiltering="False" ReadOnly="True" Display="False">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="SubcategoryID" HeaderText="Subcategory ID" DataField="SubcategoryID"
                                                                AllowFiltering="False" ReadOnly="True" Display="False">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                                AllowFiltering="False" ReadOnly="true" />
                                                            <telerik:GridCheckBoxColumn UniqueName="EnrolmentRequired" DataField="EnrolmentRequired"
                                                                HeaderText="Enrolment Required?" AllowFiltering="False" SortExpression="EnrolmentRequired"
                                                                ReadOnly="True" />
                                                            <telerik:GridBoundColumn UniqueName="Tenure" HeaderText="Required Tenure (yrs)" DataField="Tenure"
                                                                AllowFiltering="False" ReadOnly="True" />
                                                            <telerik:GridBoundColumn UniqueName="TenureInRole" HeaderText="Required Tenure in Role (yrs)"
                                                                DataField="TenureInRole" AllowFiltering="False" ReadOnly="True" />
                                                            <telerik:GridTemplateColumn UniqueName="DueDate" HeaderText="Assignment Due Date"
                                                                DataField="DueDate" SortExpression="DueDate" EditFormHeaderTextFormat="Due Date"
                                                                ShowFilterIcon="False">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblDueDate" Text='<%# Eval("DueDate" , "{0:d}") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadDatePicker ID="dpDueDateEdit" runat="server" Width="140" EnableTyping="False"
                                                                        SelectedDate='<%# Bind("DueDate") %>' ShowPopupOnFocus="True">
                                                                        <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                    </telerik:RadDatePicker>
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                        <Selecting AllowRowSelect="True" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>
            </div>
            <div>
                <asp:HiddenField ID="ctrPrereq" runat="server" />
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="dsAssignmentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT CourseAssignmentTypeID,CourseAssignmentType FROM tbl_TranscomUniversity_lkp_CourseAssignmentType WHERE Active = 1
            AND CourseAssignmentTypeID IN (3,5)" />
        <asp:SqlDataSource ID="ds_DirectReports" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="pr_TranscomUniversity_Lkp_GetDirectReports" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="hidCIM" DefaultValue="0" Name="CIM" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:HiddenField runat="server" ID="hidCIM" />
    </div>
</asp:Content>
