﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CourseRedirect.aspx.cs" Inherits="CourseRedirect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" Runat="Server">
    <div style="margin-top: 100px; text-align: center; height: 200px">
        <iframe id = "CourseURL" height = "100%" width = "100%" />
        <asp:Label ID="lblNotes" runat="server" Text="" Font-Size="Larger"></asp:Label>
    </div>
</asp:Content>
