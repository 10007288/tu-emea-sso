﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Panel runat="server" DefaultButton="btnProceed" ID="pnlMain" CssClass="pnlMain">
            <asp:Table runat="server" ID="tblLogin" HorizontalAlign="center" Width="450px" defaultbutton="btnLogin"
                CssClass="tblLogin">
                <asp:TableRow>
                    <asp:TableCell>
                        <table width="100%" align="center">
                            <tr style="height: 60px;">
                                <td colspan="2" style="color: #FFF; text-align: center;">
                                    Change Password
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Old Password
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtOldPassword" runat="server" LabelWidth="64px" Width="160px"
                                        AutoCompleteType="None" AutoComplete="Off" TextMode="Password" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtOldPassword"
                                        Font-Size="Medium" ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="changepass"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    New Password
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtNewPassword" TextMode="Password" runat="server" AutoCompleteType="None"
                                        AutoComplete="Off" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtNewPassword"
                                        Font-Size="Medium" ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="changepass"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Re-Type New Password
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtRetypePassword" TextMode="Password" runat="server" AutoCompleteType="None"
                                        AutoComplete="Off" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtRetypePassword"
                                        Font-Size="Medium" ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="changepass"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Security Question
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cboSQ" runat="server" AppendDataBoundItems="true" EmptyMessage="Select"
                                        DataSourceID="dsSQ" DataTextField="SecurityQuestion" DataValueField="ID" DropDownAutoWidth="Enabled">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="reqSQ" runat="server" ControlToValidate="cboSQ" Text="*"
                                        ValidationGroup="changepass" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:SqlDataSource ID="dsSQ" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
                                        SelectCommand="SELECT ID, SecurityQuestion FROM refSecurityQuestions"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Security Answer
                                </td>
                                <td>
                                    <p>
                                        <asp:TextBox ID="txtSA" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSA"
                                            ValidationGroup="changepass" Text="*" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" style="padding-right: 55px; padding-top: 10px">
                                    <div id="managecontrol" style="width: 100%">
                                        <asp:LinkButton ID="btnProceed" runat="server" Text="Proceed" OnClick="btnProceed_Click"
                                            ValidationGroup="changepass" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell></asp:TableRow>
            </asp:Table>
        </asp:Panel>
    </div>
</asp:Content>
