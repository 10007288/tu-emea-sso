﻿using System;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Register : Page
{
    Int32 _Reg_Id, _Country_Id, _Site_Id, _Sup_Id;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadRegion();
        }

    }

    protected void BtnSaveClick(object sender, EventArgs e)
    {
        HttpPostedFile file = fileUpload.PostedFile;

        if ((file != null) && (file.ContentLength > 0))
        {
            if (IsImage(file) == false)
            {
                divUploader.InnerText = String.Format("Note: Only .JPG, .JPEG or .PNG format will be accepted.");
                return;
            }
        }

        var iFileSize = file.ContentLength;

        if (iFileSize > 2097152)
        {
            divUploader.InnerText = String.Format("Note: 2MB file size will be accepted. You are trying to upload greater than maximum limit.");
            return;
        }

        var bytes = new byte[file.ContentLength];
        file.InputStream.Read(bytes, 0, file.ContentLength);

        ContInsertUser(bytes);


        
    }

    public string GetComboBoxSelectedItems(RadComboBox combo, string type)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            var sb = new StringBuilder();

            foreach (var item in combo.CheckedItems)
            {
                if (type == "id")
                    sb.Append(item.Value + ",");
                else
                    sb.Append(item.Text);
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                collection = sb.ToString().TrimEnd(',');
            }
        }
        return collection;
    }

    private void ContInsertUser(byte[] bytes)
    {
        //var selected = cbReceiveUpdates.Items.Cast<ListItem>().Where(li => li.Selected).ToList();

        var selecteditems = GetComboBoxSelectedItems(cbReceiveUpdates, "id");
        var strselecteditems = GetComboBoxSelectedItems(cbReceiveUpdates, "str");

        var supporttype = CheckBoxList1.Items.Cast<ListItem>()
                                       .Where(item => item.Selected)
                                       .Aggregate("", (current, item) => current + (item.Value + ","));
        var strsupporttype = CheckBoxList1.Items.Cast<ListItem>()
                                       .Where(item => item.Selected)
                                       .Aggregate("", (current, item) => current + (item.Text + ","));

        var supporttypeselecteditems = supporttype.TrimEnd(new[] { ',', ' ' });
        var strsupporttypeselecteditems = strsupporttype.TrimEnd(new[] { ',', ' ' });

        int? department;
        string otherDept;
        if (cbDepartment.Text == "Other")
        {
            department = null;
            otherDept = txtDepartment.Text;
        }
        else
        {
            department = Convert.ToInt32(cbDepartment.SelectedValue);
            otherDept = null;
        }

        int? client;
        string otherClient;
        try
        {
            switch (cbClient.Text)
            {
                case "Other":
                    client = null;
                    otherClient = txtClient.Text;
                    break;
                case "Shared":
                    client = null;
                    otherClient = cbClient.Text;
                    break;
                default:
                    client = Convert.ToInt32(cbClient.SelectedValue);
                    otherClient = null;
                    break;
            }
        }
        catch
        {
            client = null;
            otherClient = null;
        }

        int? careerpath;
        string otherCareerpath;
        if (cbCareerPath.Text == "Other")
        {
            careerpath = null;
            otherCareerpath = txtCareerPath.Text;
        }
        else
        {
            careerpath = Convert.ToInt32(cbCareerPath.SelectedValue);
            otherCareerpath = null;
        }

        var qry = Registration.GetUserInfo(txtTWWID.Text).FirstOrDefault();
        if (qry != null)
        {
            txtFName.Text = qry.FN;
            txtLName.Text = qry.LN;
            lblCountry.Text = qry.Country;
            lblSite.Text = qry.Site;
            lblRegion.Text = qry.Region;
            lblSupervisor.Text = qry.SupervisorName;

            if (lblRegion.Text != String.Empty && lblRegion.Text != "Label")
            {
                cbRegion.Visible = false;
                _Reg_Id = Convert.ToInt32(qry.RegionID);
            }
            else
            {
                lblRegion.Visible = false;
                cbRegion.Visible = true;
                _Reg_Id = Convert.ToInt32(cbRegion.SelectedValue);
            }


            if (lblCountry.Text != String.Empty && lblCountry.Text != "Label")
            {
                cbCountry.Visible = false;
                lblCountry.Visible = true;
                _Country_Id = Convert.ToInt32(qry.CountryID);
            }
            else
            {
                lblCountry.Visible = false;
                cbCountry.Visible = true;
                cbCountry.Enabled = true;
                _Country_Id = Convert.ToInt32(cbCountry.SelectedValue);
            }

            if (lblSite.Text != String.Empty && lblSite.Text != "Label")
            {
                cbSite.Visible = false;
                _Site_Id = Convert.ToInt32(qry.SiteID);
            }
            else
            {
                lblSite.Visible = false;
                cbSite.Visible = true;
                _Site_Id = Convert.ToInt32(cbSite.SelectedValue);
            }


            if (lblSupervisor.Text != String.Empty && lblSupervisor.Text != "Label")
            {
                cbSupervisor.Visible = false;
                _Sup_Id = Convert.ToInt32(qry.SupID);
            }
            else
            {
                lblSupervisor.Visible = false;
                cbSupervisor.Visible = true;
                _Sup_Id = Convert.ToInt32(cbSupervisor.SelectedValue);
            }
        }

        var positionChkr = DataHelper.GetPosition(Convert.ToInt32(txtTWWID.Text));
        string _Role = positionChkr.NombreCatTranscomDesc.ToString();

        var retVal1 = Registration.InsertUser(txtFName.Text,
                        txtLName.Text,
                        txtTWWID.Text,
                        txtEmailAddress.Text,
                        Password.CreateHash(txtPassword.Text),
                        0,
                        false,
                        true,
                        Convert.ToInt32(_Role),
                        client,
                        otherClient,
                        _Reg_Id,
                        _Country_Id,
                        _Site_Id,
                        Convert.ToInt32(cbLanguage.SelectedValue),
                        txtMobileNoIC.Text + txtMobileNo.Text, txtSpecializationSkills.Text,
                        Convert.ToInt32(cbEducLevel.SelectedValue),
                        txtCourseMajor.Text, txtCoursesTrainings.Text,
                        careerpath, otherCareerpath,
                        txtCoursesInterested.Text,
                        bytes, selecteditems,
                        department,
                        otherDept,
                        supporttypeselecteditems,
                        _Sup_Id, strselecteditems,
                        strsupporttypeselecteditems);

        var retVal2 = Registration.InsertSecurityAnswer(Convert.ToInt32(cbSecurityQuestion.SelectedValue),
                        txtTWWID.Text,
                        txtSecurityAnswer.Text.ToLower());

        if (retVal1 == 1 && retVal2 == 1)
        {
            PanelNotification.Visible = true;
            PanelNotification.Attributes.Add("class", "msgnotification");
            lblMessage.Text =
                String.Format(
                    "Congratulations! You have registered a new account. Please click <a href='Login.aspx'>here</a> to sign in.");
            var cimnumber = txtTWWID.Text;
            ClearControls();
            PanelNotification.Focus();
            Response.Redirect("~/Login.aspx?SessionID=" + cimnumber);

        }
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    private void ClearControls()
    {
        txtFName.Text = "";
        txtLName.Text = "";
        txtPassword.Text = "";
        txtTWWID.Text = "";
        txtEmailAddress.Text = "";
        txtSecurityAnswer.Text = "";

        cbCountry.Text = "";
        cbSite.Text = "";
        cbClient.Text = "";
        cbSecurityQuestion.Text = "";
        cbSupervisor.Text = "";
        cbDepartment.Text = "";
        cbReceiveUpdates.Text = "";

        cbCountry.Items.Clear();
        cbSite.Items.Clear();
        cbClient.ClearSelection();
        cbSecurityQuestion.ClearSelection();
        cbSupervisor.ClearSelection();
        cbDepartment.ClearSelection();

        cbRegion.ClearSelection();

        cbLanguage.Text = "";
        cbLanguage.ClearSelection();

        txtDepartment.Visible = false;
        txtDepartment.Text = "";
        txtClient.Visible = false;
        txtClient.Text = "";

        CheckBoxList1.ClearSelection();

        txtMobileNoIC.Text = "";
        txtMobileNo.Text = "";
        txtSpecializationSkills.Text = "";

        cbEducLevel.Text = "";
        cbEducLevel.ClearSelection();

        txtCourseMajor.Text = "";
        txtCoursesTrainings.Text = "";

        cbCareerPath.ClearSelection();
        cbReceiveUpdates.ClearSelection();

        txtCoursesInterested.Text = "";

        divAutoPopulateFields.Visible = false;
        divGender.InnerText = "";
        divBirthDate.InnerText = "";
        divRole.InnerText = "";
        divStartDate.InnerText = "";

        //divListbox.InnerText = "";
        divNotifyError.InnerText = "";
    }

    protected void CbRegionSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        LoadCountries(Convert.ToInt32(e.Value));
    }

    protected void CbCountrySelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cbSite.Text = "";
        //cbClient.Text = "";

        cbSite.Items.Clear();
        cbClient.Items.Clear();
        LoadClient(Convert.ToInt32(e.Value));
        LoadSite(Convert.ToInt32(e.Value));
    }

    private String GetValueScalar(Int32 RetMode, String SrchID, String SrchID2)
    {
        string RetVal = "";
        if (RetMode == 1)
        {
            var db = new UserDBDataContext();

            RetVal = (from a in db.Regions
                      where a.Description == SrchID
                      select new
                      {
                          a.ID
                      }
                      ).Single().ID.ToString();

        }
        else if (RetMode == 2)
        {
            var db = new UserDBDataContext();

            RetVal = (from a in db.Countries
                      where a.Country1 == SrchID && a.RegionID == Convert.ToInt32(SrchID2) && a.ACTIVE == 1
                      select new
                      {
                          a.ID
                      }
                      ).Single().ID.ToString();
        }
        else if (RetMode == 3)
        {
            var db = new UserDBDataContext();

            RetVal = (from a in db.Locations
                      where a.Description == SrchID && a.Active == 1 && a.CountryID == Convert.ToInt32(SrchID2)
                      select new
                      {
                          a.ID
                      }
                      ).Single().ID.ToString();
        }
        else if (RetMode == 4)
        {
            var db = new UserDBDataContext();
            RetVal = (from a in db.vw_AllActiveSupervisors
                      where a.supervisorname == SrchID
                      select new
                      {
                          a.TWWID
                      }
                      ).Single().TWWID.ToString();
        }
        return RetVal;
    }

    private void LoadRegion()
    {
        var db = new UserDBDataContext();

        var qry = (from a in db.Regions
                   where a.ACTIVE == 1
                   orderby a.Description
                   select new
                   {
                       a.ID,
                       a.Description
                   }
                  ).ToList();

        cbRegion.DataSource = qry;
        cbRegion.DataBind();
    }

    private void LoadCountries(int regionId)
    {
        var db = new UserDBDataContext();

        if (string.IsNullOrEmpty(cbRegion.SelectedValue)) return;
        var qry = (from a in db.Countries
                   where a.RegionID == regionId && a.ACTIVE == 1
                   orderby a.Country1
                   select new
                   {
                       a.ID,
                       a.Country1
                   }
                  ).ToList();

        cbCountry.DataSource = qry;
        cbCountry.DataBind();
    }

    private void LoadSite(int countryId)
    {
        var db = new UserDBDataContext();

        if (string.IsNullOrEmpty(cbCountry.SelectedValue)) return;
        var qry = (from a in db.Locations
                   where a.CountryID == countryId && a.Active == 1
                   orderby a.Description
                   select new
                   {
                       a.ID,
                       a.Description
                   }
                  ).ToList();

        cbSite.DataSource = qry;
        cbSite.DataBind();
    }

    private void LoadClient(int locationid)
    {
        var db = new UserDBDataContext();
        cbClient.Items.Clear();
        RadComboBoxItem rbi = new RadComboBoxItem();
        rbi.Text = "Others";
        cbClient.Items.Add(rbi);
        RadComboBoxItem rbi2 = new RadComboBoxItem();
        rbi2.Text = "Shared";
        cbClient.Items.Add(rbi2);
        //if (string.IsNullOrEmpty(cbCountry.SelectedValue)) return;
        var qry = (from a in db.Clients
                   where ((a.countryID == locationid.ToString() || a.countryID == null) && a.Active == 1)
                   orderby a.description
                   select new
                   {
                       a.ID,
                       a.description
                   }
                  ).ToList();
        //|| a.countryID == null
        cbClient.AppendDataBoundItems = true;
        cbClient.DataSource = qry;
        cbClient.DataBind();
    }

    protected void TxtTwwidTextChanged(object sender, EventArgs e)
    {
        if (!Registration.ChkUserIfTrueEmployee(txtTWWID.Text))
        {
            divAutoPopulateFields.Visible = false;
            UserIsEmployee.InnerText = String.Format("Your TWWID provided does not exist on valid Employees list.");
            txtTWWID.Text = "";
            txtTWWID.Focus();
        }
        else if (Registration.ChkTwwidIfExist(txtTWWID.Text))
        {
            divAutoPopulateFields.Visible = false;
            UserIsEmployee.InnerText = String.Format("Your ID provided is already registered. Please contact Admin for assistance.");
            txtTWWID.Text = "";
            txtTWWID.Focus();
        }
        else
        {
            var qry = Registration.GetUserInfo(txtTWWID.Text).FirstOrDefault();
            if (qry != null)
            {
                txtFName.Text = qry.FN;
                txtLName.Text = qry.LN;
                lblCountry.Text = qry.Country;
                lblSite.Text = qry.Site;
                lblRegion.Text = qry.Region;
                
                if (qry.SupervisorName != null)
                {
                    lblSupervisor.Text = qry.SupervisorName;
                    cbSupervisor.Visible = false;
                    lblSupervisor.Visible = true;
                }
                else
                {
                    cbSupervisor.Visible = true;
                    lblSupervisor.Visible =false;
                }
                divAutoPopulateFields.Visible = true;
                if (qry.CountryID != null)
                {
                    int CID_ = Int32.Parse(qry.CountryID.ToString());
                    LoadClient(CID_);
                }
                switch (qry.Gender)
                {
                    case "M":
                        //cbGender.SelectedValue = "M";
                        divGender.InnerText = "Male";
                        break;
                    case "F":
                        //cbGender.SelectedValue = "F";
                        divGender.InnerText = "Female";
                        break;
                }

                //divBirthDate.InnerText = Convert.ToDateTime(qry.BirthDate).ToLongDateString();
                divStartDate.InnerText = Convert.ToDateTime(qry.StartDate).ToLongDateString();
                divRole.InnerText = qry.Role;

                if (lblSite.Text != String.Empty && lblSite.Text != "Label")
                {
                    cbSite.Visible = false;
                }
                else
                {
                    lblSite.Visible = false;
                    cbSite.Visible = true;
                }

                if (lblCountry.Text != String.Empty && lblCountry.Text != "Label")
                {
                    cbCountry.Visible = false;
                    lblCountry.Visible = true;
                }
                else
                {
                    lblCountry.Visible = false;
                    cbCountry.Visible = true;
                    cbCountry.Enabled = true;
                }

                if (lblRegion.Text != String.Empty && lblRegion.Text != "Label")
                {
                    cbRegion.Visible = false;
                }
                else
                {
                    lblRegion.Visible = false;
                    cbRegion.Visible = true;
                }

                if (lblSupervisor.Text != String.Empty && lblSupervisor.Text != "Label")
                {
                    cbSupervisor.Visible = false;
                }
                else
                {
                    lblSupervisor.Visible = false;
                    cbSupervisor.Visible = true;
                }
            }
            else
            {
                divAutoPopulateFields.Visible = false;
            }

            UserIsEmployee.InnerText = "";
            txtEmailAddress.Focus();
        }
    }

    protected void cbSite_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        LoadClient(Convert.ToInt32(e.Value));
    }

    protected void cbDepartment_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbDepartment.Text == "Other")
        {
            txtDepartment.Visible = true;
            //rfvOtherDepartment.Enabled = true;
        }
        else
        {
            txtDepartment.Visible = false;
            //rfvOtherDepartment.Enabled = false;
        }
    }

    protected void cbClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbClient.Text == "Other")
        {
            txtClient.Visible = true;
            //rfvOtherClient.Enabled = true;
        }
        else
        {
            txtClient.Visible = false;
            //rfvOtherClient.Enabled = false;
        }
    }

    protected void cbCareerPath_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbCareerPath.Text == "Other")
        {
            txtCareerPath.Visible = true;
            rfvOtherCareerPath.Enabled = true;
        }
        else
        {
            txtCareerPath.Visible = false;
            rfvOtherCareerPath.Enabled = false;
        }
    }
}