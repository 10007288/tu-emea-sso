﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MyPlan.aspx.cs" Inherits="MyPlan" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server" />
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            Sys.Application.add_load(function () {
                $('.progressBarContainer').each(function (i, obj) {
                    var hidValue = $('input[type=hidden]', this).val();
                    jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                    var progressBarWidth = hidValue * $('#progressBar').width() / 100;
                    jQuery('[id=progress]', this).animate({ width: progressBarWidth }, 3000).html("&nbsp;");

                    if (hidValue >= 0 && hidValue <= 50) {
                        jQuery('[id=progress]', this).addClass('red');
                    }
                    if (hidValue >= 50 && hidValue <= 75) {
                        jQuery('[id=progress]', this).addClass('yellow');
                    }
                    if (hidValue >= 75 && hidValue <= 100) {
                        jQuery('[id=progress]', this).addClass('green');
                    }
                });
            });

            function ShowNote(message) {
                radalert(message, 380, 250, 'Accomplish Prerequisite');
                return false;
            }

            function LaunchCourse(urlWithParams, courseId, statusId, title, sender, args) {

            }

            function RequestFullScreen(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportExcel") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0) {
                    args.set_enableAjax(false);
                }
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollLeftOffset;
                var scrollTopOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function toggleExpandState(sender, args) {
                if (args.get_tableView().get_name() == "gridLearningPlan") {
                    args.set_cancel(true);
                }
                var item = args.get_item();
                args.get_tableView().fireCommand("ExpandCollapse", item.get_itemIndexHierarchical());
            }

            //RADWINDOW
            function CloseEnrollCourseWindow(from1) {
                from = from1;
                var window = $find('<%=windowEnrollCourse.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }

            //CONFIRM CLOSING WINDOW
            var from;
            function onBeforeClose(sender, arg) {
                function callbackFunction(arg) {
                    if (arg) {
                        sender.remove_beforeClose(onBeforeClose);
                        sender.close();
                    }
                }
                arg.set_cancel(true);
                if (from == '1') {
                    sender.remove_beforeClose(onBeforeClose);
                    sender.close();
                }
                else {
                    radconfirm("Are you sure you want to close this window?", callbackFunction, 350, 180, null, "Close Window");
                }
            }
            
        </script>
        <script type="text/javascript">
            function confirmCallBackFn(arg) {
                var ajaxManager = $find("<%=RadAjaxManager1.ClientID%>");
                if (arg) {
                    ajaxManager.ajaxRequest('ok');
                }
                else {
                    ajaxManager.ajaxRequest('cancel');
                }
            }
        </script>
    </telerik:RadScriptBlock>
    <asp:Label ID="lblRadConfirm_Res" runat="server" Text="" Visible="false"></asp:Label>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager2_AjaxRequest">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblRadConfirm_Res" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridEmployee">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEmployee" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourseName1" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourseName2" />
                    <telerik:AjaxUpdatedControl ControlID="gridEnrollmentHistory" />
                    <telerik:AjaxUpdatedControl ControlID="gridAssignmentHistory" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEmployee" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                    <telerik:AjaxUpdatedControl ControlID="rblExpandCollapse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbDirectReports">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rblExpandCollapse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEmployee" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEnrollCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEmployee" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager RenderMode="Lightweight" ID="rmPopup" runat="server" EnableShadow="true">
    </telerik:RadWindowManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="windowEnrollmentHistory" runat="server" Title="Course Enrollment History"
                Width="700px" Height="400px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div style="vertical-align: top">
                        <div style="padding: 4px" align="center">
                            <asp:Label runat="server" ID="lblCourseName1" />
                        </div>
                        <telerik:RadGrid ID="gridEnrollmentHistory" runat="server" AutoGenerateColumns="False"
                            Skin="Metro" CssClass="RadGrid2_detailtable" BorderStyle="None">
                            <MasterTableView EnableNoRecordsTemplate="False" ShowHeadersWhenNoRecords="True"
                                GridLines="None">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" Width="130" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CimName" HeaderText="Approved/Denied By" UniqueName="CimName">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Comment" HeaderText="Comments" UniqueName="Comment">
                                        <ItemStyle HorizontalAlign="Center" Font-Size="9pt" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Date" UniqueName="CreateDate"
                                        DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" Width="100" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowAssignmentHistory" runat="server" Title="Course Assignment History"
                Width="700px" Height="400px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div style="vertical-align: top">
                        <div style="padding: 4px" align="center">
                            <asp:Label runat="server" ID="lblCourseName2" />
                        </div>
                        <telerik:RadGrid ID="gridAssignmentHistory" runat="server" AutoGenerateColumns="False"
                            Skin="Metro" CssClass="RadGrid2_detailtable" BorderStyle="None">
                            <MasterTableView EnableNoRecordsTemplate="False" ShowHeadersWhenNoRecords="True"
                                GridLines="None">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" Width="130" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Assigner" HeaderText="Assigner" UniqueName="Assigner">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Comments" HeaderText="Comments" UniqueName="Comments">
                                        <ItemStyle HorizontalAlign="Center" Font-Size="9pt" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Date" UniqueName="CreateDate"
                                        DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" Width="100" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowPrereq" runat="server" Title="Accomplish Prerequisite"
                Width="450px" Height="340px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="center" style="padding: 10px">
                        <div style="padding-bottom: 15px">
                            <fieldset>
                                <legend align="right">Note</legend>
                                <label>
                                    Thank you for your interest in taking this course. The following requirement/s must
                                    be fulfilled:
                                </label>
                            </fieldset>
                        </div>
                        <div style="padding-bottom: 20px">
                            <label>
                                <asp:Literal runat="server" ID="lblPrerequisiteMessage"></asp:Literal>
                            </label>
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowEnrollCourse" runat="server" Title="Enrollment Required"
                Width="450px" Height="240px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="center" style="padding: 10px">
                        <div style="padding-bottom: 10px">
                            <fieldset>
                                <legend align="right">Note</legend>
                                <label>
                                    Thank you for your interest in taking this course. Enrollment is required to proceed
                                    with this course.
                                </label>
                            </fieldset>
                        </div>
                        <div style="padding-bottom: 10px">
                            <asp:Label runat="server" ID="lblCourseTitle" Text="Course Title" />
                        </div>
                        <div style="padding-bottom: 15px">
                            <label>
                                <strong>Enroll Now?</strong></label>
                            <br />
                            <asp:LinkButton runat="server" ID="btnEnrollCourse" Text="YES" CssClass="linkBtn"
                                OnClick="btnEnrolCourse_Click" />
                            |
                            <asp:LinkButton runat="server" ID="btnCloseEnrollCourse" Text="NO" CssClass="linkBtn"
                                CausesValidation="False" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnSearch">
            <div class="containerdefault">
                <div class="insidecontainer">
                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding-bottom: 5px">
                            <tr>
                                <td id="tdSearchCrit" runat="server" align="left" style="width: 114px" visible="False">
                                    <telerik:RadComboBox ID="cbSearchType" runat="server" AutoPostBack="True" CausesValidation="False"
                                        EmptyMessage="- search by -" OnSelectedIndexChanged="cbSearchType_SelectedIndexChanged"
                                        ToolTip="Select search criteria" Width="110">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Text="" Value="" Selected="True" />
                                            <telerik:RadComboBoxItem runat="server" Text="by CIM" Value="0" />
                                            <telerik:RadComboBoxItem runat="server" Text="by First Name" Value="1" />
                                            <telerik:RadComboBoxItem runat="server" Text="by Last Name" Value="2" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                                <td id="tdMngr" runat="server" align="left" style="width: 154px" visible="False">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                                        <telerik:RadComboBox ID="cbDirectReports" runat="server" AutoPostBack="True" CausesValidation="False"
                                            CheckBoxes="true" DropDownAutoWidth="Enabled" EmptyMessage="- direct reports -"
                                            EnableCheckAllItemsCheckBox="true" MaxHeight="300px" OnSelectedIndexChanged="cbDirectReports_SelectedIndexChanged"
                                            ToolTip="Select Cim # of your direct reports" Width="150px" DataSourceID="ds_DirectReports" />
                                        <%--DataSourceID="ds_DirectReports"--%>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td id="tdAdminSearch" runat="server" style="width: 154px" visible="False">
                                    <telerik:RadTextBox ID="txtSearch" runat="server" EmptyMessage="- search key -" Width="150px"
                                        SelectionOnFocus="SelectAll" />
                                </td>
                                <td id="tdBtnSearch" runat="server" style="width: 40px" visible="False">
                                    <telerik:RadButton ID="btnSearch" runat="server" CssClass="btnSearchImage" OnClick="btnSearch_Click">
                                        <Image EnableImageButton="true" />
                                    </telerik:RadButton>
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportExcel" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Employee/s Learning Plan to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid ID="gridEmployee" runat="server" OnNeedDataSource="GridEmployeeNeedDataSource"
                            CssClass="RadGrid2" Skin="Metro" OnItemDataBound="GridEmployeeItemDataBound"
                            EnableLinqExpressions="False" OnItemCommand="GridEmployeeItemCommand" OnDetailTableDataBind="gridEmployee_DetailTableDataBind"
                            OnPreRender="gridEmployee_PreRender">
                            <GroupingSettings CaseSensitive="False" />
                            <FilterItemStyle HorizontalAlign="Center" />
                            <SortingSettings EnableSkinSortStyles="false" />
                            <MasterTableView Name="MasterTable" AutoGenerateColumns="False" GridLines="None"
                                AllowSorting="true" AllowFilteringByColumn="True" ShowHeader="False" DataKeyNames="CimNo"
                                NoDetailRecordsText="No record found." NoMasterRecordsText="No record found."
                                EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                AllowCustomSorting="True">
                                <DetailTables>
                                    <telerik:GridTableView Name="gridLearningPlan" Width="100%" AutoGenerateColumns="False"
                                        AllowSorting="True" AllowFilteringByColumn="True" DataKeyNames="HasTest,EnrollmentId,CourseID,EnrolleeCim,Title,Description,StatusId,EncryptedCourseID,CourseLogin,Passed,CourseType,CourseTypeID,Status,EnrolmentRequired"
                                        NoDetailRecordsText="Learning Plan is empty." NoMasterRecordsText="Learning Plan is empty."
                                        EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                        ShowHeader="True" CssClass="RadGrid2_detailtable" GridLines="None">
                                        <Columns>
                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Enrollment Cancellation"
                                                ButtonType="ImageButton" CommandName="Delete" Text="Cancel Enrollment" UniqueName="DeleteColumn"
                                                ConfirmText="Are you sure you want to cancel enrollment to this course?" ImageUrl="~/Images/deletesmall.png"
                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                            </telerik:GridButtonColumn>
                                            <telerik:GridTemplateColumn ReadOnly="True" UniqueName="TitleLink" HeaderText="Title"
                                                SortExpression="Title" DataField="Title" FilterControlWidth="150" CurrentFilterFunction="Contains"
                                                AutoPostBackOnFilter="True" ShowFilterIcon="False">
                                                <ItemStyle HorizontalAlign="Center" CssClass="gridHyperlink" />
                                                <HeaderStyle HorizontalAlign="Center" Width="450" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="linkCourseTitle" runat="server" Text='<%# Eval("Title") %>' OnClick="linkCourseTitle_Click" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn UniqueName="Title" HeaderText="Course" DataField="Title"
                                                FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                                ShowFilterIcon="False" FilterControlToolTip="Search by Course name" Display="False"
                                                DataType="System.String">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Width="450" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridDateTimeColumn UniqueName="CreateDate" HeaderText="Enrollment/Assigned Date"
                                                DataField="CreateDate" AllowFiltering="False" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            </telerik:GridDateTimeColumn>
                                            <telerik:GridBoundColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                                DataType="System.String" DataFormatString="{0:yyyy-MM-dd}" AllowFiltering="False">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn UniqueName="Progress" HeaderText="Progress" DataField="Progress"
                                                AllowFiltering="false">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle Width="110" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <div class='progressBarContainer'>
                                                        <asp:HiddenField ID="hidProgressVal" runat="server" Value="" />
                                                        <div id='progressBar'>
                                                            <span id="spanval"></span>
                                                            <div id="progress">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn ReadOnly="True" UniqueName="CourseType" HeaderText="Course Type"
                                                SortExpression="CourseType" DataField="CourseType" AllowFiltering="True">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Width="90" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCourseType" runat="server" Text='<%# Eval("CourseType") %>' />
                                                </ItemTemplate>
                                                <FilterTemplate>
                                                    <telerik:RadComboBox ID="cbType" DataSourceID="ds_Type" DataTextField="CourseType"
                                                        DataValueField="CourseType" MaxHeight="200" Width="70" AppendDataBoundItems="true"
                                                        SelectedValue='<%# Container.OwnerTableView.GetColumn("CourseType").CurrentFilterValue %>'
                                                        runat="server" OnClientSelectedIndexChanged="CourseTypeIndexChanged">
                                                        <Items>
                                                            <telerik:RadComboBoxItem Text="All" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                    <telerik:RadScriptBlock ID="RadScriptBlock_Type" runat="server">
                                                        <script type="text/javascript">
                                                            function CourseTypeIndexChanged(sender, args) {
                                                                var tableView = $find("<%# Container.OwnerTableView.ClientID %>");
                                                                tableView.filter("CourseType", args.get_item().get_value(), "EqualTo");
                                                            }
                                                        </script>
                                                    </telerik:RadScriptBlock>
                                                </FilterTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn ReadOnly="True" UniqueName="StatusID" HeaderText="Status"
                                                SortExpression="StatusID" DataField="StatusID" AllowFiltering="True">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Width="140" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnPreviousReg" runat="server" ToolTip="View previous Course registration"
                                                        OnClientClick="ShowEnrollmentHistory()" OnClick="btnPreviousReg_Click" Text='<%# Eval("Status") %>' />
                                                </ItemTemplate>
                                                <FilterTemplate>
                                                    <telerik:RadComboBox ID="cbStatus" DataSourceID="ds_Status" DataTextField="Status"
                                                        DataValueField="StatusID" DropDownAutoWidth="Enabled" MaxHeight="200" Width="100"
                                                        AppendDataBoundItems="true" SelectedValue='<%# Container.OwnerTableView.GetColumn("StatusID").CurrentFilterValue %>'
                                                        runat="server" OnClientSelectedIndexChanged="StatusIndexChanged">
                                                        <Items>
                                                            <telerik:RadComboBoxItem Text="All" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                    <telerik:RadScriptBlock ID="RadScriptBlock_Stat" runat="server">
                                                        <script type="text/javascript">
                                                            function StatusIndexChanged(sender, args) {
                                                                var tableView = $find("<%# Container.OwnerTableView.ClientID %>");
                                                                tableView.filter("StatusID", args.get_item().get_value(), "EqualTo");
                                                            }
                                                        </script>
                                                    </telerik:RadScriptBlock>
                                                </FilterTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </telerik:GridTableView>
                                </DetailTables>
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="CimNo" HeaderText="CIM#" DataField="CimNo" AllowSorting="True"
                                        SortExpression="CimNo" Display="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Name" HeaderText="Name" DataField="Name" AllowSorting="True"
                                        SortExpression="CimName" Display="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="CimName" HeaderText="CIM Name" DataField="CimName"
                                        AllowSorting="True" SortExpression="CimName">
                                        <ItemStyle HorizontalAlign="Left" Width="100%" CssClass="cimName_AsHeader" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                                <ClientEvents OnRowClick="toggleExpandState" />
                                <Scrolling SaveScrollPosition="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </telerik:RadAjaxPanel>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="ds_DirectReports" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="pr_TranscomUniversity_Lkp_GetDirectReports" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="hidCIM" DefaultValue="0" Name="CIM" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="ds_Status" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="SELECT [StatusID],Status FROM [ref_TranscomUniversity_Status] WHERE [StatusId] <> 0" />
        <asp:SqlDataSource ID="ds_type" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="SELECT [CourseType] FROM [ref_TranscomUniversity_CourseType]" />
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidCIM" />
        <asp:HiddenField runat="server" ID="hidCourseId" Value="" />
        <asp:HiddenField runat="server" ID="hidUrlWithParams" Value="" />
    </div>
</asp:Content>
