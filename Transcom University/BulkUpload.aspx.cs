﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

public partial class BulkUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    void ProcessFile(FileUploadedEventArgs e)
    {
        bool hasErrors = false;

        StreamReader sr = null;
        StreamReader sr2 = null;

        try
        {
            sr = new StreamReader(e.File.InputStream, Encoding.Default, true);
            sr2 = new StreamReader(e.File.InputStream, Encoding.Default, true); ;
        }
        catch (System.IO.FileNotFoundException ex)
        {
            txtErrorLog.Text += ex.Message;
            hasErrors = true;
            return;
        }

        RadProgressContext progress = RadProgressContext.Current;
        progress.Speed = "N/A";
        pnlUploadResult.Visible = false;

        int lineCount = 0;
        while (!sr2.EndOfStream)
        {
            lineCount++;
            sr2.ReadLine();
        }

        sr2.Close();

        if (lineCount > 1)
        {
            lineCount = lineCount - 1;
            lblRecordCount.Text = lineCount.ToString();
        }

        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
            {
                string sql;
                cn.Open();
                SqlTransaction transaction = cn.BeginTransaction();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Transaction = transaction;

                //Read  Header 
                string line = sr.ReadLine();

                int columnCount = line.Length - line.Replace(",", "").Length;

                if (columnCount != 4)
                {
                    txtErrorLog.Text = "Invalid bulk insert template file, number of column count should be 11" + "\r\n";
                    hasErrors = true;
                }
                else
                {
                    string TwwID, Password, Email, Client, Language;
                    
                    //truncate the temporary table first
                    sql = @"TRUNCATE TABLE tmpUsers";
                    cmd = new SqlCommand(sql, cn, transaction);
                    cmd.ExecuteNonQuery();

                    string[] lineRow = line.Split(",".ToArray());
                    string[] arrCodeTexts = new string[20];
                    int[] arrCodes = new int[20];

                    int iRow = 0;
                    int secondaryPercent;

                    do
                    {
                        iRow++;
                        progress.PrimaryTotal = 1;
                        progress.PrimaryValue = 1;
                        progress.PrimaryPercent = 100;
                        progress.SecondaryTotal = lineCount;
                        progress.SecondaryValue = iRow;
                        secondaryPercent = Convert.ToInt32((float)iRow / (float)lineCount * (float)100);
                        progress.SecondaryPercent = secondaryPercent;

                        progress.CurrentOperationText = "Step " + iRow.ToString();

                        if (!Response.IsClientConnected)
                        {
                            //Cancel button was clicked or the browser was closed, so stop processing
                            hasErrors = true;
                            txtErrorLog.Text += "User cancelled the operation" + "\r\n";
                            transaction.Rollback();
                            break;
                        }

                        progress.TimeEstimated = (lineCount - iRow) * 100;
                        lineRow = sr.ReadLine().Split(",".ToArray());

                        if (lineRow.Length > 0)
                        {
                            TwwID = lineRow[0];
                            Password = lineRow[1];
                            Email = lineRow[2];
                            Client = lineRow[3];
                            Language = lineRow[4];
                            lineRow = line.Split(",".ToArray());

                            sql = @"INSERT INTO tmpUsers(TwwID, Password, Email, Client, Language)
                                    VALUES(@TwwID, @Password, @Email, @Client, @Language);";

                            try
                            {
                                cmd = new SqlCommand(sql, cn, transaction);
                                cmd.Parameters.AddWithValue("@TwwID", TwwID);
                                cmd.Parameters.AddWithValue("@Password", Password);
                                cmd.Parameters.AddWithValue("@Email", Email);
                                cmd.Parameters.AddWithValue("@Client", Client);
                                cmd.Parameters.AddWithValue("@Language", Language);
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    txtErrorLog.Text += ex.Message + "\r\n";
                                    hasErrors = true;
                                    break;
                                }
                                catch (Exception ex2)
                                {
                                    txtErrorLog.Text += ex2.Message + "\r\n";
                                    hasErrors = true;
                                    break;
                                }
                            }
                        }
                    } while (!sr.EndOfStream);
                }

                if (hasErrors)
                {
                    pnlErrorLog.Visible = true;
                    pnlUploadResult.Visible = false;
                    transaction.Rollback();
                }
                else
                {
                    pnlErrorLog.Visible = false;
                    pnlUploadResult.Visible = true;
                    transaction.Commit();
                    grdUsers.Rebind();
                }
            }

            sr.Close();
        }
        catch (Exception ex3)
        {
            sr.Close();
            pnlErrorLog.Visible = true;
            pnlUploadResult.Visible = false;
            txtErrorLog.Text += ex3.Message + "\r\n";
        }
    }

    protected void RadAsyncUploadCSV_FileUploaded(object sender, FileUploadedEventArgs e)
    {
        ProcessFile(e);
    }

    DataSet GetTmpUsers()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            cn.Open();
            string sql = @"
                SELECT
	                CASE WHEN emp.TWWID IS NULL
	                THEN 'Invalid'
	                ELSE
		                'Pending'
	                END AS Status,
	                *
                FROM tmpUsers u
                LEFT JOIN 
                (SELECT DISTINCT emp.*
                FROM (
	                SELECT E.TWWID AS TwwID,
		                e.Nombre AS FirstName,
		                e.Apellido1 AS LastName,
		                e.NombreCatTranscom AS 'Role',
		                dbo.fnc_GetRoleID(E.TWWID) AS 'RoleID',
		                r.Description AS 'Region',
		                r.ID AS 'RegionID',
		                c.Country AS 'Country',
		                c.ID AS 'CountryID',
		                L.Description AS 'Site',
		                L.ID AS 'SiteID',
		                dbo.fnc_GetSupID(E.TWWID) AS SupervisorID
	                FROM Empleados E
	                INNER JOIN Agents AG
		                ON ag.twwid = e.TWWID
	                INNER JOIN Locations L
		                ON AG.locationID = L.id
	                INNER JOIN Countries C
		                ON C.ID = L.CountryID
	                INNER JOIN Regions r
		                ON r.ID = C.RegionID
	
	                UNION ALL
	
	                SELECT EMP.TWWID,
		                ed.Nombre AS FirstName,
		                ed.Apellido1 AS LastName,
		                ed.NombreCatTranscom AS 'Role',
		                dbo.fnc_GetRoleID(EMP.TWWID) AS 'RoleID',
		                r2.Description,
		                r2.ID,
		                C2.Country,
		                c2.ID,
		                L2.Description,
		                L2.ID,
		                dbo.fnc_GetSupID(EMP.TWWID)
	                FROM EMPLOYEES EMP
	                LEFT JOIN Empleados ED
		                ON ED.twwid = EMP.twwid
	                INNER JOIN Agents A2
		                ON EMP.TWWID = A2.twwid
	                INNER JOIN Locations L2
		                ON L2.ID = A2.locationID
	                INNER JOIN Countries C2
		                ON C2.ID = L2.CountryID
	                INNER JOIN Regions R2
		                ON R2.ID = C2.RegionID
	                ) emp) emp
                ON u.TWWID = emp.TWWID
                ";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    protected void grdUsers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == GridRebindReason.ExplicitRebind || e.RebindReason == GridRebindReason.PostBackEvent)
        {
            try
            {
                grdUsers.DataSource = GetTmpUsers();

            }
            catch (Exception ex)
            {
                grdUsers.Controls.Add(new LiteralControl(string.Format("Error binding record: {0}", ex.Message)));
            }
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        //has no errors
        if (!ConfirmUpload())
        {
            btnConfirm.Enabled = false;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin.aspx");
    }

    bool ConfirmUpload()
    {
        bool hasError = false;

        listBoxError.Items.Clear();

        int counter = 0;
        int returnType = 0;
        int countInserted = 0;
        int countUpdated = 0;

        foreach (GridDataItem item in grdUsers.MasterTableView.Items)
        {
            counter++;

            try
            {
                returnType = InsertUpdateUser(item["TwwID"].Text,
                    item["FirstName"].Text,
                    item["LastName"].Text,
                    Password.CreateHash(item["Password"].Text),
                    item["Email"].Text,
                    item["Client"].Text,
                    item.GetDataKeyValue("CountryID") is DBNull ? 0 : Convert.ToInt32(item.GetDataKeyValue("CountryID")),
                    item.GetDataKeyValue("RegionID") is DBNull ? 0 : Convert.ToInt32(item.GetDataKeyValue("RegionID")),
                    item.GetDataKeyValue("SiteID") is DBNull ? 0 : Convert.ToInt32(item.GetDataKeyValue("SiteID")),
                    item["Language"].Text,
                    item.GetDataKeyValue("SupervisorID") is DBNull ? 0 : Convert.ToInt32(item.GetDataKeyValue("SupervisorID")));

                if (returnType == 1)
                {
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ToolTip = "Updated";
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ImageUrl = "Images/checked.gif";
                    countUpdated++;
                }
                else if (returnType == 2)
                {
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ToolTip = "Inserted";
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ImageUrl = "Images/addFilter_active.png";
                    countInserted++;

                }
                else if (returnType == 3 || returnType == 6)
                {
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ToolTip = "Invalid TWW ID";
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ImageUrl = "Images/deleteFilter_active.png";
                    hasError = true;
                }
                else if (returnType == 4)
                {
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ToolTip = "Invalid Client";
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ImageUrl = "Images/deleteFilter_active.png";
                    hasError = true;
                }
                else if (returnType == 5)
                {
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ToolTip = "Invalid Language";
                    ((ImageButton)(item["Status"].FindControl("btnStatus"))).ImageUrl = "Images/deleteFilter_active.png";
                    hasError = true;
                }
            }
            catch (Exception ex)
            {
                ((ImageButton)(item["Status"].FindControl("btnStatus"))).ToolTip = string.Format("Error at Row {0}: {1}", counter, ex.Message);
                ((ImageButton)(item["Status"].FindControl("btnStatus"))).ImageUrl = "Images/deleteFilter_active.png";
                listBoxError.Items.Add(new RadListBoxItem(string.Format("Error at Row {0}: {1}", counter, ex.Message)));
                hasError = true;
            }

            lblInsertCount.Text = countInserted.ToString();
            lblUpdateCount.Text = countUpdated.ToString();
        }

        return hasError;
    }

    int InsertUpdateUser(string twwID, string firstName, string lastName, string password, string email, string client, int? countryID, int? regionID, int? siteID, string language, int? supervisorID)
    {
        int returnType;

        string sql = @"
            DECLARE @IsTwwIdExist INT
            DECLARE @ClientID INT
            DECLARE @LanguageID INT

            SELECT @IsTwwIdExist = COUNT(*) FROM Users WHERE TwwID = @TwwID
            SELECT @ClientID = c.ID FROM Clients c WHERE UPPER(c.description) = UPPER(@Client)
            SELECT @LanguageID = c.ID FROM refLanguage c WHERE UPPER(c.Language) = UPPER(@Language)

            IF NOT EXISTS(SELECT 1 FROM Empleados WHERE TWWID = @TwwID)
            BEGIN
               SELECT 3;
            END
            ELSE IF @ClientID IS NULL
            BEGIN
                SELECT 4;
            END
            ELSE IF @LanguageID IS NULL 
            BEGIN
                SELECT 5;
            END
            ELSE IF @IsTwwIdExist = 0
            BEGIN
                SELECT 6
            END
            ELSE IF @IsTwwIdExist > 0
            BEGIN
                UPDATE u
                SET
		            u.FirstName = @FirstName,
		            u.LastName = @LastName,
		            u.[Password] = @Password,
		            u.Email = @Email,
		            u.ClientID = @ClientID,
		            u.CountryID = @CountryID,
		            u.RegionID = @RegionID,
		            u.SiteID = @SiteID,
                    u.LanguageID = @LanguageID,
		            u.supervisorid = @SupervisorID,
                    u.LastPasswordChangeDate = NULL	
                FROM Users u
                WHERE u.TWWID = @TwwID;

                SELECT 1;	
            END
            ELSE
            BEGIN
	            INSERT INTO Users(TWWID, FirstName, LastName, Password, Email, ClientID, CountryID, RegionID, SiteID, LanguageID, supervisorid, CreateDate)
	            VALUES(@TWWID, @FirstName, @LastName, @Password, @Email, @ClientID, @CountryID, @RegionID, @SiteID, @LanguageID, @supervisorid, GETDATE());

                SELECT 2;
            END
            ";

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd = new SqlCommand(sql, cn);
            cmd.Parameters.AddWithValue("@TwwID", twwID);
            cmd.Parameters.AddWithValue("@FirstName", firstName);
            cmd.Parameters.AddWithValue("@LastName", lastName);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.Parameters.AddWithValue("@Email", email);
            cmd.Parameters.AddWithValue("@Client", client);
            cmd.Parameters.AddWithValue("@CountryID", countryID);
            cmd.Parameters.AddWithValue("@RegionID", regionID);
            cmd.Parameters.AddWithValue("@SiteID", siteID);
            cmd.Parameters.AddWithValue("@Language", language);
            cmd.Parameters.AddWithValue("@SupervisorID", supervisorID);
            cmd.CommandType = CommandType.Text;

            returnType = Convert.ToInt32(cmd.ExecuteScalar());
        }

        return returnType;
    }
}