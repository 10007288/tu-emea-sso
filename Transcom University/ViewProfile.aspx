﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewProfile.aspx.cs" Inherits="ViewProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/MasterPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%--|| (args.get_eventTarget() == "<%= btnUpdateProfile2.UniqueID %>")--%>
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if ((args.get_eventTarget() == "<%= btnUpdateProfile.UniqueID %>")) {
                    args.set_enableAjax(false);
                }
            }
            function RequestFullScreen(sender, args) {
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollLeftOffset;
                var scrollTopOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function openWinContentTemplate() {
                $find("<%=rdEditClient.ClientID %>").show();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnViewMyProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEditProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="containerdefault">
                <div class="container">
                    <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" ClientEvents-OnRequestStart="conditionalPostback"
                        Width="100%">
                        <div style="border-style: inset; background-color: white; width: 165px; height: 166px;
                            border: 1px solid #999; float: left; position: absolute">
                            <asp:Image runat="server" ID="profilePhoto" alt="" Height="166" Width="165" />
                        </div>
                        <table width="100%" align="center" style="height: 170px">
                            <tr>
                                <td align="center">
                                    <div>
                                        <asp:Label runat="server" ID="lblName" Font-Size="XX-Large" ForeColor="Black" />
                                    </div>
                                    <div>
                                        <asp:Label ID="lblRole" runat="server" />
                                    </div>
                                    <div>
                                        <asp:Label runat="server" ID="lblSite" />
                                    </div>
                                    <div>
                                    </div>
                                    <div style="padding-top: 15px">
                                        <div id="managecontrol2" style="width: 100%">
                                            <asp:LinkButton ID="btnEditProfile" runat="server" Text="Edit" OnClick="btnEditProfile_Click"
                                                CausesValidation="False" ToolTip="Edit my profile" Font-Size="7.5" Visible="false"/>
                                            <asp:LinkButton runat="server" ID="btnUpdateProfile" Text="Update Profile" OnClick="btnUpdateProfile_Click"
                                                ToolTip="Update Profile" ValidationGroup="update" Font-Size="7.5" Visible="false"
                                                AutoPostBack="false" />
                                            <asp:LinkButton runat="server" ID="btnCancelUpdate" Text="View Profile" Font-Size="7.5"
                                                ToolTip="View Profile" CausesValidation="False" OnClick="btnViewMyProfile_Click"
                                                Visible="false" />
                                            <asp:LinkButton ID="btnChangePassword" runat="server" Text="Change Password" OnClick="btnChangePassword_Click"
                                                CausesValidation="False" ToolTip="Change Password" Font-Size="7.5" />
                                        </div>

                                        <telerik:RadComboBox ID="cbDirectReports" runat="server" AutoPostBack="True" CausesValidation="False" Visible="false"
                                            DataSourceID="dsDirectReports" DataTextField="Name" DataValueField="TWWID" EmptyMessage="- edit employee client -" ToolTip="Select employee to update their client"/>

                                        <telerik:RadButton ID="btnEditClient" runat="server" Text="Edit Client" Visible="false"
                                            ButtonType="StandardButton" OnClientClicked="openWinContentTemplate" AutoPostBack="false">
                                        </telerik:RadButton>
                                        <telerik:RadWindow runat="server" ID="rdEditClient" Modal="true" Width="380px" Height="180px"
                                            Behaviors="Close, Move, Resize, Maximize, Minimize, Pin" EnableViewState="True" VisibleOnPageLoad="False">
                                            <ContentTemplate>
                                                <p class="contText">
                                                    <table runat="server" align="center">
                                                        <tr>
                                                            <td class="labelMyTeam">
                                                                <asp:Label runat="server" ID="lblSelectedEmp" Visible="true" Text="Selected Employee" />
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblEditEmpClient" Visible="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="labelMyTeam">
                                                                <asp:Label runat="server" ID="Label1" Visible="true" Text="Select Client" />
                                                            </td>
                                                            <td align="center">
                                                                <telerik:RadComboBox ID="cbClients" runat="server" AutoPostBack="false" DataTextField="Description"
                                                                    CausesValidation="False" Visible="true" DataSourceID="SqlDataSource_Clients"
                                                                    DataValueField="ID" EmptyMessage="- select client -" ToolTip="Select client"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                
                                                            </td>
                                                            <td>
                                                                <br />
                                                                <telerik:RadButton ID="btnUpdateEmpClient" runat="server" Text="Update" Visible="true" AutoPostBack="true"
                                                                    ButtonType="StandardButton" OnClick="btnUpdateEmpClient_Click" ToolTip="Update employee client">
                                                                </telerik:RadButton>
                                                            </td>
                                                            <td>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </p>
                                            </ContentTemplate>
                                        </telerik:RadWindow>
                                        <asp:Label runat="server" ID="lblDirectReports" Visible="false" />
                                    </div>
                                    <div runat="server" id="divUpload" visible="False">
                                        Photo:
                                        <asp:FileUpload ID="fileUpload" runat="server" ToolTip="Select your photo" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </div>
                <div style="padding-bottom: 20px">
                    <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2" Width="100%">
                        <%--ClientEvents-OnRequestStart="conditionalPostback"--%>
                        <table runat="server" id="tblProfile" class="tableProfile">
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_CimNo" runat="server" Text="Label">CIM Number</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblTWWID" />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblEditTWWID" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_Gender" runat="server">Gender</asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblGender" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblEditGender" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_DOB" runat="server">Date of Birth</asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDateOfBirth" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblEditDateOfBirth" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_StartDate" runat="server">Start Date with Transcom</asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStartDateWithTranscom" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblEditStartDateWithTranscom" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_EAdd" runat="server">Email Address</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblEmailAddress" />
                                    <telerik:RadTextBox runat="server" ID="txtEditEmailAddress" Width="250" SelectionOnFocus="SelectAll"
                                        onpaste="return false" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtEditEmailAddress"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                    <asp:RegularExpressionValidator ID="emailValidator" runat="server" Display="Dynamic"
                                        ErrorMessage="* Invalid format " ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                        ControlToValidate="txtEditEmailAddress" CssClass="displayerror" ValidationGroup="update">
                                    </asp:RegularExpressionValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_ConfEAdd" runat="server">Confirm Email Address</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblEmailAddress2" />
                                    <telerik:RadTextBox runat="server" ID="txtEditEmailAddress2" Width="250" SelectionOnFocus="SelectAll"
                                        onpaste="return false" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtEditEmailAddress2"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                    <asp:RegularExpressionValidator ID="emailValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="* Invalid format " ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                        ControlToValidate="txtEditEmailAddress2" CssClass="displayerror" ValidationGroup="update">
                                    </asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="emailCompareValidator" runat="server" ControlToValidate="txtEditEmailAddress2"
                                        ControlToCompare="txtEditEmailAddress" Operator="Equal" Type="String" Display="Dynamic"
                                        ErrorMessage="* Email mismatched " CssClass="displayerror" ValidationGroup="update"
                                        EnableClientScript="True">
                                    </asp:CompareValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_MobNo" runat="server">Mobile No</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblMobileNo" />
                                    <telerik:RadTextBox runat="server" ID="txtEditMobileNo" SelectionOnFocus="SelectAll"
                                        Width="150" MaxLength="20">
                                    </telerik:RadTextBox>
                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_Region" runat="server">Region</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblRegion" />
                                    <asp:DropDownList runat="server" ID="cbEditRegion" DataTextField="Description" DataValueField="ID"
                                        EmptyMessage="- select region -" DataSourceID="SqlDataSource_Regions" AppendDataBoundItems="true"
                                        MaxHeight="250" DropDownAutoWidth="Enabled" Enabled="false">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cbEditRegion"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_Country" runat="server">Country</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCountryID" Visible="false" />
                                    <asp:Label runat="server" ID="lblCountry" />
                                    <asp:Label ID="lblEditCountry" runat="server" />
                                </td>
                                <td>
                                    <%--<asp:Label ID="lblEditCountry" runat="server" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_RptMgr" runat="server">Reporting Manager/Supervisor</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblSupervisor" />
                                    <%--<asp:Label ID="lblEditSupervisor" runat="server" />--%>
                                    <asp:DropDownList ID="cbEditSupervisor" runat="server" DataSourceID="SqlDataSource_Supervisor"
                                        DataTextField="ManagerName" DataValueField="ManagerCIM" EmptyMessage="- select supervisor -"
                                        MaxHeight="300" AppendDataBoundItems="True" DropDownAutoWidth="Enabled" OnSelectedIndexChanged="cbSupervisor_SelectedIndexChanged"
                                        AutoPostBack="false">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <telerik:RadTextBox runat="server" ID="txtEditSupervisor" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="cbEditSupervisor"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_Dept" runat="server">Department</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblDepartment" />
                                    <asp:DropDownList ID="cbEditDepartment" runat="server" DataSourceID="SqlDataSource_Department"
                                        DataTextField="description" DataValueField="id" EmptyMessage="- select department -"
                                        MaxHeight="300" AppendDataBoundItems="True" DropDownAutoWidth="Enabled" OnSelectedIndexChanged="cbDepartment_SelectedIndexChanged"
                                        AutoPostBack="false">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <telerik:RadTextBox runat="server" ID="txtEditDepartment" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="cbEditDepartment"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                    <asp:RequiredFieldValidator ID="rfvOtherDepartment" runat="server" ControlToValidate="txtEditDepartment"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_Client" runat="server">Client</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblClient" />
                                    <asp:DropDownList runat="server" ID="cbEditClient" DataTextField="description" DataSourceID="SqlDataSource_Clients"
                                        DataValueField="id" EmptyMessage="- select client -" MaxHeight="300" DropDownAutoWidth="Enabled"
                                        AutoPostBack="false" AppendDataBoundItems="True" OnSelectedIndexChanged="cbClient_SelectedIndexChanged"
                                        ToolTip="Please select/re-select Client to load Campaign List if necessary">
                                    </asp:DropDownList>
                                    <telerik:RadTextBox runat="server" ID="txtEditClient" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cbEditClient"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                    <asp:RequiredFieldValidator ID="rfvOtherClient" runat="server" ControlToValidate="txtEditClient"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" Enabled="False" />
                                    <asp:LinkButton runat="server" ID="btnUpdate_Client" Text="Edit" OnClick="btnUpdate_Client_Click"
                                        ToolTip="Update Client" Font-Size="6.5" Visible="false" AutoPostBack="false" />
                                    <asp:LinkButton runat="server" ID="btnSaveClient" Text="Update" OnClick="btnSaveClient_onclick"
                                        ToolTip="Update Client" Font-Size="6.5" Visible="false" AutoPostBack="false" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_AdditionalClient" runat="server">Additional Client</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblAdditionalClient" Visible="true" />
                                    <telerik:RadCombobox runat="server" ID="cboAdditionalClient" DataTextField="description" CheckBoxes="true"
                                        DataSourceID="SqlDataSource_Clients" DataValueField="id" EmptyMessage="- select client -"
                                        MaxHeight="300" DropDownAutoWidth="Enabled" AutoPostBack="false" AppendDataBoundItems="True"
                                        OnSelectedIndexChanged="cbClient_SelectedIndexChanged" Visible="false">
                                    </telerik:RadCombobox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_Campaign" runat="server">Main Campaign</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCampaign" />
                                    <asp:DropDownList runat="server" ID="cbEditCampaign" DataTextField="campaign_desc"
                                        DataSourceID="SqlDataSource_Campaigns" DataValueField="id" EmptyMessage="- select campaign -"
                                        MaxHeight="300" DropDownAutoWidth="Enabled" AutoPostBack="false" AppendDataBoundItems="True"
                                        OnSelectedIndexChanged="cbCampaign_SelectedIndexChanged" ToolTip="">
                                    </asp:DropDownList>
                                    <telerik:RadTextBox runat="server" ID="txtEditCampaign" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cbEditCampaign"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                    <asp:RequiredFieldValidator ID="rfvOtherCampaign" runat="server" ControlToValidate="txtEditCampaign"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" Enabled="False" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_PrimLang" runat="server">Primary Language</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblLanguage" />
                                    <asp:DropDownList runat="server" ID="cbEditLanguage" DataTextField="Language" DataSourceID="DataSource_Language"
                                        DataValueField="Id" EmptyMessage="- select language -" MaxHeight="300" DropDownAutoWidth="Enabled"
                                        AutoPostBack="false" AppendDataBoundItems="True" OnSelectedIndexChanged="cbEditLanguage_SelectedIndexChanged"
                                        ToolTip="Please select Language">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="0"> </asp:ListItem>
                                    </asp:DropDownList>
                                    <telerik:RadTextBox runat="server" ID="txtEditLanguage" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                    <asp:RequiredFieldValidator ID="rfvOtherLanguage" runat="server" ControlToValidate="txtEditLanguage"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_CareerPath" runat="server">I am interested in pursuing the following career path in Transcom</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCareerPath" />
                                    <asp:DropDownList ID="cbEditCareerPath" runat="server" DataSourceID="SqlDataSource_CareerPath"
                                        DataTextField="Description" DataValueField="ID" DropDownAutoWidth="Enabled" EmptyMessage="- select career -"
                                        MaxHeight="300px" AppendDataBoundItems="True" AutoPostBack="false" OnSelectedIndexChanged="cbEditCareerPath_SelectedIndexChanged">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="0"> </asp:ListItem>
                                    </asp:DropDownList>
                                    <telerik:RadTextBox runat="server" ID="txtEditCareerpath" Visible="False" Width="120"
                                        EmptyMessage="Please specify" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cbEditCareerPath"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                    <asp:RequiredFieldValidator ID="rfvOtherCareerpath" runat="server" ControlToValidate="txtEditCareerpath"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" Enabled="False" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_CourseInterested" runat="server">Please tell me what courses you are interested in taking for your professional growth
                                and development</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCoursesInterested" />
                                    <telerik:RadTextBox runat="server" ID="txtEditCoursesInterested" Width="300" Height="50"
                                        SelectionOnFocus="SelectAll" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEditCoursesInterested"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam">
                                    <asp:Label ID="lblVP_UpdateVia" runat="server">I am interested in receiving updates via</asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblReceiveUpdates" />
                                    <telerik:RadComboBox ID="cbEditReceiveUpdates" runat="server" DataSourceID="SqlDataSource_UpdateVia"
                                        DataTextField="Description" DataValueField="ID" CheckBoxes="True" DropDownAutoWidth="Enabled"
                                        EmptyMessage="- select item/s -" MaxHeight="300px" AutoPostBack="true" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cbEditReceiveUpdates"
                                        SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                        CssClass="displayerror" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelMyTeam" style="width: 45%">
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidCIM" />
    </div>
    <div>
        <asp:SqlDataSource ID="SqlDataSource_Clients" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString%>"
            SelectCommand="SELECT DISTINCT ID, Description FROM Clients WHERE Active = 1 ORDER BY Description">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource_Campaigns" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString%>"
            SelectCommand="SELECT DISTINCT ID, campaign_desc FROM [refCampaigns]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource_Regions" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [ID], [Description] FROM [Regions] WHERE ([ACTIVE] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="SqlDataSource_Supervisor" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
                            FROM TranscomUniv_local.dbo.Empleados E
	                            INNER JOIN TranscomUniv_local.dbo.Users U
		                            ON E.TWWID = U.TWWID
                            WHERE E.NombreCatTranscomDesc LIKE '%Manager%'
	                            AND E.TWWID IS NOT NULL
                            UNION ALL
                            SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
                            FROM TranscomUniv_local.dbo.Empleados E
	                            INNER JOIN TranscomUniv_local.dbo.Users U
		                            ON E.TWWID = U.TWWID
                            WHERE E.NombreCatTranscomDesc LIKE '%Team Leader%'
	                            AND E.TWWID IS NOT NULL
                            UNION ALL
                            SELECT '' AS ManagerCIM, '' AS ManagerName
                            ORDER BY 2 ASC" />
        <asp:SqlDataSource ID="SqlDataSource_Department" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [id], [description] FROM [refDepartments] WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="SqlDataSource_TypeOfSupport" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [id], [description] FROM [refSupportType] WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="DataSource_Language" runat="server" ConnectionString="<%$ ConnectionStrings: TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id], [Language] FROM [refLanguage] WHERE ([Active] = 1) ORDER BY [Language]" />
        <asp:SqlDataSource ID="DataSourse_EducLevel" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [Id], [Title] FROM [refEducationLevel] WHERE ([Active] = 1) ORDER BY [Id]" />
        <asp:SqlDataSource ID="SqlDataSource_CareerPath" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refCareerPath] WHERE ([Active] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="SqlDataSource_UpdateVia" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refUpdateVia] WHERE ([Active] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="dsDirectReports" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="DECLARE @TBL TABLE ([TWWID] INT)
BEGIN
	;WITH security_menu_Recursive
	(
		SupervisorID,
		TWWID,
		Level
	)
	AS
	(
		SELECT
			U1.SupervisorID,
			U1.TWWID, 
			0 AS Level 
		FROM TranscomUniv_Local.dbo.Users U1
		WHERE U1.TWWID = @TWWID

		UNION ALL

		SELECT
			U2.SupervisorID,
			U2.TWWID,
			Level + 1 AS Level 
		FROM TranscomUniv_Local.dbo.Users U2
			INNER JOIN security_menu_Recursive SMR
				ON SMR.TWWID = U2.SupervisorID
	)
	SELECT 
		U.TWWID,
		U.FirstName + ' ' + U.Lastname AS Name,
		Level
	FROM security_menu_Recursive R
		INNER JOIN  TranscomUniv_Local.dbo.Users U
		ON R.TWWID = U.TWWID
	ORDER BY 3
END">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblDirectReports" Name="TWWID" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <%--<asp:SqlDataSource ID="ds_DirectReports" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="sp_Get_TWWID_ByTeam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="hidCIM" DefaultValue="0" Name="CIM" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
    </div>
</asp:Content>
