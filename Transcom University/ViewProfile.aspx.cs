﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web.Configuration;

public partial class ViewProfile : BasePage
{
    private MasterPage _master;
    private string _employeeId = "";
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    private string _UpdtVia = string.Empty;
    string _Role;

    public string SearchValue = String.Empty;

    public string GetComboBoxSelectedItems(RadComboBox combo)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            var sb = new StringBuilder();

            foreach (var item in combo.CheckedItems)
            {
                sb.Append(item.Value + ",");
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                collection = sb.ToString().TrimEnd(',');
            }
        }
        return collection;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblDirectReports.Text = CurrTWWID;

        if (!IsPostBack)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            Page.Form.Enctype = "multipart/form-data";
            GetSession();

            if (User.Identity.Name.Contains("@"))
            {
                Response.Redirect("NoAccess.aspx");
            }

            hidCIM.Value = _employeeId;
            SetPageOnViewing();
            HideEditColumn();
            GetUserRole();

            if (_Role == "Admin" || _Role == "Super User")
            {
                cbDirectReports.Visible = true;
            }
            else
            {
                cbDirectReports.Visible = false;
            }
        }

        GetUserRole();

        if (_Role == "Admin" || _Role == "Super User")
        {
            cbDirectReports.Visible = true;
        }
        else
        {
            cbDirectReports.Visible = false;
        }

        if (cbDirectReports.SelectedValue != "")
        {
            btnEditClient.Visible = true;
            lblEditEmpClient.Text = cbDirectReports.SelectedItem.Text;
        }
        else
        {
            btnEditClient.Visible = false;
        }

        GetLang();
    }

    private void GetUserRole()
    {
        //Check user role of the current user
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "SELECT R.Role_Desc AS Role FROM TranscomUniv_Local.dbo.refRoles R INNER JOIN TranscomUniv_Local.dbo.Users U ON R.ID = U.RoleID WHERE R.Active = 1 AND U.TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = CurrTWWID;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    _Role = String.Format("{0}", reader["Role"].ToString());
                }
            }
        }
    }

    private void GetLang()
    {
        lblVP_CimNo.Text = Resources.LocalizedResource.CIMNo.ToString();
        lblVP_Gender.Text = Resources.LocalizedResource.Gender.ToString();
        lblVP_DOB.Text = Resources.LocalizedResource.DOB.ToString();
        lblVP_StartDate.Text = Resources.LocalizedResource.StartDate.ToString();
        lblVP_EAdd.Text = Resources.LocalizedResource.EAdd.ToString();
        lblVP_ConfEAdd.Text = Resources.LocalizedResource.ConfEAdd.ToString();
        lblVP_MobNo.Text = Resources.LocalizedResource.MobileNo.ToString();
        lblVP_Region.Text = Resources.LocalizedResource.Region.ToString();
        lblVP_Country.Text = Resources.LocalizedResource.Country.ToString();
        lblVP_RptMgr.Text = Resources.LocalizedResource.RepMgrSup.ToString();
        lblVP_Dept.Text = Resources.LocalizedResource.Dept.ToString();
        lblVP_Client.Text = Resources.LocalizedResource.Client.ToString();
        lblVP_Campaign.Text = Resources.LocalizedResource.Campaign.ToString();
        lblVP_PrimLang.Text = Resources.LocalizedResource.PrimLang.ToString();
        lblVP_CareerPath.Text = Resources.LocalizedResource.CareerPath.ToString();
        lblVP_CourseInterested.Text = Resources.LocalizedResource.ProfileCourseInterested.ToString();
        lblVP_UpdateVia.Text = Resources.LocalizedResource.UpdatesVia.ToString();
    }

    private void SetPageOnViewing()
    {
        GetSession();

        ViewShow();
        SetDetails(true, false);
        SearchValue = _employeeId;
    }

    private void GetSession()
    {
        try
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
            lblLNAME.Text = CurrLogName.ToString();
            //_employeeId = lblLNAME.Text;
        }
        catch
        {
            Label lblLNAME = (Label)Master.FindControl("lblLOGNAME");
            lblLNAME.Text = lblLNAME.Text;
            //_employeeId = lblWel.Text;
        }

        try
        {
            var lv = (MultiView)Master.FindControl("mvMenu");
            string CurrIndx = CurrVwIndx.ToString();
            lv.ActiveViewIndex = Int32.Parse(CurrIndx);
        }
        catch
        {
            var lv = (MultiView)Master.FindControl("mvMenu");
            //string CurrIndx = CurrVwIndx.ToString();
            lv.ActiveViewIndex = lv.ActiveViewIndex;
        }

        try
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            lblWel.Text = CurrTWWID.ToString();
            _employeeId = lblWel.Text;
        }
        catch
        {
            Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
            //lblWel.Text = CurrTWWID.ToString();
            _employeeId = lblWel.Text;
        }
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
            {
                ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString
            };
        _oconn.Open();
    }

    private void SetDetails(bool isView, bool isEdit)
    {
        _master = Master;
        Dbconn("NuskillCheck");
        _ocmd = new SqlCommand("sp_Get_User_ByTWWID", _oconn);
        _ocmd.Parameters.AddWithValue("@EmpID", _employeeId);
        _ocmd.Parameters.AddWithValue("@IsReport", "No");
        _ocmd.CommandType = CommandType.StoredProcedure;
        var oDataReader = _ocmd.ExecuteReader();

        if (isView)
        {
            cbDirectReports.DataBind();
            btnEditProfile.Visible = true;
            btnUpdateProfile.Visible = false;
            btnCancelUpdate.Visible = false;
            divUpload.Visible = false;

            if (cbDirectReports.SelectedValue != "")
            {
                btnEditClient.Visible = true;
                lblEditEmpClient.Text = cbDirectReports.SelectedItem.Text;

                try
                {
                    cbClients.Text = oDataReader["ClientID"].ToString() == "0" ? oDataReader["otherclient"].ToString() : oDataReader["ClientName"].ToString();
                    cbClients.SelectedIndex = Convert.ToInt32(oDataReader["ClientID"]);
                }
                catch (Exception)
                {  

                }
            }
            else
            {
                btnEditClient.Visible = false;
            }

            if (oDataReader.Read())
            {
                lblName.Text = oDataReader[0].ToString(); // oDataReader[0] + " " + oDataReader[1];
                lblTWWID.Text = oDataReader[1].ToString(); //oDataReader[2].ToString();
                lblGender.Text = oDataReader[2].ToString();//oDataReader[3].ToString();
                lblDateOfBirth.Text = oDataReader[3].ToString();  //oDataReader[4].ToString();
                lblRole.Text = oDataReader[4].ToString();//oDataReader[5].ToString();
                lblStartDateWithTranscom.Text = oDataReader[5].ToString();//oDataReader[6].ToString();
                lblEmailAddress.Text = oDataReader[6].ToString(); //oDataReader[7].ToString();
                lblEmailAddress2.Text = oDataReader[6].ToString(); // oDataReader[7].ToString();
                lblCountry.Text = oDataReader[7].ToString();
                lblCountryID.Text = oDataReader[46].ToString(); //oDataReader[47].ToString();
                lblSite.Text = oDataReader[8].ToString();//oDataReader[9].ToString();

                lblSupervisor.Text = oDataReader[32].ToString();
                lblDepartment.Text = oDataReader["departmentid"].ToString() == "0"
                                    ? oDataReader["otherdepartment"].ToString()
                                    : oDataReader["departmentname"].ToString();
                lblCampaign.Text = oDataReader["campaignid"].ToString() == "0"
                                    ? oDataReader["othercampaign"].ToString()
                                    : oDataReader["campaignname"].ToString();
                lblClient.Text = oDataReader["ClientID"].ToString() == "0"
                                    ? oDataReader["otherclient"].ToString()
                                    : oDataReader["ClientName"].ToString();
                


                try
                {
                    cboAdditionalClient.Visible = false;
                    lblAdditionalClient.Visible = true;

                    DataSet ds = new DataSet();

                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
                    {
                        cn.Open();

                        string sql = @"
                            SELECT DISTINCT c.description AS Client
                            FROM UserClients uc
                            INNER JOIN Clients c
                                ON uc.ClientID = c.ID
                            WHERE uc.UserID = @UserID
                            ";
                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(_employeeId));
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                    }

                    lblAdditionalClient.Text = string.Empty;

                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        lblAdditionalClient.Text += item["Client"].ToString() + ",";
                    }

                    if (lblAdditionalClient.Text.Length > 0)
                    {
                        lblAdditionalClient.Text = lblAdditionalClient.Text.Remove(lblAdditionalClient.Text.Length - 1, 1);
                    }
                }
                catch (Exception)
                {

                }

                lblCareerPath.Text = oDataReader["careerpathid"].ToString() == "0"
                                    ? oDataReader["othercareerpath"].ToString()
                                    : oDataReader["careerpath"].ToString();

                cbEditReceiveUpdates.DataBind();
                lblReceiveUpdates.Text = "";

                List<String> names1 = oDataReader["updatevia"].ToString().Split(',').ToList();

                foreach (RadComboBoxItem itm in cbEditReceiveUpdates.Items)
                {
                    if (names1.Contains(itm.Value))
                    {
                        lblReceiveUpdates.Text = itm.Text + ", " + lblReceiveUpdates.Text;
                    }
                }

                if (lblReceiveUpdates.Text.Length > 0)
                {
                    char[] myChar = { ',', ' ' };
                    lblReceiveUpdates.Text = lblReceiveUpdates.Text.TrimEnd(myChar);
                }

                lblLanguage.Text = oDataReader["languageid"].ToString() == "0"
                                       ? oDataReader["otherlanguage"].ToString()
                                       : oDataReader["languagename"].ToString();

                lblRegion.Text = oDataReader["region"].ToString();
                lblSupervisor.Text = oDataReader["TLName"].ToString();

                lblMobileNo.Text = oDataReader["mobileno"].ToString();
                lblCoursesInterested.Text = oDataReader["courseinterests"].ToString();

                profilePhoto.ImageUrl = "~/Resources/PhotoHandler.ashx?id=" + _employeeId;
            }
        }

        if (isEdit)
        {
            cbDirectReports.DataBind();
            Page.Validate();
            btnEditProfile.Visible = false;
            btnUpdateProfile.Visible = true;
            btnCancelUpdate.Visible = true;
            divUpload.Visible = true;
            cbDirectReports.Visible = false;
            btnEditClient.Visible = false;

            if (oDataReader.Read())
            {
                lblEditTWWID.Text = oDataReader[2].ToString();
                lblEditGender.Text = oDataReader[3].ToString();
                lblEditDateOfBirth.Text = oDataReader[4].ToString();
                lblEditStartDateWithTranscom.Text = oDataReader[5].ToString();//oDataReader[6].ToString();
                txtEditEmailAddress.Text = oDataReader[6].ToString(); //oDataReader[7].ToString();
                txtEditEmailAddress2.Text = oDataReader[6].ToString(); //oDataReader[7].ToString();
                lblEditCountry.Text = oDataReader[7].ToString(); //oDataReader[8].ToString();

                try
                {
                    if (oDataReader["regionid"].ToString() == "0")
                    {
                        lblRegion.Visible = true;
                    }

                    else
                    {
                        cbEditRegion.Items.Clear();
                        cbEditRegion.DataBind();
                        cbEditRegion.SelectedValue = oDataReader["regionid"].ToString();
                        cbEditRegion.Visible = true;
                        lblRegion.Text = cbEditRegion.SelectedItem.Text; //oDataReader["regionid"].ToString();
                        lblRegion.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "Region" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString() ;
                    //ErrStr = ErrStr + exErr;
                }

                cbEditSupervisor.Items.Clear();
                cbEditSupervisor.DataBind();
                cbEditSupervisor.SelectedItem.Text = oDataReader[32].ToString();
                cbEditSupervisor.Visible = true;

                try
                {
                    if (oDataReader["departmentid"].ToString() == "0")
                    {
                        txtEditDepartment.Visible = false;
                        rfvOtherDepartment.Visible = false;
                        cbEditDepartment.SelectedIndex = 0;
                        txtEditDepartment.Text = oDataReader["otherdepartment"].ToString();
                        cbEditDepartment.Visible = true;
                    }
                    else
                    {
                        txtEditDepartment.Visible = false;
                        rfvOtherDepartment.Visible = false;
                        cbEditDepartment.Items.Clear();
                        cbEditDepartment.DataBind();
                        cbEditDepartment.SelectedValue = oDataReader["departmentid"].ToString();
                        cbEditDepartment.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "Dept" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
                    //ErrStr = ErrStr + exErr;
                }

                try
                {
                    if (oDataReader["campaignid"].ToString() == "0")
                    {
                        if (oDataReader["othercampaign"].ToString() == "Shared")
                        {
                            cbEditCampaign.SelectedIndex = 1;
                            txtEditCampaign.Text = "";
                            txtEditCampaign.Visible = false;
                            rfvOtherCampaign.Enabled = false;
                            cbEditCampaign.Visible = true;
                            cboAdditionalClient.Visible = true;
                        }
                        else
                        {
                            cbEditCampaign.SelectedIndex = 0;
                            txtEditCampaign.Text = oDataReader["othercampaign"].ToString();
                            txtEditCampaign.Visible = false;
                            rfvOtherCampaign.Enabled = false;
                            cbEditCampaign.Visible = true;
                            cboAdditionalClient.Visible = true;
                        }
                    }
                    else
                    {
                        txtEditCampaign.Visible = false;
                        rfvOtherCampaign.Enabled = false;
                        cbEditCampaign.Items.Clear();
                        cbEditCampaign.DataBind();
                        cbEditCampaign.SelectedValue = oDataReader["campaignid"].ToString();
                        cbEditCampaign.Visible = true;
                        cboAdditionalClient.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "Campaign" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
                    //ErrStr = ErrStr + exErr;
                }

                try
                {
                    cboAdditionalClient.Visible = true;
                    lblAdditionalClient.Visible = false;

                    DataSet ds = new DataSet();

                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
                    {
                        cn.Open();

                        string sql = @"
                            SELECT ClientID FROM UserClients WHERE UserID = @UserID
                            ";
                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(_employeeId));
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                    }

                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        foreach (RadComboBoxItem cboItem in cboAdditionalClient.Items)
                        {
                            if (item["ClientID"].ToString() == cboItem.Value)
                            {
                                cboItem.Checked = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }


                try
                {
                    if (oDataReader["clientid"].ToString() == "0" && oDataReader["otherclient"].ToString() != "")
                    {
                        if (oDataReader["otherclient"].ToString() == "Shared")
                        {
                            cbEditClient.SelectedIndex = 1;
                            txtEditClient.Text = "";
                            txtEditClient.Visible = false;
                            rfvOtherClient.Enabled = false;
                            cbEditClient.Visible = true;
                        }
                        else
                        {
                            cbEditClient.SelectedIndex = 0;
                            txtEditClient.Text = oDataReader["otherclient"].ToString();
                            txtEditClient.Visible = false;
                            rfvOtherClient.Enabled = false;
                            cbEditClient.Visible = true;
                        }
                    }
                    else
                    {
                        if (oDataReader["clientid"].ToString() != "0")
                        {
                            txtEditClient.Visible = false;
                            rfvOtherClient.Enabled = false;
                            cbEditClient.Items.Clear();
                            cbEditClient.DataBind();
                            cbEditClient.SelectedValue = oDataReader["clientid"].ToString();
                            cbEditClient.Visible = true;
                        }
                        else
                        {
                            txtEditClient.Visible = false;
                            rfvOtherClient.Enabled = false;
                            cbEditClient.Visible = true;
                            cbEditClient.Items.Clear();
                            cbEditClient.DataBind();
                            cbEditClient.Items.Insert(0, new ListItem("Shared", "0"));
                            cbEditClient.Items.Insert(0, new ListItem("Others", "-1"));
                            cbEditClient.SelectedValue = oDataReader["clientid"].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "Client" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
                    //ErrStr = ErrStr + exErr;
                }

                try
                {
                    if (oDataReader["careerpathid"].ToString() == "0")
                    {
                        txtEditCareerpath.Visible = false;
                        rfvOtherCareerpath.Enabled = false;
                        cbEditCareerPath.SelectedIndex = 0;
                        txtEditCareerpath.Text = oDataReader["othercareerpath"].ToString();
                        cbEditCareerPath.Visible = true;
                    }
                    else
                    {
                        txtEditCareerpath.Visible = false;
                        rfvOtherCareerpath.Enabled = false;
                        cbEditCareerPath.Items.Clear();
                        cbEditCareerPath.DataBind();
                        cbEditCareerPath.SelectedValue = oDataReader["careerpathid"].ToString();
                        cbEditCareerPath.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "CareerPath" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
                    //ErrStr = ErrStr + exErr;
                }

                try
                {
                    cbEditReceiveUpdates.DataBind();
                    List<String> names1 = oDataReader["updatevia"].ToString().Split(',').ToList();

                    foreach (RadComboBoxItem itm in cbEditReceiveUpdates.Items)
                    {
                        if (names1.Contains(itm.Value))
                        {
                            itm.Checked = true;
                        }
                    }
                    cbEditReceiveUpdates.Visible = true;
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "UpdateVia" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
                    //ErrStr = ErrStr + exErr;
                }

                try
                {
                    if (oDataReader["languageid"].ToString() == "0")
                    {
                        txtEditLanguage.Visible = false;
                        rfvOtherLanguage.Visible = true;
                        cbEditLanguage.SelectedIndex = 0;
                        txtEditLanguage.Text = oDataReader["otherlanguage"].ToString();
                        cbEditLanguage.Visible = true;
                    }
                    else
                    {
                        txtEditLanguage.Visible = false;
                        rfvOtherLanguage.Visible = false;
                        cbEditLanguage.DataBind();
                        cbEditLanguage.SelectedValue = oDataReader["languageid"].ToString();
                        cbEditLanguage.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    //string exErr = Environment.NewLine + "LanguageID" + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
                    //ErrStr = ErrStr + exErr;
                }
                txtEditMobileNo.Text = oDataReader["mobileno"].ToString();
                txtEditCoursesInterested.Text = oDataReader["courseinterests"].ToString();
            }
        }
        oDataReader.Close();
        _oconn.Close();
    }

    protected void btnViewMyProfile_Click(object sender, EventArgs e)
    {
        SetPageOnViewing();
    }

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ChangePassword.aspx");
    }

    protected void btnEditProfile_Click(object sender, EventArgs e)
    {
        _employeeId = lblTWWID.Text;
        SetDetails(false, true);
        EditShow();
    }

    private void HideEditColumn()
    {
        foreach (HtmlTableRow thisRow in tblProfile.Rows)
        {
            thisRow.Cells[2].Visible = false;
        }
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void btnUpdateProfile_Click(object sender, EventArgs e)
    {
        _master = Master;
        GetSession();

        try
        {
            Page.Validate("update");
            if (IsValid)
            {
                var file = fileUpload.PostedFile;
                var bytes = new byte[] { };

                if ((file != null) && (file.ContentLength > 0))
                {
                    if (IsImage(file) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                            "alert('Only .JPG, .JPEG or .PNG format will be accepted.');", true);
                        return;
                    }

                    var iFileSize = file.ContentLength;

                    if (iFileSize > 2097152)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                            "alert('Maximum of 2MB file size will be accepted. You are trying to upload larger than the limit.');", true);
                        return;
                    }

                    bytes = new byte[file.ContentLength];
                    file.InputStream.Read(bytes, 0, file.ContentLength);
                }

                var updateVia = GetComboBoxSelectedItems(cbEditReceiveUpdates);
                if (updateVia == null)
                {
                    updateVia = "";
                }
                
                var supervisorChkr = DataHelper.GetSupervisor(Convert.ToInt32(lblTWWID.Text));
                string _Supervisor = supervisorChkr.SupervisorID.ToString();

                Dbconn("TranscomUniv_localConnectionString");
                _ocmd = new SqlCommand("pr_TranscomUniversity_InsertUpdateProfile", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@TWWID", lblTWWID.Text);
                _ocmd.Parameters.AddWithValue("@OtherCampaign", cbEditCampaign.SelectedIndex == 1 ? "Shared" : txtEditCampaign.Text);
                _ocmd.Parameters.AddWithValue("@CampaignID", cbEditCampaign.SelectedValue);
                _ocmd.Parameters.AddWithValue("@OtherClient", (cbEditClient.SelectedIndex == 1) ? "Shared" : txtEditClient.Text);
                _ocmd.Parameters.AddWithValue("@ClientID", (cbEditClient.SelectedValue == "Shared") || (cbEditClient.SelectedValue == "Other") ? "0" : cbEditClient.SelectedValue);
                _ocmd.Parameters.AddWithValue("@RegionID", cbEditRegion.SelectedValue);
                _ocmd.Parameters.AddWithValue("@LanguageID", cbEditLanguage.SelectedValue);
                _ocmd.Parameters.AddWithValue("@MobileNo", txtEditMobileNo.Text);
                _ocmd.Parameters.AddWithValue("@CareerPathID", cbEditCareerPath.SelectedValue);
                _ocmd.Parameters.AddWithValue("@OtherCareerPath", txtEditCareerpath.Text);
                _ocmd.Parameters.AddWithValue("@CourseInterests", txtEditCoursesInterested.Text);
                _ocmd.Parameters.AddWithValue("@UploadedPhoto", bytes);
                _ocmd.Parameters.AddWithValue("@UpdateVia", updateVia);
                _ocmd.Parameters.AddWithValue("@otherlanguage", txtEditLanguage.Text);
                _ocmd.Parameters.AddWithValue("@email", txtEditEmailAddress.Text);
                _ocmd.Parameters.AddWithValue("@departmentid", (cbEditDepartment.SelectedValue == "Shared") || (cbEditDepartment.SelectedValue == "Other") ? "0" : cbEditDepartment.SelectedValue);
                _ocmd.Parameters.AddWithValue("@OtherDepartment", txtEditDepartment.Text);
                _ocmd.Parameters.AddWithValue("@SupervisorID", (cbEditSupervisor.SelectedValue == "0") ? _Supervisor : cbEditSupervisor.SelectedValue);
                _ocmd.ExecuteReader();
                _oconn.Close();

                //Update User Clients
                try
                {
                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
                    {
                        cn.Open();

                        string sql = @"DELETE FROM UserClients WHERE UserID = @UserID";
                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(_employeeId));
                        cmd.ExecuteNonQuery();
                    }

                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
                    {
                        cn.Open();

                        string sql;

                        foreach (RadComboBoxItem item in cboAdditionalClient.CheckedItems)
                        {
                            sql = @"INSERT INTO UserClients(UserID, ClientID) VALUES(@UserID, @ClientID)";
                            SqlCommand cmd = new SqlCommand(sql, cn);
                            cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(_employeeId));
                            cmd.Parameters.AddWithValue("@ClientID", Convert.ToInt32(item.Value));
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception)
                {

                }

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    "alert('Congratulations! Your profile details are now updated.');", true);
                GetSession();
                SetDetails(true, false);
                ViewShow();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    "alert('Request failed. Please try again.');", true);
                SetDetails(false, true);
            }
        }
        catch (Exception ex)
        {
            string ErrF = ex.ToString() + Environment.NewLine + ex.InnerException.ToString();
        }
    }

    protected void cbDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbEditDepartment.Text == "Other")
        {
            txtEditDepartment.Visible = true;
            rfvOtherDepartment.Enabled = true;
        }
        else
        {
            txtEditDepartment.Visible = false;
            rfvOtherDepartment.Enabled = false;
        }
    }

    protected void cbSupervisor_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtEditSupervisor.Visible = false;
    }

    protected void cbCampaign_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbEditCampaign.Text == "Other")
        {
            txtEditCampaign.Visible = true;
            rfvOtherCampaign.Enabled = true;
        }
        else
        {
            txtEditCampaign.Visible = false;
            rfvOtherCampaign.Enabled = false;
        }
    }

    protected void cbClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbEditClient.Text == "Other")
        {
            txtEditClient.Visible = true;
            rfvOtherClient.Enabled = true;
        }
        else
        {
            txtEditClient.Visible = false;
            rfvOtherClient.Enabled = false;
        }

        if (txtEditCampaign.Visible)
        {
            cbEditCampaign.SelectedIndex = 0;
        }
    }

    protected void cbEditLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbEditLanguage.Text == "Other")
        {
            txtEditLanguage.Visible = true;
            rfvOtherLanguage.Enabled = true;
        }
        else
        {
            txtEditLanguage.Visible = false;
            rfvOtherLanguage.Enabled = false;
        }
    }

    protected void cbEditCareerPath_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (cbeditcareerpath.text == "other")
        //{
        //    txteditcareerpath.visible = true;
        //    rfvothercareerpath.enabled = true;
        //}
        //else
        //{
        //    txteditcareerpath.visible = false;
        //    rfvothercareerpath.enabled = false;
        //}
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        _employeeId = Request.Form["txtSearch"];
        SetDetails(true, false);
        SearchValue = _employeeId;

        foreach (HtmlTableRow thisRow in tblProfile.Rows)
        {
            thisRow.Cells[1].Visible = true;
            thisRow.Cells[2].Visible = false;
        }

        btnEditProfile.Visible = User.Identity.Name == Request.Form["txtSearch"];
    }

    public void ClearEditControls()
    {
        lblTWWID.Text = "";
        txtEditCampaign.Text = "";
        txtEditClient.Text = "";
        txtEditMobileNo.Text = "";
        txtEditCareerpath.Text = "";
        txtEditCoursesInterested.Text = "";
        txtEditLanguage.Text = "";
        txtEditEmailAddress.Text = "";
        txtEditEmailAddress2.Text = "";
        txtEditDepartment.Text = "";

        fileUpload.FileContent.Flush();

        cbEditReceiveUpdates.ClearSelection();

        cbEditDepartment.Text = "";
        cbEditCampaign.Text = "";
        cbEditClient.Text = "";
        cbEditRegion.Text = "";
        cbEditLanguage.Text = "";
        cbEditCareerPath.Text = "";
        cbEditCareerPath.Text = "";

        cbEditDepartment.ClearSelection();
        cbEditCampaign.ClearSelection();
        cbEditClient.ClearSelection();
        cbEditRegion.ClearSelection();
        cbEditLanguage.ClearSelection();
        cbEditCareerPath.ClearSelection();
        cbEditCareerPath.ClearSelection();
    }

    public void EditShow()
    {
        lblEmailAddress.Visible = false;
        lblEmailAddress2.Visible = false;
        lblMobileNo.Visible = false;
        lblRegion.Visible = false;
        lblCountry.Visible = false;
        lblSupervisor.Visible = false;
        lblDepartment.Visible = false;
        lblClient.Visible = false;
        lblAdditionalClient.Visible = false;
        lblCampaign.Visible = false;
        lblLanguage.Visible = false;
        lblCareerPath.Visible = false;
        lblCoursesInterested.Visible = false;
        lblReceiveUpdates.Visible = false;

        txtEditEmailAddress.Visible = true;
        txtEditEmailAddress2.Visible = true;
        txtEditMobileNo.Visible = true;
        cbEditRegion.Visible = false;
        lblEditCountry.Visible = true;

        //lblEditSupervisor.Visible = true;
        var positionChkr = DataHelper.GetPosition(Convert.ToInt32(_employeeId));
        string _Role = positionChkr.NombreCatTranscomDesc.ToString();

        if (_Role != "2")
        {
            cbEditSupervisor.Visible = true;
            cbEditClient.Visible = true;
            lblClient.Visible = false;
            cboAdditionalClient.Visible = true;
            lblAdditionalClient.Visible = false;
        }
        else
        {
            cbEditSupervisor.Enabled = false;
            cbEditClient.Visible = false;
            cboAdditionalClient.Visible = false;
            lblAdditionalClient.Visible = true;
            lblClient.Visible = true;
            cbEditClient.Visible = false;
        }

        cbEditDepartment.Visible = true;
        cbEditCampaign.Visible = true;
        cbEditLanguage.Visible = true;
        cbEditCareerPath.Visible = true;
        txtEditCoursesInterested.Visible = true;
        cbEditReceiveUpdates.Visible = true;

        lblRegion.Visible = true;


        string clientIDStr = "";

        foreach (BaseValidator validator in Page.Validators)
        {
            if (validator.Enabled && !validator.IsValid)
            {
                // Put a breakpoint here
                clientIDStr = Environment.NewLine + validator.ControlToValidate + clientIDStr;
            }
        }

        string FinalErr = clientIDStr;
    }

    public void ViewShow()
    {
        lblEmailAddress.Visible = true;
        lblEmailAddress2.Visible = true;
        lblMobileNo.Visible = true;
        lblRegion.Visible = true;
        lblCountry.Visible = true;
        lblSupervisor.Visible = true;
        lblDepartment.Visible = true;
        lblClient.Visible = true;
        lblCampaign.Visible = true;
        lblAdditionalClient.Visible = true;
        lblLanguage.Visible = true;
        lblCareerPath.Visible = true;
        lblCoursesInterested.Visible = true;
        lblReceiveUpdates.Visible = true;

        txtEditEmailAddress.Visible = false;
        txtEditEmailAddress2.Visible = false;
        txtEditMobileNo.Visible = false;
        cbEditRegion.Visible = false;
        lblEditCountry.Visible = false;
        //lblEditSupervisor.Visible = false;
        cbEditSupervisor.Visible = false;
        cbEditDepartment.Visible = false;
        cbEditClient.Visible = false;
        cboAdditionalClient.Visible = false;
        cbEditCampaign.Visible = false;
        cbEditLanguage.Visible = false;
        cbEditCareerPath.Visible = false;
        txtEditCoursesInterested.Visible = false;
        cbEditReceiveUpdates.Visible = false;

        btnUpdate_Client.Visible = false;
        cbEditClient.Visible = false;
        btnSaveClient.Visible = false;
    }

    protected void btnUpdate_Client_Click(object sender, EventArgs e)
    {
        cbEditClient.Visible = true;
        btnSaveClient.Visible = true;
        btnUpdate_Client.Visible = false;
    }

    protected void btnSaveClient_onclick(object sender, EventArgs e)
    {
        try
        {
            Dbconn("TranscomUniv_localConnectionString");
            _ocmd = new SqlCommand("pr_UpdateClient", _oconn)
            {
                CommandType = CommandType.StoredProcedure
            };

            _ocmd.Parameters.AddWithValue("@TWWID", lblTWWID.Text);
            _ocmd.Parameters.AddWithValue("@ClientID", (cbEditClient.SelectedValue == "Shared") || (cbEditClient.SelectedValue == "Other") ? "0" : cbEditClient.SelectedValue);
            _ocmd.ExecuteReader();
            _oconn.Close();

            btnSaveClient.Visible = false;
            //_master.AlertMessage(true, "Your client is now updated.");
            GetSession();

            SetDetails(true, false);
            ViewShow();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnUpdateEmpClient_Click(object sender, EventArgs e)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            connection.Open();

            string sql = "UPDATE TranscomUniv_Local.dbo.Users SET ClientID = @ClientID WHERE TWWID = @TWWID";
            SqlCommand cmd = new SqlCommand(sql, connection);

            cmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = cbClients.SelectedValue;
            cmd.Parameters.Add("@TWWID", SqlDbType.Int).Value = cbDirectReports.SelectedValue;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Client updated for employee " + cbDirectReports.SelectedItem.Text + " ');", true);
    }
}