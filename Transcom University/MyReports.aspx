﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MyReports.aspx.cs" Inherits="MyReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportExcel") >= 0) {
                    args.set_enableAjax(false);
                }
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }
                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function triggerValidation() {
                Page_ClientValidate();
            }
            function openWinContentTemplate() {
                $find("<%=rdGlossary.ClientID %>").show();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReports" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div>
                        <div align="center">
                            <telerik:RadComboBox ID="cmbReportList" DataTextField="ReportName" DataValueField="ReportID"
                                runat="server" EmptyMessage="- select report -" DropDownAutoWidth="Enabled" MaxHeight="300"
                                AutoPostBack="true" DataSourceID="dsReports" ToolTip="Select Report" OnSelectedIndexChanged="cmbReportList_SelectedIndexChanged" />
                            <%--DataSourceID="dsReports"--%>
                        </div>
                        <table width="100%">
                            <tr align="center">
                                <td>
                                    <asp:Label runat="server" ID="lblRptName" Text="Please select a Report" Font-Size="X-Large" />
                                    <asp:Label runat="server" ID="lblClientID" Visible="false" />
                                    <asp:Label runat="server" ID="lblTWWID" Visible="false" />
                                </td>
                            </tr>
                            <tr id="trDesc" runat="server" visible="true" align="center">
                                <td>
                                    <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                    <br>
                                </td>
                            </tr>
                            <tr id="trPopup" runat="server" visible="true" align="center">
                                <td>
                                    <telerik:RadButton ID="btnGlossary" runat="server" Text="Glossary" Visible="false"
                                        ButtonType="StandardButton" OnClientClicked="openWinContentTemplate" AutoPostBack="false">
                                    </telerik:RadButton>

                                    <telerik:RadWindow runat="server" ID="rdGlossary" Modal="true" Width="1200px" Height="600px">
                                        <ContentTemplate>
                                            <p class="contText">
                                                <asp:Image ID="imgGlossary" runat="server" ImageUrl="~/Images/Glossary.PNG" />
                                            </p>
                                        </ContentTemplate>
                                    </telerik:RadWindow>
                                    <br>
                                </td>
                            </tr>
                            <tr id="trHoriz" runat="server" visible="False" align="center">
                                <td>
                                    <hr />
                                </td>
                            </tr>
                        </table>
                        <table id="tblFilters" runat="server" visible="true" class="divReportParams">
                            <tr id="tr1" runat="server">
                            </tr>
                        </table>
                        <table id="tblViewAndExport" width="100%" visible="false" runat="server" style="padding-bottom: 10px">
                            <tr>
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadButton ID="btnView" runat="server" Text="View Report" OnClick="btnView_Click"
                                        ValidationGroup="vgTRA" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportExcel" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export this report to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <telerik:RadGrid runat="server" ID="gridReports" GridLines="None" OnNeedDataSource="gridReports_NeedDataSource"
                            AllowSorting="True" EnableLinqExpressions="True" CssClass="RadGrid1" Skin="Metro" >
                            <ClientSettings>
                                <Scrolling AllowScroll="True" ScrollHeight="400px" />
                                <%--<Scrolling AllowScroll="True" SaveScrollPosition="True" FrozenColumnsCount="1" />--%>
                            </ClientSettings>
                            <MasterTableView ShowHeader="true" NoMasterRecordsText="No result for selected report."
                                AllowPaging="True" PageSize="50" ShowHeadersWhenNoRecords="False" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                                TableLayout="Auto" Width="1000" >
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="True" Mode="NextPrevAndNumeric" />
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="dsReports" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_MyReports" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="RoleName" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsParameters" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select reportid,telerikcontrol,label,datasourceid,datavaluefield,datatextfield
            from tbl_TranscomUniversity_Lkp_Report_Params where active = 1  order by ordinal"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsID" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select ID from ref_TranscomUniversity_Class where active = 1 order by id"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsTwwid" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select twwid from TranscomUniv_local.dbo.Users where roleid in(1,5) order by twwid"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsCourse" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="IF EXISTS(SELECT * FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value = @ClientID) AND NOT EXISTS(SELECT * FROM Intranet.dbo.vw_Check_CourseAdmin WHERE TWWID = @TWWID) AND NOT EXISTS(SELECT * FROM Intranet.dbo.vw_Check_UserRole WHERE TWWID = @TWWID)
                            BEGIN
                                --SELECT '1'
                                SELECT C.CourseID, C.CourseName FROM tbl_NuCommUniversity_Lkp_Course C
                                INNER JOIN Intranet.dbo.tbl_TranscomUniversity_Cor_Course CC ON C.CourseID = CC.NucommCourseID
                                WHERE C.HideFromList = 0 AND CC.CourseID NOT IN (SELECT CourseID FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value NOT IN (@ClientID, 0))
                                UNION ALL
                                SELECT CourseID, Title AS CourseName FROM Intranet.dbo.tbl_TranscomUniversity_Cor_Course WHERE CourseTypeID = 2 AND CourseID 
                                NOT IN (SELECT CourseID FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value NOT IN (@ClientID, 0)) ORDER BY 2
                            END
                        ELSE IF EXISTS(SELECT * FROM Intranet.dbo.vw_Check_CourseAdmin WHERE TWWID = @TWWID) --COURSE ADMIN
                            BEGIN
                                --SELECT '2'
                                SELECT C.CourseID, C.CourseName FROM tbl_NuCommUniversity_Lkp_Course C
                                INNER JOIN Intranet.dbo.tbl_TranscomUniversity_Cor_Course CC ON C.CourseID = CC.NucommCourseID
                                WHERE C.HideFromList = 0 AND (CC.PublishedBy = @TWWID OR CC.CourseID IN (SELECT CourseID FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value IN (@ClientID)))
                                UNION ALL
                                SELECT CourseID, Title AS CourseName FROM Intranet.dbo.tbl_TranscomUniversity_Cor_Course WHERE CourseTypeID = 2 AND PublishedBy = @TWWID AND CourseID 
                                IN (SELECT CourseID FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value IN (@ClientID)) ORDER BY 2
                            END
                        ELSE IF EXISTS(SELECT * FROM Intranet.dbo.vw_Check_UserRole WHERE TWWID = @TWWID) --ADMIN
                            BEGIN
                                --SELECT '3'
                                SELECT CourseID, CourseName FROM tbl_NuCommUniversity_Lkp_Course WHERE HideFromList = 0
                                UNION ALL
                                SELECT CourseID, Title AS CourseName FROM [dbo].[tbl_TranscomUniversity_Cor_Course] WHERE CourseTypeID = 2 ORDER BY 2
                            END">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblClientID" Name="ClientID" PropertyName="Text" />
                <asp:ControlParameter ControlID="lblTWWID" Name="TWWID" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="dsCourse2" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="IF EXISTS(SELECT * FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value = @ClientID) AND NOT EXISTS(SELECT * FROM Intranet.dbo.vw_Check_CourseAdmin WHERE TWWID = @TWWID) AND NOT EXISTS(SELECT * FROM Intranet.dbo.vw_Check_UserRole WHERE TWWID = @TWWID)
                            BEGIN
                                --SELECT '1'
                                SELECT CourseID, Title AS CourseName 
                                FROM Intranet.dbo.tbl_TranscomUniversity_Cor_Course 
                                WHERE HideFromList = 0  AND CourseID NOT IN (SELECT CourseID FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value NOT IN (@ClientID, 0))
                                ORDER BY Title
                            END
                        ELSE IF EXISTS(SELECT * FROM Intranet.dbo.vw_Check_CourseAdmin WHERE TWWID = @TWWID) --COURSE ADMIN
                            BEGIN
                                --SELECT '2'
                                SELECT CourseID, Title AS CourseName 
                                FROM Intranet.dbo.tbl_TranscomUniversity_Cor_Course 
                                WHERE HideFromList = 0  AND (PublishedBy = @TWWID OR CourseID IN (SELECT CourseID FROM Intranet.dbo.tbl_transcomuniversity_rlt_prequisite WHERE PrerequisiteTypeID = 4 AND Value IN (@ClientID)))
                                ORDER BY Title
                            END
                        ELSE IF EXISTS(SELECT * FROM Intranet.dbo.vw_Check_UserRole WHERE TWWID = @TWWID) --ADMIN
                            BEGIN
                                --SELECT '3'
                                SELECT CourseID, Title AS CourseName FROM Intranet.dbo.tbl_TranscomUniversity_Cor_Course WHERE HideFromList = 0  ORDER BY Title
                            END">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblClientID" Name="ClientID" PropertyName="Text" />
                <asp:ControlParameter ControlID="lblTWWID" Name="TWWID" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="dsProgram" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT 0 AS ProgramID,'Shared' AS Program
            UNION ALL
            SELECT DISTINCT ID AS ProgramID, Description AS Program 
            FROM TranscomUniv_local.dbo.Clients
            WHERE Active = 1  ORDER BY 2" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsClassName" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT distinct [ClassName] FROM [ref_TranscomUniversity_Class] WHERE [Active] = 1"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsAudience" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT AudienceID, AudienceName FROM [dbo].[tbl_TranscomUniversity_Cor_Audience] WHERE DateEnd >= GETDATE() ORDER BY AudienceName"
            SelectCommandType="Text" />
<%--        <asp:SqlDataSource ID="dsAssignCourseReportingManager" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_lkp_AssignCourseReportingManager" SelectCommandType="StoredProcedure"
            OnSelecting="dsAssignCourseReportingManager_Selecting">
            <SelectParameters>
                <asp:Parameter Name="CIMNumber" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="dsAssignCourseReportingManager" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT E.TWWID AS Value, E.Nombre AS ReportingManager
            FROM TranscomUniv_local.dbo.Empleados E
            INNER JOIN TranscomUniv_local.dbo.Users U
            ON E.TWWID = U.TWWID
            WHERE E.NombreCatTranscomDesc LIKE '%Manager%'
            AND E.TWWID IS NOT NULL
            UNION ALL
            SELECT E.TWWID AS Value, E.Nombre AS ReportingManager
                        FROM TranscomUniv_local.dbo.Empleados E
                        INNER JOIN TranscomUniv_local.dbo.Users U
                        ON E.TWWID = U.TWWID
                        WHERE E.NombreCatTranscomDesc LIKE '%Team Leader%'
                        AND E.TWWID IS NOT NULL
                        ORDER BY 2 ASC" SelectCommandType="Text"></asp:SqlDataSource>
<%--        <asp:SqlDataSource ID="dsCourseEnrollmentReportingManager" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SElECT DISTINCT SupCimNo 'Value', SupCimNo 'ReportingManager'
	FROM tbl_TranscomUniversity_CourseEnrollments" SelectCommandType="Text"></asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="dsCourseEnrollmentReportingManager" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT E.TWWID AS Value, E.Nombre AS ReportingManager
            FROM TranscomUniv_local.dbo.Empleados E
            INNER JOIN TranscomUniv_local.dbo.Users U
            ON E.TWWID = U.TWWID
            WHERE E.NombreCatTranscomDesc LIKE '%Manager%'
            AND E.TWWID IS NOT NULL
            UNION ALL
            SELECT E.TWWID AS Value, E.Nombre AS ReportingManager
                        FROM TranscomUniv_local.dbo.Empleados E
                        INNER JOIN TranscomUniv_local.dbo.Users U
                        ON E.TWWID = U.TWWID
                        WHERE E.NombreCatTranscomDesc LIKE '%Team Leader%'
                        AND E.TWWID IS NOT NULL
                        ORDER BY 2 ASC" SelectCommandType="Text"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsHarmonyClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_Client" SelectCommandType="StoredProcedure" />
        <asp:SqlDataSource ID="dsManagers" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
            FROM TranscomUniv_local.dbo.Empleados E
            INNER JOIN TranscomUniv_local.dbo.Users U
            ON E.TWWID = U.TWWID
            WHERE E.NombreCatTranscomDesc LIKE '%Manager%'
            AND E.TWWID IS NOT NULL
            UNION ALL
            SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
                        FROM TranscomUniv_local.dbo.Empleados E
                        INNER JOIN TranscomUniv_local.dbo.Users U
                        ON E.TWWID = U.TWWID
                        WHERE E.NombreCatTranscomDesc LIKE '%Team Leader%'
                        AND E.TWWID IS NOT NULL
                        ORDER BY 2 ASC" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsSupervisors" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
            FROM TranscomUniv_local.dbo.Empleados E
            INNER JOIN TranscomUniv_local.dbo.Users U
            ON E.TWWID = U.TWWID
            WHERE E.NombreCatTranscomDesc LIKE '%Manager%'
            AND E.TWWID IS NOT NULL
            UNION ALL
            SELECT E.TWWID AS ManagerCIM, E.Nombre AS ManagerName
                        FROM TranscomUniv_local.dbo.Empleados E
                        INNER JOIN TranscomUniv_local.dbo.Users U
                        ON E.TWWID = U.TWWID
                        WHERE E.NombreCatTranscomDesc LIKE '%Team Leader%'
                        AND E.TWWID IS NOT NULL
                        ORDER BY 2 ASC" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [id] as DepartmentID, [description] as Department FROM TranscomUniv_local.dbo.refDepartments WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="dsClients" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet%>"
            SelectCommand="SELECT 0 AS ProgramID,'Shared' AS Program
            UNION ALL
            SELECT DISTINCT ID AS ProgramID, Description AS Program 
            FROM TranscomUniv_local.dbo.Clients
            WHERE Active = 1  ORDER BY 2" />
    </div>
</asp:Content>
