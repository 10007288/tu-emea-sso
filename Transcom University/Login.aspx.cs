﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls.WebParts;
using System.Threading;
using System.Globalization;
using Microsoft.AspNet.Membership.OpenAuth;


public partial class Login : BasePage
{
    #region OldTUEMEA
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    lblTWW_ID.Text = Resources.LocalizedResource.LoginName.ToString();
    //    lblPass.Text = Resources.LocalizedResource.Pass.ToString();
    //    btnLogin.Text = Resources.LocalizedResource.LoginName.ToString();
    //    lblWelHdr.Text = Resources.LocalizedResource.LoginWelHdr.ToString();
    //    btnCreateAcct.Text = Resources.LocalizedResource.CreateUser.ToString();
    //    btnForgotPass.Text = Resources.LocalizedResource.ForgotPass.ToString();

    //    string cnumber;
    //    cnumber = Request.QueryString["SessionID"];
    //    if (cnumber != null)
    //    {
    //        txtUsername.Text = cnumber;
    //        string strtrimid = txtUsername.Text;
    //        strtrimid = strtrimid.TrimStart('0');
    //        txtUsername.Text = strtrimid;

    //        lblNotify.Text = "Congratulations! You have registered a new account. Click here to sign in.";
    //    }
    //}

    //private void LogMembership()
    //{
    //    var user = Membership.GetUser(txtUsername.Text.Trim());

    //    if (user != null)
    //    {
    //        var sSafePassword = Server.HtmlEncode(txtPassword.Text.Trim());

    //        if (Membership.ValidateUser(txtUsername.Text.Trim(), sSafePassword))
    //        {
    //            FormsAuthentication.Authenticate(txtUsername.Text.Trim(), sSafePassword);
    //            FormsAuthentication.RedirectFromLoginPage(txtUsername.Text.Trim(), false);

    //            Response.Redirect("~/Default.aspx");
    //        }
    //        else
    //        {
    //            lblNotify.Text = "Invalid CIM Number or Password.";
    //        }
    //    }
    //    else
    //    {
    //        MembershipCreateStatus status1 = new MembershipCreateStatus();
    //        MembershipUser newUser = System.Web.Security.Membership.CreateUser(txtUsername.Text.Trim(), txtPassword.Text.Trim(), "", "", "", true, out status1);
    //        lblNotify.Text = "Invalid CIM Number or Password.";
    //    }
    //}

    //protected void btnLogin_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        var authUser = Registration.GetUser(txtUsername.Text).SingleOrDefault();

    //        if (authUser != null)
    //        {
    //            var isCorrectPasssword = Password.ValidatePassword(txtPassword.Text, authUser.Password);

    //            if (isCorrectPasssword)
    //            {
    //                try
    //                {
    //                    //FormsAuthentication.RedirectFromLoginPage(txtUsername.Text.Trim(), false);
    //                    var UserRoleID = Registration.GetRoleForUser(txtUsername.Text);
    //                    DropDownList dd1 = (DropDownList)Master.FindControl("ddLangSel");

    //                    Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
    //                    lblWel.Text = txtUsername.Text;

    //                    Label lblWel_1 = (Label)Master.FindControl("lblWelcome");
    //                    lblWel_1.Text = "Welcome back,";

    //                    var lv = (MultiView)Master.FindControl("mvMenu");

    //                    if (UserRoleID == "Admin")
    //                    {
    //                        var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
    //                        hpTURep.Visible = false;
    //                        var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
    //                        hpTUTestAdmin.Visible = true;
    //                        var vw = (View)lv.FindControl("vwAdmin");
    //                        lv.ActiveViewIndex = 2;
    //                        lv.SetActiveView(vw);
    //                        lv.Visible = true;
    //                    }
    //                    else if (UserRoleID == "Normal User")
    //                    {
    //                        var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
    //                        hpTURep.Visible = false;
    //                        var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
    //                        hpTUTestAdmin.Visible = false;
    //                        var vw = (View)lv.FindControl("vwNormal_User");
    //                        lv.ActiveViewIndex = 1;
    //                        lv.SetActiveView(vw);
    //                        lv.Visible = true;
    //                    }
    //                    else if (UserRoleID == "Super User")
    //                    {
    //                        var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
    //                        hpTURep.Visible = false;
    //                        var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
    //                        hpTUTestAdmin.Visible = true;
    //                        var vw = (View)lv.FindControl("vwSuper_User");
    //                        lv.ActiveViewIndex = 3;
    //                        lv.SetActiveView(vw);
    //                        lv.Visible = true;
    //                    }
    //                    else if (UserRoleID == "Course Admin")
    //                    {
    //                        var hpTURep = (HyperLink)Master.FindControl("hplTUReporting");
    //                        hpTURep.Visible = false;
    //                        var hpTUTestAdmin = (HyperLink)Master.FindControl("hplTUTestingAdmin");
    //                        hpTUTestAdmin.Visible = true;
    //                        var vw = (View)lv.FindControl("vwCourse_Admin");
    //                        lv.ActiveViewIndex = 2;
    //                        lv.SetActiveView(vw);
    //                        lv.Visible = true;
    //                    }

    //                    string persistUser = string.Empty;
    //                    persistUser = lblWel.Text + "|" + authUser.FirstName + "|" + UserRoleID + "|" + lv.ActiveViewIndex + "|" + dd1.SelectedValue.ToString();

    //                    FormsAuthentication.SetAuthCookie(persistUser, true);

    //                    //Check last password change
    //                    if (!authUser.LastPasswordChangeDate.HasValue)
    //                    {
    //                        Response.Redirect("~/ChangePassword.aspx");
    //                    }
    //                    else
    //                    {
    //                        FormsAuthentication.RedirectFromLoginPage(persistUser, true);
    //                    }
    //                }
    //                catch (Exception ex1)
    //                {
    //                    lblNotify.Text = ex1.Message.ToString();
    //                }
    //            }
    //            else
    //            {
    //                lblNotify.Text = "Invalid credentials. Please try again";
    //            }
    //        }
    //        else
    //        {
    //            lblNotify.Text = "Invalid credentials. Please try again";
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //        lblNotify.Text = ex.Message.ToString();
    //    }
    //}

    //protected void btnCreateUser_Click(object sender, EventArgs e)
    //{

    //    Response.Redirect("Register.aspx");
    //}

    //protected void btnForgotPass_Click(object sender, EventArgs e)
    //{

    //    Response.Redirect("ForgotPassword.aspx");
    //}

    //protected void PrepareLoginForm()
    //{
    //    var lv = (LoginView)Master.FindControl("LoginView1");
    //    lv.Visible = false;
    //    var lvWelcome = (LoginView)Master.FindControl("lvWelcome");
    //    lvWelcome.Visible = false;

    //    var hlpCourse = (HtmlAnchor)Master.FindControl("hlpCourseCatalog");
    //    hlpCourse.Visible = false;

    //    txtUsername.Text = String.Empty;
    //    txtUsername.Focus();
    //}

    //protected void txtUsernameChange(object sender, EventArgs e)
    //{
    //    string strtrim = txtUsername.Text;
    //    strtrim = strtrim.TrimStart('0');
    //    txtUsername.Text = strtrim;
    //}
#endregion

    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];

        lblWelHdr.Text = Resources.LocalizedResource.LoginWelHdr.ToString();

        Label lblWelcome = (Label)Master.FindControl("lblWelcome");
        lblWelcome.Visible = false;
    }

    public IEnumerable<ProviderDetails> GetProviderNames()
    {
        return OpenAuth.AuthenticationClients.GetAll();
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        Session["EmpID"] = Request.QueryString["EmpID"];
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}