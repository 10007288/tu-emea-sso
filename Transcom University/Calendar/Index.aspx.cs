﻿using System;
using System.Web.UI;
using TranscomUniversityUI;

namespace Calendar
{
    public partial class CalendarIndex : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateClientListBox();
                PopulateSiteListBox();
                PopulateRoomListBox();
            }
        }

        public void PopulateClientListBox()
        {
            var clients = ClientOnClass.GetClients();

            lstClients.DataTextField = "ClientDescription";
            lstClients.DataValueField = "ClientId";
            lstClients.DataSource = clients;
            lstClients.DataBind();
            lstClients.SelectedIndex = 0;
        }

        public void PopulateSiteListBox()
        {
            var sites = SiteOnClass.GetSites();

            lstSites.DataTextField = "SiteDescription";
            lstSites.DataValueField = "SiteId";
            lstSites.DataSource = sites;
            lstSites.DataBind();
            lstSites.SelectedIndex = 0;
        }

        public void PopulateRoomListBox()
        {
            var rooms = RoomOnClass.GetRooms();

            lstRooms.DataTextField = "RoomDescription";
            lstRooms.DataValueField = "RoomId";
            lstRooms.DataSource = rooms;
            lstRooms.DataBind();
            lstRooms.SelectedIndex = 0;
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            //string view = rbProfessional.Checked ? "1" : "2";
            string url = null;

            if (rbProfessional.Checked)
            {
                url = string.Format("CalendarView.aspx?view={0}&month={1}&id=",
                    1, DateTime.Now.Month);
            }
            else if(rbClient.Checked)
             {
                url = string.Format("CalendarView.aspx?view={0}&month={1}&id={2}",
                    2, DateTime.Now.Month, lstClients.SelectedItem.Value);
            }
            else if (rbSite.Checked)
            {
                url = string.Format("CalendarView.aspx?view={0}&month={1}&id={2}",
                    3, DateTime.Now.Month, lstSites.SelectedItem.Value);
            }
            else if (rbRoom.Checked)
            {
                url = string.Format("CalendarView.aspx?view={0}&month={1}&id={2}",
                    4, DateTime.Now.Month, lstRooms.SelectedItem.Value);
            }

            Response.Redirect(url);
        }
    }
}