﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Registration
/// </summary>
public class Registration
{
    public class Users_TL
    {
        public Decimal UserId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Twwid { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public int? FailedPasswordAttempCount { get; set; }
        public bool? IsLockedOut { get; set; }
        public Boolean Active { get; set; }
        public Int32 RoleId { get; set; }
        public int? ClientId { get; set; }
        public Int32 RegionId { get; set; }
        public Int32 CountryId { get; set; }
        public Int32 SiteId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastPasswordChangeDate { get; set; }
    }

    public class UserInfo
    {
        public int? TwwwId { get; set; }
        public String Gender { get; set; }
        public String Role { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? StartDate { get; set; }
        public String FN { get; set; }
        public String LN { get; set; }
        public String Region { get; set; }
        public int? RegionID { get; set; }
        public String Country { get; set; }
        public int? CountryID { get; set; }
        public String Site { get; set; }
        public int? SiteID { get; set; }
        public String LocationID { get; set; }
        public String SupervisorName { get; set; }
        public int? SupID { get; set; }
    }

    public static int InsertUser(string firstName, string lastName, string twwid, string email, string password, int failedPasswordAttempCount,
                                 bool isLockedOut, bool active, int roleId, int? clientId, string otherClient, int regionId, int countryId,
                                 int siteId, int languageId, string mobileNo, string specializationSkills,
                                 int educLevelId,
                                 string courseMajor, string addCoursesTrainings, int? careerPathId, string otherCareerpath,
                                 string courseInterests, byte[] photo, string updateVia, int? departmentid, string otherDept, string supporttype, int supervisorid, string strupdatevia, string strsupporttype)
    {
        var retVal = 0;
        try
        {
            var db = new UserDBDataContext();
            retVal = db.pr_Insert_User(firstName, lastName, twwid, email, password,
                                     failedPasswordAttempCount, isLockedOut, active, roleId, clientId, otherClient, regionId,
                                     countryId, siteId, languageId, mobileNo, specializationSkills, educLevelId,
                                     courseMajor, addCoursesTrainings, careerPathId, otherCareerpath, courseInterests, photo, updateVia, departmentid, otherDept, supporttype, supervisorid, strupdatevia, strsupporttype);
            db.SubmitChanges();
        }
        catch (SqlException ex)
        {
            HttpContext.Current.Response.Write("Error: " + ex.Message);
        }
        return retVal;
    }

    public static bool UpdateUser(Int64 twwid, string email, int? clientId, string otherClient,
                                  int regionId, int countryId,
                                  int siteId, int languageId, string mobileNo, string specializationSkills,
                                  int educLevelId,
                                  string courseMajor, string addCoursesTrainings, int? careerPathId, string otherCareerpath,
                                  string courseInterests, string updateVia, int? departmentid,
                                  string otherDept, string supporttype, int supervisorid, string strupdatevia, string strsupporttype)
    {
        bool retVal;
        try
        {
            var db = new UserDBDataContext();
            db.pr_Update_User(twwid, email, clientId, otherClient, regionId, countryId, siteId, languageId, mobileNo,
                              specializationSkills, educLevelId, courseMajor, addCoursesTrainings, careerPathId, otherCareerpath,
                              courseInterests, updateVia, departmentid, otherDept, supporttype, supervisorid, strupdatevia, strsupporttype);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateUsersPhoto(Int64 twwid, byte[] photo)
    {
        bool retVal;
        try
        {
            var db = new UserDBDataContext();
            db.pr_Update_User_ProfilePhoto(twwid, photo);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static int UpdateUserPassword(string twwid, string password)
    {
        var retVal = 0;
        try
        {
            var db = new UserDBDataContext();
            db.ExecuteCommand("UPDATE Users SET Password = {0}, IsPasswordReset=1 WHERE [TWWID] = {1}", password, twwid);
            retVal = 1;
        }
        catch (SqlException ex)
        {
            HttpContext.Current.Response.Write("Error: " + ex.Message);
        }
        return retVal;
    }

    public static int InsertSecurityAnswer(int questionId, string twwId, string answer)
    {
        var retVal = 0;
        try
        {
            var db = new UserDBDataContext();
            retVal = db.pr_Insert_SecurityAnswer(questionId, twwId, answer);
            db.SubmitChanges();
            retVal = 1;
        }
        catch (SqlException ex)
        {
            HttpContext.Current.Response.Write("Error: " + ex.Message);
        }
        return retVal;
    }

    public static int InsertUserFeedback(int courseId, string twwid, string empName, string email)
    {
        int? retVal = null;
        try
        {
            var db = new UserDBDataContext();
            db.pr_Insert_Feedback(courseId, twwid, empName, email, ref retVal);
            db.SubmitChanges();
        }
        catch (SqlException ex)
        {
            retVal = 0;
        }
        return (int)retVal;
    }

    public static bool InsertFeedbackAnswers(Int64 feedId, string a1, string a2, string a3, string a4, string a5,
                                             string a6, string a7, string b1, string b2, string b3, string b4, string b5,
                                             string b6, string b7, string c1, string c2, string c3, string d1, string d2,
                                             string d3, string d4)
    {
        bool retVal;
        try
        {
            var db = new UserDBDataContext();
            db.pr_Insert_FeedbackAnswers(feedId, a1, a2, a3, a4, a5, a6, a7, b1, b2, b3, b4, b5, b6, b7, c1, c2, c3, d1,
                                         d2, d3, d4);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static IList<Users_TL> GetUser(string twwid)
    {
        var db = new UserDBDataContext();

        var result = (from a in db.User_TLocs
                      where a.TWWID == twwid
                      select new Users_TL
                                 {
                                     UserId = a.UserID,
                                     FirstName = a.FirstName,
                                     LastName = a.LastName,
                                     Twwid = a.TWWID,
                                     Email = a.Email,
                                     Password = a.Password,
                                     FailedPasswordAttempCount = a.FailedPasswordAttempCount,
                                     IsLockedOut = a.IsLockedOut,
                                     Active = a.Active,
                                     RoleId = a.RoleID,
                                     ClientId = a.ClientID,
                                     RegionId = a.RegionID,
                                     CountryId = a.CountryID,
                                     SiteId = a.SiteID,
                                     CreateDate = a.CreateDate,
                                     LastPasswordChangeDate = a.LastPasswordChangeDate
                                 });
        return result.ToList();
    }


    public static IList<UserInfo> GetUserInfo(string twwid)
    {
        var db = new UserDBDataContext { CommandTimeout = 0 };
        string SupName = (from A in db.Agents
                          join E in db.EMPLOYEEs on A.employeeid equals E.EMPLOYEEID
                          join TS in db.TeamSupervisors on E.GROUPID equals TS.teamid
                          join E2 in db.EMPLOYEEs on TS.agentid equals E2.TWWID
                          join A2 in db.Agents on E2.TWWID equals A2.twwid
                          where A.twwid == Convert.ToInt32(twwid)
                          select A2.employeename).FirstOrDefault();

        string SupId = (from A in db.Agents
                        join E in db.EMPLOYEEs on A.employeeid equals E.EMPLOYEEID
                        join TS in db.TeamSupervisors on E.GROUPID equals TS.teamid
                        join E2 in db.EMPLOYEEs on TS.agentid equals E2.TWWID
                        join A2 in db.Agents on E2.TWWID equals A2.twwid
                        where A.twwid == Convert.ToInt32(twwid)
                        select Convert.ToString(A2.twwid)).FirstOrDefault();

        var db2 = new UserDBDataContext { CommandTimeout = 0 };
        int TW2 = Convert.ToInt32(twwid);
        List<pr_Get_UserInfoResult> res2 = new List<pr_Get_UserInfoResult>(db2.pr_Get_UserInfo(TW2));

        List<UserInfo> ui = new List<UserInfo>();
        ui.Add(new UserInfo()
        {
            TwwwId = res2[0].TWWID,
            Role = res2[0].Role,
            Gender = res2[0].Gender,
            BirthDate = res2[0].Bday,
            StartDate = res2[0].StartDate,
            FN = res2[0].FirstName,
            LN = res2[0].LastName,
            Region = res2[0].Region,
            RegionID = res2[0].RegionID,
            Country = res2[0].Country,
            CountryID = (int?)(res2[0].CountryID),
            Site = res2[0].Site,
            SiteID = res2[0].SiteID,
            LocationID = Convert.ToString(res2[0].SiteID),
            SupervisorName = SupName,
            SupID = Convert.ToInt32(SupId)
        });
        //A2.employeename



        //var result =
        //    (from a in db.Empleados
        //     from ag in db.Agents
        //     .Where(ag => ag.twwid == a.TWWID)
        //     .DefaultIfEmpty()
        //     from loc in db.Locations
        //     .Where(loc => loc.ID == ag.locationID)
        //     .DefaultIfEmpty()
        //     from c in db.Countries
        //     .Where(c => c.countryID == (int?)loc.CountryID)
        //     .DefaultIfEmpty()
        //     from r in db.Regions
        //     .Where(r => r.ID == c.RegionID)
        //     .DefaultIfEmpty()
        //     //join ag in db.Agents on a.TWWID equals ag.twwid
        //     //join loc in db.Locations on ag.locationID equals loc.ID
        //     //join c in db.Countries on (int?)loc.CountryID equals c.countryID
        //     //join r in db.Regions on c.RegionID equals r.ID
        //     where a.TWWID == Convert.ToInt32(twwid)
        //     select
        //         new UserInfo
        //         {
        //             TwwwId = a.TWWID,
        //             Role = a.NombreCatTranscom,
        //             Gender = a.Sexo,
        //             BirthDate = a.FechaNacimiento,
        //             StartDate = a.FechaInicio,
        //             FN = a.Nombre,
        //             LN = a.Apellido1 + ' ' + a.Apellido2,
        //             Region = r.Description,
        //             RegionID = r.ID,
        //             Country = c.Country1,
        //             CountryID = c.countryID,
        //             Site = loc.Description,
        //             SiteID = loc.ID,
        //             LocationID = loc.ID.ToString(),
        //             SupervisorName = SupName,
        //             SupID = Convert.ToInt32(SupId)
        //             //A2.employeename
        //         });
        //return result.ToList();
        return ui;
    }

    public static sp_Get_User_ByTWWIDResult GetUserProfile(string twwid)
    {
        var db = new UserDBDataContext();
        return new List<sp_Get_User_ByTWWIDResult>(db.sp_Get_User_ByTWWID(twwid)).FirstOrDefault();
    }

    public static List<pr_Get_AgentsSupervisorResult> GetAgentsSupervisor(int twwId)
    {
        var db = new UserDBDataContext();
        return new List<pr_Get_AgentsSupervisorResult>(db.pr_Get_AgentsSupervisor(twwId));
    }

    public static String GetRoleForUser(string twwid)
    {
        try
        {
            var db = new UserDBDataContext();
            return (from a in db.User_TLocs join b in db.refRoles on a.RoleID equals b.ID where a.TWWID == twwid select b.role_desc).First();
        }
        catch
        {
            return "";
        }
    }

    public static String GetTwwid(string twwid)
    {
        var db = new UserDBDataContext();
        return (from a in db.User_TLocs where a.TWWID == twwid select a.TWWID).First();
    }

    public static bool ChkUserSecurityAnswer(string twwid, int questionid, string answer)
    {
        var db = new UserDBDataContext();
        var correct = db.tblSQAnswers.Any(p => p.twwid.ToLower() == twwid.ToLower() && p.questionid == questionid && p.answer == answer);
        return correct;
    }

    public static bool ChkTwwidIfExist(string twwid)
    {
        var db = new UserDBDataContext();
        var exist = db.User_TLocs.Any(p => p.TWWID == twwid);
        return exist;
    }

    public static bool ChkUserIfEmailExist(string emailAdd)
    {
        var db = new UserDBDataContext();
        var exist = db.User_TLocs.Any(p => p.Email == emailAdd);
        return exist;
    }

    public static bool ChkUserIfTrueEmployee(string twwid)
    {
        var id = string.IsNullOrEmpty(twwid) ? "0" : twwid;
        var db = new UserDBDataContext();
        var exist = db.vw_AllEmployees.Any(p => p.twwid == Convert.ToInt64(id));
        return exist;
    }

    public static bool ChkUserIfTrainer(string twwid)
    {
        var db = new UserDBDataContext();
        var retVal = (from a in db.User_TLocs
                      join b in db.refRoles on a.RoleID equals b.ID
                      where a.TWWID == twwid && b.role_desc.Contains("train") && b.role_desc.Contains("TQM")
                      select a.TWWID).Any();
        return retVal;
    }

    public static void UserChangePassword(string twwID, string password)
    {
        using (UserDBDataContext db = new UserDBDataContext())
        {
            var user = (from a in db.User_TLocs
                        where a.TWWID == twwID
                        select a).FirstOrDefault();

            user.IsLockedOut = false;
            user.LastPasswordChangeDate = DateTime.Now;
            user.Password = Password.CreateHash(password);
            db.SubmitChanges();
        }
    }
}