﻿using System;
using System.IO;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Security
/// </summary>
public class Security
{
	public Security()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string Encrypt(string originalString)
    {
        byte[] bytecode = System.Text.Encoding.UTF8.GetBytes("12345678");

        if (String.IsNullOrEmpty(originalString))
        {
            throw new ArgumentNullException
                   ("The string which needs to be encrypted cannot be null.");
        }

        var cryptoProvider = new DESCryptoServiceProvider();
        var memoryStream = new MemoryStream();
        var cryptoStream = new CryptoStream(memoryStream,
            cryptoProvider.CreateEncryptor(bytecode, bytecode), CryptoStreamMode.Write);
        var writer = new StreamWriter(cryptoStream);
        writer.Write(originalString);
        writer.Flush();
        cryptoStream.FlushFinalBlock();
        writer.Flush();

        return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
    }

    public static string Decrypt(string cryptedString)
    {
        byte[] bytecode = System.Text.Encoding.UTF8.GetBytes("12345678");

        if (String.IsNullOrEmpty(cryptedString.Replace(' ', '+')))
        {
            throw new ArgumentNullException
               ("The string which needs to be decrypted can not be null.");
        }

        var cryptoProvider = new DESCryptoServiceProvider();
        var memoryStream = new MemoryStream
                (Convert.FromBase64String(cryptedString.Replace(' ', '+')));
        var cryptoStream = new CryptoStream(memoryStream,
            cryptoProvider.CreateDecryptor(bytecode, bytecode), CryptoStreamMode.Read);
        var reader = new StreamReader(cryptoStream);

        return reader.ReadToEnd();
    }
}