﻿using System;

public class DataHelperModel
{
    public class User
    {
        public int CimNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CimName { get; set; }
        public string Name { get; set; }
    }

    public class Categories
    {
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DateTimeUpdated { get; set; }
    }

    public class SubCategories
    {
        public int? SubcategoryId { get; set; }
        public string Subcategory { get; set; }
        public int? CategoryId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? DateTimeCreated { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public int? HideFromList { get; set; }
        public int? ParentSubcategoryId { get; set; }
    }

    public class ReservationsEvents
    {
        //RESERVATION FIELDS
        public long ReservationReqId { get; set; }
        public short? TrainingTypeId { get; set; }
        public string TrainingTypeOther { get; set; }
        public string TrainingType { get; set; }
        public short AttritionGrowthId { get; set; }
        public string AttritionGrowth { get; set; }
        public int FteCount { get; set; }
        public string RtpTandim { get; set; }
        public int? WaveNo { get; set; }
        public int SiteId { get; set; }
        public string CompanySite { get; set; }
        public int ClientId { get; set; }
        public string Client { get; set; }
        public int? CampaignId { get; set; }
        public string Campaign { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? DateTo { get; set; }
        public short TimeSlotId { get; set; }
        public string TimeSlot { get; set; }
        public short? TimeSortId { get; set; }
        public string TrainerName { get; set; }
        public string TrainingClassName { get; set; }
        public short WeekNo { get; set; }
        public string Advisory { get; set; }
        public string ApproveByCoordinator { get; set; }
        public short StatusId { get; set; }
        public string ReservationStatus { get; set; }
        public string CreatedByReserv { get; set; }
        public string CreateDateReserv { get; set; }
        public string UpdatedByReserv { get; set; }
        public bool ActiveReserv { get; set; }
        //EVENT FIELDS
        public long? EventId { get; set; }
        public short? AssignedRoomId { get; set; }
        public string RoomAssignment { get; set; }
        public string CoordinatorAdvisory { get; set; }
        public string EnrollmentInstruction { get; set; }
        public string CourseDescription { get; set; }
        public string CreatedByEvent { get; set; }
        public string UpdatedByEvent { get; set; }
        public bool? ActiveEvent { get; set; }
        public string CreateDateEvent { get; set; }
    }

    public class Campaigns
    {
        public string Campaign { get; set; }
        public int CampaignId { get; set; }
    }

    public class TrainingTypes
    {
        public short TrainingTypeId { get; set; }
        public string TrainingType { get; set; }
        public bool Active { get; set; }
    }

    public class ClassNames
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public int? ClientId { get; set; }
        public string Client { get; set; }
        public int? TrainingTypeId { get; set; }
        public string TrainingType { get; set; }
        public bool Active { get; set; }
    }

    public class Audience
    {
        public int AudienceId { get; set; }
        public string AudienceName { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime DateUpdated { get; set; }
        public int UpdatedBy { get; set; }
    }

    public class AudienceMembers
    {
        public int AudienceMemberId { get; set; }
        public string AudienceMemberName { get; set; }
        public int? ClassID { get; set; }
        public int? Class { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? DepartmentID { get; set; }
        public string Department { get; set; }
        public int? CIMID { get; set; }
        public string IndividualUsersCim { get; set; }
        public int? ReportingManagerID { get; set; }
        public string ReportingManager { get; set; }
        public int? RoleID { get; set; }
        public string Role { get; set; }
        public int? RoleGroupID { get; set; }
        public string RoleGroup { get; set; }
        public int? SiteID { get; set; }
        public string Site { get; set; }
        public int? CareerPathID { get; set; }
        public string CareerPath { get; set; }
    }

    public class CourseEnrolmentAssigned
    {
        public long EnrollmentId { get; set; }
        public long CourseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int EnrolleeCim { get; set; }
        public int? SupCim { get; set; }
        public string Comment { get; set; }
        public short StatusId { get; set; }
        public string Status { get; set; }
        public int? ApproveDenyCim { get; set; }
        public DateTime? ApproveDenyDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool Active { get; set; }
        public bool? IsViewed { get; set; }
        public string EncryptedCourseId { get; set; }
        public bool? EnrolmentRequired { get; set; }
        public string CimName { get; set; }
        public string Name { get; set; }
        public DateTime? DueDate { get; set; }
        public string CourseType { get; set; }
    }

    public class CourseEnrolmentRaw
    {
        public long EnrolmentId { get; set; }
        public long CourseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int EnroleeCim { get; set; }
        public int? SupCim { get; set; }
        public string Comment { get; set; }
        public short StatusId { get; set; }
        public string Status { get; set; }
        public int? ApproveDenyCim { get; set; }
        public DateTime? ApproveDenyDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool Active { get; set; }
        public bool? IsViewed { get; set; }
        public string EncryptedCourseId { get; set; }
        public bool? EnrolmentRequired { get; set; }
        //public string CimName { get; set; }
        //public string Name { get; set; }
    }

    public class CourseCIMListRaw
    {
        //public long EnrolmentId { get; set; }
        //public long CourseId { get; set; }
        //public string Title { get; set; }
        //public string Description { get; set; }
        public int EnroleeCim { get; set; }
        //public int? SupCim { get; set; }
        //public string Comment { get; set; }
        //public short StatusId { get; set; }
        //public string Status { get; set; }
        //public int? ApproveDenyCim { get; set; }
        //public DateTime? ApproveDenyDate { get; set; }
        //public DateTime? CreateDate { get; set; }
        //public bool Active { get; set; }
        //public bool? IsViewed { get; set; }
        //public string EncryptedCourseId { get; set; }
        //public bool? EnrolmentRequired { get; set; }
        public string CimName { get; set; }
        public string Name { get; set; }
    }



    public class Course
    {
        public long CourseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class Participant
    {
        public int ParticipantId { get; set; }
        public int ClassId { get; set; }
        public string ParticipantName { get; set; }
        public string ParticipantSite { get; set; }
        public DateTime? DateJoined { get; set; }
        public string ParticipantStatus { get; set; }
        public string CertificationStatus { get; set; }
    }


    public class FilterCol
    {
        public string FilterType { get; set; }
        public string FilterName { get; set; }
    }

    public class CIMList
    {
        public int CIMNumber { get; set; }
        public string Name { get; set; }
    }

    public class Package
    {
        public int ScoTypeId { get; set; }
        public string ScoType { get; set; }
        public string ScoTitle { get; set; }
    }
}