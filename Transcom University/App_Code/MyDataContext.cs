﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Data.Linq.SqlClient;

/// <summary>
/// Summary description for MyDataContext
/// </summary>
partial class MyDataContext
{
    //partial void OnCreated()
    //{

    //    CommandTimeout = 5 * 60;
    //}

    //public void BulkInsertAll<T>(IEnumerable<T> entities)
    //{
    //    entities = entities.ToArray();

    //    //string cs = Connection.ConnectionString; 
    //    var db = new IntranetDBDataContext();

    //    string cs = db.Connection.ConnectionString;

    //    //var conn = new SqlConnection(cs);
    //    SqlConnection conn = new SqlConnection(cs);
    //    conn.Open();
    //    string oconn = string.Empty;
    //    oconn = "Data Source=Q9VMDEVSQL04;Initial Catalog=INTRANET;User ID=app_tutesting;Password=pa$$tutesting";

    //    Type t = typeof(T);

    //    var tableAttribute = (TableAttribute)t.GetCustomAttributes(
    //        typeof(TableAttribute), false).Single();
    //    //var bulkCopy = new SqlBulkCopy(conn.ConnectionString, SqlBulkCopyOptions.FireTriggers)
    //    var bulkCopy = new SqlBulkCopy(oconn, SqlBulkCopyOptions.FireTriggers)
    //    {
    //        DestinationTableName = tableAttribute.Name
    //    };

    //    var properties = t.GetProperties().Where(EventTypeFilter).ToArray();
    //    var table = new DataTable();

    //    foreach (var property in properties)
    //    {
    //        Type propertyType = property.PropertyType;
    //        if (propertyType.IsGenericType &&
    //            propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
    //        {
    //            propertyType = Nullable.GetUnderlyingType(propertyType);
    //        }

    //        table.Columns.Add(new DataColumn(property.Name, propertyType));
    //    }

    //    foreach (var entity in entities)
    //    {
    //        table.Rows.Add(properties.Select(
    //          property => GetPropertyValue(
    //          property.GetValue(entity, null))).ToArray());
    //    }

    //    bulkCopy.WriteToServer(table);
    //    conn.Close();
    //}    
    public void BulkInsertAll<T>(IEnumerable<T> entities)
    {
        entities = entities.ToArray();

        //string cs = Connection.ConnectionString; 
        var db = new IntranetDBDataContext();

        string cs = db.Connection.ConnectionString;

        var conn = new SqlConnection(cs);
        conn.Open();

        using (SqlTransaction transaction =
                     conn.BeginTransaction())
        {
            Type t = typeof(T);

            var tableAttribute = (TableAttribute)t.GetCustomAttributes(
                typeof(TableAttribute), false).Single();
            var bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.FireTriggers, transaction)
            {
                DestinationTableName = tableAttribute.Name
            };

            var properties = t.GetProperties().Where(EventTypeFilter).ToArray();
            var table = new DataTable();

            foreach (var property in properties)
            {
                Type propertyType = property.PropertyType;
                if (propertyType.IsGenericType &&
                    propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    propertyType = Nullable.GetUnderlyingType(propertyType);
                }

                table.Columns.Add(new DataColumn(property.Name, propertyType));
            }

            foreach (var entity in entities)
            {
                table.Rows.Add(properties.Select(
                  property => GetPropertyValue(
                  property.GetValue(entity, null))).ToArray());
            }

            bulkCopy.WriteToServer(table);
            transaction.Commit();
        }
        conn.Close();
    }

    private bool EventTypeFilter(System.Reflection.PropertyInfo p)
    {
        var attribute = Attribute.GetCustomAttribute(p,
            typeof(AssociationAttribute)) as AssociationAttribute;

        if (attribute == null) return true;
        if (attribute.IsForeignKey == false) return true;

        return false;
    }

    private object GetPropertyValue(object o)
    {
        if (o == null)
            return DBNull.Value;
        return o;
    }
}