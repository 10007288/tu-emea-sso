﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using NuComm.Security.Encryption;
using System.IO;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for AdminDataHelper
/// </summary>

public class AdminDataHelper
{
    public class AdminCourse
    {
        public Int32 CourseID { get; set; }
        public String EncryptedCourseID { get; set; }
        public String ClientName { get; set; }
        public String CourseName { get; set; }
        public String CoursePath { get; set; }
        public int? CourseDuration { get; set; }
        public String OutOfQueue { get; set; }
        public String PostRevisionDate { get; set; }
        public string VersionNumber { get; set; }
        public byte HideFromList { get; set; }
        public int? ClientID { get; set; }
        
    } 

	public static List<AdminCourse> GetAdminCourseResult(int courseId, int showAll)
    {
        var db = new IntranetDBDataContext();
        //.ToString(CultureInfo.InvariantCulture))
        var result =
            (from a in db.pr_NuCommUniversity_Lkp_Course_Admin(courseId, showAll)

             select new AdminCourse
                        {
                            CourseID = a.CourseID,
                            EncryptedCourseID = EncryptString(a.CourseID),
                            CourseName = a.CourseName,
                            CoursePath = a.CoursePath,
                            ClientID =  a.ClientID,
                            //ClientName = a.ClientName, 
                            //CourseDuration = a.CourseDuration,
                            OutOfQueue = a.OutOfQueue,
                            PostRevisionDate = a.PostRevisionDate,
                            VersionNumber = a.VersionNumber,
                            HideFromList = a.HideFromList
                        });
         
        //UTF8.EncryptText(HttpUtility.UrlEncode(Convert.ToString(a.CourseID)))
	    return result.ToList();
	}

    public static int SaveCourse(string courseName, string coursePath, int duration, string version, string revDate, byte outOfQueue, int courseID, int clientID, byte hideFromList, string campaignList) //int clientID
    {
        int retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_NuCommUniversity_Sav_Course(courseName, coursePath, duration, version, revDate, outOfQueue, courseID, clientID, hideFromList, campaignList);
            db.SubmitChanges();
            retVal = 1;
        }
        catch (Exception ex)
        {
            retVal = 0;
        }
        return retVal;
    }

    public static int SaveCourse_client(string courseName, string coursePath, int duration, string version, string revDate, byte outOfQueue, int courseID, byte hideFromList, string campaignList, string clientlist) //int clientID int clientID,
    {
        int retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_NuCommUniversity_Sav_Course_Client(courseName, coursePath, duration, version, revDate, outOfQueue, courseID, hideFromList, campaignList, clientlist);
            db.SubmitChanges();
            retVal = 1;
        }
            catch (Exception ex)
        {
            retVal = 0;
        }
        return retVal;
    }

    
    private static string EncryptString(int ConvVal)
    {
        string inputString = Convert.ToString(ConvVal);
        MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = Encoding.UTF8.GetBytes(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateEncryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();

        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
        return Convert.ToBase64String(memStream.ToArray());
    }

    public string DecryptString(string inputString)
    {
        MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = new byte[inputString.Length];
            byteInput = Convert.FromBase64String(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateDecryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }

        Encoding encoding1 = Encoding.UTF8;
        return encoding1.GetString(memStream.ToArray());
    }
    //public static bool ChkUserRoleIfAdmin(string twwid)
    //{
    //    var db = new UserDBDataContext();
    //    var retVal = (from a in db.Users
    //                  join b in db.refRoles on a.RoleID equals b.ID
    //                  where a.TWWID == twwid && b.role_desc.Contains("Admin")
    //                  select a.TWWID).Any();
    //    return retVal;
    //}


}