﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Web.Security;

/// <summary>
/// Summary description for BasePage
/// </summary>
///

public class BasePage : System.Web.UI.Page
{
    public string CurrTWWID
    {
        get
        {
            return System.Web.HttpContext.Current.User.Identity.Name.Split('|')[0];
        }
        set
        {
            string currUser;
            currUser = value + "|" + CurrLogName + "|" + UserRoleID + "|" + CurrVwIndx + "|" + Session["CurrLang"];
            FormsAuthentication.SetAuthCookie(currUser, true);
        }
    }

    public string CurrLogName
    {
        get
        {
            return System.Web.HttpContext.Current.User.Identity.Name.Split('|')[1];
        }
        set
        {
            string currUser;
            currUser = CurrTWWID + "|" + value + "|" + UserRoleID + "|" + CurrVwIndx + "|" + CurrLang;
            FormsAuthentication.SetAuthCookie(currUser, true);
        }
    }

    public string UserRoleID
    {
        get
        {
            return System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2];
        }
        set
        {
            string currUser;
            currUser = CurrTWWID + "|" + CurrLogName + "|" + value + "|" + CurrVwIndx + "|" + CurrLang;
            FormsAuthentication.SetAuthCookie(currUser, true);
        }
    }

    public string CurrVwIndx
    {
        get
        {
            return System.Web.HttpContext.Current.User.Identity.Name.Split('|')[3];
        }
        set
        {
            string currUser;
            currUser = CurrTWWID + "|" + CurrLogName + "|" + UserRoleID + "|" + value + "|" + CurrLang;
            FormsAuthentication.SetAuthCookie(currUser, true);
        }
    }

    public string CurrLang
    {
        get
        {
            return System.Web.HttpContext.Current.User.Identity.Name.Split('|')[4];
        }
        set
        {
            string currUser;
            currUser = CurrTWWID + "|" + CurrLogName + "|" + UserRoleID + "|" + CurrVwIndx + "|" + value;
            FormsAuthentication.SetAuthCookie(currUser, true);
        }
    }

    protected override void InitializeCulture()
    {
        string language= String.Empty;
        try
        {
            language = Session["CurrLang"].ToString();
        }
        catch
        { language = null; }

        CultureInfo ci = CultureInfo.InstalledUICulture;
        String CI_Info = ci.Name;

        if (language != null)
        {
            String selectedLanguage = language;
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        else
        {
            String selectedLanguage = CI_Info;
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }
}