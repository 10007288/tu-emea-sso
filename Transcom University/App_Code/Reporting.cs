﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Reporting
/// </summary>
public class Reporting
{
    #region .REPORTS DATA

    public class pr_RPT_DidNotTakenCourseResultOnModel
    {
        public string TWWID { get; set; }
        public string FirstName { get; set; }
        public string AgentCompleteName { get; set; }
        public int RoleID { get; set; }
        public string Role { get; set; }
        public string OfficeRole { get; set; }
        public int? RegionID { get; set; }
        public string Region { get; set; }
        public int CountryID { get; set; }
        public string Country { get; set; }
        public int SiteID { get; set; }
        public string Site { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? CourseID { get; set; }
        public DateTime? CourseLoginTime { get; set; }
        public int? TookCourse { get; set; }
        public string CourseName { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal? SUPTWWID { get; set; }
        public string SupervisorName { get; set; }
    }
    public class pr_RPT_TakenCourseAndTestResultOnModel
    {
        public string TWWID { get; set; }
        public string FirstName { get; set; }
        public string AgentCompleteName { get; set; }
        public int? RoleID { get; set; }
        public string Role { get; set; }
        public string OfficeRole { get; set; }
        public int? RegionID { get; set; }
        public string Region { get; set; }
        public int? CountryID { get; set; }
        public string Country { get; set; }
        public int SiteID { get; set; }
        public string Site { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? CourseID { get; set; }
        public DateTime? CourseLoginTime { get; set; }
        public int? TookCourse { get; set; }
        public string CourseName { get; set; }
        public string TestName { get; set; }
        public DateTime? StartDate { get; set; }
        public int? Score { get; set; }
        public bool? Passed { get; set; }
        public DateTime DateTaken { get; set; }
        public decimal? SUPTWWID { get; set; }
        public string SupervisorName { get; set; }
    }
    public class pr_RPT_TakenCourseResultOnModel
    {
        public string TWWID { get; set; }
        public string FirstName { get; set; }
        public string AgentCompleteName { get; set; }
        public int RoleID { get; set; }
        public string Role { get; set; }
        public string OfficeRole { get; set; }
        public int? RegionID { get; set; }
        public string Region { get; set; }
        public int CountryID { get; set; }
        public string Country { get; set; }
        public int SiteID { get; set; }
        public string Site { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? CourseID { get; set; }
        public DateTime? CourseLoginTime { get; set; }
        public int? TookCourse { get; set; }
        public string CourseName { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal? SUPTWWID { get; set; }
        public string SupervisorName { get; set; }
    }
    public class pr_RPT_TakenTestWithoutTakingCourseResultOnModel
    {
        public string TWWID { get; set; }
        public string FirstName { get; set; }
        public string AgentCompleteName { get; set; }
        public int? RoleID { get; set; }
        public string Role { get; set; }
        public string OfficeRole { get; set; }
        public int? RegionID { get; set; }
        public string Region { get; set; }
        public decimal? CountryID { get; set; }
        public string Country { get; set; }
        public int SiteID { get; set; }
        public string Site { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? CourseID { get; set; }
        public DateTime? CourseLoginTime { get; set; }
        public int? TookCourse { get; set; }
        public string CourseName { get; set; }
        public string TestName { get; set; }
        public DateTime? StartDate { get; set; }
        public int? Score { get; set; }
        public bool? Passed { get; set; }
        public DateTime DateTaken { get; set; }
        public decimal? SUPTWWID { get; set; }
        public string SupervisorName { get; set; }
    }
    public class pr_RPT_TakenTestAndPassedResultOnModel
    {
        public string TWWID { get; set; }
        public string FirstName { get; set; }
        public string AgentCompleteName { get; set; }
        public int? RoleID { get; set; }
        public string Role { get; set; }
        public string OfficeRole { get; set; }
        public int? RegionID { get; set; }
        public string Region { get; set; }
        public decimal? CountryID { get; set; }
        public string Country { get; set; }
        public int SiteID { get; set; }
        public string Site { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? CourseID { get; set; }
        public DateTime? CourseLoginTime { get; set; }
        public int? TookCourse { get; set; }
        public string CourseName { get; set; }
        public string TestName { get; set; }
        public DateTime? StartDate { get; set; }
        public int? Score { get; set; }
        public bool? Passed { get; set; }
        public DateTime DateTaken { get; set; }
        public decimal? SUPTWWID { get; set; }
        public string SupervisorName { get; set; }
    }
    public class pr_RPT_TakenTestAndFailed_DidNotTakeTestResultOnModel
    {
        public string TWWID { get; set; }
        public string FirstName { get; set; }
        public string AgentCompleteName { get; set; }
        public int? RoleID { get; set; }
        public string Role { get; set; }
        public string OfficeRole { get; set; }
        public int? RegionID { get; set; }
        public string Region { get; set; }
        public decimal? CountryID { get; set; }
        public string Country { get; set; }
        public int SiteID { get; set; }
        public string Site { get; set; }
        public int? ClientID { get; set; }
        public string Client { get; set; }
        public int? CourseID { get; set; }
        public DateTime? CourseLoginTime { get; set; }
        public int? TookCourse { get; set; }
        public string CourseName { get; set; }
        public string TestName { get; set; }
        public DateTime? StartDate { get; set; }
        public int? Score { get; set; }
        public bool? Passed { get; set; }
        public DateTime DateTaken { get; set; }
        public decimal? SUPTWWID { get; set; }
        public string SupervisorName { get; set; }
    }

    public static int CountAllEmployees()
    {
        var db = new UserDBDataContext();
        return db.vw_AllEmployees.Count();
    }

    public static DataTable GetReport_ActiveEmployeesOnSite_DataTable(string sites)
    {
        var db = new NuSkillDBDataContext();
        var result = db.pr_RPT_ActiveEmployeesOnSite(sites).ToList();
        var dt = Load(result);
        return dt;
    }

    public static DataTable GetReport1_TakenCourseAndTest_DataTable(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {

        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;

        string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
        //var db = new NuSkillDBDataContext();
        var result = db.pr_RPT_TakenCourseAndTest_Display(clientIds, courseId, sites, roles, startDate, endDate, encr).ToList();
        var dt = Load(result);
        return dt;
    }

    public static DataTable GetReport2_TakenCourse_DataTable(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate, DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;

        string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
        var result = db.pr_RPT_TakenCourse_Display(clientIds, courseId, sites, roles, startDate, endDate, encr).ToList();
        var dt = Load(result);
        return dt;
    }

    public static DataTable GetReport3_DidNotTakenCourse_DataTable(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;

        string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
        var result = db.pr_RPT_DidNotTakenCourse_Display(clientIds, courseId, sites, roles, startDate, endDate, encr).ToList();
        var dt = Load(result);
        return dt;
    }

    public static DataTable GetReport4_TakenTestWithoutTakingCourse_DataTable(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;

        string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
        var result = db.pr_RPT_TakenTestWithoutTakingCourse_Display(clientIds, courseId, sites, roles, startDate, endDate, encr).ToList();
        var dt = Load(result);
        return dt;
    }

    public static DataTable GetReport5_TakenTestAndPassed_DataTable(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;

        string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
        var result = db.pr_RPT_TakenTestAndPassed_Display(clientIds, courseId, sites, roles, startDate, endDate, encr).ToList();
        var dt = Load(result);
        return dt;
    }

    public static DataTable GetReport6_TakenTestAndFailed_DidNotTakeTest_DataTable(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;

        string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
        var result = db.pr_RPT_TakenTestAndFailed_DidNotTakeTest_Display(clientIds, courseId, sites, roles, startDate, endDate, encr).ToList();
        var dt = Load(result);
        return dt;
    }

    protected static DataTable Load<T>(List<T> param)
    {
        var table = new DataTable();

        try
        {
            var info =
                param[0].GetType()
                        .GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

            foreach (var i in info)
            {
                table.Columns.Add(i.Name);
            }

            var values = new object[info.Length];

            foreach (var item in param)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    table.NewRow();
                    values[i] = info[i].GetValue(item, null);
                }
                table.Rows.Add(values);
            }
        }
        catch (Exception)
        {
        }

        return table;
    }

    public static List<pr_RPT_ActiveEmployeesOnSiteResult> GetReport_ActiveEmployeesOnSite(string sites)
    {
        var db = new NuSkillDBDataContext();
        return new List<pr_RPT_ActiveEmployeesOnSiteResult>(db.pr_RPT_ActiveEmployeesOnSite(sites)).ToList();
    }

    public static List<pr_RPT_GetEmployeeCountPerSiteResult> GetReport_EmployeeCountPerSite(string sites)
    {
        var db = new NuSkillDBDataContext();
        return new List<pr_RPT_GetEmployeeCountPerSiteResult>(db.pr_RPT_GetEmployeeCountPerSite(sites)).ToList();
    }

    //Employees that have Taken the Course and Test
    public static List<pr_RPT_TakenCourseAndTestResultOnModel> GetReport1_TakenCourseAndTest(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            //string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());

            string encr = EncryptString(Convert.ToInt32(courseId));

            var list = db.pr_RPT_TakenCourseAndTest_Display(clientIds, courseId, sites.Replace("'", "''"), roles, startDate, endDate, encr)
                .Select(i => new pr_RPT_TakenCourseAndTestResultOnModel
                {
                    TWWID = i.TWWID,
                    FirstName = i.FirstName,
                    AgentCompleteName = i.AgentCompleteName,
                    RoleID = i.RoleID,
                    Role = i.Role,
                    OfficeRole = i.OfficeRole,
                    RegionID = i.RegionID,
                    Region = i.Region,
                    CountryID = i.CountryID,
                    Country = i.Country,
                    SiteID = i.SiteID,
                    Site = i.Site,
                    ClientID = i.ClientID,
                    Client = i.Client,
                    CourseID = i.CourseID,
                    TookCourse = i.TookCourse,
                    CourseName = i.CourseName,
                    TestName = i.TestName,
                    Score = i.Score,
                    Passed = i.Passed,
                    DateTaken = i.DateTaken,
                    StartDate = i.StartDate,
                    SUPTWWID = i.SUPTWWID,
                    SupervisorName = i.SupervisorName
                })
                .ToList();

            return list;
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }

    //Employees that have Taken the Course
    public static List<pr_RPT_TakenCourseResultOnModel> GetReport2_TakenCourse(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate, DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());

            var list = db.pr_RPT_TakenCourse_Display(clientIds, courseId, sites, roles, startDate, endDate, encr)
                .Select(i => new pr_RPT_TakenCourseResultOnModel
                {
                    TWWID = i.TWWID,
                    FirstName = i.FirstName,
                    AgentCompleteName = i.AgentCompleteName,
                    RoleID = i.RoleID ?? 0,
                    Role = i.Role,
                    OfficeRole = i.OfficeRole,
                    RegionID = i.RegionID,
                    Region = i.Region,
                    CountryID = i.CountryID ?? 0,
                    Country = i.Country,
                    SiteID = i.SiteID,
                    Site = i.Site,
                    ClientID = i.ClientID,
                    Client = i.Client,
                    CourseID = i.CourseID,
                    CourseLoginTime = i.CourseLoginTime,
                    TookCourse = i.TookCourse,
                    CourseName = i.CourseName,
                    StartDate = i.StartDate,
                    SUPTWWID = i.SUPTWWID,
                    SupervisorName = i.SupervisorName
                })
                .ToList();

            return list;
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }

    //Employees that have Not Taken the Course
    public static List<pr_RPT_DidNotTakenCourseResultOnModel> GetReport3_DidNotTakenCourse(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            //string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
            string encr = EncryptString(Convert.ToInt32(courseId));

            var list = db.pr_RPT_DidNotTakenCourse_Display(clientIds, courseId, sites, roles, startDate, endDate, encr)
                .Select(i => new pr_RPT_DidNotTakenCourseResultOnModel
                {
                    TWWID = i.TWWID,
                    FirstName = i.FirstName,
                    AgentCompleteName = i.AgentCompleteName,
                    RoleID = i.RoleID ?? 0,
                    Role = i.Role,
                    OfficeRole = i.OfficeRole,
                    RegionID = i.RegionID,
                    Region = i.Region,
                    CountryID = i.CountryID ?? 0,
                    Country = i.Country,
                    SiteID = i.SiteID,
                    Site = i.Site,
                    ClientID = i.ClientID,
                    Client = i.Client,
                    CourseID = i.CourseID,
                    CourseLoginTime = i.CourseLoginTime,
                    TookCourse = i.TookCourse,
                    CourseName = i.CourseName,
                    StartDate = i.StartDate,
                    SUPTWWID = i.SUPTWWID,
                    SupervisorName = i.SupervisorName
                })
                .ToList();

            return list.ToList();
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }

    //Employees that have taken the Test without Taking the Course
    public static List<pr_RPT_TakenTestWithoutTakingCourseResultOnModel> GetReport4_TakenTestWithoutTakingCourse(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            var list = db.pr_RPT_TakenTestWithoutTakingCourse(clientIds, courseId, sites, roles, startDate, endDate)
                .Select(i => new pr_RPT_TakenTestWithoutTakingCourseResultOnModel
                {
                    TWWID = i.TWWID,
                    FirstName = i.FirstName,
                    AgentCompleteName = i.AgentCompleteName,
                    RoleID = i.RoleID,
                    Role = i.Role,
                    OfficeRole = i.OfficeRole,
                    RegionID = i.RegionID,
                    Region = i.Region,
                    CountryID = i.CountryID,
                    Country = i.Country,
                    SiteID = i.SiteID,
                    Site = i.Site,
                    ClientID = i.ClientID,
                    Client = i.Client,
                    CourseID = i.CourseID,
                    TookCourse = i.TookCourse,
                    CourseName = i.CourseName,
                    TestName = i.TestName,
                    Score = i.Score,
                    Passed = i.Passed,
                    DateTaken = i.DateTaken,
                    StartDate = i.StartDate,
                    SUPTWWID = i.SUPTWWID,
                    SupervisorName = i.SupervisorName
                })
            .ToList();

            return list;
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }

    //Employees that have Taken the Test
    public static List<pr_RPT_TakenTestResult> GetReport_TakenTest(string clientIds, int? courseId,
                                                                                  string sites,
                                                                                  string roles, DateTime? startDate,
                                                                                  DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            var list = new List<pr_RPT_TakenTestResult>(db.pr_RPT_TakenTest(clientIds, courseId, sites, roles, startDate, endDate));
            return list.ToList();
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }

    //Employees that have Taken the Test and Passed
    public static List<pr_RPT_TakenTestAndPassedResultOnModel> GetReport5_TakenTestAndPassed(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            //string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
            string encr = EncryptString(Convert.ToInt32(courseId));

            var list = db.pr_RPT_TakenTestAndPassed_Display(clientIds, courseId, sites, roles, startDate, endDate, encr)
            .Select(i => new pr_RPT_TakenTestAndPassedResultOnModel
            {
                TWWID = i.TWWID,
                FirstName = i.FirstName,
                AgentCompleteName = i.AgentCompleteName,
                RoleID = i.RoleID,
                Role = i.Role,
                OfficeRole = i.OfficeRole,
                RegionID = i.RegionID,
                Region = i.Region,
                CountryID = i.CountryID,
                Country = i.Country,
                SiteID = i.SiteID,
                Site = i.Site,
                ClientID = i.ClientID,
                Client = i.Client,
                CourseID = i.CourseID,
                TookCourse = i.TookCourse,
                CourseName = i.CourseName,
                TestName = i.TestName,
                Score = i.Score,
                Passed = i.Passed,
                DateTaken = i.DateTaken,
                StartDate = i.StartDate,
                SUPTWWID = i.SUPTWWID,
                SupervisorName = i.SupervisorName
            })
            .ToList();

            return list;
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }

    //Employees that have Taken the Test and Failed, Did Not Take Test
    public static List<pr_RPT_TakenTestAndFailed_DidNotTakeTestResultOnModel> GetReport6_TakenTestAndFailed_DidNotTakeTest(string clientIds, int? courseId,
                                                                                   string sites,
                                                                                   string roles, DateTime? startDate,
                                                                                   DateTime? endDate)
    {
        var db = new NuSkillDBDataContext();
        db.CommandTimeout = 400;
        try
        {
            //string encr = NuComm.Security.Encryption.UTF8.EncryptText(courseId.ToString());
            string encr = EncryptString(Convert.ToInt32(courseId));

            var list = db.pr_RPT_TakenTestAndFailed_DidNotTakeTest_Display(clientIds, courseId, sites, roles, startDate, endDate, encr)
                .Select(i => new pr_RPT_TakenTestAndFailed_DidNotTakeTestResultOnModel
                {
                    TWWID = i.TWWID,
                    FirstName = i.FirstName,
                    AgentCompleteName = i.AgentCompleteName,
                    RoleID = i.RoleID,
                    Role = i.Role,
                    OfficeRole = i.OfficeRole,
                    RegionID = i.RegionID,
                    Region = i.Region,
                    CountryID = i.CountryID,
                    Country = i.Country,
                    SiteID = i.SiteID,
                    Site = i.Site,
                    ClientID = i.ClientID,
                    Client = i.Client,
                    CourseID = i.CourseID,
                    TookCourse = i.TookCourse,
                    CourseName = i.CourseName,
                    TestName = i.TestName,
                    Score = i.Score,
                    Passed = i.Passed,
                    DateTaken = i.DateTaken,
                    StartDate = i.StartDate,
                    SUPTWWID = i.SUPTWWID,
                    SupervisorName = i.SupervisorName
                })
                .ToList();
            return list;
        }
        catch (Exception e)
        {
            //return null;
            throw e;
        }
    }
    private static string EncryptString(int ConvVal)
    {
        string inputString = Convert.ToString(ConvVal);
        MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = Encoding.UTF8.GetBytes(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateEncryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();

        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
        return Convert.ToBase64String(memStream.ToArray());
    }

    #endregion
}