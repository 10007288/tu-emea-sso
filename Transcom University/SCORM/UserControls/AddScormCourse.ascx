﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddScormCourse.ascx.cs"
    Inherits="ScormCourse.SCORM_UserControls_AddScormCourse" %>
<fieldset>
    <legend align="right">Create Bundle</legend>
    <asp:Panel runat="server" DefaultButton="btnAddNewPackage" ID="pnl">
        <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnDeletePackage">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadPanelBar1" LoadingPanelID="LoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManagerProxy>
        <div runat="server" id="divPackageControls">
            <table width="100%">
                <tr>
                    <td style="width: 95%">
                        <asp:Label ID="AddButtonSenderID" runat="server" Text="0" Visible="false"></asp:Label>
                        <telerik:RadPanelBar runat="server" ID="RadPanelBar1" Width="100%" Skin="Sitefinity"
                            ExpandMode="SingleExpandedItem" CssClass="RadPanelBarPackage" OnItemClick="RadPanelBar1_ItemClicked"
                            CausesValidation="False">
                            <Items>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="1" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--Title:--%>
                                            <asp:Label runat="server" ID="lblPackageTitle1" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle1" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="PackageTitle1" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addPackage1"
                                                CssClass="displayerror" />--%></br>
                                            <%--Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc1" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType1" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="cbSCOType1" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addPackage1"
                                                CssClass="displayerror" />--%>
                                            <asp:LinkButton ID="btnAddSCO1" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <telerik:RadGrid runat="server" ID="gridPackage1" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="2" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle2" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle2" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%-- Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc2" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType2" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO2" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <telerik:RadGrid runat="server" ID="gridPackage2" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="3" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%-- Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle3" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle3" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%--Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc3" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType3" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO3" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%-- <telerik:RadDockZone runat="server" ID="RadDockZone3" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage3" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="4" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle4" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle4" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%-- Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc4" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType4" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO4" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%--<telerik:RadDockZone runat="server" ID="RadDockZone4" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage4" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="5" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%-- Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle5" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle5" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%--Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc5" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType5" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO5" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%--<telerik:RadDockZone runat="server" ID="RadDockZone5" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage5" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="6" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle6" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle6" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%-- Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc6" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType6" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO6" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%--<telerik:RadDockZone runat="server" ID="RadDockZone6" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage6" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="7" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--      Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle7" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle7" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%--  Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc7" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType7" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO7" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%--<telerik:RadDockZone runat="server" ID="RadDockZone7" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage7" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="8" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%-- Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle8" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle8" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%-- Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc8" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType8" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO8" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%--<telerik:RadDockZone runat="server" ID="RadDockZone8" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage8" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="9" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle9" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle9" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%--Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc9" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType9" runat="server" EmptyMessage="- sco type -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO9" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%--<telerik:RadDockZone runat="server" ID="RadDockZone9" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage9" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                                <telerik:RadPanelItem CssClass="RadPanelBarPackage" Value="10" Visible="False">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0 5px 0">
                                            <%--  Title:--%>
                                             <asp:Label runat="server" ID="lblPackageTitle10" Visible="false"  Text="" />
                                            <telerik:RadTextBox runat="server" ID="PackageTitle10" EmptyMessage="- title -" Width="600px"
                                                MaxLength="100" />
                                            <%-- Description:--%>
                                            <telerik:RadTextBox runat="server" ID="PackageDesc10" EmptyMessage="- description -"
                                                Height="100px" Width="600px" TextMode="MultiLine" MaxLength="1000" />
                                            <telerik:RadComboBox ID="cbSCOType10" runat="server" EmptyMessage="- sco type -"
                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="scotypeid" DataTextField="scotype"
                                                DataSourceID="dsScoType" />
                                            <asp:LinkButton ID="btnAddSCO10" runat="server" Text="Add" CssClass="linkBtn" ToolTip="Add"
                                                OnClientClick="View_UploadWindow1()" OnClick="btnAddSCO_Click" />
                                        </div>
                                        <%-- <telerik:RadDockZone runat="server" ID="RadDockZone10" Visible="False" />--%>
                                        <telerik:RadGrid runat="server" ID="gridPackage10" AutoGenerateColumns="False" CssClass="RadGridPackage"
                                            Skin="Metro" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
                                            OnItemDataBound="gridPackage_ItemDataBound" OnItemCommand="gridPackage_ItemCommand">
                                            <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
                                                Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
                                                TableLayout="Auto">
                                                <Columns>
                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                                                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                                                        ImageUrl="~/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                        <HeaderStyle Width="50px" />
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                                                        Display="False" />
                                                    <telerik:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
                                                </Columns>
                                                <GroupByExpressions>
                                                    <telerik:GridGroupByExpression>
                                                        <SelectFields>
                                                            <telerik:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                                                        </SelectFields>
                                                        <GroupByFields>
                                                            <telerik:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                                                        </GroupByFields>
                                                    </telerik:GridGroupByExpression>
                                                </GroupByExpressions>
                                            </MasterTableView>
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                    <td style="width: 5%; vertical-align: bottom; padding-bottom: 10px" align="center">
                        <asp:ImageButton ID="btnDeletePackage" ImageUrl="../../Images/deleteFilter_active.png"
                            runat="server" ToolTip="Remove Package" OnClick="btnDeletePackage_Click" Visible="False" />
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="divNote" align="center">
            <label style="color: red">
                (1) Please specify the package name.
            </label>
            <br />
            <label style="color: red">
                (2) Click 'Add More Courses' to build package.
            </label>
        </div>
        <div runat="server" id="divAddPackage" align="center" style="padding-top: 5px">
            <telerik:RadTextBox runat="server" ID="txtPackageTitle" EmptyMessage="- package name -"
                Width="200px" Visible="false" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="txtPackageTitle" SetFocusOnError="True" Display="Dynamic"
                Enabled="false" EnableClientScript="false" ValidationGroup="addPackTitle" CssClass="displayerror" />
            <asp:LinkButton runat="server" ID="btnAddNewPackage" Text="Add More Courses >" OnClick="btnAddNewPackage_OnClick"
                CssClass="linkBtn" ValidationGroup="addPackTitle" />
        </div>
        <div>
            <asp:SqlDataSource ID="dsScoType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                SelectCommand="select scotypeid,scotype,maxallowed from tbl_scorm_lkp_scotype where inactive = 0 order by ordinal">
            </asp:SqlDataSource>
        </div>
    </asp:Panel>
</fieldset>
