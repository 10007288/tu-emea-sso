using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.LearningComponents;
using Microsoft.LearningComponents.Storage;
using DotNetSCORM.LearningAPI;
using System.Data.SqlClient;
using Telerik.Web.UI;

using Schema = DotNetSCORM.LearningAPI.Schema;
//using ASP;

using ScormCourse;

public partial class DNSControls_UploadContent : DotNetSCORMControlBase
{
    public Boolean UploadComplete = false;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;

    readonly ScormCourse.SCORM_UserControls_AddScormCourse _asc = new ScormCourse.SCORM_UserControls_AddScormCourse();

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    public static string CompletePath
    {
        get
        {
            var value = HttpContext.Current.Session["CompletePath"];
            return value.ToString();
        }
        set { HttpContext.Current.Session["CompletePath"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int _employeeId;
        _employeeId = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.Name.Split('|')[0]);
    }

    protected void UploadPackageButton_OnClick(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                //add course form
                SCORM_UserControls_AddScormCourse asc1 = new SCORM_UserControls_AddScormCourse();
                RadMultiPage mpCourses = (RadMultiPage)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("pvCourse")).FindControl("mpCourses");

                Label AddButtonSenderID;
                RadComboBox cbSCOType;
                RadGrid gridPackage;

                if (mpCourses.SelectedIndex == 1)
                {
                    UserControl AddScormCourse = (UserControl)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("pvCourse")).FindControl("AddScormCourse");
                    AddButtonSenderID = (Label)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("AddButtonSenderID");
                    cbSCOType = (RadComboBox)((RadPanelItem)((RadPanelBar)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("cbSCOType" + AddButtonSenderID.Text);
                    gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("gridPackage" + AddButtonSenderID.Text);
                }
                else
                {
                    RadGrid gridCourse = (RadGrid)((RadPageView)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("pvCourse")).FindControl("pvCourseList")).FindControl("gridCourse");
                    ASP.scorm_usercontrols_addscormcourse_ascx EditScormCourse = LogicHelper.FindControl<ASP.scorm_usercontrols_addscormcourse_ascx>(gridCourse.Controls, "EditScormCourse");
                    AddButtonSenderID = (Label)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("AddButtonSenderID");
                    cbSCOType = (RadComboBox)((RadPanelItem)((RadPanelBar)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("cbSCOType" + AddButtonSenderID.Text);
                    gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("gridPackage" + AddButtonSenderID.Text);
                }

                _asc.CreatePackageDataTableColumns();

                foreach (GridDataItem row in gridPackage.Items)
                {
                    var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
                    var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
                    var ScoType = (string)row.GetDataKeyValue("ScoType");
                    var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
                    var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
                    var curriculumcourseID = (Int32)row.GetDataKeyValue("curriculumcourseID");

                    DataRow dr = _asc.DtPackage1.NewRow();
                    dr["ScoPackageId"] = ScoPackageId;
                    dr["ScoTitle"] = ScoTitle;
                    dr["ScoType"] = ScoType;
                    dr["ScoTypeId"] = ScoTypeId;
                    dr["CourseTypeID"] = CourseTypeID;
                    dr["curriculumcourseID"] = curriculumcourseID;
                    _asc.DtPackage1.Rows.Add(dr);
                }

                //--MATT 06132015 - Added Learning Guide as "Other Resource" upload and in clause for better code readability
                if (cbSCOType.Text.In("Trainer Guide", "Additional Resources (Trainer)", "Additional Resources (Learner)", "Trainer Presentation", "Learning Guide"))
                {
                    string completePath;
                    string uniqName = null;

                    //Upload all the files to a permanent location
                    foreach (UploadedFile file in RadAsyncUpload1.UploadedFiles)
                    {
                        string FolderPath = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"];
                        uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();
                        completePath = Server.MapPath(FolderPath + "\\" + uniqName);
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('" + completePath + "');", true);
                        file.SaveAs(completePath);
                    }

                    //=======================================================================

                    DataRow drPackage = _asc.DtPackage1.NewRow();
                    drPackage["ScoPackageId"] = 0;
                    drPackage["ScoTitle"] = uniqName; //RadAsyncUpload1.UploadedFiles[0].FileName;
                    drPackage["ScoType"] = cbSCOType.SelectedItem.Text;
                    drPackage["ScoTypeId"] = cbSCOType.SelectedValue;
                    drPackage["CourseTypeID"] = 1; //1 - non scorm, 2- scorm
                    drPackage["curriculumcourseID"] = AddButtonSenderID.Text; //id of grid
                    _asc.DtPackage1.Rows.Add(drPackage);
                    gridPackage.DataSource = _asc.DtPackage1;
                    gridPackage.DataBind();
                }
                else
                {
                    //this part gets the TWWID of the scorm package creator

                    //int _employeeId;
                   //_employeeId = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    DotNetSCORM.LearningAPI.LStoreUserInfo currentUser = GetCurrentUserInfo();
                    PackageItemIdentifier packageId;

                    using 
                    (
                        PackageReader packageReader = PackageReader.Create(RadAsyncUpload1.UploadedFiles[0].InputStream)
                    )

                    {
                        AddPackageResult result = PStore.AddPackage(packageReader, new PackageEnforcement(false, false, false));
                        packageId = result.PackageId;
                    }

                    LearningStoreJob job = LStore.CreateJob();
                    Dictionary<string, object> properties = new Dictionary<string, object>();
                    properties[Schema.PackageItem.Owner] = currentUser.Id;
                    properties[Schema.PackageItem.FileName] = RadAsyncUpload1.UploadedFiles[0].FileName;
                    properties[Schema.PackageItem.UploadDateTime] = DateTime.Now;

                    job.UpdateItem(packageId, properties);
                    job.Execute();
                    job = LStore.CreateJob();
                    RequestMyTraining(job, packageId);
                    job.Execute<DataTable>();

                    //=============================================================
                    //get package title
                    string PackageTitle;
                    Dbconn("Intranet");
                    _ocmd = new SqlCommand("pr_Scorm_Cor_GetPackageDetails", _oconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    _ocmd.Parameters.AddWithValue("@ScoPackageId",
                                                  packageId.ToString().Replace("ItemType:PackageItem, Key:", ""));

                    _ocmd.Parameters.Add("@Title", SqlDbType.VarChar, -1);
                    _ocmd.Parameters["@Title"].Direction = ParameterDirection.Output;
                    _ocmd.ExecuteReader();
                    _oconn.Close();
                    PackageTitle = _ocmd.Parameters["@Title"].Value.ToString();

                    //=======================================================================

                    DataRow drPackage = _asc.DtPackage1.NewRow();
                    drPackage["ScoPackageId"] = packageId.ToString().Replace("ItemType:PackageItem, Key:", "");
                    drPackage["ScoTitle"] = PackageTitle;
                    drPackage["ScoType"] = cbSCOType.SelectedItem.Text;
                    drPackage["ScoTypeId"] = cbSCOType.SelectedValue;
                    drPackage["CourseTypeID"] = 2; //1 - non scorm, 2- scorm
                    drPackage["curriculumcourseID"] = AddButtonSenderID.Text; //id of grid

                    _asc.DtPackage1.Rows.Add(drPackage);
                    gridPackage.DataSource = _asc.DtPackage1;
                    gridPackage.DataBind();
                    gridPackage.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Error Uploading SCORM Package: " + ex.StackTrace.Replace("'", "") + "');", true);
        }
    }

    protected void UploadDone_Click(object sender, EventArgs e)
    {
        UploadComplete = true;
    }
}
