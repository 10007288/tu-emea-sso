﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class ForgotPassword : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GetLang();
    }

    private void GetLang()
    {
        lblTitle.Text = Resources.LocalizedResource.ForgotPass.ToString();
        lblFP_TWWID.Text = Resources.LocalizedResource.TWWID.ToString();
        lblFP_SecQues.Text = Resources.LocalizedResource.SecQues.ToString();
        lblFP_SecAns.Text = Resources.LocalizedResource.SecAns.ToString();
    }

    public void ClearControls()
    {
        txtTWWID.Text = "";
        txtSecurityAnswer.Text = "";
        cbSecurityQuestion.Text = "";
        cbSecurityQuestion.ClearSelection();
    }

    protected void TWWID_TextChanged(object sender, EventArgs e)
    {
        if (!Registration.ChkUserIfTrueEmployee(txtTWWID.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                            "alert('Your TWWID provided does not exist on valid Employees list.');", true);
            txtTWWID.Text = string.Empty;
        }
        else
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("pr_Get_SecurityQuestions", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TWWID", txtTWWID.Text);

                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                    ds.Tables[0].Rows[0]["SecurityQuestion"].ToString();

                    cmd.ExecuteReader();
                    con.Close();

                    string securityQuestion = ds.Tables[0].Rows[0]["SecurityQuestion"].ToString();

                    if (securityQuestion == "What was your childhood nickname?")
                    {
                        cbSecurityQuestion.Text = "What was your childhood nickname?";
                        cbSecurityQuestion.SelectedValue = "1";
                    }
                    else if (securityQuestion == "What is your mothers maiden name?")
                    {
                        cbSecurityQuestion.Text = "What is your mothers maiden name?";
                        cbSecurityQuestion.SelectedValue = "8";
                    }
                    else if (securityQuestion == "What was the name of your elementary / primary school?")
                    {
                        cbSecurityQuestion.Text = "What was the name of your elementary / primary school?";
                        cbSecurityQuestion.SelectedValue = "11";
                    }
                    else if (securityQuestion == "What is the name of your first pet?")
                    {
                        cbSecurityQuestion.Text = "What is the name of your first pet?";
                        cbSecurityQuestion.SelectedValue = "15";
                    }
                }
            }
        }
    }

    protected void BtnContinueClick(object sender, EventArgs e)
    {
        var correct = Registration.ChkUserSecurityAnswer(txtTWWID.Text, Convert.ToInt32(cbSecurityQuestion.SelectedValue), txtSecurityAnswer.Text.ToLower());

        if (correct)
        {
            const string defaultPass = "Tr@nscom01";
            var retVal = Registration.UpdateUserPassword(txtTWWID.Text, Password.CreateHash(defaultPass));

            if (retVal == 1)
            {
                PanelNotification.Visible = true;
                PanelNotification.Attributes.Add("class", "msgnotification");
                lblMessage.Text =
                    String.Format(
                        "Your password is now back to default <b />Tr@nscom01<b />. Please click <a href='Login.aspx'>here</a> to sign in and change password.");
                lblMessage.ForeColor = Color.Gray;
                ClearControls();
            }
        }
        else
        {
            PanelNotification.Visible = true;
            PanelNotification.Attributes.Add("class", "msgnotificationerror");
            lblMessage.Text =
                    String.Format("You have provided an incorrect details. Please contact Administrator for assistance.");
            lblMessage.ForeColor = Color.Red;
        }
    }
}