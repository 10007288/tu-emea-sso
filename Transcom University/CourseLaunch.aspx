﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CourseLaunch.aspx.cs" Inherits="CourseLaunch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Course Launch</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112026746-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-112026746-1');
    </script>

    <link href="Styles/MasterPage.css" rel="stylesheet" type="text/css" />
    <link href="Styles/styles.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Responsive.css" rel="stylesheet" type="text/css" />
    <link href="Styles/progressbar.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" >
    <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True"
        AsyncPostBackTimeout="360000">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            Sys.Application.add_load(function () {
                $('.progressBarContainer').each(function (i, obj) {
                    var hidValue = $('input[type=hidden]', this).val();
                    //alert(parseFloat(hidValue) + '%');
                    jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                    var progressBarWidth = hidValue * $('#progressBar').width() / 100;
                    jQuery('[id=progress]', this).animate({ width: progressBarWidth }, 3000).html("&nbsp;");

                    if (hidValue >= 0 && hidValue <= 50) {
                        jQuery('[id=progress]', this).addClass('red');
                    }
                    if (hidValue >= 50 && hidValue <= 75) {
                        jQuery('[id=progress]', this).addClass('yellow');
                    }
                    if (hidValue >= 75 && hidValue <= 100) {
                        jQuery('[id=progress]', this).addClass('green');
                    }
                });
            });

            //SCORM
            function OpenTraining(strOrgOrAtt) {
                // open training content; <strOrgOrAtt> is either of the form "Org:<organizationId>"
                // (for content that has not been launched yet) or "Att:<attemptId>" for content that's
                // previously been launched -- in the former case we need to create an attempt for the
                // content...
                var a;

                if ((a = strOrgOrAtt.match(/^Pkg:([0-9]+)$/)) != null) {
                    // display the dialog to create an attempt on this organization; if the attempt is
                    // successfully created, OnAttemptCreated() will be called from the the dialog to
                    // update TrainingGrid and display the training
                    var args = new Object;
                    args.OrganizationId = a[1];
                    ShowDialog("SCORM/CreateAttempt.aspx?PkgID=" + a[1], args, 450, 250, false);
                }
                else if ((a = strOrgOrAtt.match(/^Att:([0-9]+)$/)) != null) {
                    // open training in a new window
                    OpenFrameset(a[1]);
                }
            }

//            function OnAttemptCreated(strOrganizationId, strAttemptId) {
//                OpenFrameset(strAttemptId);
//                // called after CreateAttempt.aspx has successfully created an attempt; update the
//                // anchor tag to include the attempt number, then open the frameset
//                //                var anchor = document.all["Org_" + strOrganizationId];
//                //                anchor.href = "javascript:OpenTraining('Att:" + strAttemptId + "')";
//                //                anchor.title = "Continue training";
//                //                anchor.parentElement.parentElement.cells[3].innerHTML =
//                //            	"<A href=\"javascript:ShowLog(" + strAttemptId + ")\" title=\"Show Log\">Active</A>";
//                OpenFrameset(strAttemptId);
//            }

            function ShowDialog(strUrl, args, cx, cy, fScroll) {
                // display a dialog box with URL <strUrl>, arguments <args>, width <cx>, height <cy>,
                // scrollbars if <fScroll>; this can be done using either showModalDialog() or
                // window.open(): the former has better modal behavior; the latter allows selection
                // within the window

                var useShowModalDialog = false;
                var strScroll = fScroll ? "yes" : "no";
                if (useShowModalDialog) {
                    showModalDialog(strUrl, args,
				    "dialogWidth: " + cx + "px; dialogHeight: " + cy +
					"px; center: yes; resizable: yes; scroll: " + strScroll + ";");
                }
                else {
                    dialogArguments = args; // global variable accessed by dialog
                    var x = Math.max(0, (screen.width - cx) / 2);
                    var y = Math.max(0, (screen.height - cy) / 2);
                    window.open(strUrl, "myWindow", "left=" + x + ",top=" + y +
					",width=" + cx + ",height=" + cy +
					",location=no,menubar=no,scrollbars=" + strScroll +
					",status=no,toolbar=no,resizable=yes");
                }
            }

            function OpenFrameset(strAttemptId) {
                window.open('<%=Page.ResolveClientUrl("~/Frameset/Frameset.aspx?View=0&AttemptId=")%>' + strAttemptId, "myWindow", "width=800,height=600,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,resizable=yes")
                location.reload();
            }

            function OpenCourse(completePath) {
                alert('Open Course');
                window.open(completePath, "myWindow", "width=800,height=600,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,resizable=yes")
                //window.open(completePath, '_blank', "width=800,height=600,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,resizable=yes");
                location.reload();
            }
            //END SCORM
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:Panel ID="pnlMain" runat="server">
        <div id="wrapper">
            <div id="Header">
            </div>
            <div id="Body">
                <div id="divBundleDetails" style="padding-bottom: 15px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblBundleTitle" Text="Bundle Title" Style="font-size: x-large;
                                    font-weight: 700" CssClass="lblTUTorquise" />
                            </td>
                            <td style="width: 120px">
                                <div class="progressBarContainer">
                                    <div id="progressBar">
                                        <asp:HiddenField ID="hdnOverall" runat="server" Value="" />
                                        <span id="spanval" style="line-height: 16px;"></span>
                                        <div id="progress">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <label class="lblTUTorquise">
                        Course Description:
                    </label>
                    <asp:Label runat="server" ID="lblBundleDesc" Text="Bundle Description" />
                </div>
                <div id="divLearner" style="padding-bottom: 15px">
                    <label>
                        <span class="lblMedium">Learner Resources</span></label>
                    <hr />
                    <telerik:RadGrid ID="gridBundle" runat="server" AutoGenerateColumns="False" GridLines="None"
                        EnableLinqExpressions="True" OnNeedDataSource="gridBundle_NeedDataSource" OnPreRender="GridBundlePreRender"
                        OnItemCommand="GridBundleItemCommand" OnItemDataBound="GridBundleItemDataBound"
                        CssClass="RadGrid3" Skin="Metro" Width="99.8%" ShowHeader="False">
                        <GroupingSettings CaseSensitive="false" />
                        <FilterItemStyle HorizontalAlign="Center" />
                        <MasterTableView DataKeyNames="CurriculumId,Title,curriculumcourseID,Score,description"
                            HierarchyDefaultExpanded="true" AutoGenerateColumns="false" GroupLoadMode="Server"
                            HierarchyLoadMode="ServerBind" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                            ExpandCollapseColumn-ButtonType="ImageButton" ExpandCollapseColumn-CollapseImageUrl="Images/collapse2.png"
                            ExpandCollapseColumn-ExpandImageUrl="Images/expand2.png" GridLines="None">
                            <NestedViewSettings>
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="CurriculumId" MasterKeyField="CurriculumId" />
                                </ParentTableRelation>
                            </NestedViewSettings>
                            <NestedViewTemplate>
                                <div style="padding: 10px">
                                    <fieldset>
                                        <table width="100%" style="padding-bottom: 10px">
                                            <tr>
                                                <td>
                                                    <label class="lblTUTorquise">
                                                        Title:
                                                    </label>
                                                    <asp:Label runat="server" ID="lblTitle1" Text="title here" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="lblTUTorquise">
                                                        Description:
                                                    </label>
                                                    <asp:Label runat="server" ID="lblDesc1" Text="description here" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="lblCurriculumID" Text='<%# Eval("CurriculumId") %>' Visible="false"
                                            runat="server" />
                                        <asp:Label ID="lblCurriculumCourseID" Text='<%# Eval("curriculumcourseID") %>' Visible="false"
                                            runat="server" />
                                        <telerik:RadGrid ID="gridPackageScos" runat="server" Width="100%" DataSourceID="dsPackageScos"
                                            EnableLinqExpressions="True" GridLines="None" CssClass="RadGrid4" Skin="Metro"
                                            BorderStyle="None" BorderWidth="0" BorderColor="White" OnItemDataBound="gridPackageScosItemDataBound"
                                            OnItemCreated="gridPackageScosItemCreated">
                                            <MasterTableView Name="PackageScos" DataKeyNames="CurriculumID,ScoID,FileName,AttemptID,RedirectID,ScoPackageID,coursetypeID,EncryptedOrManifest,AttemptStatus,SuccessStatus,PackageFormat,LessonStatus,Score,SCOTYPEID"
                                                AutoGenerateColumns="False" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                                                ShowHeader="False" BorderStyle="None" BorderWidth="0" BorderColor="White" GridLines="None">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="ScoTitle" HeaderText="ScoTitle" SortExpression="ScoTitle"
                                                        UniqueName="ScoTitle">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridTemplateColumn UniqueName="Progress" HeaderText="Progress" HeaderStyle-Width="160px">
                                                        <ItemTemplate>
                                                            <div align="center" style="font-size: x-small !important">
                                                                <asp:Label runat="server" ID="lblStatusMarker" Text="Status:" CssClass="lblTUTorquise" />
                                                                <asp:Label runat="server" ID="lblStatus" Text="Not Taken" />
                                                                <br />
                                                                <asp:Label runat="server" ID="lblScoreMarker" Text="Score:" CssClass="lblTUTorquise" />
                                                                <asp:Label runat="server" ID="lblScore" Text="N/A" />
                                                            </div>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="Launch" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:Button runat="server" ID="btnLaunch" ClientIDMode="Static" Text="Launch" CssClass="linkBtn" ToolTip="Launch Course" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="DownloadCert">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnDownloadCert" runat="server" ImageUrl="~/Images/Certificate.png"  OnClick="btnDownloadCert_Click" 
                                                                ToolTip="Download Certificate" Visible="false" style="width: 16px; height: 16px;">
                                                            </asp:ImageButton>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </fieldset>
                                    <asp:SqlDataSource ID="dsPackageScos" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                        ProviderName="System.Data.SqlClient" OnSelecting="dsPackageScosr_Selecting" SelectCommand="pr_TranscomUniversity_lkp_PackageDetails"
                                        SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter Name="CurriculumID" ControlID="lblCurriculumID" Type="Int32"
                                                PropertyName="Text" />
                                            <asp:ControlParameter Name="CurriculumCourseID" ControlID="lblCurriculumCourseID"
                                                Type="Int32" PropertyName="Text" />
                                            <asp:Parameter Name="CIM" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </NestedViewTemplate>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Title" HeaderText="Title" SortExpression="Title"
                                    UniqueName="Title">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
                <div id="divTrainer" runat="server">
                    <label>
                        <span class="lblMedium">Trainer Resources</span></label>
                    <hr />
                    <telerik:RadGrid ID="gridTrainer" runat="server" AutoGenerateColumns="False" GridLines="None"
                        EnableLinqExpressions="True" OnNeedDataSource="GridTrainer_NeedDataSource" OnPreRender="GridTrainerPreRender"
                        OnItemCommand="GridBundleItemCommand" CssClass="RadGrid3" Skin="Metro" Width="99.8%"
                        ShowHeader="False">
                        <GroupingSettings CaseSensitive="false" />
                        <FilterItemStyle HorizontalAlign="Center" />
                        <MasterTableView DataKeyNames="ScoTypeID" AutoGenerateColumns="false" GroupLoadMode="Server"
                            HierarchyLoadMode="ServerBind" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                            ExpandCollapseColumn-ButtonType="ImageButton" ExpandCollapseColumn-CollapseImageUrl="Images/collapse2.png"
                            ExpandCollapseColumn-ExpandImageUrl="Images/expand2.png" GridLines="None">
                            <NestedViewSettings>
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="ScoTypeID" MasterKeyField="ScoTypeID,CurriculumID" />
                                </ParentTableRelation>
                            </NestedViewSettings>
                            <NestedViewTemplate>
                                <div style="padding: 10px">
                                    <fieldset>
                                        <asp:Label ID="lblScoTypeID" Text='<%# Eval("ScoTypeID") %>' Visible="false" runat="server" />
                                        <asp:Label ID="lblCurriculumID" Text='<%# Eval("CurriculumId") %>' Visible="false"
                                            runat="server" />
                                        <telerik:RadGrid ID="gridTrainerResources" runat="server" Width="100%" DataSourceID="dsTrainer"
                                            EnableLinqExpressions="True" GridLines="None" CssClass="RadGrid4" Skin="Metro"
                                            BorderStyle="None" BorderWidth="0" BorderColor="White" OnItemDataBound="gridTrainerResourcesItemDataBound">
                                            <MasterTableView Name="PackageScos" DataKeyNames="FileName" AutoGenerateColumns="False"
                                                EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" ShowHeader="False"
                                                BorderStyle="None" BorderWidth="0" BorderColor="White" GridLines="None">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="ResourceName" HeaderText="Resource Name" SortExpression="ResourceName"
                                                        UniqueName="ResourceName">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridTemplateColumn UniqueName="Launch" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <label>
                                                                |</label>&nbsp;&nbsp;
                                                            <asp:Button runat="server" ID="btnLaunch" Text="Launch" CssClass="linkBtn" ToolTip="Launch Course" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </fieldset>
                                    <asp:SqlDataSource ID="dsTrainer" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                        ProviderName="System.Data.SqlClient" SelectCommand="pr_TranscomUniversity_lkp_GetTrainerResourcesDetails"
                                        SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter Name="ScoTypeID" ControlID="lblScoTypeID" Type="Int32" PropertyName="Text" />
                                            <asp:ControlParameter Name="CurriculumID" ControlID="lblCurriculumID" Type="Int32"
                                                PropertyName="Text" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </NestedViewTemplate>
                            <Columns>
                                <telerik:GridBoundColumn DataField="ScoType" HeaderText="ScoType" SortExpression="ScoType"
                                    UniqueName="ScoType">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </div>
            <div id="Footer">
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
