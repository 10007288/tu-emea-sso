﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BulkUpload.aspx.cs" Inherits="BulkUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2>
            Users Bulk Upload
        </h2>
        <hr />
        <div>
            <asp:Panel ID="pnlUpload" runat="server">
                <telerik:RadAsyncUpload ID="RadAsyncUploadCSV" runat="server" TargetFolder="~/App_Data"
                    TemporaryFolder="~/App_Data" HideFileInput="true" MultipleFileSelection="Disabled"
                    OnClientValidationFailed="OnClientValidationFailed" OnClientFilesUploaded="OnClientFilesUploaded"
                    OnFileUploaded="RadAsyncUploadCSV_FileUploaded" AllowedFileExtensions="csv,txt"
                    TemporaryFileExpiration="00:59:00" Localization-Select="Select File" MaxFileInputsCount="1"
                    EnableInlineProgress="false">
                </telerik:RadAsyncUpload>
            </asp:Panel>
            <asp:Panel ID="pnlErrorLog" runat="server" Visible="false">
                <p>
                    <b>An error occured, please check if the template file is a valid.</b></p>
                <asp:TextBox ID="txtErrorLog" runat="server" TextMode="MultiLine" Rows="10" Width="400px"
                    Text="Error Logs: "></asp:TextBox>
            </asp:Panel>
            <telerik:RadProgressManager ID="RadProgressManager1" runat="server" />
            <telerik:RadProgressArea RenderMode="Lightweight" ID="RadProgressArea1" runat="server"
                Width="100%" Localization-UploadedFiles="Uploaded Records" Localization-TotalFiles="Total Records" />
            <br>
            <asp:Panel ID="pnlUploadResult" runat="server" Visible="false">
                <fieldset style="width: 350px">
                    <legend>
                        <p>
                            <b>Upload Result</b></p>
                    </legend>
                    <p>
                        Total Records:
                        <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
                    </p>
                    <p>
                        Inserted Records:
                        <asp:Label ID="lblInsertCount" runat="server" Text="Pending"></asp:Label>
                    </p>
                    <p>
                        Updated Records:
                        <asp:Label ID="lblUpdateCount" runat="server" Text="Pending"></asp:Label>
                    </p>
                    <p>
                        <asp:Button ID="btnConfirm" runat="server" Text="Confirm Upload" Width="100px" OnClick="btnConfirm_Click" /></p>
                    <asp:Button ID="btnBack" runat="server" Text="Back to Admin Page" OnClick="btnBack_Click" /></p>
                </fieldset>
                <br />
                <telerik:RadTabStrip ID="tabStrip1" runat="server" ReorderTabsOnSelect="false" MultiPageID="MultiPage1"
                    SelectedIndex="0">
                    <Tabs>
                        <telerik:RadTab Text="Upload Result" Selected="true">
                        </telerik:RadTab>
                        <telerik:RadTab Text="Error Logs">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="PageView1" runat="server" Selected="true">
                        <telerik:RadGrid ID="grdUsers" runat="server" OnNeedDataSource="grdUsers_NeedDataSource">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" ScrollHeight="400px" />
                                <Resizing AllowColumnResize="true" EnableRealTimeResize="true" />
                            </ClientSettings>
                            <MasterTableView Name="Users" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true"
                                EditMode="Batch" EnableHeaderContextMenu="true" DataKeyNames="Status,TwwID,FirstName,LastName,RoleID,RegionID,CountryID,SiteID,SupervisorID">
                                <BatchEditingSettings OpenEditingEvent="Click" />
                                <PagerStyle AlwaysVisible="true" Mode="Advanced" />
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Upload Status" UniqueName="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnStatus" runat="server" ImageUrl="Images/pending.png" ToolTip="Pending" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="TwwID" HeaderText="TwwID" SortExpression="TwwID"
                                        ReadOnly="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Role" HeaderText="Role" SortExpression="Role" ReadOnly="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RoleID" HeaderText="RoleID" SortExpression="RoleID"
                                        Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SupervisorID" HeaderText="SupervisorID" SortExpression="SupervisorID"
                                        ReadOnly="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Password" HeaderText="Password" SortExpression="Password">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Client" HeaderText="Client" SortExpression="Client">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Country" HeaderText="Country" SortExpression="Country" ReadOnly="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CountryID" HeaderText="CountryID" SortExpression="CountryID"
                                        Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Region" HeaderText="Region" SortExpression="Region" ReadOnly="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RegionID" HeaderText="RegionID" SortExpression="RegionID"
                                        Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site" ReadOnly="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SiteID" HeaderText="SiteID" SortExpression="SiteID"
                                        Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Language" HeaderText="Language" SortExpression="Language">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="PageView2" runat="server">
                        <telerik:RadListBox ID="listBoxError" runat="server" Width="100%" Height="400px">
                        </telerik:RadListBox>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </asp:Panel>
        </div>
        <telerik:RadScriptManager ID="scriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager ID="ajaxManagerProxy1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="trvCodes">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="trvCodes" LoadingPanelID="LoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadScriptBlock ID="radScriptBlock1" runat="server">
            <script type="text/javascript">
                function OnClientValidationFailed(sender, args) {
                    //Display notification in case that user selects file different than .csv
                    alert("Invalid file type! Please select .csv file.");
                }

                function OnClientFilesUploaded(sender, args) {
                    //Cause postback to upload and display .csv file
                    __doPostBack('', '');
                }
            </script>
        </telerik:RadScriptBlock>
    </div>
    </form>
</body>
</html>
