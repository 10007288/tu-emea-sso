﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" Title="Welcome to Transcom University" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                $("#content").load('<%= Page.ResolveClientUrl("~/PageFiles/Welcome Message/welcome.html") %>' + '?myRand=' + guid2());
                $(".lnkreset").click(function (event) {
                    //                    event.preventDefault();
                    var hidCim = $("input:hidden[id$=hidCim]").val();
                    $.ajax({
                        type: "POST",
                        url: "ServiceContainer.aspx/resetAssignCourseNotifications",
                        data: '{CIM: "' + hidCim + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function OnSuccess(response) {

                            var msg = response.d;
                            if (msg != '') {
                                alert(msg);
                            }
                        },
                        failure: function (response) {
                            alert('Error Occured cannot update notifications.');
                        }
                    });
                });
            });
            function s4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }
            function guid2() {
                return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
            }
        </script>
    </telerik:RadCodeBlock>
    <div>
        <telerik:RadAjaxPanel ID="panel" runat="server">
            <telerik:RadNotification ID="RadNotification_CourseEnrolment" runat="server" Width="250"
                Animation="Fade" EnableRoundedCorners="true" Title="Alert Inbox!" ShowTitleMenu="true"
                OffsetX="-40" OffsetY="-40" AutoCloseDelay="0" Skin="Silk" EnableShadow="True"
                CssClass="importantalert_parent">
                <NotificationMenu Visible="False" Enabled="False" />
                <ContentTemplate>
                    <div style="width: 240px; height: 140px; overflow: auto;">
                        <div runat="server" id="divEnrolment_ForApproval" visible="False" class="importantalert">
                            <span>You have pending Course Enrollment waiting for approval!</span>
                            <br />
                            <br />
                            For Approval:
                            <asp:Label runat="server" ID="lblForApprovalCnt" Text="0" Font-Bold="True" />
                            <br />
                            <br />
                            <asp:HyperLink ID="HyperLink2" runat="server" Text="Review Enrollments" NavigateUrl="MyTeam.aspx"
                                Target="_blank" CssClass="linkBtn" />
                        </div>
                        <div runat="server" id="divEnrolment_Status" visible="False" class="importantalert">
                            <span>A status change has been made on your Course registration!</span>
                            <br />
                            <br />
                            Approved:
                            <asp:Label runat="server" ID="lblApprovedCnt" Text="0" Font-Bold="True" />
                            Denied:
                            <asp:Label runat="server" ID="lblDeniedCnt" Text="0" Font-Bold="True" />
                            <br />
                            <br />
                            <asp:HyperLink ID="HyperLink1" runat="server" Text="View Learning Plan" NavigateUrl="MyPlan.aspx"
                                Target="_blank" CssClass="linkBtn" />
                        </div>
                        <div runat="server" id="divEvent_ForApproval" visible="False" class="importantalert">
                            <span>You have pending room reservation/s waiting for approval!</span>
                            <br />
                            <br />
                            <asp:HyperLink ID="lnkViewReservations" runat="server" Text="Review Reservations"
                                NavigateUrl="CalendarEvents.aspx" Target="_blank" CssClass="linkBtn" />
                        </div>
                        <div runat="server" id="divEvent_AssignCourseAssigned" visible="False" class="importantalert">
                            <span>A new course has been assigned to you.</span>
                            <br />
                            <br />
                            <%--DO NOT DELETE THIS!!!--%>
                            <%--Newly Assigned:
                            <asp:Label runat="server" ID="lblAssignedCnt" Text="0" Font-Bold="True" />
                            <br />
                            Moved:
                            <asp:Label runat="server" ID="lblMovedCnt" Text="0" Font-Bold="True" />
                            Removed:
                            <asp:Label runat="server" ID="lblRemovedCnt" Text="0" Font-Bold="True" />
                            <br />--%>
                            <br />
                            <a class="linkBtn lnkreset" href="MyPlan.aspx" target="_blank">View My Learning Plan</a>
                        </div>
                    </>
                </ContentTemplate>
            </telerik:RadNotification>
        </telerik:RadAjaxPanel>
    </div>
    <div class="containerdefault">
        <div class="containerdefault">
            <div id="content">
            </div>
            <div style="padding-bottom: 15px;">
                <asp:LinkButton runat="server" ID="btnContinue" Text="Click here to continue" CssClass="linkBtn"
                    PostBackUrl="Courses.aspx" />
            </div>
            <div runat="server" id="ctrTULinkInternal" style="padding-bottom: 15px;">
                <label style="color: rgb(1, 132, 126);">
                    To access this outside the Transcom University Network, please use this address:
                </label>
                <asp:LinkButton runat="server" ID="lbnTUInternal" Text="https://applications.transcom.com/transcomuniversity"
                    PostBackUrl="https://applications.transcom.com/transcomuniversity" CssClass="linkBtn" />
            </div>
            <div runat="server" id="ctrInternal" style="padding-bottom: 15px;">
                <p style="line-height: 150%; margin-top: 0; margin-bottom: 0;" align="center">
                    <span style="font-size: 10pt;">
                        <img src="http://s1.simplecount.com/c/nucuni" border="0" alt="This Many People Have Been Here" /></span>
                </p>
            </div>
            <div runat="server" id="ctrExternal" style="padding-bottom: 15px;">
                <script type="text/javascript" language="javascript">
                    var sc_project = 1353008;
                    var sc_invisible = 1;
                    var sc_partition = 12;
                    var sc_security = "6e928017"; 
                </script>
                <script type="text/javascript" language="javascript" src="http://www.statcounter.com/counter/frames.js"></script>
                <noscript>
                    <a href="http://www.statcounter.com/" target="_blank">
                        <img src="http://c13.statcounter.com/counter.php?sc_project=1353008&amp;java=0&amp;security=6e928017&amp;invisible=1"
                            alt="hidden hit counter" border="0" /></a>
                </noscript>
            </div>
        </div>
        <div>
            <asp:HiddenField ID="hidCim" Value="" runat="server" />
        </div>
    </div>
</asp:Content>
