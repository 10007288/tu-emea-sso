﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using System.IO;

public partial class CourseLauncher : BasePage
{
    public Boolean DelVisible;
    string UserId = "";
    string CourseID = "";
    int CommentID_ = 0;
    String connStr = ConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string CurrURL = Session["CourseLaunch"].ToString();
                this.CourseURL.Attributes["src"] = CurrURL;

                UserId = Session["USERID"].ToString();
                CourseID = Session["COURSEID"].ToString();
                CourseNo_HF.Value = CourseID;
                TWWID_HF.Value = UserId;

                var UserRoleID = Registration.GetRoleForUser(UserId);
                if (UserRoleID == "Admin")
                {
                    DelVisible = true;
                }
                else
                {
                    DelVisible = false;
                }

                var db = new IntranetDBDataContext();
                db.pr_NuCommUniversity_Sav_EmployeeCourseLogin(Convert.ToInt32(CurrTWWID), Convert.ToInt32(CourseID));
            }
            catch
            {

            }
        }
        else
        {
            try
            {
                UserId = Session["USERID"].ToString();
                CourseID = Session["COURSEID"].ToString();

                var UserRoleID = Registration.GetRoleForUser(UserId);
                if (UserRoleID == "Admin")
                {
                    DelVisible = true;
                }
                else
                {
                    DelVisible = false;
                }
            }
            catch
            {

            }
        }
    }

#region "COMMENT"

    public void Grid_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
    {
        if (e.Column.OrderIndex < 0)
            e.Column.Visible = false;
    }

    protected void btnDelComment_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        GridItem editItem = (GridItem)btn1.NamingContainer;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(RadGrid1.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
        string ToRunSql = "Update tblComment set status = 0 where CommId = " + KeyVal;
        RunQuery(ToRunSql);
        String StrSQL = "select " +
                        "((select S.Parent_RowNo " +
                        "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                        "FROM tblComment where ParentID is NULL) as S " +
                        "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                        "ROW_NUMBER() OVER( ORDER BY CommID ) -1  AS 'rownumber', CommID," +
                        "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                        "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                        "FROM tblComment where Status = 1 and ParentID is NULL";
        RadGrid1.DataSource = GetDataTable(StrSQL);
        RadGrid1.Rebind();
    }

    protected void btnDelComment2_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        GridItem editItem = (GridItem)btn1.NamingContainer;
        RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo2");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
        string ToRunSql = "Update tblComment set status = 0 where CommId = " + KeyVal;
        RunQuery(ToRunSql);
        ToRunSql = "Select ParentID from tblComment where CommId = " + KeyVal;
        KeyVal = Convert.ToInt32(GetDataValue(ToRunSql));
        String StrSQL = "select " +
                "((select S.Parent_RowNo " +
                "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                "FROM tblComment where ParentID is NULL) as S " +
                "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
        rd.DataSource = GetDataTable(StrSQL);
        rd.Rebind();
    }

    protected void btnDelComment3_Click(object sender, EventArgs e)
    {

        ImageButton btn1 = (ImageButton)sender;
        GridItem editItem = (GridItem)btn1.NamingContainer;
        RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo3");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
        string ToRunSql = "Update tblComment set status = 0 where CommId = " + KeyVal;
        RunQuery(ToRunSql);
        ToRunSql = "Select ParentID from tblComment where CommId = " + KeyVal;
        KeyVal = Convert.ToInt32(GetDataValue(ToRunSql));
        String StrSQL = "select " +
                "((select S.Parent_RowNo " +
                "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                "FROM tblComment where ParentID is NULL) as S " +
                "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
        rd.DataSource = GetDataTable(StrSQL);
        rd.Rebind();
    }

    protected void btnDelComment4_Click(object sender, EventArgs e)
    {

        ImageButton btn1 = (ImageButton)sender;
        GridItem editItem = (GridItem)btn1.NamingContainer;
        RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo4");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
        string ToRunSql = "Update tblComment set status = 0 where CommId = " + KeyVal;
        RunQuery(ToRunSql);
        ToRunSql = "Select ParentID from tblComment where CommId = " + KeyVal;
        KeyVal = Convert.ToInt32(GetDataValue(ToRunSql));
        String StrSQL = "select " +
                "((select S.Parent_RowNo " +
                "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                "FROM tblComment where ParentID is NULL) as S " +
                "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
        rd.DataSource = GetDataTable(StrSQL);
        rd.Rebind();
    }

    protected void btnDelComment5_Click(object sender, EventArgs e)
    {

        ImageButton btn1 = (ImageButton)sender;
        GridItem editItem = (GridItem)btn1.NamingContainer;
        RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo5");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
        string ToRunSql = "Update tblComment set status = 0 where CommId = " + KeyVal;
        RunQuery(ToRunSql);
        ToRunSql = "Select ParentID from tblComment where CommId = " + KeyVal;
        KeyVal = Convert.ToInt32(GetDataValue(ToRunSql));
        String StrSQL = "select " +
                "((select S.Parent_RowNo " +
                "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                "FROM tblComment where ParentID is NULL) as S " +
                "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
        rd.DataSource = GetDataTable(StrSQL);
        rd.Rebind();

    }

    protected void btnVwComment_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/comment.png")
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply");
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = true;
            GridItem gd = RadGrid1.MasterTableView.Items[RowNo];
            gd.Expanded = true;
            GridNestedViewItem gd_nv = (GridNestedViewItem)RadGrid1.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = true;
            Int32 KeyVal = Int32.Parse(RadGrid1.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
            RadGrid grid = RadGrid1.MasterTableView.Items[RowNo].ChildItem.FindControl("doc_grid") as RadGrid;
            String StrSQL = "select " +
                            "((select S.Parent_RowNo " +
                            "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                            "FROM tblComment where ParentID is NULL) as S " +
                            "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                            "ROW_NUMBER() OVER( ORDER BY CommID ) -1  AS 'rownumber', CommID," +
                            "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                            "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                            "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
            grid.DataSource = GetDataTable(StrSQL);
            grid.Rebind();
            btn1.ImageUrl = "~/Images/comment2.png";
        }
        else
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply");
            TextBox txtRPCommID = (TextBox)editItem.FindControl("txtRepComm_CommID");
            Par_Item.Visible = false;

            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            Par_Item.Visible = false;
            editItem.Expanded = false;
            GridItem gd = RadGrid1.MasterTableView.Items[RowNo];
            gd.Expanded = false;
            GridNestedViewItem gd_nv = (GridNestedViewItem)RadGrid1.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = false;
            btn1.ImageUrl = "~/Images/comment.png";
        }
    }

    protected void btnVwComment2_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/comment.png")
        {
            GridItem editItem = (GridItem)btn1.NamingContainer;
            RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo2");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = true;
            GridItem gd = (GridItem)rd.MasterTableView.Items[RowNo];
            gd.Expanded = true;
            GridNestedViewItem gd_nv = (GridNestedViewItem)rd.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = true;

            GridItem gd2 = (GridItem)rd.MasterTableView.Items[RowNo].ChildItem;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());
            RadGrid grid = gd2.FindControl("gridLvl3") as RadGrid;
            String StrSQL = "select " +
                            "((select S.Parent_RowNo " +
                            "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                            "FROM tblComment where ParentID is NULL) as S " +
                            "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                            "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                            "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                            "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                            "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
            grid.DataSource = GetDataTable(StrSQL);
            grid.Rebind();
            btn1.ImageUrl = "~/Images/comment2.png";
        }
        else
        {
            GridItem editItem = (GridItem)btn1.NamingContainer;
            RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo2");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = false;
            GridItem gd = (GridItem)rd.MasterTableView.Items[RowNo];
            gd.Expanded = false;
            GridNestedViewItem gd_nv = (GridNestedViewItem)rd.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = false;
            btn1.ImageUrl = "~/Images/comment.png";
        }
    }

    protected void btnVwComment3_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/comment.png")
        {
            GridItem editItem = (GridItem)btn1.NamingContainer;
            RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo3");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = true;
            GridItem gd = (GridItem)rd.MasterTableView.Items[RowNo];
            gd.Expanded = true;
            GridNestedViewItem gd_nv = (GridNestedViewItem)rd.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = true;

            GridItem gd2 = (GridItem)rd.MasterTableView.Items[RowNo].ChildItem;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString()); ;
            RadGrid grid = gd2.FindControl("gridLvl4") as RadGrid;
            String StrSQL = "select " +
                            "((select S.Parent_RowNo " +
                            "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                            "FROM tblComment where ParentID is NULL) as S " +
                            "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                            "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                            "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                            "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                            "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
            grid.DataSource = GetDataTable(StrSQL);
            grid.Rebind();
            btn1.ImageUrl = "~/Images/comment2.png";
        }
        else
        {
            GridItem editItem = (GridItem)btn1.NamingContainer;
            RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo3");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = false;
            GridItem gd = (GridItem)rd.MasterTableView.Items[RowNo];
            gd.Expanded = false;
            GridNestedViewItem gd_nv = (GridNestedViewItem)rd.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = false;
            btn1.ImageUrl = "~/Images/comment.png";
        }
    }

    protected void btnVwComment4_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/comment.png")
        {
            GridItem editItem = (GridItem)btn1.NamingContainer;
            RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo4");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = true;
            GridItem gd = (GridItem)rd.MasterTableView.Items[RowNo];
            gd.Expanded = true;
            GridNestedViewItem gd_nv = (GridNestedViewItem)rd.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = true;

            GridItem gd2 = (GridItem)rd.MasterTableView.Items[RowNo].ChildItem;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString()); ;
            RadGrid grid = gd2.FindControl("gridLvl5") as RadGrid;
            String StrSQL = "select " +
                            "((select S.Parent_RowNo " +
                            "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                            "FROM tblComment where ParentID is NULL) as S " +
                            "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                            "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                            "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                            "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                            "FROM tblComment where Status = 1 and ParentID = " + KeyVal + "";
            grid.DataSource = GetDataTable(StrSQL);
            grid.Rebind();
            btn1.ImageUrl = "~/Images/comment2.png";
        }
        else
        {
            GridItem editItem = (GridItem)btn1.NamingContainer;
            RadGrid rd = (RadGrid)editItem.NamingContainer.Parent;
            Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo4");
            Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
            editItem.Expanded = false;
            GridItem gd = (GridItem)rd.MasterTableView.Items[RowNo];
            gd.Expanded = false;
            GridNestedViewItem gd_nv = (GridNestedViewItem)rd.MasterTableView.Items[RowNo].ChildItem;
            gd_nv.Expanded = false;
            btn1.ImageUrl = "~/Images/comment.png";
        }
    }

    protected void btnRepComment_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/reply.png")
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply");
            Par_Item.Visible = true;
            btn1.ImageUrl = "~/Images/reply2.png";
        }
        else
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply");
            Par_Item.Visible = false;
            btn1.ImageUrl = "~/Images/reply.png";
        }
    }

    protected void btnRepComment2_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/reply.png")
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply2");
            Par_Item.Visible = true;
            btn1.ImageUrl = "~/Images/reply2.png";
        }
        else
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply2");
            Par_Item.Visible = false;
            btn1.ImageUrl = "~/Images/reply.png";
        }
    }

    protected void btnRepComment3_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/reply.png")
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply3");
            Par_Item.Visible = true;
            btn1.ImageUrl = "~/Images/reply2.png";
        }
        else
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply3");
            Par_Item.Visible = false;
            btn1.ImageUrl = "~/Images/reply.png";
        }
    }

    protected void btnRepComment4_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/reply.png")
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply4");
            Par_Item.Visible = true;
            btn1.ImageUrl = "~/Images/reply2.png";
        }
        else
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply4");
            Par_Item.Visible = false;
            btn1.ImageUrl = "~/Images/reply.png";
        }
    }

    protected void btnRepComment5_Click(object sender, EventArgs e)
    {
        ImageButton btn1 = (ImageButton)sender;
        if (btn1.ImageUrl == "~/Images/reply.png")
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply5");
            Par_Item.Visible = true;
            btn1.ImageUrl = "~/Images/reply2.png";
        }
        else
        {
            GridDetailTemplateItem editItem = (GridDetailTemplateItem)btn1.NamingContainer;
            Panel Par_Item = (Panel)btn1.NamingContainer.FindControl("pnReply5");
            Par_Item.Visible = false;
            btn1.ImageUrl = "~/Images/reply.png";
        }
    }

    protected void btnReply_Click(object sender, EventArgs e)
    {
        UserId = Session["USERID"].ToString();
        CourseID = Session["COURSEID"].ToString();
        Button btn1 = (Button)sender;

        Telerik.Web.UI.GridDetailTemplateItem editItem = (Telerik.Web.UI.GridDetailTemplateItem)btn1.NamingContainer;
        TextBox txt = (TextBox)editItem.FindControl("txtReply");
        string strKey = txt.Text;

        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(RadGrid1.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());

        UserDBDataContext UDB = new UserDBDataContext();
        tblComment TC = new tblComment();
        TC.ParentId = KeyVal;
        TC.CommentText = txt.Text;
        TC.CommDate = DateTime.Now;
        TC.CommBy = UserId;
        TC.CourseID = CourseID;
        TC.Status = 1;
        UDB.tblComments.InsertOnSubmit(TC);
        UDB.SubmitChanges();
        txt.Text = String.Empty;
        GridNestedViewItem NV = (GridNestedViewItem)RadGrid1.MasterTableView.Items[RowNo].ChildItem;
        RadGrid rd = (RadGrid)NV.FindControl("doc_grid");
        rd.Rebind();
    }

    protected void btnReply2_Click(object sender, EventArgs e)
    {
        UserId = Session["USERID"].ToString();
        CourseID = Session["COURSEID"].ToString();
        Button btn1 = (Button)sender;
        Telerik.Web.UI.GridDetailTemplateItem editItem = (Telerik.Web.UI.GridDetailTemplateItem)btn1.NamingContainer;
        TextBox txt = (TextBox)editItem.FindControl("txtReplyLvl2");
        string strKey = txt.Text;

        RadGrid RdPar = (RadGrid)editItem.Parent.Parent.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo2");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(RdPar.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());

        UserDBDataContext UDB = new UserDBDataContext();
        tblComment TC = new tblComment();
        TC.ParentId = KeyVal;
        TC.CommentText = txt.Text;
        TC.CommDate = DateTime.Now;
        TC.CommBy = UserId;
        TC.CourseID = CourseID;
        TC.Status = 1;
        UDB.tblComments.InsertOnSubmit(TC);
        UDB.SubmitChanges();
        txt.Text = String.Empty;
        GridNestedViewItem NV = (GridNestedViewItem)RdPar.MasterTableView.Items[RowNo].ChildItem;
        RadGrid rd = (RadGrid)NV.FindControl("gridLvl3");
        rd.Rebind();
    }

    protected void btnReply3_Click(object sender, EventArgs e)
    {
        UserId = Session["USERID"].ToString();
        CourseID = Session["COURSEID"].ToString();
        Button btn1 = (Button)sender;
        Telerik.Web.UI.GridDetailTemplateItem editItem = (Telerik.Web.UI.GridDetailTemplateItem)btn1.NamingContainer;
        TextBox txt = (TextBox)editItem.FindControl("txtReplyLvl3");
        string strKey = txt.Text;

        RadGrid RdPar = (RadGrid)editItem.Parent.Parent.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo3");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(RdPar.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());

        UserDBDataContext UDB = new UserDBDataContext();
        tblComment TC = new tblComment();
        TC.ParentId = KeyVal;
        TC.CommentText = txt.Text;
        TC.CommDate = DateTime.Now;
        TC.CommBy = UserId;
        TC.CourseID = CourseID;
        TC.Status = 1;
        UDB.tblComments.InsertOnSubmit(TC);
        UDB.SubmitChanges();
        txt.Text = String.Empty;
        GridNestedViewItem NV = (GridNestedViewItem)RdPar.MasterTableView.Items[RowNo].ChildItem;
        RadGrid rd = (RadGrid)NV.FindControl("gridLvl4");
        rd.Rebind();
    }
    protected void btnReply4_Click(object sender, EventArgs e)
    {
        UserId = Session["USERID"].ToString();
        CourseID = Session["COURSEID"].ToString();
        Button btn1 = (Button)sender;
        Telerik.Web.UI.GridDetailTemplateItem editItem = (Telerik.Web.UI.GridDetailTemplateItem)btn1.NamingContainer;
        TextBox txt = (TextBox)editItem.FindControl("txtReplyLvl4");
        string strKey = txt.Text;

        RadGrid RdPar = (RadGrid)editItem.Parent.Parent.Parent;
        Label txtRowNo = (Label)btn1.NamingContainer.FindControl("txtRowNo4");
        Int32 RowNo = Convert.ToInt32(txtRowNo.Text);
        Int32 KeyVal = Int32.Parse(RdPar.MasterTableView.Items[RowNo].GetDataKeyValue("CommID").ToString());

        UserDBDataContext UDB = new UserDBDataContext();
        tblComment TC = new tblComment();
        TC.ParentId = KeyVal;
        TC.CommentText = txt.Text;
        TC.CommDate = DateTime.Now;
        TC.CommBy = UserId;
        TC.CourseID = CourseID;
        TC.Status = 1;
        UDB.tblComments.InsertOnSubmit(TC);
        UDB.SubmitChanges();
        txt.Text = String.Empty;
        GridNestedViewItem NV = (GridNestedViewItem)RdPar.MasterTableView.Items[RowNo].ChildItem;
        RadGrid rd = (RadGrid)NV.FindControl("gridLvl5");
        rd.Rebind();
    }

    protected void btnComment_Click(object sender, EventArgs e)
    {
        UserId = Session["USERID"].ToString();
        CourseID = Session["COURSEID"].ToString();
        if (btnComment.Text == "Comment")
        {

            UserDBDataContext UDB = new UserDBDataContext();
            tblComment TC = new tblComment();
            TC.ParentId = null;
            TC.CommentText = txtComment.Text;
            TC.CommDate = DateTime.Now;
            TC.CommBy = UserId;
            TC.CourseID = CourseID;
            TC.Status = 1;
            UDB.tblComments.InsertOnSubmit(TC);
            UDB.SubmitChanges();
            RadGrid1.Rebind();
            txtComment.Text = string.Empty;
        }
        else
        {
            UserDBDataContext UDB = new UserDBDataContext();
            tblComment TC = new tblComment();
            TC.ParentId = CommentID_;
            TC.CommentText = txtComment.Text;
            TC.CommDate = DateTime.Now;
            TC.CommBy = UserId;
            TC.CourseID = CourseID;
            UDB.tblComments.InsertOnSubmit(TC);
            UDB.SubmitChanges();
            btnComment.Text = "Comment";
        }
    }
    //====================================================================================//
    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
        {

            GridDataItem parentItem = e.Item as GridDataItem;
            parentItem.Selected = true;
            GridDataItem selectedItem = (GridDataItem)RadGrid1.SelectedItems[0];
            int index = selectedItem.ItemIndex;
            Int32 KeyVal = Int32.Parse(RadGrid1.MasterTableView.Items[index].GetDataKeyValue("CommID").ToString());
            TextBox tx = parentItem.ChildItem.FindControl("txtPGridComm") as TextBox;
            RadGrid grid = parentItem.ChildItem.FindControl("doc_grid") as RadGrid;
            String StrSQL = "select " +
                            "((select S.Parent_RowNo " +
                            "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                            "FROM tblComment where ParentID is NULL) as S " +
                            "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                            "ROW_NUMBER() OVER( ORDER BY CommID ) AS 'rownumber', CommID," +
                            "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                            "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                            "FROM tblComment where ParentID = " + KeyVal + "";
            grid.DataSource = GetDataTable(StrSQL);
            grid.Rebind();
        }
        else if (e.CommandName == "TEST")
        {
            GridDataItem selectedItem = (GridDataItem)e.Item;
            selectedItem.Selected = true;
        }
    }

    protected void RadGrid2_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
        {
            GridDataItem parentItem = e.Item as GridDataItem;
            parentItem.Selected = true;
            int index = parentItem.ItemIndex;
            RadGrid rd = (RadGrid)parentItem.NamingContainer.Parent;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[index].GetDataKeyValue("CommID").ToString()); ;
            RadGrid grid = parentItem.ChildItem.FindControl("gridLvl3") as RadGrid;
            String StrSQL = "select " +
                            "((select S.Parent_RowNo " +
                            "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                            "FROM tblComment where ParentID is NULL) as S " +
                            "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                            "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                            "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                            "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                            "FROM tblComment where ParentID = '" + KeyVal + "'";
            grid.DataSource = GetDataTable(StrSQL);
            grid.Rebind();
        }
    }

    protected void RadGrid3_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
        {

            GridDataItem parentItem = e.Item as GridDataItem;
            parentItem.Selected = true;
            int index = parentItem.ItemIndex;
            RadGrid rd = (RadGrid)parentItem.NamingContainer.Parent;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[index].GetDataKeyValue("CommID").ToString()); ;
            RadGrid grid = parentItem.ChildItem.FindControl("gridLvl4") as RadGrid;
            grid.DataSource = GetDataTable("select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'rownumber', CommID,(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate FROM tblComment sc where ParentID= '" + KeyVal + "'");
            grid.Rebind();
        }
    }

    protected void RadGrid4_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
        {
            GridDataItem parentItem = e.Item as GridDataItem;
            parentItem.Selected = true;
            int index = parentItem.ItemIndex;
            RadGrid rd = (RadGrid)parentItem.NamingContainer.Parent;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[index].GetDataKeyValue("CommID").ToString()); ;
            RadGrid grid = parentItem.ChildItem.FindControl("gridLvl5") as RadGrid;
            grid.DataSource = GetDataTable("select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'rownumber', CommID,(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate FROM tblComment sc where ParentID= '" + KeyVal + "'");
            grid.Rebind();
        }
    }

    protected void RadGrid5_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
        {

            GridDataItem parentItem = e.Item as GridDataItem;
            parentItem.Selected = true;
            int index = parentItem.ItemIndex;
            RadGrid rd = (RadGrid)parentItem.NamingContainer.Parent;
            Int32 KeyVal = Int32.Parse(rd.MasterTableView.Items[index].GetDataKeyValue("CommID").ToString()); ;
        }
    }

    protected void RadGrid1_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (!e.IsFromDetailTable)
        {
            String StrSQL = @"select 
                            ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID,
                            (Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, 
                            RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate 
                            FROM tblComment where Status = 1 and ParentID is NULL and CourseID = '" + CourseID + "'";
            RadGrid1.DataSource = GetDataTable(StrSQL);
        }
    }

    protected void RadGrid2_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        GridDataItem parentItem = ((source as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
        string KeyVal = parentItem.GetDataKeyValue("CommID").ToString();
        String StrSQL = "select " +
                        "((select S.Parent_RowNo " +
                        "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                        "FROM tblComment where ParentID is NULL) as S " +
                        "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                        "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                        "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                        "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                        "FROM tblComment where Status = 1 and ParentID = '" + KeyVal + "'";
        (source as RadGrid).DataSource = GetDataTable(StrSQL);
    }

    protected void RadGrid3_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        GridDataItem parentItem = ((source as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
        string KeyVal = parentItem.GetDataKeyValue("CommID").ToString();
        String StrSQL = "select " +
                "((select S.Parent_RowNo " +
                "FROM (select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'Parent_RowNo',CommID " +
                "FROM tblComment where ParentID is NULL) as S " +
                "where COmmid = '" + KeyVal + "') - 1) as 'Parent_RowNo', " +
                "ROW_NUMBER() OVER( ORDER BY CommID ) - 1 AS 'rownumber', CommID," +
                "(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, " +
                "RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate " +
                "FROM tblComment where Status = 1 and ParentID = '" + KeyVal + "'";
        (source as RadGrid).DataSource = GetDataTable(StrSQL);
    }

    protected void RadGrid4_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        GridDataItem parentItem = ((source as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
        string KeyVal = parentItem.GetDataKeyValue("CommID").ToString();
        (source as RadGrid).DataSource = GetDataTable("select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'rownumber', CommID,(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate FROM tblComment sc where Status = 1 and ParentID= '" + KeyVal + "'");
    }

    protected void RadGrid5_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        GridDataItem parentItem = ((source as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
        string KeyVal = parentItem.GetDataKeyValue("CommID").ToString();
        (source as RadGrid).DataSource = GetDataTable("select ROW_NUMBER() OVER( ORDER BY CommID ) AS 'rownumber', CommID,(Select FirstName + SPACE(1) + LastName from Users where twwid = CommBy) as CommBy,CommentText, RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,CommDate,100),8)),7)  + ' on ' +  RIGHT('0' + DATENAME(DAY, CommDate), 2) + ' ' + LEFT(DATENAME(MONTH, CommDate),3) + ' ' + DATENAME(YEAR, CommDate) as CommDate FROM tblComment sc where Status = 1 and ParentID= '" + KeyVal + "'");
    }

    protected void RadGrid1_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
    {
        GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
        switch (e.DetailTableView.Name)
        {
        case "Reply":
            {
                string CustomerID = dataItem.GetDataKeyValue("CommID").ToString();
                e.DetailTableView.DataSource = GetDataTable("select CommID,CommentText,(select count(*) FROM tblComment WHERE ParentID=sc.CommID) childnodecount FROM tblComment sc where ParentID= '" + CustomerID + "'");
                break;
            }
        case "OrderDetails":
            {
                string OrderID = dataItem.GetDataKeyValue("OrderID").ToString();
                e.DetailTableView.DataSource = GetDataTable("SELECT * FROM [Order Details] WHERE OrderID = " + OrderID);
                break;
            }
        }
    }

    public DataTable GetDataTable(string query)
    {
        String ConnString = connStr;
        SqlConnection conn = new SqlConnection(ConnString);
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = new SqlCommand(query, conn);

        DataTable myDataTable = new DataTable();
        if (conn.State != ConnectionState.Open)
        {
            conn.Open();
        }
        else
        {
            conn.Close();
            conn.Open();
        }

        try
        {
            adapter.Fill(myDataTable);
        }
        catch (Exception ex)
        {
            conn.Close();
        }
        adapter.Dispose();
        conn.Close();

        return myDataTable;
    }

    public String GetDataValue(string query)
    {
        String ConnString = connStr;
        SqlConnection conn = new SqlConnection(ConnString);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        var RetVal = cmd.ExecuteScalar();

        return (RetVal == null) ? string.Empty : RetVal.ToString();
    }

    public void RunQuery(string query)
    {
        String ConnString = connStr;
        SqlConnection conn = new SqlConnection(ConnString);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        cmd.ExecuteNonQuery();
    }

    public void SetActiveRow(Int32 DtKey, RadGrid rd, Boolean bol)
    {
        foreach (GridDetailTemplateItem item in rd.MasterTableView.GetItems(GridItemType.DetailTemplateItem))
        {
            if (item.ItemType == GridItemType.DetailTemplateItem)
            {
                if (item.Selected == true)
                {
                    item.Expanded = true;
                }
            }
        }
    }
    protected void RadGrid1_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
#endregion

    protected void btnFT_Click(object sender, EventArgs e)
    {
        string fileToCopy = "C:/Users/Percival.ponte/Documents/WORK/NOTES/Source";
        string directoryPath = "C:/Users/Percival.ponte/Documents/WORK/NOTES/Destination";

        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }

        if (Directory.Exists(directoryPath))
        {
            //Copy sub folders
            foreach (string subFolder in Directory.GetDirectories(fileToCopy, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(subFolder.Replace(fileToCopy, directoryPath));
            }

            //Copy files
            foreach (string subFolder in Directory.GetDirectories(fileToCopy, "*", SearchOption.AllDirectories))
            {
                foreach (string files in Directory.GetFiles(subFolder))
                {
                    string FileDest = directoryPath + '/' + Path.GetFileName(Path.GetDirectoryName(files)) + '/' + Path.GetFileName(files);
                    File.Copy(files, FileDest, true);
                }
            }

        }
    }
}