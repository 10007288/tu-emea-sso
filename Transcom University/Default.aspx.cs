﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : BasePage
{
    private MasterPage2 _master;

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["TwwIdSession"] = value;
        }
    }

    public static string AccessMode
    {
        get
        {
            var value = HttpContext.Current.Session["AccessModeSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["AccessModeSession"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //GetGlobals();
            SetPage_RolePreviledges();
            try
            {
                var lv = (MultiView)Master.FindControl("mvMenu");
                string CurrIndx = CurrVwIndx.ToString();
                lv.ActiveViewIndex = Int32.Parse(CurrIndx);

                Label lblWel = (Label)Master.FindControl("lblLOG_TWWID");
                lblWel.Text = CurrTWWID.ToString();
            }
            catch(Exception ex)
            {
            }

        }
    }

    protected void GetGlobals()
    {
        TwwId = User.Identity.Name;
        hidCim.Value = TwwId;
        //AccessMode = Page.User.Identity.Name.Contains("@") ? "External" : ConfigurationManager.AppSettings["AccessMode"];
    }

    private int SetAssignCourseNotifications()
    {
        var retVal = 0;
        if (!Page.User.Identity.Name.Contains("@"))
        {
            var assigncourselst = DataHelper.GetAssignCourseNotificationCount(Convert.ToInt32(CurrTWWID));

            if (assigncourselst != null)
            {
                retVal = assigncourselst.AssignedCount + assigncourselst.MovedCount + assigncourselst.RemovedCount;
                if (retVal > 0)
                {
                    divEvent_AssignCourseAssigned.Visible = true;
                    //DO NOT DELETE THIS!!!
                    //lblAssignedCnt.Text = assigncourselst.AssignedCount.ToString();
                    //lblMovedCnt.Text = assigncourselst.MovedCount.ToString();
                    //lblRemovedCnt.Text = assigncourselst.RemovedCount.ToString();
                }
            }
        }
        return retVal;
    }

    private void SetPage_RolePreviledges()
    {
        _master = Master;
        var notifyCnt = 0;

        string UserRole = string.Empty;
        try
        {
            UserRole = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2] != null ? System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2] : "";
        }
        catch
        {

        }

        var NotifyEnrollmentCnt = DataHelper.CountEnrollments_Sup_ForApproval(Convert.ToInt32(CurrTWWID));

        if (NotifyEnrollmentCnt > 0)
        {
            notifyCnt = notifyCnt + 1;
            divEnrolment_ForApproval.Visible = true;
            lblForApprovalCnt.Text = NotifyEnrollmentCnt.ToString();
        }
        else
        {
            divEnrolment_ForApproval.Visible = false;
        }

        if (_master.NotifyEnrolApprovedCnt > 0 || _master.NotifyEnrollDeniedCnt > 0)
        {
            notifyCnt = notifyCnt + 1;
            divEnrolment_Status.Visible = true;
            lblApprovedCnt.Text = _master.NotifyEnrolApprovedCnt.ToString();
            lblDeniedCnt.Text = _master.NotifyEnrollDeniedCnt.ToString();
        }
        else
        {
            divEnrolment_Status.Visible = false;
        }

        //DISABLED FOR NOW
        //if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Coordinator"))
        //{
        //    if (_master.NotifyEventForApprovalCnt > 0)
        //    {
        //        notifyCnt = notifyCnt + 1;
        //        divEvent_ForApproval.Visible = true;
        //    }
        //    else
        //    {
        //        divEvent_ForApproval.Visible = false;
        //    }
        //}

        notifyCnt = notifyCnt + SetAssignCourseNotifications();

        switch (notifyCnt)
        {
            case 1:
                RadNotification_CourseEnrolment.Height = 172;
                RadNotification_CourseEnrolment.Show();
                break;
            case 2:
                RadNotification_CourseEnrolment.Height = 304;
                RadNotification_CourseEnrolment.Show();
                break;
            case 3:
                RadNotification_CourseEnrolment.Height = 435;
                RadNotification_CourseEnrolment.Show();
                break;
            case 0:
                RadNotification_CourseEnrolment.Visible = false;
                break;
            default:
                RadNotification_CourseEnrolment.Height = 435;
                RadNotification_CourseEnrolment.Show();
                break;
        }

        if (AccessMode == "Internal")
        {
            ctrInternal.Visible = true;
            ctrTULinkInternal.Visible = true;
            ctrExternal.Visible = false;
        }
        else
        {
            ctrInternal.Visible = false;
            ctrTULinkInternal.Visible = false;
            ctrExternal.Visible = true;
        }

        //var strPage = "Courses.aspx?AM=" + Encrypt(AccessMode);
        //btnContinue.PostBackUrl = strPage;
    }
}
