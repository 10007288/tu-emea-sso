﻿using System;
using System.Web;
using System.Data;
using System.IO;
using NuComm.Security.Encryption;

public partial class ServiceContainer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static pr_TranscomUniversity_lkp_PrerequisiteMetResult GetPrerequisiteDetails(int courseId,string twwid)
    {
        DataTable dt = LogicHelper.ConvertToDataTable(DataHelper.GetPrereqEncryptedIDs(courseId));
        dt.TableName = "encCourseIDs";

        for (var i = 0; i < dt.Rows.Count; i++)
        {
            dt.Rows[i][1] = dt.Rows[i][1].ToString() != string.Empty ? UTF8.DecryptText(HttpUtility.UrlDecode(dt.Rows[i][1].ToString())) : string.Empty;
        }

        string strXml;
        using (var sw = new StringWriter())
        {
            dt.WriteXml(sw);
            strXml = sw.ToString();
        }

        var list = DataHelper.ArePrerequisiteMet(Convert.ToInt32(twwid), courseId, strXml);
        return list;
    }

    [System.Web.Services.WebMethod]
    public static pr_TranscomUniversity_lkp_FilterCoursesWithPrerequisiteResult GetCoursesWithPrerequisite(string courseIds)
    {
        var list = DataHelper.FilterPrerequisiteCourses(courseIds);
        return list;
    }

    [System.Web.Services.WebMethod]
    public static string ResetAssignCourseNotifications(string cim)
    {
        var reset = DataHelper.ResetNotifCount(Convert.ToInt32(cim));
        var retVal = string.Empty;
        if (reset == false)
        {
            retVal = "Error Occured cannot update notifications.";
        }
        return retVal;
    }

}