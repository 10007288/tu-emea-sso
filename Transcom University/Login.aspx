﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>--%>
<%--
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Panel runat="server" DefaultButton="btnLogin" ID="pnlMain" CssClass="pnlMain">
            <asp:Table runat="server" ID="tblLogin" HorizontalAlign="center" Width="350px" defaultbutton="btnLogin"
                CssClass="tblLogin">
                <asp:TableRow>
                    <asp:TableCell>
                        <table width="100%" align="center">
                            <tr style="height: 60px;">
                                <td colspan="2" style="color: #FFF; text-align: center;">
                                    <asp:Label ID="lblWelHdr" runat="server" Text="Label">Welcome to Transcom University</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    <asp:Label ID="lblTWW_ID" runat="server" Text="TWW ID"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtUsername" runat="server" LabelWidth="64px" Width="160px"
                                        AutoCompleteType="None" AutoComplete="Off" OnTextChanged="txtUsernameChange" />&nbsp;
                                    <asp:RequiredFieldValidator ControlToValidate="txtUsername" Font-Size="Medium" Display="Dynamic"
                                        ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="login"
                                        CssClass="displayerror"   />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    <asp:Label ID="lblPass" runat="server" Text="Label">Password</asp:Label> 
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtPassword" TextMode="Password" runat="server" AutoCompleteType="None"
                                        AutoComplete="Off" />&nbsp;
                                    <asp:RequiredFieldValidator ControlToValidate="txtPassword" Font-Size="Medium" ErrorMessage="*"
                                        runat="server" SetFocusOnError="True" ValidationGroup="login" CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" style="padding: 10px 38px 15px 0">
                                    <div id="managecontrol" style="width: 100%">
                                        <asp:LinkButton ID="btnLogin" runat="server" Text= "Log In"  OnClick="btnLogin_Click"
                                            ValidationGroup="login" />
                                    </div>
                                    <p>
                                        <asp:Label ID="lblNotify" ForeColor="red" runat="server" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1" align="right" >
                                    <div id="Div1" style="width: 100%">
                                        <asp:LinkButton ID="btnCreateAcct" runat="server" Text="Create Account" OnClick="btnCreateUser_Click" 
                                             ForeColor="White" Font-Bold="True" />
                                    </div>
                                </td>
                                <td colspan="1" align="left" >
                                    <div id="Div2" style="width: 100%">
                                        <asp:LinkButton ID="btnForgotPass" runat="server" Text="Forgot Password" OnClick="btnForgotPass_Click" 
                                             ForeColor="White" Font-Bold="True" />
                                    </div>
                                </td>
                            </tr>
                        </table> <br />
                    </asp:TableCell></asp:TableRow></asp:Table></asp:Panel><div runat="server" id="ctrTULinkInternal">
            <div id="footerLink">
                <label>
                    To access this outside the Transcom University Network, please use this address: </label><asp:LinkButton runat="server" ID="lbnTUInternal" Text="https://applications.transcom.com/transcomuniversity"
                    PostBackUrl="https://applications.transcom.com/transcomuniversity" />
            </div>
        </div>
    </div>
</asp:Content>
--%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" Runat="Server">
    <div class="containerdefault">
        <asp:Panel ID="pnlMain" runat="server" CssClass="pnlMain">
            <asp:Table runat="server" ID="tblLogin" HorizontalAlign="center" Width="350px" defaultbutton="btnLogin" CssClass="tblLogin">
                <asp:TableRow>
                    <asp:TableCell>
                        <table width="100%" align="center">
                            <tr style="height: 60px;">
                                <td colspan="2" style="color: #FFF; text-align: center;">
                                    <asp:Label ID="lblWelHdr" runat="server" Text="Label">Welcome to Transcom University! To log in, use your Google account.</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LoginView ID="LoginView1" runat="server">
                                        <AnonymousTemplate>
                                            <asp:Label ID="ModelErrorMessage1" runat="server"></asp:Label>
                                            <div style="text-align: center;">
                                                <asp:ImageButton ID="btnRequestLogin" runat="server" Text="Google Login" 
                                                    OnClick="btnRequestLogin_Click" ImageUrl="~/Images/btn_google_signin_dark_normal_web@2x.png" 
                                                    ToolTip="Sign in with your Google Account" style="height: 60px; margin: 20px;" />
                                            </div>
                                        </AnonymousTemplate>
                                   </asp:LoginView>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <div runat="server" id="ctrTULinkInternal">
            <div id="footerLink">
                <label>
                    To access this outside the Transcom University Network, please use this address: </label><asp:LinkButton runat="server" ID="lbnTUInternal" Text="https://applications.transcom.com/transcomuniversity"
                    PostBackUrl="https://applications.transcom.com/transcomuniversity" />
            </div>
        </div>
    </div>
</asp:Content>