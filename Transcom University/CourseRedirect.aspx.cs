﻿using System;
using System.Web.UI;
//using TWW.Data.Intranet;
using NuComm.Security.Encryption;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data;

public partial class CourseRedirect : BasePage
{
    void BasePage_PreInit(object sender, EventArgs e)
    {
        this.MasterPageFile = null;
        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                string CurrURL = Session["CourseLaunch"].ToString();
                HtmlControl frame1 = (HtmlControl)this.FindControl("CourseURL");
                frame1.Attributes["src"] = CurrURL;
            }
            catch
            {

            }
        }
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}

