﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ScormToMaster
/// </summary>
public class ScormToMaster
{
	public ScormToMaster()
	{
        
    }
    public String GetUserName()
    {
        String RetVal = "";
        var pageHandler = HttpContext.Current.CurrentHandler;
        if (pageHandler is System.Web.UI.Page)
        {
            Label lblUserName = (Label)((System.Web.UI.Page)pageHandler).Master.FindControl("lblLOGNAME");
            RetVal = lblUserName.Text;
        }
        return RetVal;
    }
    public String GetUserIDTWWID()
    {
        String RetVal = "";
        var pageHandler = HttpContext.Current.CurrentHandler;
        if (pageHandler is System.Web.UI.Page)
        {
            Label lblUserName = (Label)((System.Web.UI.Page)pageHandler).Master.FindControl("lblLOG_TWWID");
            RetVal = lblUserName.Text;
        }
        return RetVal;
    }
}