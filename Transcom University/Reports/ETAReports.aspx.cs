﻿using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Reports_ETAReports : Page
{
    readonly ArrayList _arrCampaign = new ArrayList();
    readonly ArrayList _arrCompanySite = new ArrayList();
    private const string _etaReportByQuestion = "~/Reports/ReportControls/ETAReportByQuestion.ascx";
    private const string _etaReportByAgent = "~/Reports/ReportControls/ETAReportByAgent.ascx";
    private const string _etaReportByDate = "~/Reports/ReportControls/ETAReportByDate.ascx";
    private const string _etaReportByClientAndQueue = "~/Reports/ReportControls/ETAReportByClientAndQueue.ascx";
    private const string _etaReportByIncorrectAnswer = "~/Reports/ReportControls/ETAReportByIncorrectAnswer.ascx";
    private const string _etaReportByNotAnsweredCorrectly = "~/Reports/ReportControls/ETAReportByNotAnsweredCorrectly.ascx";
    private const string _etaReportByQuestionCompletionRate = "~/Reports/ReportControls/ETAReportByQuestionCompletionRate.ascx";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cbReportList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        divParameterCampaign.Visible = true;
        divParameterCampaign.Visible = true;
        divParameterCompanySite.Visible = true;

        switch (cbReportList.SelectedValue)
        {
            case "ETA Report by Question":
            case "ETA Report by Date":
                divParameterCampaign.Visible = false;
                break;
            case "ETA Report by Client and Queue":
            case "ETA Report by Not Answered Correctly":
            case "ETA Report by Incorrect Answer":
            case "ETA Report by Question Completion Rate":
                divParameterCampaign.Visible = false;
                divParameterCompanySite.Visible = false;
                break;
        }
    }

    protected void cvParameterControls_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (cbReportList.SelectedValue == "ETA Report by Question" || cbReportList.SelectedValue == "ETA Report by Date")
        {
            if (!string.IsNullOrEmpty(dpStartDate.SelectedDate.ToString()) &&
                !string.IsNullOrEmpty(dpEndDate.SelectedDate.ToString()) &&
                !string.IsNullOrEmpty(cbClient.SelectedValue) &&
                !string.IsNullOrEmpty(cbQuestion.SelectedValue) &&
                rlbCompanySite.CheckedItems.Count > 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                cvParameterControls.ErrorMessage = "Provide all of this: Start Date, End Date, Client, Question, Company Site";
            }
        }
        else if (cbReportList.SelectedValue == "ETA Report by Client and Queue" ||
            cbReportList.SelectedValue == "ETA Report by Not Answered Correctly" ||
            cbReportList.SelectedValue == "ETA Report by Incorrect Answer" ||
            cbReportList.SelectedValue == "ETA Report by Question Completion Rate")
        {
            if (!string.IsNullOrEmpty(dpStartDate.SelectedDate.ToString()) &&
                !string.IsNullOrEmpty(dpEndDate.SelectedDate.ToString()) &&
                !string.IsNullOrEmpty(cbClient.SelectedValue) &&
                !string.IsNullOrEmpty(cbQuestion.SelectedValue))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                cvParameterControls.ErrorMessage = "Provide all of this: Start Date, End Date, Client, Question";
            }
        }
        else if (cbReportList.SelectedValue == "ETA Report by Agent")
        {
            if (!string.IsNullOrEmpty(dpStartDate.SelectedDate.ToString()) &&
                !string.IsNullOrEmpty(dpEndDate.SelectedDate.ToString()) &&
                !string.IsNullOrEmpty(cbClient.SelectedValue) &&
                !string.IsNullOrEmpty(cbQuestion.SelectedValue) &&
                rlbCompanySite.CheckedItems.Count > 0 &&
                rlbCampaign.CheckedItems.Count > 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                cvParameterControls.ErrorMessage = "Provide all of this: Start Date, End Date, Client, Question, Company Site/s, Campaign/s";
            }
        }
    }

    private static void GetCheckedItems(RadListBox listBox, IList arr)
    {
        var collection = listBox.CheckedItems;

        foreach (var item in collection)
        {
            arr.Add(item.Value);
        }
    }

    private void HideAllReportControls()
    {
        divETAReports.Visible = false;
    }

    protected void btnViewReport_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            HideAllReportControls();

            switch (cbReportList.SelectedValue)
            {
                case "ETA Report by Question":
                    divETAReports.Visible = true;
                    var report1 = Page.LoadControl(_etaReportByQuestion);
                    divETAReports.Controls.Add(report1);
                    break;
                case "ETA Report by Agent":
                    divETAReports.Visible = true;
                    var report2 = Page.LoadControl(_etaReportByAgent);
                    divETAReports.Controls.Add(report2);
                    break;
                case "ETA Report by Date":
                    divETAReports.Visible = true;
                    var report3 = Page.LoadControl(_etaReportByDate);
                    divETAReports.Controls.Add(report3);
                    break;
                case "ETA Report by Client and Queue":
                    divETAReports.Visible = true;
                    var report4 = Page.LoadControl(_etaReportByClientAndQueue);
                    divETAReports.Controls.Add(report4);
                    break;
                case "ETA Report by Incorrect Answer":
                    divETAReports.Visible = true;
                    var report5 = Page.LoadControl(_etaReportByIncorrectAnswer);
                    divETAReports.Controls.Add(report5);
                    break;
                case "ETA Report by Not Answered Correctly":
                    divETAReports.Visible = true;
                    var report6 = Page.LoadControl(_etaReportByNotAnsweredCorrectly);
                    divETAReports.Controls.Add(report6);
                    break;
                case "ETA Report by Question Completion Rate":
                    divETAReports.Visible = true;
                    var report7 = Page.LoadControl(_etaReportByQuestionCompletionRate);
                    divETAReports.Controls.Add(report7);
                    break;
            }
        }

        //GetCheckedItems(rlbCompanySite, _arrCampaign);
    }
}