﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TUReports.aspx.cs" Inherits="Reports_TUReports" %>

<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_ActiveEmployees.ascx" %>
<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_TakenTestAndPassed.ascx" %>
<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_TakenTestAndFailed_DidNotTakeTest.ascx" %>
<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_TakenCourseAndTest.ascx" %>
<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_TakenCourse.ascx" %>
<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_DidNotTakenCourse.ascx" %>
<%@ Reference VirtualPath="~/Reports/ReportControls/RPT_TakenTestWithoutTakingCourse.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Transcom University Reporting</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112026746-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-112026746-1');
    </script>

    <%--<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />--%>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link id="CSS" rel="stylesheet" type="text/css" href="~/Styles/styles.css" runat="server" />
    <style type="text/css">
        .reportTitle
        {
            margin-top: 15px;
            padding-left: 5px;
            height: 30px;
            font-size: 2em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= btnExportToExcel.UniqueID %>") {
                    args.set_enableAjax(false);
                }
            }
            function RequestFullScreen(sender, args) {
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
<%--            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
<%--            <telerik:AjaxSetting AjaxControlID="gridReportOverview">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="hidSelectedRpt" />
                    <telerik:AjaxUpdatedControl ControlID="divExportBtn" />
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnViewReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="cbSite">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cbClient" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="cbClient">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cbRoles" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="container">
                <div runat="server" id="divParameterControls" style="width: 100%; padding-bottom: 10px">
                    <div class="insidecontainer" style="height: 50px">
                        <div style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>
                                    Course</label>
                                <asp:RequiredFieldValidator ID="rqCourse" runat="server" ControlToValidate="cbCourse"
                                    ValidationGroup="view" SetFocusOnError="True" ErrorMessage="* select course"
                                    Display="Dynamic" CssClass="displayerror" />
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox runat="server" ID="cbCourse" DataTextField="Title" DataValueField="CourseID"
                                    EmptyMessage="- select course -" DropDownAutoWidth="Enabled" 
                                    CheckBoxes="False" MaxHeight="300px" MarkFirstMatch="True" ChangeTextOnKeyBoardNavigation="True"
                                    HighlightTemplatedItems="True" AllowCustomText="False" />
                            </div>
                        </div>
                        <div id="Div1" runat="server" style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>
                                    Country</label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cbCountry" ValidationGroup="view"
                                    SetFocusOnError="True" ErrorMessage="* select country/s" Display="Dynamic" CssClass="displayerror" />
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox ID="cbCountry" runat="server"  EnableCheckAllItemsCheckBox="true"
                                    DataTextField="Country1" DataValueField="ID" CheckBoxes="true" DropDownAutoWidth="Enabled"
                                    EmptyMessage="- select country/s -" MaxHeight="300px" MarkFirstMatch="True" ChangeTextOnKeyBoardNavigation="True"
                                    HighlightTemplatedItems="True" AllowCustomText="False" OnSelectedIndexChanged="cbCountry_SelectedIndexChanged"
                                    AutoPostBack="True" ToolTip="Please select Site/s at once to load the clients properly. Thank you.">
                                    <%--DataSourceID="CountryDataSource"--%>
                                    <%--OnItemChecked="cbSite_ItemChecked"--%>
                                </telerik:RadComboBox>
                            </div>
                        </div>
                        <div id="Div1a" runat="server" style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>
                                    Site</label>
                                <asp:RequiredFieldValidator ID="rqSite" runat="server" ControlToValidate="cbSite" ValidationGroup="view"
                                    SetFocusOnError="True" ErrorMessage="* select site/s" Display="Dynamic" CssClass="displayerror" />
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox ID="cbSite" runat="server"  EnableCheckAllItemsCheckBox="true"
                                    DataTextField="Description" DataValueField="ID" CheckBoxes="true" DropDownAutoWidth="Enabled"
                                    EmptyMessage="- select site/s -" MaxHeight="300px" MarkFirstMatch="True" ChangeTextOnKeyBoardNavigation="True"
                                    HighlightTemplatedItems="True" ViewStateMode="Enabled" AllowCustomText="False" OnSelectedIndexChanged="cbSite_SelectedIndexChanged"
                                    AutoPostBack="true" ToolTip="Please select Site/s at once to load the clients properly. Thank you." EnableViewState="True">
                                    <%----%>
                                    <%--OnItemChecked="cbSite_ItemChecked" DataSourceID="CompanySiteDataSourse"--%>
                                </telerik:RadComboBox>
                            </div>
                        </div>
                        <div style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>Client</label>
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox ID="cbClient" DropDownAutoWidth="Enabled" runat="server" DataTextField="description" EnableCheckAllItemsCheckBox="true"
                                    DataValueField="ID" Height="16px" EmptyMessage="- select client/s -" AppendDataBoundItems="True"
                                    CheckBoxes="true" EnableLoadOnDemand="True" MaxHeight="300px" MarkFirstMatch="True"
                                    ChangeTextOnKeyBoardNavigation="True" HighlightTemplatedItems="True" AllowCustomText="False"
                                    AutoPostBack="true" ViewStateMode="Enabled" EnableViewState="True">
                                    <%--<Items>
                                        <telerik:RadComboBoxItem runat="server" Text="Others" Value="0" />
                                    </Items>--%>
                                </telerik:RadComboBox>
                                <%--OnItemsRequested="CbClientItemsRequested"--%>
                            </div>
                        </div>
                        <%--<div runat="server" style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>
                                    Campaign</label>
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox ID="cbCampaign" runat="server" DataSourceID="CampaignDataSourse"
                                    DataTextField="campaign_desc" DataValueField="ID" CheckBoxes="true" DropDownAutoWidth="Enabled"
                                    EmptyMessage="- select campaign/s -" MaxHeight="300">
                                </telerik:RadComboBox>
                            </div>
                        </div>--%>
                        <div style="float: left; width: 175px">
                            <div class="divFieldLabel">
                                <label>
                                    Role</label>
                                <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="cbRoles" ValidationGroup="view"
                                    SetFocusOnError="True" ErrorMessage="* select role/s" Display="Dynamic" CssClass="displayerror" />--%>
                            </div>
                            <div class="divField">
                                <telerik:RadComboBox runat="server" ID="cbRoles" DataValueField="NombreCatTranscom" EnableCheckAllItemsCheckBox="true"
                                    DataTextField="Position" EmptyMessage="- select role/s -" DataSourceID="RolesDataSource"
                                    DropDownAutoWidth="Enabled" CheckBoxes="True" AppendDataBoundItems="True" MaxHeight="300px"
                                    MarkFirstMatch="True" ChangeTextOnKeyBoardNavigation="True" HighlightTemplatedItems="True"
                                    AllowCustomText="False">
                                    <%--<Items>
                                        <telerik:RadComboBoxItem runat="server" Text="Others" Value="Others" />
                                    </Items>--%>
                                </telerik:RadComboBox>
                            </div>
                        </div>

                    </div>
                    <br />
                       <div style="float: left; width: 350px">
                            <div class="divFieldLabel">
                                <label>
                                    Date From - To</label>
                                <asp:CompareValidator ID="DateCompareValidator" runat="server" ControlToValidate="dpEndDate"
                                    ControlToCompare="dpStartDate" Operator="GreaterThanEqual" Type="Date" ErrorMessage="Invalid entry!"
                                    ValidationGroup="view" Display="Dynamic" CssClass="displayerror" />
                            </div>
                            <div class="divField">
                                <telerik:RadDatePicker runat="server" ID="dpStartDate" Width="100" />
                                <label>
                                    -</label>
                                <telerik:RadDatePicker runat="server" ID="dpEndDate" Width="100" />
                            </div>
                        </div>
                    <br />
                    <div id="managecontrol" style="width: 100%">
                        <asp:LinkButton runat="server" ID="btnViewReport" Text="Generate Report" ValidationGroup="view"
                            OnClick="BtnViewReportClick" />
                        <asp:LinkButton runat="server" ID="btnClearParams" Text="Reset Filters" CausesValidation="False"
                            OnClick="BtnClearParamsClick" />
                    </div>
                    <p style="display: block">
                        <asp:Label id="lbl1" runat="server" />
                        <asp:Label id="lbl2" runat="server" />
                        <asp:Label id="lbl3" runat="server" />
                    </p>
                </div>
                <div class="paneldefault">
                    <div class="insidecontainer">
                        <div runat="server" id="divTUMain">
                            <telerik:RadGrid ID="gridReportOverview" runat="server" CellSpacing="0" AutoGenerateColumns="False"
                                AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False" OnItemDataBound="GridReportOverviewItemDataBound">
                                <MasterTableView DataKeyNames="Id,Total" CommandItemDisplay="Top" TableLayout="Auto">
                                    <CommandItemTemplate>
                                        <div class="reportTitle">
                                            <asp:Label ID="lblTitle" Text="Overview" runat="server" />
                                        </div>
                                    </CommandItemTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Id" HeaderText="ID" UniqueName="Id" Display="False">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Title" HeaderText="Title" UniqueName="Title">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Total" HeaderText="Total" UniqueName="Total">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" UniqueName="Percentage">
                                            <HeaderStyle Width="50px"></HeaderStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="View Details">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnViewDetails" runat="server" ToolTip="Preview Sheet" Text="View"
                                                    OnClientClick="javascript:return confirm('Do you want to view this Report?')"
                                                    OnClick="BtnViewDetailsClick" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px"></HeaderStyle>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                        <div style="width: 100%;">
                            <div runat="server" style="width: 150px; position: absolute; right: 0; margin-right: 25px;
                                padding-top: 10px" align="right" id="divExportBtn" visible="False">
                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" ClientEvents-OnRequestStart="conditionalPostback"
                                    Width="100%">
                                    <asp:LinkButton ID="btnExportToExcel" runat="server" Text="Export to Excel" AlternateText="ExcelML"
                                        ToolTip="Export to Excel" OnClick="BtnExportToExcelClick" CausesValidation="False" />
                                </telerik:RadAjaxPanel>
                            </div>
                            <asp:Panel runat="server" ID="divTUReports" EnableViewState="True">
                            </asp:Panel>
                        </div>
                        <div runat="server" id="divSiteSummary">
                            <telerik:RadGrid ID="gridSiteSummary" runat="server" CellSpacing="0" AutoGenerateColumns="False"
                                AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False" OnItemDataBound="GridSiteSummaryItemDataBound">
                                <MasterTableView DataKeyNames="SiteID" CommandItemDisplay="Top" TableLayout="Auto">
                                    <CommandItemTemplate>
                                        <div class="reportTitle">
                                            <asp:Label ID="lblTitle" Text="Site Summary" runat="server" />
                                        </div>
                                    </CommandItemTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="SiteID" HeaderText="ID" UniqueName="SiteID" Display="False">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Site" HeaderText="Location" UniqueName="Site">
                                            <ItemStyle Font-Bold="True"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PeopleCount" HeaderText="Total Employee" UniqueName="PeopleCount">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EmployeeTookTest" HeaderText="Employee Took Test"
                                            UniqueName="EmployeeTookTest">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" UniqueName="Percentage">
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>                        
                        <div align="right" style="width: 100%; padding-top: 10px">
                            <asp:LinkButton runat="server" ID="btnBack" Text="Back to top" OnClientClick="javascript:scroll(0,0); return false;" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
         <asp:SqlDataSource ID="CountryDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [ID], [Country] FROM [Countries] WHERE ([Active] = 1)">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="ReportsTypeDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT [ID], [Title] FROM [refReportsType] WHERE ([Active] = 1)">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="CompanySiteDataSourse" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT DISTINCT ID, Description FROM [dbo].[Locations] WHERE Active = 1 ORDER BY Description" />
        <%--SELECT DISTINCT ID, Description FROM [dbo].[Locations] WHERE Active = 1 ORDER BY Description--%>
        <asp:SqlDataSource ID="CampaignDataSourse" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT ID, campaign_desc FROM [dbo].[refCampaigns] WHERE active = 1 ORDER BY campaign_desc" />
        <asp:SqlDataSource ID="QuestionDataSourse" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbClient" Name="ClientID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <%--<asp:SqlDataSource ID="CoursesDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:INTRANETConnectionString %>"
            SelectCommand="SELECT * FROM ( SELECT TestTakenID AS CourseID, Title = C.TestName FROM nuskillcheck.dbo.tbl_testing_testTaken T INNER JOIN nuskillcheck.dbo.tbl_testing_testCategory C ON T.TestCategoryID = C.TestCategoryID WHERE UserID IN (9000065) AND T.HideFromList = 0 UNION ALL SELECT DISTINCT (SELECT TOP 1 [CourseID] FROM [tbl_TranscomUniversity_Cor_Course] WHERE [Title] = T.[Title]) [CourseID], T.[Title] FROM [tbl_TranscomUniversity_Cor_Course] T WHERE (T.[HideFromList] = 0)) AS TBL1 ORDER BY Title">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="HideFromList" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="CoursesDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheckConnectionString %>">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="HideFromList" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="RolesDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT DISTINCT NombreCatTranscom, NombreCatTranscom + ' - ' + NombreCatTranscomDesc AS Position FROM Empleados WHERE NombreCatTranscomDesc IS NOT NULL ORDER BY Position">
        </asp:SqlDataSource>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidSelectedRpt" Value="0" />
    </div>
    </form>
</body>
</html>
