﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ETAReports.aspx.cs" Inherits="Reports_ETAReports" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ETA Reporting</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112026746-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-112026746-1');
    </script>

    <link id="CSS" rel="stylesheet" type="text/css" href="../styles.css" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock runat="server">
        <script type="text/javascript">
            
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="container">
                <div style="padding-bottom: 5px">
                    <label>
                        Choose Report</label>
                    <telerik:RadComboBox runat="server" ID="cbReportList" ShowWhileLoading="False" EnableLoadOnDemand="True"
                        SortCaseSensitive="False" Filter="Contains" MarkFirstMatch="True" IsCaseSensitive="False"
                        AutoPostBack="True" ChangeTextOnKeyBoardNavigation="True" EnableVirtualScrolling="True"
                        EmptyMessage="- choose report -" HighlightTemplatedItems="True" Width="300" OnSelectedIndexChanged="cbReportList_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Question" Value="ETA Report by Question" />
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Agent" Value="ETA Report by Agent" />
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Date" Value="ETA Report by Date" />
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Client and Queue" Value="ETA Report by Client and Queue" />
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Incorrect Answer" Value="ETA Report by Incorrect Answer" />
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Not Answered Correctly"
                                Value="ETA Report by Not Answered Correctly" />
                            <telerik:RadComboBoxItem runat="server" Text="ETA Report by Question Completion Rate"
                                Value="ETA Report by Question Completion Rate" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="cbReportList" ValidationGroup="view"
                        SetFocusOnError="True" ErrorMessage="report name required!" Display="Dynamic"
                        CssClass="displayerror" />
                </div>
                <div runat="server" id="divParameterControls" style="width: 100%; height: 60px">
                    <div class="paneldefault" style="height: 60px">
                        <div style="float: left; width: 245px; height: 60px; padding-right: 10px">
                            <div id="divStartDate" style="padding-bottom: 10px">
                                <label>
                                    Start Date</label>
                                <telerik:RadDatePicker runat="server" ID="dpStartDate" />
                            </div>
                            <div id="divEndDate" style="padding-bottom: 10px">
                                <label style="padding-right: 8px">
                                    End Date</label>
                                <telerik:RadDatePicker runat="server" ID="dpEndDate" />
                            </div>
                        </div>
                        <div style="float: left; width: 350px; height: 60px">
                            <div id="divClient" style="padding-bottom: 10px">
                                <label style="padding-right: 26px">
                                    Client</label>
                                <telerik:RadComboBox runat="server" ID="cbClient" DataTextField="client_desc" DataValueField="ID"
                                    DataSourceID="Client" MaxHeight="300" EmptyMessage="- choose report -" Width="250"
                                    AutoPostBack="True" />
                            </div>
                            <div id="divQuestion">
                                <label style="padding-right: 9px">
                                    Question
                                </label>
                                <telerik:RadComboBox runat="server" ID="cbQuestion" DataValueField="ID" EmptyMessage="- choose report -"
                                    Width="250px">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Text="1" Value="1" />
                                        <telerik:RadComboBoxItem runat="server" Text="2" Value="2" />
                                        <telerik:RadComboBoxItem runat="server" Text="3" Value="3" />
                                        <telerik:RadComboBoxItem runat="server" Text="4" Value="4" />
                                        <telerik:RadComboBoxItem runat="server" Text="5" Value="5" />
                                    </Items>
                                </telerik:RadComboBox>
                            </div>
                        </div>
                        <div runat="server" id="divParameterCompanySite" style="float: left; width: 350px;
                            height: 60px">
                            <div style="padding-bottom: 10px">
                                <telerik:RadPanelBar ID="pbCompanySite" runat="server" ExpandMode="MultipleExpandedItems"
                                    Width="330" ForeColor="White" Font-Names="Helvetica" ValidationGroup="view" CausesValidation="True">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Text="select company site/s" Expanded="False"
                                            ToolTip="Click to Expand/Collapse">
                                            <ContentTemplate>
                                                <div style="padding: 5px">
                                                    <telerik:RadListBox ID="rlbCompanySite" runat="server" Height="115px" Width="300"
                                                        DataSourceID="CompanySite" DataTextField="site_desc" DataValueField="ID" SelectionMode="Multiple"
                                                        CheckBoxes="True" ValidationGroup="view" />
                                                </div>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </div>
                        </div>
                        <div runat="server" id="divParameterCampaign" style="float: left; width: 350px; height: 60px">
                            <div style="padding-bottom: 10px">
                                <telerik:RadPanelBar ID="pbCampaign" runat="server" ExpandMode="MultipleExpandedItems"
                                    Width="330" ForeColor="White" Font-Names="Helvetica" ValidationGroup="view" CausesValidation="True">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Text="select campaign/s" Expanded="False" ToolTip="Click to Expand/Collapse">
                                            <ContentTemplate>
                                                <div style="padding: 5px">
                                                    <telerik:RadListBox ID="rlbCampaign" runat="server" Height="115" SelectionMode="Multiple"
                                                        Width="300" DataSourceID="Campaign" DataTextField="campaign_desc" DataValueField="ID"
                                                        CheckBoxes="True" ValidationGroup="view" />
                                                </div>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div id="managecontrol">
                    <asp:LinkButton runat="server" ID="btnViewReport" Text="View Report" ValidationGroup="view"
                        OnClick="btnViewReport_Click" />
                    <asp:CustomValidator ID="cvParameterControls" runat="server" ErrorMessage="*" ValidationGroup="view"
                        SetFocusOnError="True" Display="Dynamic" OnServerValidate="cvParameterControls_ServerValidate"
                        CssClass="displayerror" />
                </div>
                <div class="paneldefault">
                    <label>
                        Grid Result</label>
                    <div class="insidecontainer">
                        <div runat="server" id="divETAReports" visible="False">
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="CompanySite" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT ID, site_desc FROM [dbo].[refSites] ORDER BY site_desc" />
        <asp:SqlDataSource ID="Campaign" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT ID, campaign_desc FROM [dbo].[refCampaigns] ORDER BY campaign_desc" />
        <asp:SqlDataSource ID="Client" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="SELECT ID, client_desc FROM [dbo].[refClients] ORDER BY client_desc" />
        <asp:SqlDataSource ID="Question" runat="server" ConnectionString="<%$ ConnectionStrings:TranscomUniv_localConnectionString %>"
            SelectCommand="">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbClient" Name="ClientID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
