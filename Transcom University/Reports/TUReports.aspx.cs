﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlClient;


public partial class Reports_TUReports : BasePage
{
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    private static string _rptLink;
    private string _clientIds;
    private int _courseId;
    private string _sites;
    private string _roles;
    private DateTime? _startDate;
    private DateTime? _endDate;

    private decimal _countR1;
    private decimal _countR2;
    private decimal _countR3;
    private decimal _countR4;
    private decimal _countR5;
    private decimal _countR6;
    private decimal _countR7;

    private List<pr_RPT_TakenTestResult> _tookTest;

    public class ReportSummary
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public String Total { get; set; }
        public String Percentage { get; set; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //string _cim = Registration.GetTwwid(Context.User.Identity.Name);
        //CoursesDataSource.SelectCommand =  string.Format("SELECT * FROM ( SELECT TestTakenID AS CourseID, Title = C.TestName FROM nuskillcheck.dbo.tbl_testing_testTaken T INNER JOIN nuskillcheck.dbo.tbl_testing_testCategory C ON T.TestCategoryID = C.TestCategoryID WHERE UserID IN ({0}) AND T.HideFromList = 0 UNION ALL SELECT DISTINCT (SELECT TOP 1 [CourseID] FROM [tbl_TranscomUniversity_Cor_Course] WHERE [Title] = T.[Title]) [CourseID], T.[Title] FROM [tbl_TranscomUniversity_Cor_Course] T WHERE (T.[HideFromList] = 0)) AS TBL1 ORDER BY Title", _cim);
        //        CoursesDataSource.SelectCommand = string.Format(@"
        //            		SELECT 
        //			            A.[CourseID],             
        //                        A.[CourseName] AS TITLE
        //		            FROM 
        //			            [Intranet].[dbo].[tbl_NuCommUniversity_Lkp_Course] AS A 
        //                    WHERE 
        //			            CourseID =	CASE 0 
        //						            WHEN 0 THEN CourseID             
        //                                    ELSE 0             
        //                                    END             
        //		            ORDER BY 
        //                        HideFromList, 
        //			        CourseID Desc");

        //        CoursesDataSource.SelectCommand = string.Format(@"
        //            		SELECT 
        //			            A.[CourseID],             
        //                        A.[TITLE] AS TITLE
        //		            FROM 
        //			            [Intranet].[dbo].[tbl_TranscomUniversity_Cor_Course] AS A 
        //                    WHERE 
        //			            A.HideFromList = 0           
        //		            ORDER BY 
        //                        Title, 
        //			        CourseID Desc");

        if (!IsPostBack)
        {
            IntranetDBDataContext db = new IntranetDBDataContext();

            var courses = db.tbl_TranscomUniversity_Cor_Courses.Where(i => i.HideFromList == 0).Select(i => new
            {
                i.EncryptedCourseID,
                i.Title
            })
            .ToList();

            cbCourse.Items.Clear();

            foreach (var item in courses)
            {
                try
                {
                    cbCourse.Items.Add(new RadComboBoxItem
                    {
                        Text = item.Title,
                        Value = DecryptString(item.EncryptedCourseID)
                    });
                }
                catch
                {
                }

            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            GetParameterValues();
        }

        try
        {
            LoadCountry();
            var _employeeId = CurrTWWID;

            if (_employeeId == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                SetCountry(Convert.ToString(_employeeId));
            }
        }
        catch(Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
            {
                ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString
            };
        _oconn.Open();
    }

    private void SetCountry(string twwid_no)
    {
        Dbconn("NuskillCheck");
        _ocmd = new SqlCommand("sp_Get_User_ByTWWID", _oconn);
        _ocmd.Parameters.AddWithValue("@EmpID", twwid_no);
        _ocmd.Parameters.AddWithValue("@IsReport", "No");
        _ocmd.CommandType = CommandType.StoredProcedure;
        var oDataReader = _ocmd.ExecuteReader();
        if (oDataReader.Read())
        {
            string CountryID = oDataReader[7].ToString();
            foreach (RadComboBoxItem rb in cbCountry.Items)
            {
                string rbTxt = rb.Text;
                if (rbTxt == CountryID)
                {
                    rb.Checked = true;
                    cbCountry.Text = CountryID;
                }
            }
            cbCountry.Enabled = false;
            if (cbSite.CheckedItems.Count < 1)
            {
                LoadSites();
                string SiteName = oDataReader[8].ToString();
                cbSite.Text = SiteName;

                foreach (RadComboBoxItem rb1 in cbSite.Items)
                {
                    string rbTxt1 = rb1.Text;
                    if (rbTxt1 == SiteName)
                    {
                        rb1.Checked = true;
                        cbSite.Text = SiteName;
                    }
                }
                cbSite.Enabled = false;
                LoadClients();
            }
            else
            {

            }
        }
    }

    public string GetComboBoxSelectedItemValues(RadComboBox combo)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            var sb = new StringBuilder();

            foreach (var item in combo.CheckedItems)
            {
                sb.Append(item.Value + ",");
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                collection = sb.ToString().TrimEnd(',');
            }
        }
        return collection;
    }

    public string GetComboBoxSelectedItemTexts(RadComboBox combo,Int32 mode)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            if (mode == 1)
            {
                var sb = new StringBuilder();

                foreach (var item in combo.CheckedItems)
                {
                    sb.Append(item.Value + ",");
                }

                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    collection = sb.ToString().TrimEnd(',');
                }
            }
            else
            {
                var sb = new StringBuilder();

                foreach (var item in combo.CheckedItems)
                {
                    sb.Append(item.Text + ",");
                }

                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    collection = sb.ToString().TrimEnd(',');
                }
            }
        }
        return collection;
    }

    private void GetParameterValues()
    {
        try
        {
            divTUReports.Controls.Clear();
            _clientIds = GetComboBoxSelectedItemValues(cbClient);
            _courseId = Convert.ToInt32(cbCourse.SelectedValue);
            _sites = GetComboBoxSelectedItemTexts(cbSite, 0);
            _roles = GetComboBoxSelectedItemValues(cbRoles);
            _startDate = !string.IsNullOrEmpty(dpStartDate.SelectedDate.ToString()) ? dpStartDate.SelectedDate : null;
            _endDate = !string.IsNullOrEmpty(dpEndDate.SelectedDate.ToString()) ? dpEndDate.SelectedDate : null;
        }
        catch (Exception)
        {

        }
    }

    protected void BtnViewReportClick(object sender, EventArgs e)
    {
        _sites = GetComboBoxSelectedItemTexts(cbSite, 0);
        _roles = GetComboBoxSelectedItemValues(cbRoles);
        _startDate = dpStartDate.SelectedDate.Value;
        _endDate = dpEndDate.SelectedDate.Value;
        divExportBtn.Visible = false;

        _tookTest = Reporting.GetReport_TakenTest(_clientIds, _courseId, _sites, _roles, _startDate, _endDate);
        _countR1 = Reporting.GetReport1_TakenCourseAndTest(_clientIds, _courseId, _sites, _roles, _startDate, _endDate).Count;
        _countR2 = Reporting.GetReport2_TakenCourse(_clientIds, _courseId, _sites, _roles, _startDate, _endDate).Count;
        _countR3 = Reporting.GetReport3_DidNotTakenCourse(_clientIds, _courseId, _sites, _roles, _startDate, _endDate).Count;
        _countR4 = Reporting.GetReport4_TakenTestWithoutTakingCourse(_clientIds, _courseId, _sites, _roles, _startDate, _endDate).Count;
        _countR5 = Reporting.GetReport5_TakenTestAndPassed(_clientIds, _courseId, _sites, _roles, _startDate, _endDate).Count;
        _countR6 = Reporting.GetReport6_TakenTestAndFailed_DidNotTakeTest(_clientIds, _courseId, _sites, _roles, _startDate, _endDate).Count;
        _countR7 = Reporting.GetReport_ActiveEmployeesOnSite(_sites).Count;

        BindReportOverview();
        BindSiteSummary();
    }

    private void BindReportOverview()
    {
        var db = new UserDBDataContext();
        var data = (from a in db.refReportsTypes
                    where a.Active == true
                    select new ReportSummary { Id = a.ID, Title = a.Title, Total = "", Percentage = "" }).ToList();

        gridReportOverview.DataSource = data;
        gridReportOverview.DataBind();
    }

    private void BindSiteSummary()
    {
        _sites = GetComboBoxSelectedItemTexts(cbSite, 0);
        var data = Reporting.GetReport_EmployeeCountPerSite(_sites);

        gridSiteSummary.DataSource = data;
        gridSiteSummary.DataBind();
    }

    protected void BtnClearParamsClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Reports/TUReports.aspx");
    }

    public static int[] ToIntArray(string value, char separator)
    {
        return Array.ConvertAll(value.Split(separator), s => int.Parse(s));
    }

    protected void BtnViewDetailsClick(object sender, EventArgs e)
    {
        divTUReports.Controls.Clear();
        var btnViewDetails = sender as LinkButton;
        var item = btnViewDetails.NamingContainer as GridDataItem;
        var id = item.GetDataKeyValue("Id");

        switch (id.ToString())
        {
            case "1": //Employees that have Taken the Course and Test
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "1";
                _rptLink = "~/Reports/ReportControls/RPT_TakenCourseAndTest.ascx";

                var rep1 = new Reports_ReportControls_RPT_TakenCourseAndTest.ReportControl
                {
                    CourseId = _courseId,
                    ClientIds = _clientIds,
                    Sites = _sites,
                    Roles = _roles,
                    StartDate = _startDate,
                    EndDate = _endDate
                };
                break;
            case "2": //Employees that have Taken the Course
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "2";
                _rptLink = "~/Reports/ReportControls/RPT_TakenCourse.ascx";

                var rep2 = new Reports_ReportControls_RPT_TakenCourse.ReportControl
                {
                    CourseId = _courseId,
                    ClientIds = _clientIds,
                    Sites = _sites,
                    Roles = _roles,
                    StartDate = _startDate,
                    EndDate = _endDate
                };
                break;
            case "3": //Employees that have Not Taken the Course
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "3";
                _rptLink = "~/Reports/ReportControls/RPT_DidNotTakenCourse.ascx";

                var rep3 = new Reports_ReportControls_RPT_DidNotTakenCourse.ReportControl
                {
                    CourseId = _courseId,
                    ClientIds = _clientIds,
                    Sites = _sites,
                    Roles = _roles,
                    StartDate = _startDate,
                    EndDate = _endDate
                };
                break;
            case "4": //Employees that have taken the Test without Taking the Course
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "4";
                _rptLink = "~/Reports/ReportControls/RPT_TakenTestWithoutTakingCourse.ascx";

                var rep4 = new Reports_ReportControls_RPT_TakenTestWithoutTakingCourse.ReportControl
                {
                    CourseId = _courseId,
                    ClientIds = _clientIds,
                    Sites = _sites,
                    Roles = _roles,
                    StartDate = _startDate,
                    EndDate = _endDate
                };
                break;
            case "5": //Employees that have Taken the Test and Passed
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "5";
                _rptLink = "~/Reports/ReportControls/RPT_TakenTestAndPassed.ascx";

                var rep5 = new Reports_ReportControls_RPT_TakenTestAndPassed.ReportControl
                {
                    CourseId = _courseId,
                    ClientIds = _clientIds,
                    Sites = _sites,
                    Roles = _roles,
                    StartDate = _startDate,
                    EndDate = _endDate
                };
                break;
            case "6": //Employees that have Taken the Test and Failed, Did Not Take Test
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "6";
                _rptLink = "~/Reports/ReportControls/RPT_TakenTestAndFailed_DidNotTakeTest.ascx";

                var rep6 = new Reports_ReportControls_RPT_TakenTestAndFailed_DidNotTakeTest.ReportControl
                {
                    CourseId = _courseId,
                    ClientIds = _clientIds,
                    Sites = _sites,
                    Roles = _roles,
                    StartDate = _startDate,
                    EndDate = _endDate
                };
                break;
            case "7": //Employees that have Taken the Test and Failed, Did Not Take Test
                divExportBtn.Visible = true;
                hidSelectedRpt.Value = "7";
                _rptLink = "~/Reports/ReportControls/RPT_ActiveEmployees.ascx";

                var rep7 = new Reports_ReportControls_RPT_ActiveEmployees.ReportControl
                {
                    Sites = _sites
                };
                break;
            default:
                divExportBtn.Visible = false;
                //_selectedRpt = 0;
                break;
        }

        var rpt = LoadControl(_rptLink);
        divTUReports.Controls.Add(rpt);
    }

    private decimal ComputePercentage(decimal count)
    {
        decimal result = 0;
        if (count == 0)
        {
            result = 0;
        }
        else
        {
            if (_countR7 != 0)
                result = (count / _countR7) * 100;
        }
        return result;
    }

    protected void GridReportOverviewItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataBoundItem = e.Item as GridDataItem;

            if (int.Parse(dataBoundItem["Id"].Text) == 1)
            {
                var result = ComputePercentage(_countR1);
                dataBoundItem["Total"].Text = _countR1.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
            if (int.Parse(dataBoundItem["Id"].Text) == 2)
            {
                var result = ComputePercentage(_countR2);
                dataBoundItem["Total"].Text = _countR2.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
            if (int.Parse(dataBoundItem["Id"].Text) == 3)
            {
                var result = ComputePercentage(_countR3);
                dataBoundItem["Total"].Text = _countR3.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
            if (int.Parse(dataBoundItem["Id"].Text) == 4)
            {
                var result = ComputePercentage(_countR4);
                dataBoundItem["Total"].Text = _countR4.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
            if (int.Parse(dataBoundItem["Id"].Text) == 5)
            {
                var result = ComputePercentage(_countR5);
                dataBoundItem["Total"].Text = _countR5.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
            if (int.Parse(dataBoundItem["Id"].Text) == 6)
            {
                var result = ComputePercentage(_countR6);
                dataBoundItem["Total"].Text = _countR6.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
            if (int.Parse(dataBoundItem["Id"].Text) == 7)
            {
                dataBoundItem["Total"].Text = _countR7.ToString();
                dataBoundItem["Total"].Font.Bold = true;
                dataBoundItem["Title"].Font.Bold = true;
            }
        }
    }

    protected void GridSiteSummaryItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataBoundItem = e.Item as GridDataItem;

            var siteId = int.Parse(dataBoundItem["SiteID"].Text);

            if (!string.IsNullOrEmpty(siteId.ToString()))
            {
                decimal count = decimal.Parse(dataBoundItem["PeopleCount"].Text);

                decimal siteTookTest = _tookTest.Where(p => p.SiteID == siteId).Count();

                decimal result = (siteTookTest / count) * 100;
                dataBoundItem["EmployeeTookTest"].Text = siteTookTest.ToString();
                dataBoundItem["Percentage"].Text = Math.Round(result, 2).ToString(CultureInfo.InvariantCulture) + "%";
            }
        }
    }

    protected void BtnExportToExcelClick(object sender, EventArgs eventArgs)
    {
        DataTable data;

        if (hidSelectedRpt.Value == "1")
        {
            data = Reporting.GetReport1_TakenCourseAndTest_DataTable(_clientIds, _courseId, _sites,
                                                                         _roles, _startDate, _endDate);
            CreateGridToExport(data, "RPT_TakenCourseAndTest");
        }
        if (hidSelectedRpt.Value == "2")
        {
            data = Reporting.GetReport2_TakenCourse_DataTable(_clientIds, _courseId, _sites, _roles,
                                                                  _startDate, _endDate);
            CreateGridToExport(data, "RPT_TakenCourse");
        }
        if (hidSelectedRpt.Value == "3")
        {
            data = Reporting.GetReport3_DidNotTakenCourse_DataTable(_clientIds, _courseId, _sites,
                                                                        _roles, _startDate, _endDate);
            CreateGridToExport(data, "RPT_DidNotTakenCourse");
        }
        if (hidSelectedRpt.Value == "4")
        {
            data = Reporting.GetReport4_TakenTestWithoutTakingCourse_DataTable(_clientIds, _courseId, _sites, _roles, _startDate,
                                                                                   _endDate);
            CreateGridToExport(data, "RPT_TakenTestWithoutTakingCourse");
        }
        if (hidSelectedRpt.Value == "5")
        {
            data = Reporting.GetReport5_TakenTestAndPassed_DataTable(_clientIds, _courseId, _sites,
                                                                         _roles, _startDate, _endDate);
            CreateGridToExport(data, "RPT_TakenTestAndPassed");
        }
        if (hidSelectedRpt.Value == "6")
        {
            data = Reporting.GetReport6_TakenTestAndFailed_DidNotTakeTest_DataTable(_clientIds, _courseId, _sites, _roles,
                                                                                        _startDate, _endDate);
            CreateGridToExport(data, "RPT_TakenTestAndFailed_DidNotTakeTest");
        }
        if (hidSelectedRpt.Value == "7")
        {
            data = Reporting.GetReport_ActiveEmployeesOnSite_DataTable(_sites);
            CreateGridToExport(data, "RPT_ActiveEmployees");
        }
    }

    public void CreateGridToExport(DataTable datasource, string rptTitle)
    {
        try
        {
            var grid = new RadGrid { DataSource = datasource };
            grid.DataBind();
            Page.Controls.Add(grid);

            if (grid.MasterTableView.Items.Count > 0)
            {
                grid.ExportSettings.ExportOnlyData = true;
                grid.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                grid.ExportSettings.HideStructureColumns = true;
                grid.ExportSettings.FileName = rptTitle + "(" + DateTime.Now + ")";
                grid.MasterTableView.ExportToExcel();
            }
        }
        catch (Exception)
        {
        }
    }

    public void AlertNoExportData()
    {
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                    "alert('No data to be exported.');",
                                                                    true);
    }

    protected void cbSite_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

    }

    protected void cbCountry_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

    }

    private void LoadClients()
    {
        var sites = GetComboBoxSelectedItemTexts(cbCountry,1);

        if (!string.IsNullOrEmpty(sites))
        {
            cbClient.Items.Clear();
            cbClient.DataSource = null;
            var colCountry = ToIntArray(sites, ',');

            List<Client> _all = new List<Client>();

            var db = new UserDBDataContext();

            var qry = (from a in db.Clients
                       where (a.Active == 1) && ((a.countryID == cbCountry.SelectedValue) || (a.countryID == null)) //(a.InactiveDate == null) && (colCountry.Contains(Convert.ToInt32(a.countryID)))
                       orderby a.description
                       select new
                       {
                           a.ID,
                           a.description
                       }
                 ).ToList();
            cbClient.DataSource = qry;

            cbClient.DataBind();
        }
    }

    private void LoadSites()
    {
        var country = GetComboBoxSelectedItemValues(cbCountry);

        if (!string.IsNullOrEmpty(country))
        {
            cbSite.Items.Clear();
            cbSite.DataSource = null;
            var collection = ToIntArray(country, ',');

            List<Client> _all = new List<Client>();

            var db = new UserDBDataContext();

            var qry = (from a in db.Locations
                       where (a.Active == 1) && (collection.Contains(Convert.ToInt32(a.CountryID)))
                       orderby a.Description
                       select new
                       {
                           a.ID,
                           a.Description
                       }
                 ).ToList();
            cbSite.DataSource = qry;
            cbSite.DataBind();
        }
    }

    private void LoadCountry()
    {
        cbCountry.Items.Clear();
        cbCountry.DataSource = null;
        List<Client> _all = new List<Client>();
        var db = new UserDBDataContext();
        var qry = (from a in db.Countries
                    where (a.ACTIVE == 1)
                    orderby a.Country1
                    select new
                    {
                        a.ID,
                        a.Country1
                    }
                ).ToList();

        cbCountry.DataSource = qry;
        cbCountry.DataValueField = "ID";
        cbCountry.DataTextField = "Country1";
        cbCountry.DataBind();
    }

    public string DecryptString(string inputString)
    {
        System.IO.MemoryStream memStream = null;
        try
        {
            byte[] key = { };
            byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
            string encryptKey = "aXb2uy4z"; // MUST be 8 characters
            key = Encoding.UTF8.GetBytes(encryptKey);
            byte[] byteInput = new byte[inputString.Length];
            byteInput = Convert.FromBase64String(inputString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            memStream = new MemoryStream();
            ICryptoTransform transform = provider.CreateDecryptor(key, IV);
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(byteInput, 0, byteInput.Length);
            cryptoStream.FlushFinalBlock();
        }
        catch (Exception ex)
        {

        }

        Encoding encoding1 = Encoding.UTF8;
        return encoding1.GetString(memStream.ToArray());
    }
}