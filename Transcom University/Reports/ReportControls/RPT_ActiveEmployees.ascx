﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RPT_ActiveEmployees.ascx.cs"
    Inherits="Reports_ReportControls_RPT_ActiveEmployees" %>
<style type="text/css">
    .reportTitle
    {
        margin-top: 15px;
        padding-left: 5px;
        height: 30px;
        font-size: 2em;
    }
</style>
<telerik:RadGrid ID="gridReport" runat="server" CellSpacing="0" GridLines="None"
    AutoGenerateColumns="False" OnItemDataBound="gridReport_ItemDataBound">
    <MasterTableView CommandItemDisplay="Top" TableLayout="Auto">
        <CommandItemTemplate>
            <div class="reportTitle">
                <asp:Label ID="lblTitle" runat="server" />
                
            </div>
        </CommandItemTemplate>
        <Columns>
            <telerik:GridBoundColumn DataField="TWWID" HeaderText="TWW ID" SortExpression="TWWID"
                UniqueName="TWWID">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AgentCompleteName" HeaderText="Employee Name" SortExpression="AgentCompleteName"
                UniqueName="AgentCompleteName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="OfficeRole" HeaderText="Role" SortExpression="OfficeRole"
                UniqueName="OfficeRole">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site"
                UniqueName="Site">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SUPTWWID" DataType="System.Decimal" HeaderText="Supervisor TWW ID"
                SortExpression="SUPTWWID" UniqueName="SUPTWWID">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SupervisorName" HeaderText="Supervisor Name"
                ReadOnly="True" SortExpression="SupervisorName" UniqueName="SupervisorName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StartDate" DataType="System.DateTime" HeaderText="Start Date"
                ReadOnly="True" SortExpression="StartDate" UniqueName="StartDate">
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>
