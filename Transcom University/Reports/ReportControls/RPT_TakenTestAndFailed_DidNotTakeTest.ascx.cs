﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Reports_ReportControls_RPT_TakenTestAndFailed_DidNotTakeTest : UserControl
{
    public static string _clientIds;
    public static int _courseId;
    public static string _sites;
    public static string _roles;
    public static DateTime? _startDate;
    public static DateTime? _endDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        var data = Reporting.GetReport6_TakenTestAndFailed_DidNotTakeTest(_clientIds, _courseId, _sites, _roles, _startDate, _endDate);

        gridReport.DataSource = data;
        gridReport.DataBind();
    }

    protected void gridReport_ItemDataBound(object sender, GridItemEventArgs e)
    {
        var commandItem = (GridCommandItem)gridReport.MasterTableView.GetItems(GridItemType.CommandItem)[0];
        var title = commandItem.FindControl("lblTitle") as Label;
        title.Text = "Employees that have Taken the Test and Failed, Did Not Take Test";
    }

    public class ReportControl : Control
    {
        public string ClientIds
        {
            get { return _clientIds; }
            set { _clientIds = value; }
        }

        public int CourseId
        {
            get { return _courseId; }
            set { _courseId = value; }
        }

        public string Sites
        {
            get { return _sites; }
            set { _sites = value; }
        }

        public string Roles
        {
            get { return _roles; }
            set { _roles = value; }
        }

        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }
    }
}