﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RPT_TakenTestAndPassed.ascx.cs"
    Inherits="Reports_ReportControls_RPT_TakenTestAndPassed" %>
<style type="text/css">
    .reportTitle
    {
        margin-top: 15px;
        padding-left: 5px;
        height: 30px;
        font-size: 2em;
    }
</style>
<telerik:RadGrid ID="gridReport" runat="server" CellSpacing="0" 
    AutoGenerateColumns="False" OnItemDataBound="gridReport_ItemDataBound">
    <MasterTableView CommandItemDisplay="Top" TableLayout="Auto">
        <CommandItemTemplate>
            <div class="reportTitle">
                <asp:Label ID="lblTitle" runat="server" />
            </div>
        </CommandItemTemplate>
        <Columns>
            <telerik:GridBoundColumn DataField="TWWID" HeaderText="TWW ID" SortExpression="TWWID"
                UniqueName="TWWID">
            </telerik:GridBoundColumn>
            <%--<telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                UniqueName="FirstName">
            </telerik:GridBoundColumn>--%>
            <telerik:GridBoundColumn DataField="AgentCompleteName" HeaderText="Employee Name"
                SortExpression="AgentCompleteName" UniqueName="AgentCompleteName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="OfficeRole" HeaderText="Role" SortExpression="OfficeRole"
                UniqueName="OfficeRole">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Region" HeaderText="Region" SortExpression="Region"
                UniqueName="Region">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Country" HeaderText="Country" SortExpression="Country"
                UniqueName="Country">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Site" HeaderText="Site" SortExpression="Site"
                UniqueName="Site">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Client" HeaderText="Client" SortExpression="Client"
                UniqueName="Client">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CourseName" HeaderText="Course Name" SortExpression="CourseName"
                UniqueName="CourseName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Score" DataType="System.Int32" HeaderText="Score"
                SortExpression="Score" UniqueName="Score">
            </telerik:GridBoundColumn>
            <telerik:GridCheckBoxColumn DataField="Passed" DataType="System.Boolean" HeaderText="Passed"
                SortExpression="Passed" UniqueName="Passed">
            </telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="DateTaken" DataType="System.DateTime" HeaderText="Date Taken"
                SortExpression="DateTaken" UniqueName="DateTaken">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SUPTWWID" DataType="System.Decimal" HeaderText="Supervisor TWW ID"
                SortExpression="SUPTWWID" UniqueName="SUPTWWID">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SupervisorName" HeaderText="Supervisor Name"
                ReadOnly="True" SortExpression="SupervisorName" UniqueName="SupervisorName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StartDate" DataType="System.DateTime" HeaderText="Employee Start Date"
                ReadOnly="True" SortExpression="StartDate" UniqueName="StartDate">
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>
