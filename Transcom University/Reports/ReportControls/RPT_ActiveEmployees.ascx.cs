﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Reports_ReportControls_RPT_ActiveEmployees : UserControl
{
    public static string _sites;
    private List<pr_RPT_ActiveEmployeesOnSiteResult> data;

    protected void Page_Load(object sender, EventArgs e)
    {
        data = Reporting.GetReport_ActiveEmployeesOnSite(_sites);

        gridReport.DataSource = data;
        gridReport.DataBind();
    }

    public class ReportControl : Control
    {
        public string Sites
        {
            get { return _sites; }
            set { _sites = value; }
        }
    }

    protected void gridReport_ItemDataBound(object sender, GridItemEventArgs e)
    {
        var commandItem = (GridCommandItem)gridReport.MasterTableView.GetItems(GridItemType.CommandItem)[0];
        var title = commandItem.FindControl("lblTitle") as Label;
        title.Text = "Active Employees";
    }
}