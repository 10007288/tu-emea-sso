﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

public partial class Users : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            grdUsers.Rebind();
        }
    }

    protected void grdUsers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == GridRebindReason.ExplicitRebind || e.RebindReason == GridRebindReason.PostBackEvent)
        {
            grdUsers.DataSource = GetUsers();
        }
    }

    //protected void grdUsers_ItemDataBound(object sender, GridItemEventArgs e)
    //{
    //    if (e.Item is GridEditFormItem)
    //    {
    //        GridEditFormItem editItem = e.Item as GridEditFormItem;
    //        RadComboBox cboRole = editItem.FindControl("cboRole") as RadComboBox;
    //        cboRole.DataSource = GetRoles();
    //        cboRole.DataTextField = "role_desc";
    //        cboRole.DataValueField = "Id";
    //        cboRole.DataBind();

    //        try
    //        {
    //            cboRole.SelectedValue = editItem["RoleID"].Text;
    //        }
    //        catch (Exception)
    //        {
    //        }
    //    }         
    //}

    DataSet GetUsers()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            cn.Open();

            string sql = @"
                SELECT [UserID],
	                [Username],
	                [FirstName],
	                [LastName],
	                [TWWID],
	                [DomainName],
	                [WindowsUsername],
	                [Email],
	                [Password],
	                [FailedPasswordAttempCount],
	                [IsPasswordReset],
	                [IsLockedOut],
	                r.Id AS RoleID,
	                r.role_desc AS ROLE,
	                [ClientID],
	                cl.description AS Client,
	                [OtherClient],
	                u.[RegionID],
	                rg.Description AS Region,
	                u.[CountryID],
	                ct.Country,
	                [SiteID],
	                loc.Description AS Site,
	                [LanguageID],
	                lan.LANGUAGE,
	                [MobileNo],
	                [Specialization_Skills],
	                [EducationLevelID],
	                edu.Title AS EducationLevel,
	                [CourseMajor],
	                [AdditionalCourses_Trainings],
	                [CareerPathID],
	                car.Description AS CareerPath,
	                [CourseInterests],
	                NULL AS UploadedPhoto,
	                [UpdateVia],
	                [strUpdateVia],
	                u.Active,
	                [CreateDate],
	                [departmentid],
	                [OtherDepartment],
	                [supporttype],
	                [strsupporttype],
	                [supervisorid],
	                [OtherCareerPath],
	                [otherlanguage],
	                [CampaignID],
	                [OtherCampaign],
                    u.CreateDate
                FROM [TranscomUniv_local].[dbo].[Users] u
                LEFT JOIN refRoles r
	                ON u.RoleID = r.Id
		                AND r.active = 1
                LEFT JOIN dbo.Clients cl
	                ON u.ClientID = cl.ID
		                AND cl.Active = 1
                LEFT JOIN dbo.Countries ct
	                ON u.CountryID = ct.ID
		                AND ct.ACTIVE = 1
                LEFT JOIN dbo.Regions rg
	                ON u.RegionID = rg.ID
		                AND rg.ACTIVE = 1
                LEFT JOIN dbo.Locations loc
	                ON u.SiteID = loc.ID
		                AND loc.Active = 1
                LEFT JOIN dbo.refLanguage lan
	                ON u.LanguageID = lan.Id
		                AND lan.Active = 1
                LEFT JOIN dbo.refEducationLevel edu
	                ON u.EducationLevelID = edu.Id AND edu.Active = 1
		                AND edu.Active = 1
                LEFT JOIN dbo.refCareerPath car
	                ON u.CareerPathID = car.ID
		                AND car.Active = 1
                LEFT JOIN dbo.refDepartments dep
	                ON u.departmentid = dep.id
		                AND dep.active = 1
                ORDER BY u.CreateDate DESC
                ";

            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    DataSet GetRoles()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["TranscomUniv_localConnectionString"].ConnectionString))
        {
            cn.Open();

            string sql = @"
                SELECT Id,Role_Desc
                FROM [TranscomUniv_local].[dbo].[refRoles] 
                ";

            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }


    void Insertuser()
    {
        //var retVal1 = Registration.InsertUser(txtFName.Text,
        //                txtLName.Text,
        //                txtTWWID.Text,
        //                txtEmailAddress.Text,
        //                Password.CreateHash(txtPassword.Text),
        //                0,
        //                false,
        //                true,
        //                Convert.ToInt32(_Role),
        //                client,
        //                otherClient,
        //                _Reg_Id,
        //                _Country_Id,
        //                _Site_Id,
        //                Convert.ToInt32(cbLanguage.SelectedValue),
        //                txtMobileNoIC.Text + txtMobileNo.Text, txtSpecializationSkills.Text,
        //                Convert.ToInt32(cbEducLevel.SelectedValue),
        //                txtCourseMajor.Text, txtCoursesTrainings.Text,
        //                careerpath, otherCareerpath,
        //                txtCoursesInterested.Text,
        //                bytes, selecteditems,
        //                department,
        //                otherDept,
        //                supporttypeselecteditems,
        //                _Sup_Id, strselecteditems,
        //                strsupporttypeselecteditems);

        //var retVal2 = Registration.InsertSecurityAnswer(Convert.ToInt32(cbSecurityQuestion.SelectedValue),
        //                txtTWWID.Text,
        //                txtSecurityAnswer.Text.ToLower());
    }
}